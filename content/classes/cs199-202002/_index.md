---
title: "CS 199 (Spring 2020)"
courseNumber: "CS 199"
courseTitle: "Introduction to Programming and Problem Solving: A First Exploration"
sidebarItems:

meetings: "Online"
location: "Canvas"

shortDescription: |
    Introduction to programming and computer science, for students with _no prior programming experience_.

    This course is designed to be a pre-CS 161 course for students who are not computer science majors, might benefit from a more accessible pace, do not have a strong math or computing background, and/or want to explore programming and computer science.

layout: "class-home"

menu: 
    main:
        parent: "Teaching"
        
cascade:
    quarter: 202002
    type: "class"
    date: 2019-11-01
    cc: true
---

<p class="warning" style="text-align:center;">
<strong>This course has moved entirely online for the Spring 2020 term.</strong>
</p>

**If you are a student in the class, you can access all materials on <a href="https://pdx.instructure.com/">Canvas</a>.**

If you are interested in prior offerings of this class, or want to get an idea of what it is usually like, you might like to browse the [Fall 2019](../cs199-201904) offering.

<p class="warning" style="text-align:center;">
. . . . . . . . . .
</p>

CS 199 is an approachable introduction to programming and computer science, for students with _no prior programming experience_.

We will learn to use JavaScript and Python to manipulate text and graphics. **You do not need a strong math background to succeed in this class.**

Computer coding does not have to be hard, and it can let you harness the power of your computer in new and creative ways. Coding can be a useful tool in many fields including healthcare, art, journalism, biology, chemistry, sociology, and more.

Throughout the course, we will also explore the history of computing and some of its impacts on contemporary society: such as artificial intelligence, surveillance, or big data. These topics will be chosen in week 2 based on your interests.

This course is designed to be a pre-CS 161 course for students who:

* are not computer science majors
* might benefit from a more accessible pace
* do not have a strong math or computing background
* want to explore programming and computer science


### This Website is a Living Document

This website is a starting point for the course. It is subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced via email, so attend class and check your inbox.

