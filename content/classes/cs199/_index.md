---
title: "CS 199 (Fall 2020)"
courseNumber: "CS 199"
courseTitle: "Introduction to Programming and Problem Solving: A First Exploration"
sidebarItems:

meetings: "Asynchronous"
location: "[Canvas](http://pdx.instructure.com)"

shortDescription: |
    Introduction to programming and computer science, for students with _no prior programming experience_.

    This course is designed to be a pre-CS 161 course for students who are not computer science majors, might benefit from a more accessible pace, do not have a strong math or computing background, and/or want to explore programming and computer science.

layout: "class-home"

menu: 
    main:
        parent: "Teaching"
        
cascade:
    quarter: 202004
    type: "class"
    date: 2020-06-01
    cc: true
---

<p class="warning" style="text-align:center;">
<strong>This course will be entirely online for the Fall 2020 term.</strong>
</p>

The course is designed such that you may complete it entirely asynchronously if that works best for you. We will have some optional Zoom meetings during the originally scheduled class meeting times.

**If you are a student in the class, you can access all materials on <a href="https://pdx.instructure.com/">Canvas</a>.** I do not anticipate updating this website with further details.

If you want more info about the course, please reach out to me directly at: <harmon8@pdx.edu>.



## Course Description

CS 199 is an approachable introduction to programming and computer science, for students with _no prior programming experience_.

We will learn to use JavaScript and Python to manipulate text and graphics. **You do not need a strong math background to succeed in this class.**

Computer coding does not have to be hard, and it can let you harness the power of your computer in new and creative ways. Coding can be a useful tool in many fields including healthcare, art, journalism, biology, chemistry, sociology, and more.

Throughout the course, we will also explore the history of computing and some of its impacts on contemporary society: such as artificial intelligence, surveillance, and big data. Specific topics will be chosen in week 2 based on your interests.

This course is designed to be **a pre-CS 161 course** for students who would benefit from a more accessible pace, who might not have a strong math or computing background, who want to explore programming and computer science but are unsure about pursing a major/minor, and/or who are not computer science majors.

## Course Goals

By the end of this course, you will be able to:

- ... write simple programs to express yourself creatively and/or answer questions about the world, using foundational programming concepts such as loops, conditionals, and functions.
- ... *explain some of the interactions between computing technology and social life, and offer thoughtful assessments and critiques of computing in workplaces and social life.
- ...learn new computing concepts and/or programming languages in future coursework or through independent study. Like an introductory writing class will not transform you into a Pulitzer prize winning author immediately, this course will not make you into a rockstar programmer overnight. However, you should leave this course with the confidence and competence to pursue future study formally or informally.


