---
title: Reading Responses
cc: true
type: "class"
layout: "subpage"
icon: "fa-book"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "cs199-201804-reading-responses"
---

At three points in the quarter we will read some articles about the relationship between computing technologies and society. 

At each point you should: 

1. Read the assigned article(s)
2. Make some notes about the reading that answer each of these questions: 
    1. What is the question or problem that prompted the author to write this article?
    2. What is the answer to that question that the author presents? Or, what is it that they think should happen in response to the problem? 
    3. What evidence does the author present to convince you of their answer/solution? Or, how do they make their argument?
    4. Who benefits from the situation described in the article?
    5. Who is harmed or stands to lose from the situation described in the article?
    6. What do you think should be done after reading the article? Anything?
    7. What is one question you have after reading the article? _or_ Provide a link to a recent news article that relates to the topic. Briefly explain (a couple sentences) how the news item relates to the topic.
1. Post your notes to Piazza _**at least 48 hours before our class meeting**_. 
2. Sometime before class, read some of the notes posted by your classmates. Write at a comment or response to at least one other student's post. 


You can earn up to five points for each reading activity: 

- **2 points**: Participating at all.
- **3 points**: Posting a note that answers all 7 questions.
- **4 points**: Posting a note that answers all 7 questions  **and** writing *something* in response to a peers' post. 
- **4.5 points**: _**Either**_ your note _or_ your comment on someone else's post demonstrates a high level of thoughtfulness and effort, and could be used as an example of good work in a future class.
- **5 points**: **_Both_** your note _and_ your comment on someone else's post demonstrate a high level of thoughtfulness and effort, and could be used as an example of good work in a future class.