---
title: Schedule
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-calendar-alt"
weight: 500

menu:
    main:
        parent: "Teaching"
        identifier: "cs199-201804-schedule"
---


### Unit 1: Microbits

#### Mon 24-Sep

Course Introduction, Microbit Introduction

- In Class:
    - Rock Paper Scissors Game: <https://makecode.microbit.org/_M620spCgxiY3>
- _Assigned:_
    - Read. The. Course. [Policies](../policies/).
    - Explore microbits, be prepared to demo something to your group next Monday
    - [Self-Asessment](../self-assessment/#start-of-term-self-assessment), Due Wednesday 26-Sept, 8pm
    - Visit my office hours on Tuesday October 2 (3-5pm). OR. [Schedule a 1-on-1 with me](http://ellieharmon.youcanbookme.com) for some other time during week 2.

#### Wed 26-Sep

**NO CLASS MEETING**: Continue exploring microbits. Be prepared to demo something to your group next Monday.

- **Due:**
    - [Self-Asessment](../self-assessment/#start-of-term-self-assessment), 8pm



#### Mon 1-Oct
Microbits: Output, Variables, Sprites, Coordinate Grid, On Start Loop, Forever Loop, Random Numbers

- In class:
    - Microbit Exploration Demos
    - Dodge the Dot, Part I
- _Assigned:_
    - [Project 1](../programming/#p1-microbit), due Friday 5-Oct, 8pm


#### Wed 3-Oct
Microbits: Events (Input), Conditionals, Keeping Score, Functions
- In class:
    - Dodge the Dot, Part II

<h4 class="deadline-only">Friday 5-Oct</h4>

- Due:
    - [Project 1](../programming/#p1-microbit), 8pm

### Unit 2: Graphics with Javascript

#### Mon 8-Oct
Introduction to JavaScript and text-based programming, using <http://repl.it>, using the processing library, drawing shapes, coordinate grids.

- Before class:
    - Create an account on <http://repl.it>

#### Wed 10-Oct
Drawing with processing, using the documentation.

- Before Class:
    - [Learning on Khan Academy](https://www.khanacademy.org/computing/computer-programming/programming/intro-to-programming/a/learning-programming-on-khan-academy)
    - [KA: Drawing Basics (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/drawing-basics/pt/making-drawings-with-code)
    - [KA: Coloring (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/coloring/pt/coloring-with-code)


#### Mon 15-Oct
Variables, random numbers, animation + timer (on start vs. forever loops)

- Before Class:
    - [KA: Variables (all 6 sections)](https://www.khanacademy.org/computing/computer-programming/programming/variables/pt/intro-to-variables)
- _Assigned:_
    - [Project 2](../programming/#p2-javascript), Due Friday, 26-Oct, 8pm

#### Wed 17-Oct
Functions, functions with parameters.

- Before Class:
    - [KA: Functions (**only the first 4 parts**)](https://www.khanacademy.org/computing/computer-programming/programming/functions/pt/functions)
- _Assigned:_
    - Take home midterm.
    - Computing and Society Reading 1:
        - Read: Cherry, Miriam A. 2014. "A Eulogy for the EULA." *Duquesne University Law Review* 52 (2): 335--44. Official version: <https://issuu.com/duquesnelaw/docs/52.2?e=15059800/58167323> Also available at: <https://scholarship.law.slu.edu/cgi/viewcontent.cgi?article=1012&context=faculty>
        - [Reading response 1](../reading-responses/), due Saturday 20-Oct, 2pm
        - 1 comment on someone else's reading response, due anytime _**BEFORE**_ class on Monday 22-Oct.

<h4 class="deadline-only">Saturday 20-Oct</h4>

- Due:
    - [Reading response 1](../reading-responses/), 2pm, Piazza

#### Mon 22-Oct

- In Class:
    - Computing and Society Conversation #1
- Due:
    - 1 comment on someone else's reading response, 1:00p
    - Take home midterm, start of class


#### Wed 24-Oct

- In class:
    - Javascript project demos


<h4 class="deadline-only">Friday 26-Oct</h4>

- Due:
    - [Project 2](./programming/#p2-javascript), 8pm

### Unit 3: Python I

#### Mon 29-Oct
Input, output, variables, conditionals

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapters 2+3


#### Wed 31-Oct
Functions, without and with parameters

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 4
- _Assigned:_
    - [Project 3](../programming/#p3-python-i), due Friday 9-Nov, 8pm
    - Computing and Society Reading 2:
        - **Choose one:**
            - Interview with Katherine Lo on _Behind the Screen_: <https://behindthescreenshow.com/2018/05/31/katherine-lo-researcher/>
            - Roberts, Sarah T. (2016) Commercial content moderation: Digital laborers’ dirty work. In Noble, S.U. and Tynes, B. (Eds.), _The intersectional internet: Race, sex, class and culture online_ (pp. 147-159). New York: Peter Lang. [PDF](http://ir.lib.uwo.ca/commpub/12/)
        - [Reading response 2](../reading-responses/), due Saturday 20-Oct, 2pm
        - 1 comment on someone else's reading response, due anytime _**BEFORE**_ class on Monday 22-Oct.

<h4 class="deadline-only">Friday 2-Nov</h4>

- Due:
    - Reading Response 2, 8pm

#### Mon 5-Nov  

- In Class:
    - Computing and Society Conversation #2
- Due:
    - 1 comment on someone else's reading response, 1:00p


#### Wed 7-Nov

- In Class:
    - Python Project I Demos


#### Mon 12-Nov - **NO CLASS**
Veteran's Day. Entire university is closed!


<h4 class="deadline-only">Tuesday 13-Nov</h4>

- Due:
    - [Project 3](../programming/#p3-python-i), 8pm
    - _This deadline was extended because I was late posting the assignment specification. Sorry!_


### Unit 4: Python II

#### Wed 14-Nov
Loops (while, for), Strings (for each)

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapters 5 + 6




#### Mon 19-Nov
CSV files, Data sources, Files in python, Lists

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapters 7 + 8



#### Wed 21-Nov
Catchup, or working locally

- _Assigned:_
    - [Project 4](../programming/#p4-python-ii), due Friday 30-Nov, 8pm
    - Computing and Society Reading 3:
        - _**Read:**_ AI Now Institute. "Gender, Race and Power: Outlining a New AI Research Agenda". _Medium_ <https://medium.com/@AINowInstitute/gender-race-and-power-5da81dc14b1b>
        - **AND CHOOSE ONE of the articles linked at the end to read as well.**
        - Reading Response, due Friday 23-Nov, 8pm
        - 1 comment on someone else's reading response, due Monday 26-Nov, 1:00p

<h4 class="deadline-only">Friday 23-Nov</h4>

- Due:
    - Reading response 3, 8pm

#### Mon 26-Nov

- In Class:
    - Computing + Society Conversation #3
    - Python II Demos
- Due:
    - 1 comment on someone else's reading response, 1:00p

#### Wed 28-Nov

Final Exam Review

- _Assigned:_
    - [SA2 - End of Term Self Assessment](../self-assessment/), due final exam period, start of class


<h4 class="deadline-only">Friday 30-Nov</h4>

- Due:
    - [P4 - Python](../programming/#p4-python-ii)

#### Final Exam

- In class:
    - Final Exam
- Due:
    - [SA2 - End of Term Self Assessment](../self-assessment/), start of class
