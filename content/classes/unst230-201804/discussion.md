---
title: Discussion Questions
cc: true
type: "class"
layout: "subpage"
icon: "fa-comments"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "unst230-201804-discussion"
    
---

_This assignment is from Dr. Robin James's [Feminist Philosophy](https://docs.google.com/document/d/1oxNiiHLKqRrGWk1kUzTWfnU70kXTRWTcfFux2MF4nEY/edit) course._

In preparation for each class meeting, you will post a discussion question to the class Piazza forum that engages with the reading assignments, no later than **2 hours before our scheduled meeting**.

Please note that a _discussion_ question -- as opposed to any random question you might have -- is something that should engage the rest of the class in thinking through the reading assignment and ongoing course discussions. A good discussion question will be at least a full paragraph in length.

Review this excellent explanation of [what makes a good discussion question](https://docs.google.com/document/d/12gVLnUdf8ua2ILvknlRbJlJy_DtAEy5lEuGhPN_94kY/) by Dr. Robin James.

Each discussion question is worth a half a point towards your attendance and participation grade (There are 17 total, this makes them worth 8.5 points on your overall course grade). They are graded quite simply: 

- **0.5 / 0.5**: Excellent discussion question, demonstrates that you read the assigned reading, includes all three parts of the question as outlined in the explanation linked above and could be used as an example in future classes.
- **0.4 / 0.5**: An attempt at a good discussion question, but not quite example worthy. Perhaps it is missing one of the three parts, or is more of an informational question, but it still demonstrates that you read the assigned reading and thought about it.
- **0.3 / 0.5**: The question is purely informational, is lacking one or more of the three points, and/or does not demonstrate that you actually read beyond the title of the assigned reading.
- **0.0 / 0.0**: Not turned in / not turned in on time.