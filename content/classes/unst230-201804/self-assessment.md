---
title: Self-Assessments
cc: true
type: "class"
layout: "subpage"
icon: "fa-file-signature"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "unst230-201804-self-assessments"
---

Two self-assessments will bookend the course, offering you an opportunity to set your own personal goals for the class, and reflect on how well you have been able to achieve them. These self-assessments also offer you an opportunity to communicate directly with me about what you hope to learn in the class, so that I might take that into account as the quarter progresses, _and_ what went well and what could be improved in future offerings. I appreciate your honesty and forthrightness. 

### Start of Term Self-Assessment

A short written document to help me get to know you and adjust the class to meet your needs, and to also help you start of the course with a clear set of goals in mind. We will refer back to this document at the end of the term.

#### Logistics

- **Deadline**: Wednesday, September 26, 8:00pm
- **What to turn in:**: A .pdf or .docx file
- **How to turn it in:**: D2L Folder **SA1: Beginning of Term Self Assessment**

#### Specification 

For this assignment, you should create a word processing document in an application of your choice, and export it to either .pdf or .docx to turn in.

Please copy all of the following questions into the document, and answer each one in line. 

In addition, please include one image in the document that represents you. This could be a photo of yourself, a photo of a favorite place you like to go, a drawing of a favorite animal, etc. 

The final document should be about 700-1000 words long, including the questions.

1. What is your name?
2. What pronoun should I use to refer to you?
3. What is your major? [If you are undeclared/exploring, what majors are you considering?]
4. How did you find out about this class? Did you consider any other SINQ classes? Why did you choose this one?
5. Describe what success looks like for you in this class. At the end of the term, what do you hope to have accomplished?
6. What is one question or concern you have about technology?
7. What is one thing that excites or interests you about technology?
8. Read the 4 university studies goals (listed on the [course homepage](../#course-goals)), and write 2-4 sentences about your own competency within _each_ of the four areas. What are you already good at doing within each area? Where do you think you need to improve?
8. After looking over the syllabus:
    - What are you most excited about reading or doing this quarter? Why?
    - What are you _least_ excited about reading or doing this quarter? Why?
    - What one thing would you add to this class if you were in charge? _Make a compelling case, and maybe we can find time for it._
8. Anything else you would like me to know?

#### Grading

This assignment is worth 5 points total. You can earn these points by:

- **0 / 5.0**
    - Not completed or document contains plagiarized content
- **3.5 / 5.0**
    - Something is turned in that answers at least some of the questions
- **4 / 5.0** All requirements for 3.5 points and also:
    - Answers all questions and includes an image.
    - Document is in either .docx or .pdf format.
    - Document is at least 700 words long, including the questions.
- **5.0 / 5.0** All requirements for 4 points and also:
    - Document could be used as an example of excellent work in future classes.
    - Document contents show evidence of significant student effort, seriousness, thoughtfulness, and reflection.
    - Document formatting is creative and professional. This could include the use of things like bullets, tables, images, or any other means for conveying information in some format other than just a wall of text!
    - The writing is of high quality and conforms to the conventions of edited, revised English.

No grades will be assigned between 0 and 3.5; nor between 3.5 and 4. Grades between 4 and 5 may be given for assignments that exceed the minimum requirements but do not meet all criteria for a 100%.

### End of Term Self-Assessment

The end of term self-assessment will be conducted through a simple online survey. 

#### Logistics
- **Available**: Wednesday, November 28
- **Deadline**: Thursday, December 6, 8:00pm
- **Survey Link:** <https://goo.gl/forms/nvea72auZVttSxm22>

We will allocate some time for this in class on Wednesday, November 28. If you do not finish it in class, you may finish it on your own time. 
