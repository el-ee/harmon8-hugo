---
title: Implosion Project
cc: true
type: "class"
layout: "subpage"
icon: "fa-users"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "unst230-201804-implosion"
---

This assignment is adapted from Joe Dumit's "Writing the Implosion" project. See <https://culanth.org/articles/741-writing-the-implosion-teaching-the-world-one>

In this project, you will work in teams of 4 to research a technological artifact in detail. 

Then, you will design a website that communicates the key findings from your research in an organized and interesting format. 

A draft of your website will be due at the start of your mentor session in week 7, when you will present your draft site to your peers for feedback and critique. 

Your final website is due at the start of the final exam period, when you will give a closing presentation to the entire class.

### Requirements

You have some freedom in what this final product looks like, as long as it meets the following basic requirements: 

1. It includes a bibliography with at least **4 scholarly references** and at least **4 reputable journalistic references**:
    - Your website must include a bibliography where the reader can find more information, and you must include citations to the items in this bibliography in the body of the text. Think along the lines of a wikipedia entry. 
    - _**All bibliography entries must be referenced somewhere in the text on your site, and all facts presented on the site must be backed up with a citation.**_
    - You may include a separate bibliography on each page, or one bibliography page that includes all references. 
    - You are *not* required to get links working between the body text and each bibliography entry. 
    - Your bibliography must include at least four (4) scholarly or peer-reviewed sources.
    - Your bibliography must include at least two (2) reputable journalistic sources.
2. It has at least **4 distinct sections or pages**:  
    - Your website must have at least 4 distinct sections or pages that each explore a different question about your artifact. 
    - Each section must contain at least 400 words of main body text, not including the bibliography.
    - At least 3 of your questions must be from the list below. We will discuss an example of how to answer each of these questions in weeks 4-6: 
        1. What is its history? How does its history matter today?
        2. Who makes it? How is it made?
        3. Who is it for? What is it for? Who tells these stories?
        4. How does it change social order? How does it impact formal institutions (e.g., schools, businesses, governments) and informal social structures (e.g., family, friendship)?
        5. How does it change what we know about the world? How does it change what we know about ourselves? How does it change what kinds of information or knowledge we value?
    - Your fourth question may either come from the list above, or it can be something your team comes up with on your own. You might peruse the list here for inspiration: <https://culanth.org/articles/741-writing-the-implosion-teaching-the-world-one>
3. The design of your website is interesting, well-organized, creative, and professional. 
    - You should take advantage of the online medium to incorporate images, video, links, and other content as part of your presentation.
    - You should think of your audience for this project as _your fellow peers._ Please do not write this like a term paper for a professor. Instead, think about what it is that is really important for your fellow PSU students to know about this technology. When designing your site, think about questions like: What should others in this class know? What should your roommate know? What should your family know? What is interesting about this technology? Why did you choose it? What is dangerous or scary about it? What is exciting or promising about it?


### Examples & Inspiration
- Dumit, Joseph. 2014. "Writing the Implosion: Teaching the World One Thing at a Time." *Cultural Anthropology* 29 (2): 344--62. <https://doi.org/10.14506/ca29.2.09>
- Crawford, Kate, and Vladan Joler. n.d. "Anatomy of an AI System." Anatomy of an AI System. Accessed September 17, 2018. <http://www.anatomyof.ai>
- Bady, Aaron. 2018. "Heavy Stuff: Lead Is Useful; Lead Is Poison." *Popula*, July 18, 2018. <https://popula.com/2018/07/18/ingredients-lead/>
- Bessette, Lee Skallerup. 2018. "Chlorine." *Popula*, July 23, 2018. <https://popula.com/2018/07/23/chlorine/>
- Blackmore, Willy. 2018. "Where Sea Glass Comes From." *Popula*, July 9, 2018. <https://popula.com/2018/07/09/sea-glass/> 
- Blackmore, Willy. 2018. "Fire Season." *Popula*, July 24, 2018. <https://popula.com/2018/07/24/fire-season/> 
- Burrington, Ingrid. 2018. "Neodymium: Magnets: How Do They Work?" *Popula*, July 30, 2018. <https://popula.com/2018/07/30/neodymium/>

<!-- TODO: Next time: frame around the Quartz Obsession pieces. Magnetic strip is a good one. -->
    

### IP1: Knowledge Memo

In your week 3 mentor session, you will work with your team to map what it is you know and don't know about your artifact. After the session, you will collaboratively write a short memo (no more than 1 page) about where you're at with your project.

#### Logistics
- **Deadline**: Friday, October 12, 8:00pm
- **What to turn in**: A .pdf or .docx file
- **How to turn it in:**: D2L Folder, **IP2: Knowledge Memo**

#### Specification

The memo should include:

- All team member names, on the first page.
- The artifact you are researching.
- A brief report on your knowledge map exercise:
    - An image of your knowledge map.
    - A brief text summary of what your knowledge map contains. What do you know? What do you need to research?
    - What are the four questions that your team will answer?
- A Team Contract:
    - A list of all team member names and email addresses.
    - A communication plan: how will you communicate with each-other?
    - A work plan: 
        - When/how will you work outside of class? Will you have a regular meeting or schedule meetings as needed? 
        - How will you communicate: Will you make a slack channel, use email, a group SMS, a Facebook group, or what?
        - What tools will you use to collaborate: Will you use Google Docs to draft text, and provide feedback to each other before entering it on the website? If not, what is your alternative plan?
        - What service will you use to create your website, e.g., Wordpress, Google Sites, Pebble Pad, Blogger, something else?
    - An outline of individual responsibilities and project timeline: What are all the things that need to be done in each of the next 3 weeks so that you will have a draft ready at the start of week 7? Who will take the lead on each of these tasks? Who will take the lead on motivating and coordinating the group, making sure everyone else has done their part?
- Request for Feedback:
    - What kind of feedback do you want from me right now? What questions do you have? 

#### Grading

The grading for the memo is very simple. It is worth up to 5 points:

- **0: Not turned in or contains plagiarized content.**
- **3: Turned in something.**
- **4: Memo addresses all items in the list above, at least superficially.**
- **5: Memo is not only complete but also thoughtful. It could be used as an example of excellent work in a future class.**


### IP2: Website Draft + Memo

During the mentor session in week 7, you will present a draft of your website to your peers. You will also prepare a short memo to me summarizing your work thus far, and requesting specific feedback. This memo is due by Friday, November 9.

#### Logistics
- **Website Presentation**: During Mentor Session, Week 7
- **Memo Deadline**: Friday, November 9, 8:00pm
- **What to turn in:** Memo  in .pdf, .docx format
- **How to turn it in:**: Upload Memo to D2L Folder **IP2: Website Draft (Memo)**



#### Specification

##### Memo

Include all of the following in a short 1-page memo to me:

- A list of all team members' names.
- A link to your website.
- A summary of all team members' individual contributions to the project thus far.
- A summary of feedback from your mentor session. What did your peers think about your site? What do you need to improve or revise before the final website is due?
- A plan for your future work. What are the specific tasks that must be done between now and the end of the quarter? Who will take the lead on each task?
- The grade you think your team deserves for the draft site, along with an explanation of **why** your team has earned this grade.
- A clear statement of what feedback you want from me in order to help you revise the website. What questions do you have? What are the best parts of your site? What are the parts that you think may not be working? What are the challenges you've faced in completing this project that I might be able to help with? _If you do not request any specific feedback, do not expect to receive any feedback from me beyond a grade._

##### Website + Presentation

At this point, you should have a complete draft of your team's website, which communicates the key findings of all of your research to date. 

You will have about 15 minutes to share your website. I recommend that you spend about 5 minutes showing the website to your classmates, and 10 minutes getting their feedback.

While I expect some things to be not quite perfect about the writing and information layout on the website, it should still be _complete_. See the [Requirements](#requirements) section above for project requirements. 

Please do _not_ make slides for your presentation. Instead, you should demo your website to the audience, and solicit their feedback. Think in advance of the presentation what it is you want to know. For example, 
- Are you concerned about whether the writing makes sense to an outsider? 
- Does your team have some questions about how to layout or present some of the information? 
- Does your team have questions about whether you have gone into enough detail for some dimensions/questions of your implosion? 

#### Grading

##### Memo

The grading for the memo is very simple. It is worth up to 5 points:

- **0: Not turned in or contains plagiarized content.**
- **3: Turned in something.**
- **4: Memo addresses all items in the list above, at least superficially.**
- **5: Memo is not only complete but also thoughtful. It could be used as an example of excellent work in a future class.**


##### Website + Presentation

This website draft, and your presentation of it is worth 10 Points. You can earn those points by: 

- **0: Not turned in, no presentation in class, and/or contains plagiarized content.**
- **6: Website exists and you give a presentation in class.**
- **7: All criteria for 6 points, and:**
    - Website contains at least 4 sections of at least 250 words each.
    - Website has a bibliography.
    - Bibliography includes at least 2 scholarly sources 
    - Bibliography includes at least 2 reputable journalistic sources 
- **8: All criteria for 7 points, and:**
    - Website contains at least 4 sections of at least 400 words each.
    - Bibliography includes at least 3 scholarly sources.
    - Bibliography includes at least 4 reputable journalistic sources.
    - All references include a full and complete citation.
    - References are meaningfully integrated in the body of the text
    - Your in class presentation is well-planned and thought out. 
        - _Note: You should **not** create powerpoint slides for this presentation. You are expected to walk the class through your website._
    - Your presentation is no more than 5 minutes long (before questions)
- **9: All criteria for 8 points, and:**
    - Bibliography includes at least 4 scholarly sources.
    - Every team member participates in the in-class presentation.
    - You actively solicit feedback from your peers for 5-10 minutes.
- **10: All criteria for 9 points, and:**
    - The website has a clear and interesting structure, and good transitions between ideas. The website might be a multi-paragraph essay, or it might be broken up with headers and short subsections and split across multiple pages. Different formats work better for different topics and different people; the point is that the website is well-organized for the reader.
    - The ideas presented show that you are working towards synthesizing multiple pieces of evidence to develop a coherent point in your own voice. Your text may still be in draft form, but I can see that you are trying to move beyond a point by point summary, listing, or comparison of others' ideas and beginning to develop your own voice.
    - On the whole, the writing and site design are of high quality and the ideas presented are compelling, provocative, interesting, creative and/or insightful.

### IP3: Final Website & Presentation

#### Logistics
- **Memo + Slide Deck Deadline**: Wednesday, December 5, 12:30p
- **What to turn in:** Memo in `.pdf` or `.docx` format _**AND**_ Slide Deck in `.pptx` or `.pdf` format
- **How to turn it in:**: D2L Folder **IP3: Final Website and Presentation**
- **FinalPresentation**: During Final Exam Period, Thursday, December 6, 12:30p

#### Specification

##### Memo

You should write a short memo to accompany your final turn-in. This memo should include: 

- A list of all team members' names.
- A link to your final website.
- A summary of each team member's individual contributions to the project.
- A summary of changes you made in response to peer feedback you received in week 7.
- The grade you think you deserve for the final project, along with an explanation of **why** your team has earned this grade.
 
##### Presentation

You **do** need to create powerpoint slides for this presentation. You should **not** walk the class through your website. Instead, you should develop a separate presentation that is designed to teach the rest of the class about your technical artifact. Give us _the presentation version of your website content_. What should we know about your artifact?

##### Website

This is the equivalent of a final term paper. See the [Project Requirements](#requirements) section above for details.

#### Grading

##### Memo (5 points)

The grading for the memo is very simple. It is worth up to 5 points:

- **0: Not turned in or contains plagiarized content.**
- **3: Turned in something.**
- **4: Memo addresses all items in the list above, at least superficially.**
- **5: Memo is not only complete but also thoughtful. It could be used as an example of excellent work in a future class.**

##### Presentation (5 points)

**Details to follow in November**

The grading criteria for the presentation is based on two components: (1) the slide deck you prepare, and (2) the delivery of the presentation in class.

**_You must upload your slide deck to D2L in either `.pdf` or `.pptx` format 24 hours before our final meeting._**

You can earn up to 5 points for the presentation portion of this assignment by: 

- **+.5: Turned in slide deck on time.**
- **+.5: Gave a presentation in class that lasted at least 5 minutes and all present group members participated**
- **+1.5: Slide deck quality**
    - Your slide design is professional, creative and visually engaging.
    - You use graphics and charts appropriately. They carry meaning and add to the presentation.
    - Your slides are appropriate to the presentation medium -- they do not consist of giant walls of text or lengthy bulleted lists.
- **+1.5: Presentation quality**
    - You are prepared to give your presentation and have clearly practiced what you are going to say. 
    - You may use notes while presenting, but you look up from them at least occasionally to make eye contact with the audience, and do not simply read off a piece of paper.
    - Everyone in the room can hear and understand what you are saying.
    - Your presentation is engaging to the audience, and spurs at least one question from the class.
    - You are able to confidently answer questions from the class and instructor about your project.

###### Some suggested guidelines:

- Start with a title slide. Include the name of your project and all team member names.
- Give the audience a roadmap! Make the second slide an outline of what's ahead.
- Include a reference slide at the very end so that I can see what references you use in your presentation when I download the slide deck. You should _**not**_ present this slide during your talk.

###### Other Resources:
- Presentitude. "10 ways to avoid death by bullet points." March 4, 2015. (Blog Post) <http://presentitude.com/10-ways-avoid-death-bullet-points/>
- Dustin Wax. "10 Tips for More Effective PowerPoint Presentations." January 10, 2018. _LifeHack_ (Blog Post) <https://www.lifehack.org/articles/featured/10-tips-for-more-effective-powerpoint-presentations.html>
- Scot LeDuc. "How to present with power and poise." May 31, 2016. _CapitalComTech_ (Blog Post) <http://capitalcomtech.info/2016/05/31/how-to-present-with-power-and-poise/>


##### Website (10 points)

The grading criteria for the website is very similar to Milestone 2. However, the structure and design becomes more important, and there is an additional criteria for having improved upon your draft.

- **0: Not turned in, and/or contains plagiarized content.**
- **6: Website exists.**
- **7: All criteria for 6 points, and:**
    - Website contains at least 4 sections.
    - Each section is at least 250 words in length.
    - Website has a bibliography.
    - Bibliography includes at least 2 scholarly sources 
    - Bibliography includes at least 2 reputable journalistic sources 
- **8: All criteria for 7 points, and:**
    - Website contains at least 4 sections.
    - All sections are at least 400 words in length.
    - Bibliography includes at least 4 scholarly sources.
    - Bibliography includes at least 4 reputable journalistic sources.
    - All references include a full and complete citation.
    - References are meaningfully integrated in the body of the text
- **9: All criteria for 8 points, and:**
    - The website has a clear and interesting structure, and good transitions between ideas. The website might be a multi-paragraph essay, or it might be broken up with headers and short subsections and split across multiple pages. Different formats work better for different topics and different people; the point is that the website is well-organized for the reader.
    - Your website shows evidence of improvement from the draft version. You have responded to the feedback of your peers. 
    - Website makes good use of images, charts, video, audio, or other non-text-based media.
- **10: All criteria for 9 points, and:**
    - Website could be used as an example of excellent work in a future class.
    - The ideas presented show that you can synthesize multiple pieces of evidence to develop a coherent point in your own voice. Your text moves beyond a point by point summary, listing, or comparison of others' ideas and shows your team's own voice.
    - On the whole, the writing and site design are of high quality and the ideas presented are compelling, provocative, interesting, creative and/or insightful.
