---
title: Schedule
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-calendar-alt"
weight: 500

menu: 
    main:
        parent: "Teaching"
        identifier: "unst230-201804-schedule"
---

This class involves a significant amount of reading and writing. 
The free online book, _[The Word on College Reading and Writing](https://openoregon.pressbooks.pub/wrd/chapter/reading-effectively/)_, has some useful advice for those of you who are new to reading- and writing-intensive college courses. 

### Unit 0: Course Intro

#### Mon 24-Sep
Introduction to the course and major assignments.

- No Readings.
- _Assigned:_
    - [SA1: Start of Term Self-Assessment](/~harmon8/classes/unst230/self-assessment/#start-of-term-self-assessment), due Wednesday 26-Sep, 8:00p
    - [JE1: Critical Observation Exercise](/~harmon8/classes/unst230/journal/#je1-technology), due Friday, 8:00p

#### Wed 26-Sep
No Class Meeting. Use this time for [JE1: Critical Observation Exercise](/~harmon8/classes/unst230/journal/#je1-technology)

- Read, before you begin JE1:
    - Beattie, Alex. 2018. “Review: Stand out of Our Light by James Williams,” _The Disconnect_. Issue 2: Summer 2018. <https://thedisconnect.co/two/review-stand-out-of-our-light>
- Due:
    - [SA1: Start of Term Self-Assessment](/~harmon8/classes/unst230/self-assessment/#start-of-term-self-assessment), 8:00p

<h4 class="mentor"> Mentor 1</h4>

Introductions


<h4 class="deadline-only">Friday 28-Sep</h4>

- Weekly Journal Check, 8:00p:
    - [JE1: Critical Observation Exercise](/~harmon8/classes/unst230/journal/#je1-technology)
    - _Note: no free write required this week_


### Unit 1: Freedom + Privacy

#### Mon 1-Oct
What is freedom? Who can experience freedom? Is it important? Why (not)? What are its limits?

- Read before class:
    - Davis, Angela. 2008. "The Meaning of Freedom." Metropolitan State College, Denver, CO, February 15. Available at:  <http://sfaq.us/2016/06/angela-davis-the-meaning-of-freedom/>
    - gnu.org. n.d. "What Is Free Software?" Accessed September 17, 2018. <https://www.gnu.org/philosophy/free-sw.html>
    - Swartz, Aaron. n.d. "Guerilla Open Access Manifesto." Accessed September 17, 2018. Available at: <http://archive.org/details/GuerillaOpenAccessManifesto>
- Due:  
    - [Discussion question 1](../discussion), 2 hours before class, Piazza

#### Wed 3-Oct
Freedom on the Internet; Research about/on Facebook: emotional contagion + voter influence studies. [Slides \[PDF\]](https://drive.google.com/file/d/1lKvnYciOSiPvPjj064jAFJ7iOFt0icKk/view?usp=sharing)

- Read before class:
    - (Podcast) Belmont, Veronica. 2017. "Free Speech, Limited?" *Mozilla: IRL*. <https://irlpodcast.org/season1/episode7/>
    - Tufekci, Zeynep. 2018. "It's the (Democracy-Poisoning) Golden Age of Free Speech." *Wired*, January 16, 2018. <https://www.wired.com/story/free-speech-issue-tech-turmoil-new-censorship/>
- Due: 
    - [Discussion question 2](../discussion), 2 hours before class, Piazza

<h4 class="mentor">Mentor 2</h4>

Form Project groups; choose platform [Wordpress or PebblePad or Google Sites]; choose artifact.

<h4 class="deadline-only">Friday 5-Oct</h4>

- Weekly Journal Check, 8:00p:
    - [JE2: Freedom](/~harmon8/classes/unst230/journal/#je2-freedom)
    - + Weekly free-write entry.

 
#### Mon 8-Oct
Is privacy a right? 

- Read before class:
    - Warren, Samuel D., and Louis D. Brandeis. 1890. "The Right to Privacy." *Harvard Law Review* 4 (5): 193--220. <https://doi.org/10.2307/1321160>
- Due: 
    - [Discussion question 3](../discussion), 2 hours before class, Piazza

#### Wed 10-Oct
How does privacy interact with new technology? What's in your data profile?

- Read before class:
    - Choose one post to read from the Privacy section of the Tactical Tech website: <https://tacticaltech.org/themes/privacy/> **Be prepared to share in class.**
    - Find and read one recent news article related to privacy and technology. **Post a link + note about it on Piazza.**
- In-Class: 
    - Activity: What's in your data profile? 
    - Review and consider feasibility and desirability: <https://datadetox.myshadow.org/en/detox>
    - Technology pro tips: Password Managers, 2 Factor Authentication, Firefox Profiles
* Due: 
    - NO DISCUSSION QUESTION. Instead, post your news article on Piazza. **Include a link and short summary.** What is the article about? How does it relate to things we have talked about in class? Why is it important? (Graded as Discussion Question 4)

<h4 class="mentor"> Mentor 3</h4>

Knowledge convergence exercise: What do you know about your artifact? What do you need to research?

<h4 class="deadline-only">Friday 12-Oct</h4>

- Implosion Project Milestone 1, 8:00pm:
    - [IP1: Knowledge Memo + Team Contract](/~harmon8/classes/unst230/implosion/#ip1-knowledge-memo)
- Weekly Journal Check, 8:00p:
    - [JE3: Privacy](/~harmon8/classes/unst230/journal/#je2-privacy)
    - + Weekly free-write entry.


### Unit 2: Technology

#### Mon 15-Oct
Conducting Research, Using Citation Managers, Summarizing sources

- Read before class:
    - Selections from _The Word on College Reading and Writing_:
        - "Summaries"  <https://openoregon.pressbooks.pub/wrd/chapter/writing-summaries/>
        - "Paraphrasing" <https://openoregon.pressbooks.pub/wrd/chapter/paraphrasing/>
        - "Quoting" <https://openoregon.pressbooks.pub/wrd/chapter/quoting/>
        - "Crediting and Citing your Sources" <https://openoregon.pressbooks.pub/wrd/chapter/crediting-your-sources/>
- Due: 
    - **Instead of a regular discussion question, read ONE article for your implosion project, and post a summary of it.** (Graded as Discussion Question 5, based on criteria for a good summary in the "Summaries" reading above)


	
#### Wed 17-Oct 
Implosion Question 1: What is its history? How does its history matter today?

- Read before class: 
	- Lee, Timothy B. 2014. "40 Maps That Explain the Internet." *Vox.Com*, June 2, 2014. [http://www.vox.com/a/internet-maps](http://www.vox.com/a/internet-maps).
- Due: 
	- Choice:
        - [Discussion Question / Summary 6](../discussion), 2 hours before class, Piazza
        - You may write EITHER a discussion question OR a summary of the above piece. See the chapter ["Summaries"](https://openoregon.pressbooks.pub/wrd/chapter/writing-summaries/) for detail on what makes a good summary.


<h4 class="mentor"> Mentor 4</h4>

Citations & Research 

<h4 class="deadline-only">Friday 19-Oct</h4>

- Weekly Journal Check, 8:00p:
    - [JE4: TBD](/~harmon8/classes/unst230/journal/#je4-tbd)
    - + Weekly free-write entry.

#### Mon 22-Oct 
Implosion Question 2: Who makes it? How is it made?

- Read before class: 
    - Shapin, Steven. 1989. "The Invisible Technician." *American Scientist* 77 (6): 554--63. <http://www.jstor.org/stable/27856006>
- Due: 
	- [Discussion Question / Summary 7](../discussion), 2 hours before class, Piazza
	- You may write EITHER a discussion question OR a summary of the above piece. See the chapter ["Summaries"](https://openoregon.pressbooks.pub/wrd/chapter/writing-summaries/) for detail on what makes a good summary.
	
#### Wed 24-Oct
Implosion Question 3: Who is it for? What is it for? Who tells these stories?

- Read before class: 
	- Fischer, Claude S. 1994. "Educating the Public", Chapter 3 of *America Calling: A Social History of the Telephone to 1940*. University of California Press. [PSU Library e-Book](https://search.library.pdx.edu/primo-explore/fulldisplay?docid=CP7194981110001451&context=L&vid=PSU&search_scope=all&tab=default_tab&lang=en_US)
- Due: 
	- [Discussion Question / Summary 8](../discussion), 2 hours before class, Piazza
    - You may write EITHER a discussion question OR a summary of the above piece. See the chapter ["Summaries"](https://openoregon.pressbooks.pub/wrd/chapter/writing-summaries/) for detail on what makes a good summary.


<h4 class="mentor"> Mentor 5</h4>

Group work time, implosion project.


<h4 class="deadline-only">Friday 26-Oct</h4>

- Weekly Journal Check, 8:00p:
    - [JE5: TBD](/~harmon8/classes/unst230/journal/#je5-tbd)
    - + Weekly free-write entry.


#### Mon 29-Oct 
Implosion Question 4: How does it change social order? How does it impact formal institutions (e.g., schools, businesses, governments) and informal social structures (e.g., family, friendship)?

- Read before class: 
	- Barley, Stephen R. 1986. "Technology as an Occasion for Structuring: Evidence from Observations of CT Scanners and the Social Order of Radiology Departments." *Administrative Science Quarterly* 31 (1): 78--108. <https://doi.org/10.2307/2392767>
- Due: 
	- [Discussion Question / Summary 9](../discussion), 2 hours before class, Piazza
	- You may write EITHER a discussion question OR a summary of the above piece. See the chapter ["Summaries"](https://openoregon.pressbooks.pub/wrd/chapter/writing-summaries/) for detail on what makes a good summary.

#### Wed 31-Oct 
Implosion Question 5: How does it change what we know about the world? How does it change what we know about ourselves? How does it change what kinds of information or knowledge we value?

- Read before class: 
	-  Levy, Stephen. 2014. “A Spreadsheet Way of Knowledge.” _WIRED | Backchannel_, October 2014. <https://wired.com/2014/10/a-spreadsheet-way-of-knowledge/>
- Due: 
	- [Discussion Question / Summary 10](../discussion), 2 hours before class, Piazza
	- You may write EITHER a discussion question OR a summary of the above piece. See the chapter ["Summaries"](https://openoregon.pressbooks.pub/wrd/chapter/writing-summaries/) for detail on what makes a good summary.


<h4 class="mentor"> Mentor 6 </h4>

Continue Discussion + Group Work Time

<h4 class="deadline-only">Friday 2-Nov</h4>

- Weekly Journal Check, 8:00p:
    - [JE6: TBD](/~harmon8/classes/unst230/journal/#je6-tbd)
    - + Weekly free-write entry.



### Unit 3: Book Project


#### Mon 5-Nov
In-class workshop I: Black Mirror Episode (TBD) + Discussion

- Read before class: 
    - N/A
- Due: 
    - N/A

#### Wed 7-Nov 
[Book Project](https://drive.google.com/open?id=1LONzIvrOW2dZ0NFUBJhwxvlUwF6DYT1A), Selection 1

- Read before class: 
	- Selection 1 from your chosen book
    
- Due: 
	- Contribution to the book notes for selection 1

<h4 class="mentor"> Mentor 7 </h4>

**Present Draft website and exchange feedback with peers.**

<h4 class="deadline-only">Friday 9-Nov</h4>

- Implosion Project Milestone 2, 8:00pm:
    - [IP2: Website Draft + Feedback Memo](/~harmon8/classes/unst230/implosion/#ip2-website-draft-memo)
- Weekly Journal Check, 8:00p:
    - [JE7: TBD](/~harmon8/classes/unst230/journal/#je7-tbd)
    - + Weekly free-write entry.


#### Mon 12-Nov - **NO CLASS** 
Veteran's Day.

#### Wed 14-Nov
[Book Project](https://drive.google.com/open?id=1LONzIvrOW2dZ0NFUBJhwxvlUwF6DYT1A), Selection 2

- Read before class:
    - Selection 2 from your chosen book
- Due: 
    - Contribution to the book notes for selection 2.
    

<h4 class="mentor"> Mentor 8</h4>

**Wednesday mentor sessions only**

<h4 class="deadline-only">Friday 16-Nov</h4>

- Weekly Journal Check, 8:00p:
    - [JE8: TBD](/~harmon8/classes/unst230/journal/#je8-tbd)
    - + Weekly free-write entry.



#### Mon 19-Nov
Workshop II: Programming **Bring a laptop if you have one**

- Read before class: 
    - N/A
- Due: 
    - N/A


#### Wed 21-Nov
[Book Project](https://drive.google.com/open?id=1LONzIvrOW2dZ0NFUBJhwxvlUwF6DYT1A), Selection 3

- Read before class:
    - Selection 3 from your chosen book
- Due: 
    - Contribution to the book notes for selection 3.
    
<h4 class="mentor"> Mentor 9 </h4>

**Monday mentor sessions only**

<h4 class="deadline-only">Friday 23-Nov</h4>

- Weekly Journal Check, 8:00p:
    - [JE9: TBD](/~harmon8/classes/unst230/journal/#je9-tbd)
    - + Weekly free-write entry.


#### Mon 26-Nov 

Workshop III: Poster/Meme/PSA **Bring a laptop if you have one**

- Read:
    - N/A
- Due:
    - N/A

#### Wed 28-Nov 

[Book Project](https://drive.google.com/open?id=1LONzIvrOW2dZ0NFUBJhwxvlUwF6DYT1A), Selection 4

- Read before class:
    - Selection 4 from your chosen book
- Due: 
    - Contribution to the book notes for selection 4.

<h4 class="mentor"> Mentor 10 </h4>

Project Work Session


<h4 class="deadline-only">Friday 30-Nov</h4>

- Final Journal Portfolio, 8:00p:
    - [JE10: Final Portfolio](/~harmon8/classes/unst230/journal/#je10-final-portfolio)

    
<h4 class="deadline-only">Wednesday 5-Dec</h4>

- Implosion Project Milestone 3, 12:30pm
    - [IP3: Slide Deck + Memo](/~harmon8/classes/unst230/implosion/#ip3-final-website-presentation) due to D2L 24 hours before presentation. 

#### Final Meeting, Thursday, December 6, 12:30pm-2:30pm
Final Team Presentations During Class Meeting. No Final Exam. **Attendance is mandatory.**

- Due:
    - [End of Term Self-Assessment](/~harmon8/classes/unst230/self-assessment/#end-of-term-self-assessment), 8:00pm




