---
title: "UNST 230 (Fall 2018)"
date: 2018-08-15
quarter: 201804
courseNumber: "UNST 230"
courseTitle: "Freedom, Privacy, and Technology (SINQ)"
meetings: "MW 12:30-1:45"
location: "Cramer 103"
type: "class"
layout: "class-home"

menu: 
    main:
        parent: "Teaching"
---
**This course is the Sophomore Inquiry (SINQ) course for the University
Studies "Freedom, Privacy, and Technology" cluster.** As described on
the UNST website:

> The aim of this cluster is to provide the knowledge
that will enable those who complete the cluster to face thoughtfully the
question of the appropriate use of and limitations upon modern
technology. One important feature of the cluster is that it brings
together actual sciences with humanistic and social science
disciplines.[^1]

[^1]: See <https://sinq-clusters.unst.pdx.edu/cluster/freedom-privacy-and-technology>

### Course goals
This course is designed around the four core University Studies goals[^2]: 

[^2]: See <https://www.pdx.edu/unst/university-studies-goals>

1. **Diversity, Equity, and Social Justice**: Students will explore and analyze identity, power relationships, and social justice in historical contexts and contemporary settings from multiple perspectives.
2. **Communication**: Students will enhance their capacity to communicate in various ways—writing, graphics, numeracy, and other visual and oral means—to collaborate effectively with others in group work, and to be competent in appropriate communication technologies.
3. **Inquiry and Critical Thinking**: Students will learn various modes of inquiry through interdisciplinary curricula—problem-posing, investigating, conceptualizing—in order to become active, self-motivated, and empowered learners.
4. **Ethics and Social Responsibility**: Students will expand their understanding of the impact and value of individuals and their choices on society, both intellectually and socially, through group projects and collaboration in learning communities.


### Course Content

This course is organized in four units. _(See the course [schedule](schedule) for more detail)_ 

1.	**What do we mean by freedom and privacy in America?** _(Weeks 1-3)_: How are values of freedom and privacy influenced, constrained, supported, and/or violated by contemporary technologies? By exploring these two core American values, we will lay the groundwork for future conversations about the impacts and effects of science and technology. 

2.	**What is a technology?** _(Weeks 4-6)_: In order to really understand the effects, limitations, and appropriate uses of a technology, what do we need to know about it? Each class meeting will focus on elaborating a different set of questions we might ask about a technological artifact, by drawing on a set of historical cases: What is the history of the internet? How did the marketing of telephones impact their use? How have imaging technologies changed science and medicine? In this unit we will deepen our knowledge of what constitutes a technological artifact, and work through some of the questions you will need to ask in your implosion projects.

3.	**What are algorithms, and what are their effects?** _(Weeks 7-9)_: The third unit will take the figure of ‘the algorithm’ in contemporary American society as an object of inquiry to think through the implosion project questions again, and focus in on the impacts and effects of this technology. How are algorithms used? What are their effects? What are their limitations? How _should_ they be used? How _should_ they be limited?

4.	**Science Fictional Futures** _(Weeks 9-10)_: This unit is designed to be (a) a bit of a breather towards the end of the quarter and (b) an opportunity to look more towards the future as we close out the class. How do futures come to be? Which futures arrive -- and which don't? What kind of futures do we want (or want to avoid)? How can and should technology be part of those futures?

### Major Assignments

**The course will begin and end with a written self-assessment** in which you
will set your own learning goals for the quarter, and reflect on your
challenges and successes. 

_&raquo; [Self-Assessment Details](self-assessment)_

**The major course project is an "implosion" of a scientific or
technological artifact.**[^3] You will work in groups of 4 to research an
artifact of your choice in weeks 3-6. You will share your research by
creating a website that examines the multiple dimensions of your
artifact: What is its history? Who makes it? How is it made? Who is it
for? How is it used? What are its effects? What are its limitations? How
does it impact freedom, privacy, and/or other human values? How *should*
it be used? In week 7, you will present your draft website in your
mentor session, and solicit feedback from your peers. During weeks 8-10,
you will conduct further research and refine your website in response to
this peer feedback. You will share your final website with the entire
class in a concluding presentation delivered during the final exam
period. 

[^3]: This assignment is adapted from Joe Dumit's Implosion Project. See <https://culanth.org/articles/741-writing-the-implosion-teaching-the-world-one>

_&raquo; [Implosion Project Details](implosion)_

In parallel to your in-depth examinations of specific artifacts, the
course readings will help us explore the same questions about the
appropriate uses and limitations of a variety of new and old
technologies: the landline telephone, the photograph, social media, cell
phone tracking/surveillance, and algorithmic decision making. Your
preparation for and participation in course discussions are crucial to
your own learning and your peers' learning. **_Your attendance and
participation in all class meetings and mentor sessions is mandatory and part of your grade._** 

_&raquo; See the [Schedule](schedule) for a reading list and the [Policies](policies) page for details on this and other course policies._

In order to help you prepare for course discussions and develop your writing 
skills, you will each maintain **a personal journal in which you will write
both on topics of you own choice, and in response to exercises associated with 
the course content**. You should add to these reflections during and after 
class as part of taking notes on the course content and processing your
learning. Each Friday at 8pm, you will turn in your new journal entries, and I
will grade them simply for completion. At the end of week 10, you will turn in
the entire journal, along with a review that highlights 5 of your best entries. At that time, you will receive a grade for the quality of work in the journal. 

_&raquo; [Journal Details](journal)_

_Note: The work in this course is front-loaded, in an effort to offset
the major time commitments of this course from the major time
commitments of your other classes. There is no written final exam;
however, **attendance during the final exam period is mandatory**._

_&raquo; See the [Policies](policies) page for further details on this and other course policies._


### This Website is a Living Document

This website is a starting point for the course. It is subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced via email, so attend class and check your inbox. Major updates will be noted here on the course home page.

