---
title: Personal Journal
cc: true
type: "class"
layout: "subpage"
icon: "fa-book"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "unst230-201804-journal"
    
---

_This assignment is adapted from the journaling exercise for Dr. Nick Seaver's Attention seminar[^1]._

One of the primary goals of this class is to help you become a better writer. One of the best ways to become a better writer is ... to write! Therefore, you will all be keeping a journal in this class. 

At minimum, you should be writing two entries in this journal each week: a response to a specific prompt/exercise, and a 20-minute free write session. I would encourage you to also use the journal as a space to also take notes about the course readings and discussions, but it is not required.

The journal exercises will connect directly to course content, and are described at the bottom of this page. The weekly free write session can be on any topic you like. The only requirement is that you find a 20-minute block and spend the entire time writing (or trying to write). 

You **must** bring your journal with you to every class period. Sometimes we will discuss the journal exercises in class and you will need them with you! 

Aim for a mix of jottings (i.e. scratchy notes that may only make sense to you), and more thought-out notes written in intelligible sentences. You may want to revise old entries, annotate them with things you learned later, or use them as raw materials for later reflections. Practice thinking in dialog with your journal.

### Format

You may keep your journal electronically or on paper. It should be in a format that works best for you, but keep in mind that you will need to bring it with you to every class meeting, and you will need to be able to turn in the entire journal at the end of the quarter. Some example formats: 

- A single document on your computer that you add to each week. 
- An online blog.
- A paper notebook. 
- A series of documents -- that are well-organized -- in a folder on your computer.
- etc.

Sometimes there will be sketching components to the weekly exercise. If you choose to keep a digital journal you can take photos and insert them.  Likewise, if you keep a paper journal, you may want to reference digital materials or photos, in which case you re welcome to print such things out and tape them into the journal.


### Logistics

Each week you should add two new entries to your journal: 
- A response to the weekly exercise.
- A 20-minute free write session.

_Exceptions: For week 1, you only need to complete the exercise. There is no required free-write. For week 10, the closing exercise is a portfolio review of your work throughout the quarter. You might create this as a final entry, or you can prepare it as its own document. It's up to you._ 

**Every Friday at 8pm**, you will turn in your new journal entries to me in the appropriate folder on D2L. This turn in may look different depending on what format you choose: 
- **Paper Journal:** Take photos of all entries since the last turn in and upload them. Please number the photos sequentially.
- **Digital Document-based Journal:** Turn in the document itself (.docx, .pdf, .txt, .rtf are all acceptable formats. Contact me if you have a different one). You are welcome to turn in the entire journal.
- **Online Journal:** Email me a link to your journal no later than 8pm on Friday, September 28. Please use the subject line: "[UNST 230] Journal Link" I will check this same link once a week. Please do _not_ re-send it every Friday. Please _do_ make sure to date your entries. 



#### Final Portfolio

At the end of week 10, the journal exercise for the week will be to create a portfolio highlighting five of your journal entries from throughout the quarter. [Details](#je10-final-portfolio)

### Grading

#### Week 1

The week 1 journal entry is more involved than the others, and therefore you get more credit for it. 

- **0 / 4**
    - Not completed or document contains plagiarized content
- **2.8 / 4**
    - Something is turned in that responds to most of the prompts
- **3.2 / 4** All requirements for 3 points and also:
    - Shows evidence of having completed all three parts of the exercise. 
    - Comments are thorough and complete, and move beyond the obvious.
- **4 / 4** All requirements for 3.2 points and also:
    - Could be used as an example of excellent work in future classes.
    - Demonstrates original thought and creativity.
    - Shows evidence of significant student effort, seriousness, thoughtfulness, and reflection. 

#### Week 2-9, Weekly checks 

The grading for the weekly checks is very simple:
- **0 points**: Not turned in and/or the weekly exercise is not complete.
- **1 point**: Complete response to the weekly exercise, but no free-write.
- **2 points**: Both required entries are complete for the week.

_**To count as a complete response, each entry must be at least 250 words in length.**_

#### Final Portfolio, Week 10

This culminating exercise is worth a full 10 points towards your final grade. Further details will be posted later in the term.


### Weekly Exercises 

#### JE1: Technology

**Due: Friday, 28-September, 8pm**

_Note: This exercise has multiple parts, and will take more time than the usual journal exercises. Our class meeting on Wednesday this week is cancelled, so you will have some extra time to complete the exercise._

The goal of this journaling exercise is to reflect on the role of technology in your own and others' lives.

1. Preparation:

    - Before beginning this assignment, you need to read the assigned article for Wednesday 26-September: Beattie, Alex. 2018. "Review: Stand out of Our Light by James Williams," _The Disconnect_. Issue 2: Summer 2018. <https://thedisconnect.co/two/review-stand-out-of-our-light>

    - You will need to set aside a 70-90 minute period during which you will minimize your use of technology. No phones, laptops, computers, books, bicycles, etc.

    - Exceptions: you will need to have a pen/pencil and few pieces of blank paper for your writing. If you are planning to keep a digital journal, you can scan or photograph these pages and insert them into your digital journal. Please do not use a computer or phone during this exercise.

    - You will need to set up in a public location where there are other people [e.g., a cafe, a park, the PSU Urban Plaza, Pioneer Square, etc.]

2. The writing: _**You should spend at least 20 minutes writing in response to each set of prompts.**_ Note the start and end times for each section on your paper.

    - **Personal Reflection:** 
        - Start by marking off an area on one of your pages to make notes about distractions. _Every time you feel distracted during this assignment, make a short note about what triggered the distraction._
        - Make two lists: All the technologies you have with you right now, and all the technologies you used yesterday from the moment you woke up to the moment you went to bed. _As you work through the rest of this exercise, make a note every time you think about wanting to use one of these technologies._ 
            - Which of these technologies will be the hardest for you to not use for the next hour? 
            - Which of these technologies would be the hardest for you to stop using _entirely_?
        - Choose five important technologies on your lists. What would be the ramifications if you stopped using one of these technologies entirely? Are there any that would be impossible to not use at all? Why or why not?
        - Which technologies are important for... 
            - ... your ability to complete your school work?
            - ... your ability to do your job (current job and/or anticipated future job)?
            - ... your ability to fully participate in your family life?
            - ... your ability to maintain friendships and social ties?
            - ... your own personal sanity?  
        - What other things are technologies important for?
    - **Observing others:** 
        - First, make some general notes about your observation site: Where are you? What time of day is it? Who is around you? How many people? What are they doing? Who are they doing it with? Who is alone, and who is in a group? It may be useful to sketch the general scene.
        - Start to look around you, and spend a half hour taking notes about people's technology use and attention.
            - You might start with another list: what are all the technologies you can see? 
                - Who is using them? How are they being used? Can you tell? 
            - Are people using some technologies individually, in pairs, or in groups? You might try sketching some of these scenes. How are technologies part of the social fabric around you? 
            - What are the things people are paying attention to? How are they paying attention?
            - Can you see anyone who looks focused? Watch them for five minutes. Do they get distracted? What triggers their change of attention? 
        - What else do you notice?
    - **Closing Reflection:**
        - Did you notice anything surprising about yourself or others? 
        - How many times were you distracted? What kinds of things distracted you or re-directed your attention? 
        - How many times did you think about wanting to use a technology during this exercise?
            - Were you able to finish the whole exercise without using any technologies beyond pencil and paper? If not, why not? What exceptions did you make? Why?
        - What did you think about the Beattie and Williams article? Do the technologies in your lists feel distracting? Why or why not? Which ones? When? How?
        - What additional preparations would you have to make if the assignment had been to minimize your technology use for 24 hours? Could you go an entire day without checking your email? Logging into D2L? Texting a friend or family member? What would be the most challenging part of an extended assignment?
        - How have you felt during this exercise? What kinds of things did you pay attention to? What kinds of things distracted you?
        - What technology are you most excited to start using now? Why? What is the first thing you will do with it?
        - Did you learn anything from this exercise? What would make it a more valuable exercise?
    
#### JE2: Freedom
**Due: Friday, 5-October, 8pm**

This week also has an observation and personal reflection component. You will need to complete the exercise in a public location of your choosing.

You only need to spend about 30 minutes total on this exercise. I recommend spending some time first on a 10-15 minute personal reflection, and then spending an additional 15-20 minutes observing others. 

- **Personal Reflection:** 
    - Write a short definition of freedom. What does it mean to you? Why is it important? _Is_ it important? What are its limits? 
    - What kinds of freedoms do you have in the current moment, and what kinds of freedoms do you not have?
    - Think of a time in your life when you felt a very high degree of freedom. What about that time/place/setting was freeing? 
    - What is the opposite of freedom for you? When have you felt least free? What did you feel instead?
- **Observing others:**
    - First, make some general notes about your observation site: Where are you? What time of day is it? Who is around you? How many people? What are they doing? Who are they doing it with? Who is alone, and who is in a group? It may be useful to sketch the general scene.
    - Look around you. Who looks the _most_ free? What looks free about them? Do you think they would describe themselves as feeling free? 
    - What kinds of things are the people around you free to do? What are they not free to do?
    - How are people's actions, behavior, thoughts, or feelings being directed or managed or coerced? Who or what is directing, managing, or coercing them? Why? How?
    - Identify at least one limit on the freedoms of the people around you. Why is that limit in place? Who decided to create that limit? How is it implemented? What would it be like if that limit went away?
    - What else do you notice?


#### JE3: Privacy

**Due: Friday, 12-October, 8pm**

This week also has an observation and personal reflection component. You will need to complete the exercise in a public location of your choosing. 

You only need to spend about 30 minutes total on this exercise. I recommend observing for about 15-20 minutes, and then spending 10-15 minutes on your personal reflection. 

- **Observing others:** 
    - First, make some general notes about your observation site: Where are you? What time of day is it? Who is around you? How many people? What are they doing? Who are they doing it with? Who is alone, and who is in a group? It may be useful to sketch the general scene.
    - How do you feel about watching these people in the location you chose? What kinds of privacy do you think the people around you expect? What kinds of privacy do they deserve? 
    - Are there things that are not okay to look at? What things? Why? 
    - Would you feel comfortable taking photos? Why or why not? Are there limits to what you would take a photo of?
- **Personal Reflection:** 
    - What kind of privacy do you expect right now? How would you feel if someone came over and took the paper you're writing on and started reading it aloud? What if they didn't take the paper, but just read it silently to themselves while looking over your shoulder?
    - What strategies do you have for protecting your own privacy? How do you keep secrets? What kind of things do you need privacy for? 

 
#### JE4: Your Future

**Due: Friday, 19-October, 8pm**

This was posted late, if you would like to simply do two free-writes for this week, that is acceptable. 

_Optional Prompt:_ In five years I will be .... 

#### JE5: Pitch a New Technology

**Due: Friday, 26-October, 8pm**

Come up with a new technology that would change your life. It doesn't have to be realistic/possible. Write a pitch to convince a venture capitalist to invest in the development and production of this technology. What story will you tell about who would want this technology, why they would want it, how it would change the world, what your business model is, etc?

#### JE6: A World Without Cars

**Due: Friday, 2-November, 8pm**

Imagine a world without automobiles, motorcycles, or busses [but trains, planes, bicycles, etc. all still exist]. Write about what a day in your life would look like from a transportation perspective. When writing, think about things like: How would the world would be different if these everyday technologies had never been invented? What other technologies would be primary modes of transportation? How would cities be different? 

#### JE7: Favorite Algorithms

**Due: Friday, 9-November, 8pm**

What's your favorite algorithm? What does it do for you? Do you remember what life was like before this algorithm? How has it changed your life?

#### JE8: Future of A Technology

**Due: Friday, 16-November, 8pm**

Think about the technology that you have been studying all quarter. Write a short fictional story in which you imagine the role of this technology 15 years in the future. Will people still be using it in 2033? Will the technology change? Will it be obsolete? What will people say about it? 

#### JE9: Future of Attention

**Due: Friday, 23-November, 8pm**

Review the first journal entry you wrote, in which you spent time observing others' interactions and attentional practices in public space. Write a short fictional story about what you might see if you were conducting this same observation in 2033 (15 years from now).

#### JE10: Final portfolio

**Due: Friday, 30-November, 8pm**

For this final journal exercise, you will create a portfolio highlighting five of your journal entries from throughout the quarter. You might create this portfolio as a final entry in your journal, or you can prepare it as its own separate document.

##### Logistics

- **Deadline:** Friday, 30-November, 8pm
- **What to turn in:** A _single_ PDF in which you have compiled all required elements
- **How to turn it in:** In the D2L Folder for JE10


##### Specification
You should include at least two free writing sessions, two responses to a journal exercise, and one additional entry of your choice. 

You should choose the pieces of your work that you think best demonstrate your engagement with the course ideas, your own intellectual growth, and your own improvement as a writer. 

**We will talk about this more in class on Wednesday, November 21. Please bring your questions about this assignment to class on the 21st.**

Your portfolio should begin with a 1-page memo that tells me why you chose the entries you chose, how they fit together, what you learned from them, and how they show your progress over the quarter. This may be informally written. The point is to direct my attention to features of the portfolio that are important. 

After the 1-page introduction, please include each of your five chosen entries. They should all be integrated in a single final portfolio document so that it is _easy_ for me to review all 5 entries.

If you have a paper journal, this means compiling photos of the prior entries all as part of your turn in and putting everything into one master document. If you have an online journal, please either print each entry to a PDF and compile those pages, or copy and paste the contents into a document. _**See me in my office hours if you have questions about how to make this happen.**_

In the process of building your portfolio, you are welcome to annotate the prior entries however you like. However, please do not re-write them entirely. Instead, think of this as an opportunity to add additional reflections about the content, notes about how your writing or thinking has changed since you wrote them, or comments that point out passages of particular importance to make sure I see them when I am grading. 


 
 [^1]: Seaver, Nick. "How To Pay Attention." Anthropology 185-20, Tufts University, Spring 2018. <https://static1.squarespace.com/static/55eb004ee4b0518639d59d9b/t/5a73576fec212dcd8f379609/1517508463478/HTPA-syllabus-2.2.pdf>


