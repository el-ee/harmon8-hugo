---
title: Syllabus
credits: <p>In addition to the credits noted throughout, this syllabus has benefited greatly from the input of many people. In particular, policies have been adapted from those of <a href="http://aschrock.com">Andrew Schrock</a>, <a href="http://lynndombrowski.com">Lynn Dombrowski</a>, <a href="https://caseyfiesler.com">Casey Fiesler</a>, <a href="https://jedbrubaker.com">Jed Brubaker</a>, <a href="http://cs.pdx.edu/~karlaf">Karla Fant</a>, and <a href="http://cs.pdx.edu/~markem">Mark Morrissey</a>.</p>
cc: true
type: "class"
layout: "subpage"
icon: "fa-info-circle"
weight: 100

menu: 
    main:
        parent: "Teaching"
        identifier: "hci-201801-syllabus"
---

### Course Information


|  |  |
|:-----|:-----|
| **Section**| TOP: INTRO TO HUMAN COMP INTER - 45784 - CS 410 - 050|
||TOP: INTRO TO HUMAN COMP INTER - 45786 - CS 510 - 053|
| **Prerequisites** | None. |
| **Location**      | Fourth Avenue Building 150|
| **Meeting Time**  | Mondays & Wednesdays 12:45p-2:35p|
| **Final Exam**    | Monday, March 19, 12:30p-2:20p |
|| No written exam. We will use this time slot for final presentations. Attendance is mandatory.|
| **Holidays**      | Monday, January 15 |
| **Website**       |<http://cs.pdx.edu/~harmon8/hci.html>|
| **Instructor**    | Dr. Ellie Harmon                           |
|                   | Suite 120, Fourth Avenue Building<br/>1900 SW 4th Avenue (corner of SW Harrison) |                             |
|                   | <ellie.harmon@pdx.edu>                     |
| **Office Hours**  | Office 120-15                    |
|   | Monday 3:00-4:30  |
|   | Friday 10:00-10:30  |


#### Notice of Potential Changes

This is a new course at PSU, which is being developed as a possibility for a
more permanent offering. It is being adapted from an
[Intro to HCI](http://ellieharmon.com/teaching/2014-131/) course taught
previously at the University of California, Irvine. As such, the syllabus and
schedule may be further adapted as necessary as the term progresses. You will be
notified of any changes no less than one week before a relevant due date. The
website contains the most up-to-date version of this syllabus, the schedule,
and the assignment requirements.

You are responsible for any announcements made in class, and any messages sent
to the official class email list. You should check your pdx.edu email at least
once per day.

#### Course Description[^1]

This course will introduce you to the fundamental principles of human-computer interaction (HCI), more often referred to as "User Experience" in industry and professional contexts. It will expose you to a wide variety of research and design techniques through a team-based  human-centered design project. By the end of the quarter you should have material of appropriate quality for including in a portfolio to demonstrate your experience with HCI/UX.

You will not become a UX expert in a single 10-week course, but you will leave this course with a better understanding of what skills you might wish to develop further on your own or in future coursework. The larger goal of the course is to help you understand how, when, what, and why HCI/UX expertise can contribute to a design and development project.



#### Course Goals[^2]

Upon the successful completion of this course students will be able to:

1. Understand why, when, and how to engage stakeholders in an iterative human-centered design process
2. Apply some basic HCI principles (e.g., affordances, constraints, Fitt's law) to improve the design of computer systems
5. Create sketches to capture, envision, and critique design ideas
3. Understand some human-centered research techniques such as interviewing and observations to better understand stakeholder needs
4. Understand techniques such as storyboarding, user stories, and personas that are useful for synthesizing research and incorporating research findings into a design process
6. Create a low-fidelity prototype suitable for basic user evaluation, and perform some basic types of evaluation
7. Identify some of the current research areas and open questions in the field of HCI
8. Analyze how computer systems influence human experience, and recognize the value of human-centered design practices for computer systems development


#### Course Technology

##### Readings

There will be no required textbook to purchase for this course. Because this is an introductory course to a broad field, readings will be selected from several HCI textbooks, UX handbooks, and published articles. This strategy will expose you to the variety of both academically-grounded and practitioner-focused resources available to you for future work in this area.

All required readings will be linked from the course
[schedule](/~harmon8/hci/schedule.html).

**ACM Digital Library Links:**

Many course readings come from publications available through the ACM Digital
Library. I have attempted to provide direct links to these, via the University
library's proxy service, so that you can easily access them from on- or
off-campus. You will have to log in through the university's single-sign-on
system to access the links. If you have trouble with any of the ACM
links, you should look up these readings yourself through the Portland State
Library website. Full bibliographic detail is provided. The failure of any
syllabus link is not an excuse to skip the assigned reading.

**Scanned PDFs**

A few readings are available as scanned PDFs; these are shared through the
Portland State University Google Drive service, and you will need to be
logged in to your PSU account in order to access the links.


##### Optional Reference Books

Some of the course readings will come from the following books. You may want
to consider purchasing one or more of them if you plan to pursue any UX or HCI
work in the future.

- {% reference Hartson:2012 %} *Note: New edition coming out in Fall 2018*
- {% reference Hannington:2012 %}
- {% reference Lidwell:2010 %}
- {% reference Baxter:2015 %}
- {% reference Goodman:2012 %}

##### Sketchbook

Sketching is a common practice used by designers that fosters the ability to think critically about existing objects and interactions, and generating ways of improving them.

As part of this class, you will keep a sketchbook, in which you will think about objects or interactions in your daily life and sketch ideas for how they could be improved.

You will need a blank sketchbook for this assignment that you can turn in at the end of the quarter. This book should be:

- Unlined
- At least 30 pages
- Sized appropriately to carry around with you
- Dedicated only to sketching

The PSU Bookstore sells many sizes and types of unlined books that would be appropriate for this. This does not need to be expensive. You could also
[make your own book](http://www.marcjohns.com/blog/2013/01/30/i-make-my-own-sketchbooks)
if you are feeling DIY!

##### Laptops

Laptops are allowed in this class, and may be useful for taking notes. If laptops become a problem (_e.g._, if I receive complaints from students that there is a lot of distracting YouTube watching in class), then I reserve the right to alter this policy, and potentially ban laptops from the classroom.

### Course Schedule Overview

Week | Topic
-|-
1| Intro to Course & Sketching Journal
2| What is HCI? History of the field
3| User Research
4| Values & Design + Making Sense of Data I
5| Making Sense of Data II + Computing in Contemporary Life
6| Brainstorming & Ideation + Computing in the Workplace
7| Storyboards + Human Factors
8| Wireframes & Prototypes + Theories of the User
9| Simple Evaluation + Dark Patterns
10| Designing for Change + Limits of Design
F| Class Presentations
 | _This meeting is during the finals time slot_

### Grading

Points|Assignment
-|-
15|Attendance & Participation
24|Reading Responses
1|Topic Selection
8|Individual User Research
24|Group Design Project (4 milestones)
8|Weekly Sketching Assignments
8|Sketching Final Reflection
12|Individual Design Reflection + Group Evaluation
-|-
100|Total

#### Conversion to Letter Grades:

| Total Points | Letter Grade |
|--------------|--------------|
| 93-100       | A            |
| 90-92        | A-           |
|-|-|
| 87-89        | B+           |
| 83-86        | B            |
| 80-82        | B-           |
|-|-|
| 77-79        | C+           |
| 73-76        | C           |
| 70-72        | C-           |
|-|-|
| 67-69        | D+           |
| 63-66        | D            |
| 60-62        | D-           |
|-|-|
| Below 60     | F            |

### Course Culture & Student Resources

_Note: The Code of Conduct section is very minimally adapted from from course materials of [Dr. Jennifer Parham-Mocello of Oregon State University](https://classes.engr.oregonstate.edu/eecs/fall2017/cs161-001/syllabus/community.html) with additional credit to Dr. Susan Shaw also of Oregon State University._

#### Code of Conduct[^3]

Many open source projects and professional societies have recognized
that the lack of diversity amongst contributors is a problem since they
miss out on ideas, perspectives, and contributions from underrepresented
groups. To address this, they have established community guidelines and
codes of conduct to support communities that are more welcoming to new
and diverse contributors. For example:

- [Contributor Covenant](http://contributor-covenant.org/): a code of conduct
  shared by many open source projects, including Atom, Eclipse, Mono, Rails,
  Swift, and many more.
- [Mozilla Community Participation Guidelines](https://www.mozilla.org/en-US/about/governance/policies/participation/)
- [Python Diversity Statement](https://www.python.org/community/diversity/)
- [Ubuntu Code of Conduct](https://www.ubuntu.com/about/about-ubuntu/conduct)

Within computer science programs, similar issues often exist. Research
shows that underrepresented groups leave computer science programs at a
higher rate, and that this attrition is a result of environmental
conditions[^4] . Therefore, in this course, we will also have a set of
community guidelines.

These guidelines start from the premise that every student should feel
safe and welcome to contribute in this course. As the instructor, I will
try to establish this tone whenever possible, but ultimately the
responsibility for cultivating a safe and welcoming community belongs to
the students—that means you!

Fortunately, being part of a safe and welcoming community is not too
hard. A good place to start is to recognize (and continually remind
yourself) of the following facts:

- Your classmates come from a variety of cultural, economic, and
  educational backgrounds. Something that is obvious to you may not be
  obvious to them.
- Your classmates are human beings with intelligence and emotions. This
  applies even when sending emails or posting messages on D2L.
- Your classmates are here to learn. They have the right to pursue their
  education without being distracted by others' disruptive behavior, or
  made uncomfortable by inappropriate jokes or unwanted sexual interest.

If each of us remembers these facts and act with corresponding decency,
respect, and professionalism, the course will certainly be better for
everyone.

Some students might be inclined to shrug this off and perhaps crack a
joke about safe spaces or political correctness. If that’s you, please
also know that if you make a fellow student uncomfortable by mocking
them, making inappropriate jokes, or making unwanted advances, that is
harassment and will be taken seriously. (If you are a victim of
harassment, please see the brief list of resources in the section
[What To Do About Harassment](#what-to-do-about-harassment)).

However, I hope that we can all approach this positively. Treat your
classmates as respected colleagues, support each other when needed, have
fun without spoiling it for anyone else, and everybody wins.

##### Course Guidelines and Etiquette

1. Make a personal commitment to learning about, understanding, and
   supporting your peers and instructor.
1. Think through and re-read your comments before presenting them.
1. Never make derogatory comments toward another person in the class,
   including the instructor or assistants.
1. Do not make sexist, racist, homophobic, or victim-blaming comments at
   all.
1. Disagree with ideas, but do not make personal attacks.
1. Assume the best of others in the class and expect the best from them.
1. Acknowledge the impact of sexism, racism, ethnocentrism, classism,
   heterosexism, ageism, and ableism on the lives of class members.
1. Recognize and value the experiences, abilities, and knowledge each
   person brings to class. Value the diversity of the class.
1. Pay close attention to what your classmates say. Ask clarifying
   questions, when appropriate. These questions are meant to probe and shed
   new light, not to minimize or devalue comments.
1. Be open to being challenged or confronted on your ideas or prejudices.
1. Challenge others with the intent of facilitating growth. Do not demean
   or embarrass others.
1. Encourage others to develop and share their ideas.
1. Participate actively in the discussions, having completed the readings
   and thought about the issues.
1. Be willing to change.

##### What to Do About Harassment

If you are a victim of harassment of any kind in this class, there are
several resources available to you:

- You may schedule a private meeting to talk to me[\*](#meeting-note)
- Fill out the PSU
  [Bias Incident Report Form](https://goo.gl/forms/PMrV0tUbhDWqcBAy1)
- File a
  [formal complaint](https://www.pdx.edu/diversity/file-a-complaint-of-discriminationharassment)
- Contact the **Office of Global Diversity and Inclusion**: Market Center
  Building, 1600 SW 4th Avenue, Suite 830
- Contact the Dean of Student Life: <askdos@pdx.edu>

<a id="meeting-note">\*</a>Please note the
[Title IX reporting obligations](#title-ix-reporting-obligations)
with regards to any harassment or discrimination that
you may choose to discuss with me.

#### Student Resources

There are many many resources available to you as a PSU student. I
strongly recommend taking advantage of them so that you will be
successful both at your academic and non-academic pursuits. I have
included a few highlights, relevant to this class and/or computer
science[^5]:

**Women in Computer Science**: The WiCS group encourages and empowers
women to pursue computer science. They are dedicated to building an
inclusive, supportive community of technical women at Portland State
University. They hold weekly WiCS meetings for any woman or
underrepresented group in computer science. <http://wics.cs.pdx.edu>

**Center for Student Health and Counseling (SHAC)**: a community-based
health care organization that provides high quality, accessible mental
health, physical health, dental services, and testing services.
<http://pdx.edu/shac>

**C.A.R.E. Team**: As a member of the Portland State University
community you are in a unique position to identify signs of distress and
connect students to supportive services.  Our goal is for you to be safe
and successful.  The CARE Team can help if you or someone you know is
having a difficult time. <http://pdx.edu/dos/care-team>

#### Access and Inclusion for Students with Disabilities[^6]

PSU values diversity and inclusion; we are committed to fostering mutual
respect and full participation for all students. My goal is to create a
learning environment that is equitable, useable, inclusive, and
welcoming. If any aspects of instruction or course design result in
barriers to your inclusion or learning, please notify me. The Disability
Resource Center (DRC) provides reasonable accommodations for students
who encounter barriers in the learning environment.

If you have, or think you may have, a disability that may affect your
work in this class and feel you need accommodations, contact the
Disability Resource Center to schedule an appointment and initiate a
conversation about reasonable accommodations. The DRC is located in 116
Smith Memorial Student Union, 503-725-4150, <drc@pdx.edu>,
<https://www.pdx.edu/drc>.

- If you already have accommodations, **_please contact me_** to make sure
  that I have received a faculty notification letter and discuss your
  accommodations.
- Students who need accommodations for tests and quizzes are expected to
  schedule their tests to overlap with the time the class is taking the test.
- Please be aware that the accessible tables or chairs in the room should
  remain available for students who find that standard classroom seating
  is not useable.
- For information about emergency preparedness, please go to the
  [Fire and Life Safety](https://www.pdx.edu/environmental-health-safety/fire-and-life-safety)
  webpage for information.

#### Title IX Reporting Obligations[^7]

Portland State University is committed to supporting students’ safe
access to their education. Sexual assault, sexual/gender-based
harassment, dating violence, domestic violence and stalking are all
prohibited at PSU. Students have many options for accessing support,
both on and off campus.

As an instructor, one of my responsibilities is to help create a safe
learning environment for my students and for the campus as a whole.
Please be aware that federal, state, and PSU policies require faculty
members to report any instances of sexual harassment, sexual violence
and/or other forms of prohibited discrimination. Similarly, PSU faculty
are required to file a report if they have reasonable cause to believe
that a child with whom they come into contact has suffered abuse, or
that any person with whom they come into contact has abused a child.

***If you would rather share information about these experiences with an
employee who does not have these reporting responsibilities and can keep
the information confidential***, please contact one of the following
campus resources:

- **[PSU’s Sexual Misconduct Options Website](https://www.pdx.edu/sexual-assault/get-help)**
- **[Women’s Resource Center](https://www.pdx.edu/wrc/contact)
  :** (503) 725-5672
- **[Queer Resource Center](https://www.pdx.edu/queer/interpersonal-violence-ipv-resources)
  :** (503) 725-9742
- **[Center for Student Health and Counseling (SHAC)](http://pdx.edu/shac)
  :** (503) 725-2800
- **[Student Legal Services](https://www.pdx.edu/sls) :** (503) 725-4556

For more information about the applicable regulations please complete
the required student module [Creating a Safe Campus in your D2L](https://www.pdx.edu/sexual-assault/safe-campus-module).

### Course Policies

#### Academic Honesty [^8]

Our expressions are not our own. Humans communicate with words and concepts— and within cultures and arguments—that are not of our own making. Writing, like other forms of communication, is a matter of combining existing materials in communicative ways. Different groups of people have different norms that govern these combinations: modernist poets and collagists, mashup artists and programmers, blues musicians and attorneys, documentarians and physicists all abide by different sets of rules about what counts as “originality,” what kinds of copying are acceptable, and how one should relate to the materials from which one draws.

In this course, you will continue to learn the norms of citation and attribution shared by the community of scholars at Portland State University and at other higher education institutes in the United States. Failure to abide by these norms is considered plagiarism, as laid out in the Student Code of Conduct with which you should familiarize yourself:

> \(9) Academic Misconduct. Academic Misconduct is defined as, actual or
> attempted, fraud, deceit, or unauthorized use of materials prohibited or
> inappropriate in the context of the academic assignment. Unless
> otherwise specified by the faculty member, all submissions, whether in
> draft or final form, must either be the Student’s own work, or must
> clearly acknowledge the source(s). Academic Misconduct includes, but is
> not limited to: (a) cheating, (b) fraud, (c) plagiarism, such as word
> for word copying, using borrowed words or phrases from original text
> into new patterns without attribution, or paraphrasing another writer’s
> ideas; (d) the buying or selling of all or any portion of course
> assignments and research papers; (e) performing academic assignments
> (including tests and examinations) in another person’s stead; (f)
> unauthorized disclosure or receipt of academic information; (g)
> falsification of research data (h) unauthorized collaboration; (i) using
> the same paper or data for several assignments or courses without proper
> documentation; (j) unauthorized alteration of student records; and (k)
> academic sabotage, including destroying or obstructing another student’s
> work.[^9]

Any academic misconduct, including plagiarism, will result in a grade of zero for the assignment concerned. I am required to report all incidents of academic misconduct to the department chair and the dean of students.

Because I have had issues in the past with plagiarism in my classes, I strongly
recommend that all students **take the
[plagiarism test offered by Indiana University](https://www.indiana.edu/~academy/firstPrinciples/index.html) during
the first two weeks of the term.** If you are confident in your knowledge of
plagiarism, you can skip directly to the certification test; it is very short
(it took me 5.1 minutes to complete). If you are not confident in your
knowledge of plagiarism, you will save time by working through the tutorials.

_You should print and sign your certificate and turn it in on paper no later than the end of the second week of the term._

_You may opt out of this test by printing, signing, and dating
[this form](https://drive.google.com/open?id=1FFkM8W-5iB3o0EAeRKIa5faVYVYK3Amx)
and turning it in no later than the end of the second week of the term._

_**Failing to turn in either the opt-out form or your certificate will result in a 10-point deduction from your participation score (this will lower your final grade in the class by a full letter grade).**_

Admittedly, many university plagiarism policies tend to focus on the less
productive side of the issue, urging students to be “original” and telling them
what not to do (buying papers, copying text from the internet and passing it
off as one’s own, etc.). While you should follow these rules, I encourage you
to take a more expansive view of what academic integrity means. Academic
integrity is not a matter of producing purely original thought, but of
recognizing and acknowledging the resources on which you draw (you will notice
in the acknowledgements throughout, that this syllabus draws heavily from a
wide range of other university professors, for example). In light of this,
I do not use “plagiarism detection” services like Turnitin. Rather than
expending your energy worrying about originality, I suggest that you think
instead about what kind of citational network you are locating yourself in.
What thinkers are you thinking with? Where do they come from? How might their
positions in the world inform their thoughts – and, in turn, your thoughts?
What is your position relative to these thinkers? How might you re-shape your
citational network to better reflect your priorities or ideals?

If you are interested in these issues, I recommend these pieces:

- Ahmed, Sara. 2013. “Making Feminist Points.” _feministkilljoys_.
    <http:// feministkilljoys.com/2013/09/11/making-feminist-points/>
- Frank, Will. 2015. “IP-rimer: A Basic Explanation of Intellectual Property.”
    _Personal Blog Post (Medium)_.
    <https://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711>
- Introna, Lucas. 2016. “Algorithms, Governance, and Governmentality: On
    Governing Academic Writing.” _Science, Technology & Human Values_ 41(1):
    17–49.

You may write a 500-word response to these readings for extra credit. See me in
office hours for details.



#### Communication

**Slack:** There is a slack channel for this course. It is a great place
to ask questions publicly so that your classmates can benefit from the
answers as well. Also, sometimes your classmates know the answers to
your questions and can respond more quickly than I can.

**Email:** You are responsible for checking your pdx.edu email address _**at
least once a day**._ Important class announcements will be made through the
official course mailing list. You are responsible for reading all messages that
arrive in your pdx.edu inbox within _**one business day**_.

I will aim to answer all emails sent to me within ***one business day***
of receiving them. If you do not hear back from me within one business
day, please re-send your email.

Please note that I do not (cannot) respond to emails 24/7. Do not email
me ten minutes before an assignment is due expecting immediate
clarification on some detail. Instead, use this course as an opportunity
to develop your time management skills. You would not want to ask your
manager about requirements for a project an hour before a sprint ends;
you should start working well in advance of a deadline so that you have
time to ask questions.

#### Participation

All students are expected to come to class prepared to participate fully
in-group discussions, and come to class with questions about the reading
material and viewpoints from their own experiences and other relevant
literature and coursework. A significant portion of your grade consists
of an attendance and participation score.

Here are some examples of how you can participate:

- Treat all with respect: be constructive in all discussions
- Come to class prepared: read carefully prior to class meetings
- Practice active listening: be attentive, be engaged, and use in-class
  technology with discretion
- Ask challenging questions in class
- Comment, build on, or clarify others’ contributions
- Post useful or interesting information to the class slack channel
- Visit the instructor during office hours to chat, ask questions, or give
  feedback
- Contribute to the collaborative course notes

**Collaborative Course Notes:** We will use a shared Google Doc to
create an archive of notes on class material. This document will remain
accessible to you after you finish the class, serving as a resource of
information discussed. If you are someone who does not like talking in
class, you can participate in the course by contributing to this shared
archive.

_**NOTE:** In order for me to see your notes contributions and count
them towards your participation grade, you will need to make sure you
are logged into the Google Doc with your pdx.edu user account._

#### Attendance

This class is taught in an interactive format that includes discussion
and in-class work. Participation is essential, and ***attendance is
mandatory***. Every unexcused absence will reduce your attendance &
participation score by 0.75 point (Attendance and participation is worth
15 points; there are 20 course meetings).

***Attendance is tracked through the use of notecards which are handed
out at the beginning of class each day.*** You must write your name,
ODIN ID, the date, and one comment or question about the class on the
card. ***These should be dropped off at the podium on your way out the
door.*** Filling out a notecard for another student is prohibited, and
considered a form of academic misconduct.

If you are more than 15 minutes late to a class meeting, you will be
counted present, but will not receive participation points for that day.

When you are absent ***for any reason, excused or not, you are responsible for:***
1. Contacting me as soon as you know you will be absent
2. Initiating a plan to catch up on any missed materials
3. Initiating a plan for any missed assignments

##### One Free Absence

Everyone receives one (1) free absence to be used for any reason you
want — you overslept, you decided to drive out to the coast because it
was the first sunny day in 3 months, there was \*gasp\* traffic in
Portland. I don't care, and I don't need to know about the details.
***However, you (not me) are responsible for making yourself aware of any
and all content covered in class on the day that you are absent.***

#### Deadlines

Since many assignments build on each-other (and often we will use
homework as material for in-class discussions or activities),
***late work will not be accepted***. You will
receive partial credit for work that is partially complete. Turn in
whatever you have finished at the deadline.

#### Exceptional Circumstances

Please contact me in the case of any exceptional or unpredictable event
that significantly impacts your ability to complete work or attend
class.

Please note that **_traffic, TriMet delays, travel, and work schedules
are not considered exceptional or unpredictable events_**. I, too, ride
the bus to work, and have to cross a bridge to get here. I plan ahead to
make sure I am on time. You, too, can plan ahead. Please warn your
family in advance (_i.e._ today) that they need to consult you regarding
any future travel plans.

**_Please do not come to class sick._** Instead: email me and tell me
you'll be out, go to the health center, get a note (and maybe some
healthcare!), and show me the note later. Absences accompanied by a
legitimate note from SHAC will always be excused.

#### Group Work

In the professional world, you will almost never work alone; you will
always be part of teams. In order to help prepare you for this, portions
of this class may involve group work. Major group projects will include
a peer evaluation component in which you will fill out a team-assessment
and a self-assessment. These assessments will be used to determine
students’ grades for the group assignment. It is important that you
professionally and meaningfully contribute to and communicate with your
group regarding team projects and expectations.

##### Some strategies for successful group work

- Communicate early and often with your team during class meetings and via
  email.
- Follow the course code of conduct. Treat your peers with the respect with
  which you would like to be treated.
- Meet regularly outside of class to work in a co-located space, to share
  updates, and to coordinate any individual contributions.
- Plan ahead and create a **_reasonable_** timeline for team deliverables.
  Establish a clear set of expectations for your own and others’
  contributions. Consider putting these in writing (email is good for
  keeping a record of everyone's agreement!).
- Familiarize yourself with the skills and work of each individual student
  in your team. Be honest about your own strengths and weaknesses as well.
  Articulate these to your teammates. Ask for help when you need it. Show
  a mature and professional attitude in sharing responsibility.
- Pay attention to details without losing the big picture in your group
  work. Practice professional communication within your planning,
  documentation, and deliverables.
- If major conflicts arise, and students are not able to solve these
  conflicts, the whole group must meet with me to devise a working
  strategy. You should alert me to any major issues in a timely manner
  (i.e. no later than when the assignment in question is turned in).

##### Things to avoid

- Meeting with your group only at the last minute before class (or the
  same day) to patch things up and quickly integrate material and
  deliverables. This typically results in low quality deliverables, poor
  work integration within the team, clear evidence of disorganization and
  lack of coordination, unprofessional work, and lower grade.
- Alerting me to major team issues at the very end of the term or after
  grades are released.

### Notes

[^1]: This section is slightly revised text from: <https://www.pdx.edu/computer-science/cs-410510-top-introduction-to-hci>

[^2]: This section is slightly revised text from: <https://www.pdx.edu/computer-science/cs-410510-top-introduction-to-hci>


[^3]: This section is minimally adapted from Dr. Jennifer
    Parham-Mocello, Oregon State University
    <https://classes.engr.oregonstate.edu/eecs/fall2017/cs161-001/syllabus/community.html>
    with additional credit to Dr. Susan Shaw, Oregon
    State University

[^4]: See, e.g., <http://dl.acm.org/citation.cfm?id=374367>

[^5]: A more complete list is available on the Dean of Students site:
    <https://www.pdx.edu/dos/student-resources>

[^6]: This section is lightly edited from:
      <https://www.pdx.edu/drc/syllabus-statement>

[^7]: This section is lightly edited from the suggested syllabus
    statement available here: <https://www.pdx.edu/sexual-assault/faculty-staff-resources-responding-to-students-in-distress>

[^8]: Much of this section of the syllabus is copied (with
        permission) from the policy of [Nick Seaver](http://nickseaver.net/).

[^9]: PSU Student Code of Conduct:
    <https://www.pdx.edu/dos/psu-student-code-conduct>
