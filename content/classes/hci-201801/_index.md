---
title: "HCI (Winter 2018)" 
date: 2018-01-01 
quarter: 201801 
courseNumber: "CS 410 / 510" 
courseTitle: "Introduction to Human Computer Interaction (HCI)" 
# meetings: "TBD"
# location: "TBD"
type: "class"
layout: "class-home"
shortDescription: "This course will introduce you to the fundamental principles of human-computer interaction (HCI), more often referred to as “User Experience” in industry and professional contexts. It will expose you to a wide variety of research and design techniques through a team-based human-centered design project."

menu: 
    main:
        parent: "Teaching"
---

**Instructor: Ellie Harmon	\|  MW 12:45-2:35   \|   CRN: 45784 + 45786**

_Note: This is the website for the Winter **2018** offering of this class. Future offerings will be very similar, but you should expect some differences._

This course will introduce you to the fundamental principles of human-computer interaction (HCI), more often referred to as "User Experience" in industry and professional contexts. It will expose you to a wide variety of research and design techniques through a team-based human-centered design project. By the end of the quarter you should have material of appropriate quality for including in a portfolio to demonstrate your experience with HCI/UX.

You will not become a UX expert in a single 10-week course, but you will leave this course with a better understanding of what skills you might wish to develop further on your own or in future coursework. The larger goal of the course is to help you understand how, when, and why HCI/UX expertise can contribute to technology design and development.

**There are no pre-requisites for this class.** All students are expected to be prepared to read and write at the college level, and should be willing to learn new things! There will be **no programming** in this class.


_Note: This text is slightly revised from: <https://www.pdx.edu/computer-science/cs-410510-top-introduction-to-hci>_