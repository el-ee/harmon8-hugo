---
title: Reading Responses
credits: <p>This reading response assignment has benefited greatly from the input of <a href="http://lynndombrowski.com">Lynn Dombrowski</a>.</p>
cc: true
type: "class"
layout: "subpage"

menu: 
    main:
        parent: "Teaching"
        identifier: "hci-201801-reading"
---

Reading responses are meant to help you synthesize and reflect on assigned readings. They are also meant to be an opportunity for you to reflect on your own opinions in the context of the class.


### Logistics

**Deadline**: Start of class on the day readings are assigned (12:45 pm)

**Turn in format**: `.docx`, `.doc`, or `.pdf` upload to D2L

Late reading responses [will not be accepted](/~harmon8/hci/syllabus.html#deadlines)

Please make sure to include your name, the date, and the assignment name (e.g.,
RR1) at the top of the file you turn in.

**Reminder**: There will be a total of ten (10) opportunities to write a reading response for this class. **_You are only required to complete six (6)_**.

#### Extra Credit

For each extra reading response completed, one lowest reading response score will be dropped.

For each extra reading responses on which you score at least a 90%, a half point will be added to your final grade (for a maximum total of 2 extra points if you complete all 10 reading responses, and score at least a 90% on each one).

#### Grading

Grading is based on how well you demonstrate knowledge of the assigned reading AND how well you communicate your own ideas about the topic.

#### References & Acknowledgements

You are **not** required to include a bibliography list with your reading
responses unless you refer to materials from outside the class (in which case I
would appreciate a citation). But, in general, you are free to assume that I know
the material to which you are referring in a reading response as long as you
acknowledge it by the author's name / title of the piece / etc.

Please do make use of proper use of quotation marks when making direct quotes.

You are not required to include page numbers when making direct quotes. For
better or worse, page numbers are not generally part of the citation practices
of the ACM community.

Please make it clear when you are summarizing something from the assigned
reading versus when you are sharing your own opinion. This is especially
important since sometimes I am explicitly asking for your opinion! And, you
should make it clear to the grader when you have one.


### RR1: History & Foundations

A complete answer to each question should take about 2-5 sentences.

1. Before starting this class, what did you think "HCI" was all about? What did
you think this class would be about?

1. Based on the interviews from the Moggridge book, why was Stu Card frustrated with precursors to HCI like 'Human Factors'? What did he want to do differently?

1. Based on the interviews from the Moggridge book, what was Tim Mott's initial reaction to the POLOS system when he first flew out to visit PARC? How did he go about deciding how to change the system?

1. Based on reading the Bødker article, how would you summarize "second wave HCI" and "third wave HCI"? Give some examples of key differences.

1. What is the one key takeaway for you, after reading the Bødker article? -- OR -- What is one new question you have after reading the Bødker article?

1. After reading the Bannon article, write a few sentences summarizing the ways in which the field of HCI has changed since the 1980s.

1. What further changes does Bannon want to see?

1. What is the one key takeaway for you, after reading the Bannon article? -- OR -- What is one new question you have after reading the Bannon article?

1. Thinking across all of the readings for today, and the first class meeting, how would you summarize the central methods, goals, or ideas that are important to HCI today? Has your question to answer 1 changed? If so, how?

1. How do you think the field of HCI has impacted your own life? What would be different without it?

### RR2: Design as an Iterative process

1. Give an overview of the design cycle described in _The UX Book_. Include a
short description of each of the four key parts of the wheel lifecycle.

2. According to the authors of _The UX Book_, why is it important to use an
    **iterative** design cycle? Do you agree, why or why not?

3. List four different UX team member roles. For each one, briefly describe
    what that role is, and how that role fits in to the design cycle.

4. Which parts of the design cycle do you already have the most experience with?

5. Which parts of the design cycle are you most interested in learning about
    this quarter?

6. After reading the Willbanks blog post about design, what three pieces of
    advice are most important or relevant for you, personally? For each
    one you list, explain in a sentence or two why you chose that one.

7. Have your ideas about HCI and design changed since the beginning of the
    quarter? If so, how?

### RR3: Values & Design

1. Give a 2-sentence summary of what “value sensitive design” is.

2. Schulman writes about trying to change some of the vocabulary of design.
    For each of these three shifts that Schulman says we should make, describe
    what the shift is really about. That is, beyond just a word change, what
    change in design practice does each vocabulary shift entail?
    2.1. Imagining to Immersing
    2.2. Designing For to Making With
    2.3. Public to Private

2. Give a short summary, in your own words, of each of the three
    inclusive design principles central to Microsoft's toolkit.

2. Brainstorm a list of values that are important to your project for this class. List at least **4 values**. For at least two (2) of these values, reflect on the meaning of the value in the context of your project. Your reflection should address all of the following questions:

    1. What does the value mean in the context of your project?
    2. Who is the value is important to?
    3. Does the value have different meanings or are there multiple ways to
        enact it?
    4. How is technology related?

_Example:  My project is about family communication. In family life, family members often value ‘togetherness.’ This value is about spending time or being with another family member, and feeling connected. People sometimes enact this value by turning off their phones around the dinner table in order to feel more present and in the moment with other family members. Other times people enact this value by using their phones to share a photo with someone who cannot be present. The same technology (a phone) can be both supportive of this value (when people use it to share pictures), and can threaten this value (when people feel it is distracting)._

### RR4: Computing in Contemporary Life


1. There are four types of 'work' described in the Ames et al. article
    related to family use of video chat.
    1.1. Give a short definition, in your own words, of what "work" means
        in this context. That is, it doesn't mean "work" as in go to
        work and do your job. It means something else. What?
    1.2. Choose 2 of the four types of work described in the article and
        describe what that kind of work entails. What are some examples
        of that kind of work? How does that kind of work relate to
        possible improvements for videochat?

2. Think back to the prior discussion on values and design in this class.
    What is the distinction between "small-v" values and "large-V" Values
    discussed in the Ames et al. project? How does this relate to the
    kind of values discussed by Friedman in the prior reading?

3. In the first page of her article, Sengers says that design is part of
    an "ongoing process of modernization." Name and briefly describe
    the three values that Sengers identifies as related to that larger process.

4. Phoebe Sengers ends her article, “What I learned on Change Islands” with a
    question, “Can IT design as an influence compete with a pervasive cultural
    atmosphere of overwork and overload?” Thinking about the rest of the
    article, explain the relationships between IT and the cultural logics or
    values of choice, control, and productivity that lead her to this question.
    Does IT create these logics?

5. Thinking across the two readings for today, what is one question or
    key takeaway for you?

### RR5: Computing in the Workplace

1. Like the reading last week about video chat, Suchman focuses her paper around a concept of 'work' and 'work practice.' What does 'work practice' mean for Suchman?
2. What does it mean to make 'work practice' visible? Why is that something that might need to be done?
3. What are some of the effects of making something visible [that previously was not visible]?
4. What is the power of maps according to Wood (see p. 61) and how does that relate to the representations of work that Suchman is talking about?
5. Give a brief 3-5 sentence summary of the Grudin paper. What is the historical narrative it traces?
6. Written in the 1990s, the paper stops far short of the present day! Thinking about the more contemporary papers we read and discussed last week -- and the Bødker and Bannon readings from earlier in the term -- how would you add to Grudin's narrative today? Where is the current focus of interface development?
7. Does the analogy of a computer 'reaching out' make sense to you, today? Is it still a useful way to think about the location of HCI? If yes, why? If no, why not? What other metaphors can you think of?


### RR6: Human Factors

1. What is Fitt's law?
2. In the Smashing Magazine article, they divide Fitt's law into 4 'rules.' Choose two of these rules and explain the pros and cons of following them in different circumstances.
3. In Willett Kempton's article, he describes "Two Theories of Home Heat Control." Name and describe the particular type of theory that he is talking about.
4. What are the two theories that user's have about how a thermostat and furnace work together to regulate temperature? Name and briefly describe each.
5. Which of the two theories is closer to the technically correct way that furnaces and thermostats work? Is the more technically correct theory better in practice (explain why or why not)?
6. What is your main takeaway from this article? What about its contents is important for a designer to know?

### RR7: Theories of the User

1. What is unique about distributed cognition? What is different about it than the cognitive science theories that were mainstream when this paper was written?
2. How was distributed cognition used to analyze the air traffic control system? What did the analysis show or suggest?
3. After reading the Halverson article, how would you describe the role of theory in HCI?
4. Give one example of an ‘affordance’ or ‘signifier’ that you encounter in your daily life. Explain why it is an affordance or signifier.
5. Give an example of a time when you or someone you know had trouble using some kind of technology and the situation could have been improved with a better-designed signifier.

### RR8: Dark Patterns

1. What is a 'dark pattern'? Give a short definition and an example.
2. Why might Dark Patterns not work in the long term?
3. After reading the John Brownlee article, do you think Dark Patterns will ever go away? (Why or why not?)
4. What does Kelsey Campbell-Dollaghan say about how UX design decisions matter for news consumption?
5. Review the website for the habit summit (<https://habitsummit.com/>) look up the book _Hooked_ by Nir Eyal on Amazon or Google Books. Flip through a few of its pages. Do you think the techniques discussed at this conference and in this book are examples of Dark Patterns? (Why or why not?)
6. Do you think dark patterns are ethical? What about the techniques discussed at the habit summit and in the book _Hooked_? (Why or why not?)
7. What's the line between a dark pattern and good design -- and how do the evaluation metrics you're using for produce success matter in answering this question? Are there any design patterns / techniques that you would refuse to work on or implement?


### RR9: Designing for Change

1. Describe what Asad and LeDantec mean by "illegitimate civic participation."
2. Review the section ICTs and Civic Participation. Describe how ICTs relate to the three models of democracy that Asad and LeDantec highlight in reviewing van Dijk's prior work.
3. Review the background and methods section. Give a short (2-5 sentence) overview of the methods used by Asad and LeDantec, and the site where they conducted their research.
4. In the 'design orientations' section, Asad and LeDantec are reviewing some of the key takeaways from their work. What do you takeaway from reading this section? How does their work challenge traditional design processes?
5. Summarize the main argument that Courtney Martin makes in "The Reductive Seduction of Other People’s Problems."
6. What are two reasons that "reductive seduction" is dangerous?
7. Thinking across these two articles, why do you think I had you read them on the same day? What are the main takeaways _for you_ after reading these two pieces? (Your answer should be at least 4 sentences long and must engage the content of both pieces.)


### RR10: Limits of Design & Interaction

1. Summarize Baumer & Silberman’s argument for why we should not always frame design in terms of problems and solutions.
2. In “When the Implication is Not to Design (Technology),” Eric Baumer and Six Silberman present a set of three specific questions. Answer _**each**_ of these questions with regards to the design project you’ve been working on this quarter.
3. What does Taylor mean by "world making" and how does it relate to design? Beyond 'the interface,' what work does design do?
5. In many ways the Taylor article brings us back to the place we started the term, reading Bannon's article about reimagining HCI.
    1. Where are you at at the end of the quarter, have your ideas about HCI changed? If so, how? If not, what is the most important thing about HCI you've learned in the class?
    1. What questions do you have now about HCI and Design?
