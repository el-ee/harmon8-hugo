---
title: Design Project
credits: <p>The design of this assignment has benefited greatly from the input of <a href="http://lynndombrowski.com">Lynn Dombrowski</a>, <a href="https://jedbrubaker.com">Jed Brubaker</a>, <a href="http://www.gillianhayes.com/">Gillian Hayes</a>, <a href="http://www.dourish.com/">Paul Dourish</a>, and <a href="https://quote.ucsd.edu/lirani/">Lilly Irani</a>.</p>
cc: true
type: "class"
layout: "subpage"

menu: 
    main:
        parent: "Teaching"
        identifier: "hci-201801-designproject"
---

This project is meant to give you hands-on experience with a variety of techniques for HCI design. By the end of the quarter you will have engaged in substantive individual as well as group work, and should have material of appropriate quality for including in a portfolio to demonstrate your knowledge of HCI methods.

In weeks 3-4, you will engage in individual exploratory user research related to your chosen topic area.

In weeks 5-10, you will develop the project further in a collaborative team, working through data analysis, prototyping, and initial user evaluation.

Note: Directions for each milestone are subject to change until the date that the
milestone is assigned and discussed in class.

### D1: Topic Proposal

**This milestone should be completed individually.**

For this milestone, you will choose a final topic area for the design project
you will carry out for the rest of the quarter. You will specify at
least one location where you can conduct observations to learn more
about this topic area.

#### Logistics

**Assigned & Discussed in Class:** Wednesday, January 17

**Due:** Monday, January 22, 12:45p

**Turn In Directions:** For this milestone, all you need to do is answer the 2-question survey on D2L titled, "D1: Topic Choice."

_The survey will not be available until sometime after class on Wednesday January 17. I will email the class mailing list when it is available._

#### Grading

This assignment is only worth one point! Do it, and you get the point. Don't do it (or only do part of it) and you don't get the point.

### D2: Formative Research Report

**This milestone should be completed individually.**

For this milestone, you will conduct at least 30 minutes of observation in a
public location, and 2 interviews related to your project topic area. You will
then prepare a short report summarizing this formative research.

#### Logistics

**Assigned & Discussed in Class:** Monday, January 22 (Observations) and Wednesday, January 14 (Interviews)

**Due:** Wednesday, January 31, 12:45p

**Turn In Directions:** You should upload to D2L:

- 2 audio files or other proof of completing the interviews
- A _**single**_, _**well-organized**_ `.pdf` containing all other
    required contents

_You will need access to the contents of this file in class on Wednesday,
January 31. If you cannot bring the file with you on a laptop or tablet, then
you should consider printing at least the summary and reflection section._

#### Project Requirements / Directions

There are two parts to this assignment, observations and interviews

##### Observations

You should conduct two periods of observation in a public location. Each period should be at least 15 minutes long. You should vary either the time or location of your 2 observation periods meaningfully.

Because this is a very short class assignment, and you are a novice, you should
not conduct any observations in a location where people would have a
reasonable expectation of privacy.

- For example, you might observe for 20 minutes at the food trucks at lunch time, and come back and conduct observations again at the same location for 15 minutes at 5pm.
- Or, you might observe people at the bus stop near city hall at 5pm on one day, and observe people at the bus stops at Hollywood Transit center at 5pm on a different day.

You should take field notes in a medium of your choice (e.g., on your phone, laptop, or in a small notebook).

- Field notes should total **at least 4 pages** in  length. It's okay if they are really messy. Fieldnotes will not be graded for grammar, etc. We just want to see that you took notes in the field. I should see things like time stamps, written notes, small sketches or maps of the location. You can see examples of my own fieldnotes in the lecture slides from January 22.

You should take at least 4 photographs while conducting observations.

- If you are uncomfortable taking photos at your fieldsite, you should still take 1 photo from each session showing you went somewhere (e.g., if you are observing at the gym, you could take a picture of the outside of the gym building if you don’t feel comfortable taking photos inside the gym.)
- And, you should make 4 detailed sketches, depicting scenes from your fieldsite that are relevant to your project.

##### Interviews

You should conduct at least 2 interviews:

- Each interview must be at least 10 minutes long.
- You should prepare an interview guide with at least 10 questions on it before
    the first interview.
- You should take notes during each interview on the interview guide.
- You should revise the interview guide between interview 1 and 2.
- You should record the interviews.
    - There are many tools for making recordings, you should be able to do it on your phone or laptop fairly easily. If you do not have access to a phone or laptop with recording capabilities, contact the instructor ASAP for assistance.
- At least one interview must be conducted with someone who is NOT a member of
    this class.
- You may conduct your interviews in or out of context. There is no requirement
    regarding the location or setting of the interviews.

#### Document Specification

Your document contains two main parts, a section with summaries and reflections
on your research, and an appendix containing the raw
materials documenting your work.

##### Summaries & Reflections

- **Observation Summary** Write 2-3 paragraphs summarizing what you observed.
This should be a simple descriptive report that tells us where you went to
observe, what days and times you were there, and what you observed.

- **Observation Reflection**: Write 1-3 paragraphs reflecting on your experience conducting observations. What surprised you? What did you learn that you might not have noticed otherwise? What did you think about what you saw -- did you notice any  problems that people were having with technologies or notice things that you think you could improve? What would you do differently the next time you do observations? What parts were easy? What was challenging? Did you get bored?

- **Interview Summaries**: For each interview, write a short (2-3 paragraph) summary, or 1-2 page bulleted-list summary. This should include:
    - A general background / brief bio of the interviewee. Is the interviewee in this class? What is your relationship to this person (friend, family, roommate, stranger?) How old is the interviewee? What year in school is he/she? What is his/her major? What else is relevant to know for your project (e.g. if your project is about bicycles, does this person ride a bike? How often?). **Do NOT include any personally identifiable information (e.g. name)**.
    - The setting of the interview
    - The length of the interview
    - 3 key findings -- what did you learn?

- **Interview Reflection**: Write 1-3 paragraphs reflecting on your experience conducting both interviews. What surprised you? What did you learn that you didn't already know? What would you do differently the next time you interview someone? What parts were easy? What was challenging? How were the two interviews different? Why? Who would you interview next for this project if you had more time?

##### Appendix

- **A copy of your fieldnotes**
    - If you only have hand-written notes, please scan these or take high resolution photographs that you can embed in a PDF or Word document.
    - These should be at least 4 letter-size pages long (for example, if you took notes in a very small 3"x5" notebook, you should have ~16 pages).
- **The 4 photographs** you took while conducting observations:
    - Each photo should be captioned with a short 1-2 sentence description, explaining what they show and why it is interesting for your project.
    You should use the Standford d.school what-how-why prompts for this. (See  Stanford D-School Understand Mixtape [PDF](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98))
    - If you did not take photos at your fieldsite, you should instead submit:
        - The photo from each observation session showing that you actually went somewhere
        - AND your 4 detailed sketches. Sketches should be captioned like photos above. You should use the Standford d.school what-how-why prompts. (See  Stanford D-School Understand Mixtape [PDF](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98))
        - AND a 3-5 sentence reflection on why public photography is not acceptable in this setting -- and what does this mean for the kind of technology that would be acceptable to design for this setting?
- **Interview Questions**
    - A photo or scan of the interview guide you used from each interview.
    - The grader must be able to tell from this photo/scan that you took notes
        as you went through the interview.
    - The grader should also be able to tell from this document that you
        revised your questions between interview 1 and interview 2.

#### Grading

| 8 points total | | |
|-
| Points | Section | Criteria Details |
|1.5 points | **Observation Summary** | Meets requirements and clearly shows effort was put into the assignment.|
|2 points | **Observation Reflection** | Meets basic length requirements and demonstrates thoughtful engagement with the process of conducting observations, including references to course readings and/or discussions.|
| 1.5 points | **Interview Summary** | Meets requirements and clearly shows effort was put into the assignment. |
| 2 points | **Interview Reflection** | Meets basic length requirements and demonstrates thoughtful engagement with the process of conducting interviews, including references to course readings and/or discussions.|
| 1 point | **Appendix** | Complete and meets all requirements; documents the user research process. |

### D3: Data Analysis + Problem Identification

**THIS IS A GROUP PROJECT. Only one member of your group should upload a file to D2L.**

In this project, you will work with your team to integrate your formative research findings and narrow in on a specific problem to focus on in your design project.

We will begin this project (and you will be assigned to your group) in class
on Wednesday, January 31.

#### Logistics

**Assigned & Discussed in Class:** Wednesday, January 31 and Monday, February 5

**Due:** Monday, February 12, 12:45p

**Turn In Directions:** You should upload a _**single**_, **_well-organized_** `.pdf` to D2L, containing all required components.

- [HERE IS AN EXAMPLE!](http://ellieharmon.com/wp-content/uploads/D2-ExampleFile.pdf)
- _Please reduce image sizes when you generate your PDF._

#### Directions

1. You will create an affinity diagram in class on Wednesday January 31.
    This diagram will be used to sort the findings from all of your teammates'
    exploratory research.
2. As a team, you will choose a second diagram, of your choice, to further
    explore your research and topic space.
1. After creating these diagrams, your team should prepare a short 1-2
    paragraph problem statement about what problem you will tackle with your
    project. Ideally, you should at least have completed a draft of this
    problem statement before the class meeting on Monday February 5.
4. As a team, you should create two (2) personas / persona stories / modular
    personas.

##### A list of diagramming & mapping techniques:

- Experience Mapping from Adaptive Path [[Website]](http://mappingexperiences.com/) and [[Guide]](http://ellieharmon.com/wp-content/uploads/Adaptive_Paths_Guide_to_Experience_Mapping.pdf)
- Universal Methods of Design: [Cognitive Mapping](http://ellieharmon.com/wp-content/uploads/UMD-Cognitive-Mapping.pdf)
- Universal Methods of Design: [Concept Mapping](http://ellieharmon.com/wp-content/uploads/UMD-Concept-Mapping.pdf)
- Universal Methods of Design: [Thematic Networks](http://ellieharmon.com/wp-content/uploads/UMD-Thematic-Networks.pdf)
- Universal Methods of Design: [Stakeholder Maps](http://ellieharmon.com/wp-content/uploads/UMD-Stakeholder-Maps.pdf)
- EMPATHY MAPS in the d.school [Understand Mixtape](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98)

#### Document Specification

Your document should include:

1. Data Analysis Diagrams
    1. A photo of the affinity diagram you made in class.
    2. A photo of the second diagram your team chose to make.
    3. A short reflection that answers each of these questions:
        - What 2nd diagram did your team choose to make?
        - Why did you chose this diagram?
        - Who participated in the 2nd diagramming activity?
        - How did this activity differ from affinity diagramming?
        - Were these diagramming activities useful? Why or why not?
        - What would you do differently next time?

2. Problem Statement
    - Write approximately 1-2 paragraphs that explain the problem that your
        team has chosen to tackle for your project.

3. Personas / persona stories / modular personas
    - Explain the problem you want to solve from the perspective of 2 prospective users. Each persona story should include at least 1/2 page of text as well as one image or visual, and **follow the guidelines discussed in class on Monday February 5**.

#### Grading

This project is worth 6 points towards your final grade and will be graded as follows:

- .5 - Document is well organized as discussed in class
- .5 - Photo of affinity diagram made in class
- 1 - Additional diagram made outside of class:
    - (.5) for a photo of the diagram (.5) for answering related questions
- 2 - Problem statement: Clearly communicates a single problem; meets criteria as explained in discussion
- 2 - Personas/Persona Stories/Modular Personas (1 point each) Clearly communicates a story about 2 potential users that meets guidelines discussed in class on February 5.


### D4: Design Statement

The goal of this week's project work is twofold:

1. **User needs / criteria for a good solution**: Expanding on your work from last week to scope your design problem space, in this project you will identify a set of user needs in the form of 'how might we' questions.

2. **Design Ideas & Solutions**: After identifying your how might we questions, you will move forward in coming up with three proposed design solutions to your identified problem.

#### Logistics

We will have a brainstorming session in class on Monday, February 12 to get you started on this milestone.

**Assigned & Discussed in Class:** Monday, February 12 and Monday, February 19

**Due:** Monday, February 26, 12:45p

**Turn In Directions:** You should upload a _**single**_, **_well-organized_** `.pdf` to D2L, containing all required components.

- THIS IS A GROUP PROJECT. Only one member of your group should upload a file to D2L.
- [Here is an example for the last milestone](http://ellieharmon.com/wp-content/uploads/D2-ExampleFile.pdf). Your report will have different contents this time, but it should follow a similar formatting strategy.
- _Please reduce image sizes when you generate your PDF._

#### Directions

**Part 1: Brainstorming**

You should aim to conduct all of your brainstorming work **before** Monday, March 19.

1. **Brainstorming session 1**: "How Might We" questions
    - In class, at least 15 minutes, with your team.
    - Thinking about the user research you have all been conducting identify **at least 5 'how might we' questions** for your project. Coming up with these questions is a different way of thinking about user 'needs' before you jump to specific design solutions. What is it that your users need/desire to be different? From the d.school mixtape:
        > What do you (and your team) know about this challenge that no one else has considered? Not in terms of your long-held expertise, but instead unique user-centered insights. What’s your specific vision (not solution) about how you want change people’s lives? Another way to think about this is: what assumption are you going to disrupt on your way to a successful solution?

    - _Note: In class we discussed having a LOT (i.e. more than 5) of these how-might-we questions and then separately narrowing down a list of 3-5 key user needs that you can use to compare your various design alternatives. See notes in turn in section below about what I expect in your final document._

2. **Brainstorming session 2**: Solutions I
    - Start in class if time, otherwise outside of class, at least 20 minutes, with your team.
    - Thinking about your identified problem area, and _using your how-might-we questions as prompts,_ **identify at least 25 potential design solutions or innovations** with your team mates. These can be far-fetched ideas -- in fact, wild ideas are encouraged at this stage of the process!

3. **Brainstorming session 3**: Solutions II
    - Outside of class, with _at least 2 people not on your team_ and at least 1 team mate present
    - Facilitate a brainstorming session with people who aren't just group members. Identify at least **10 additional design solutions** -- or iterations on how you might implement one of your chosen solutions.

_After sessions 2 & 3: Decide on 3 solutions that you will storyboard next week and consider in more depth. These should be sufficiently **different** from one another._

**Part 2: Storyboarding**

_We will discuss storyboards in class on Monday February 19. I recommend waiting
until then to start this portion of the milestone._

I am personally a big fan of simple paper sketches. However, you may prefer to use some online tool like, [StoryboardThat](http://www.storyboardthat.com)

For each of your three proposed design solutions, you should create **two** storyboards of 3-5 frames each. Each storyboard should depict a key use case for that design solution. Your storyboards must meet the requirements discussed in class on February 19th!

After creating the six storyboards (2 per design alternative x 3 design alternatives), you should decide, as a group, which one you are going to move forward with. In making this decision, you should consider how well each solution is likely to solve the problem you identified in D3. You should also consider how well each solution responds to the how might we questions / user needs you identified as part of the brainstorming portion of this assignment.

#### Document Specification

Turn in a single, well-organized file, with all of the following sections:

1. **Brainstorming Documentation:** A photo of brainstorming sessions 2 and 3. With each photo, include a note stating who facilitated the session and who participated in coming up with ideas.

2. **User Needs Summary:** List your 3-5 most important how-might-we questions and/or your set of 3-5 key user needs. Describe how each question or specified user need relates to your prior research and your design problem (1-2 sentences each). Why are these questions or user needs the most important things to keep in mind as you transition into storyboarding and prototyping?

3. **Design Alternatives:** For _each_ of your three design alternatives:
    - Give a brief description or overview of the solution (2-3 sentences).
    - Include an image of your 2 storyboards for this solution. You should include a short textual description of the use case beneath each storyboard (1-3 sentences).
    - Explain how this solution addresses the problem you identified in D3 (1-3 sentences).
    - Explain how this solution responds to your how-might-we questions and/or satisfies your 3-5 key user needs (2-5 sentences).

4. **Design Choice:** Write a 1-3 paragraph explanation about which design solution you will move forward with (or how you will combine your different solutions together). This paragraph should clearly explain what the design solution is, how it addresses your design problem, and *why* you chose this particular solution over the others. Reference your user research and how-might-we questions and/or key user needs.

5. **Reflection:** A one to three paragraph reflection on this milestone. You must, at minimum, respond to these questions:
    - How did the the brainstorming session with your team differ from the one with outsiders?
        - Which session produced the ideas that you chose for your project?
        - Which method was more valuable, or did they each produce different results?
        - What would you do differently the next time you need to brainstorm design ideas?
    - How did you decide what three design ideas to storyboard?
    - What did you learn about your project from creating the storyboards? Do you have different ideas now about your users? Did you use your personas to think about how to create the storyboards?
    - How did you decide which one solution to move forward with after your storyboarding? Did you use a voting mechanism as discussed in the d.school mixtape? Did you just talk about it? Was it challenging to choose which design alternative you wanted to move forward with?

#### Grading

_Note: Grades for this project will include a set aside component for the 'wow factor' -- presentation is important in design work._

|||
|-
|0.5| Brainstorming Documentation |
|1  | User Needs Summary / How-Might-We Questions |
|2.0| Design Alternatives Section, including storyboards |
|1.5| Design Choice  |
|1.0| Reflection  |

### D5: Prototype + Evaluation

For this milestone, you will create paper or other low-fi prototypes for your chosen design solution, and conduct some simple evaluations of the system.

#### Logistics

**Assigned & Discussed in Class:** Monday, February 26 and Monday, March 5

**Due:** Monday, March 12, 12:45p

**Turn In Directions:** You should upload a _**single**_, **_well-organized_** `.pdf` to D2L, containing all required components.

- THIS IS A GROUP PROJECT. Only one member of your group should upload a file to D2L.
- _Please reduce image sizes when you generate your PDF._

##### Prototyping Tools

I recommend paper. Here is an example of using [paper prototyping for website development](http://alistapart.com/article/paperprototyping)

For mobile apps, you might consider using a tool like [POP App](https://popapp.in/) to make your paper prototypes come alive!

You can do something similar with presentation software like [PowerPoint](http://boxesandarrows.com/interactive-prototypes-with-powerpoint/), also see this article on [PowerPoint](http://keynotopia.com/guides-ppt/).

You may also use specialized tools like [Sketch](https://www.sketchapp.com/), [UX Pin](https://www.uxpin.com/), [Balsamiq](http://balsamiq.com/) or [Mockingbird](https://gomockingbird.com/mockingbird/#). It will take you longer. You have been warned.

Some people like to use diagramming tools like Visio or OmniGraffle.


#### Directions
1. Create a prototype! See prototyping tools section above. Your prototype must be detailed enough to allow you to examine at least one of the use cases described in your storyboards turned in for the last milestone. If you have a major issue with this, you should meet with me about what is an appropriate scope for your prototype.
2. At least **two** of your team-members should **separately** conduct **one** self-reflective evaluation. You may choose to conduct either a cognitive walk-through or a heuristic evaluation. Each team mate can do the same one, or you can do different ones.
    - **Heuristics Details**: Consider [Jakob Neilsen's famous list](http://www.nngroup.com/articles/ten-usability-heuristics/) or others that we discussed in class, or another of your choosing. If you choose your own, you **must explain where you found it, why you chose it, and why I should believe it's a legit list.**
        - You should use this form for recording your heuristic evaluations: Usability Aspect Report [UAR Word Template](https://drive.google.com/open?id=1sv3dMNvk3_e9wv8-qJz-GlaRor2gXzm3)
    - **Cognitive Walkthrough Details**: In addition to the assigned reading, you may find these two write-ups helpful for conducting a cognitive walkthrough. Both have very detailed examples of using the method: [Wharton et al 1994](https://drive.google.com/open?id=1Mk9txWxZB92AEghQNqZjX0v4JHFbyCba) OR [Spencer et al 2000](https://drive.google.com/open?id=1L8Aio1Ijve1tAccVVbWK_A8bPqM_x95L). You may follow the style of either one (they are different!). Regardless, you should use the form linked in the document specification below to record your findings.
        - Use this cognitive walk-through form for recording your evaluation: [CW Word Template](https://drive.google.com/open?id=1UWjvjp8xFHU0mYyRdzYNEzTBoJYwH6vk)

3. You should also recruit at least 2 people to participate in a short think-aloud exercise with the prototype. Review the relevant sections in [this chapter](https://drive.google.com/open?id=1e23Pt4SyuuE2tNzrUa1iHg01P7-xuDzi) from Bill Buxton's sketching book for a detailed explanation. You will give them a task or two to do and then you will observe how they carry it out. You should use the [Usability Aspect Report](https://drive.google.com/open?id=1sv3dMNvk3_e9wv8-qJz-GlaRor2gXzm3) to record your observations.


#### Document Specification

Turn in A SINGLE well-organized PDF file containing:

1. **Pictures & Description of your prototype**: include an explanation of what features or use cases your prototype allows you to text, a description of what prototyping methods/tools you chose, and why you made both of those decisions.
2. **Summary & Next Steps** Based on all of your evaluations, write a brief report:
    - What evaluations did you conduct?
    - What were the key problems that you identified?
    - If you had more time to work on this project:
        - What would you change in response do feedback?
        - What would you not change about your design?
        - Would you want to conduct more evaluations before iterating the design or are you ready to change things now?
3. **Reflection** (3-6 paragraphs)
    - A reflection on the experience of prototyping
        - What worked well?
        - What did not work well?
    - Reflection on the evaluation process
        - At a high level, what kind of problems did each evaluation surface? Were they similar or different? Why do you think that is?
        - Which evaluation was most helpful (or were they all equally helpful)? Why?
    - What would you do differently next time regarding **both** prototyping and evaluation?
    - Did you learn anything from this exercise? If so, what? If not, why not?
4. **Appendix**: One evaluation report for each of your **four** evaluations (2x self-reflective evaluation + 2x think aloud):
    - [UAR Word Template](https://drive.google.com/open?id=1sv3dMNvk3_e9wv8-qJz-GlaRor2gXzm3) -- used for think aloud and heuristic evaluations
    - [CW Word Template](https://drive.google.com/open?id=1UWjvjp8xFHU0mYyRdzYNEzTBoJYwH6vk) -- used only for cognitive walkthrough

#### Grading
- .5 -- **Format and Presentation:** correct format and quality of presentation style
- 1 -- **Prototype:** images fully document the prototype; description answers all questions, throughly and clearly communicating the proposed solution, and what aspects of that solution can be tested with the prototype; compelling explanation of _why_ prototyping decisions were made.
- 1.5 -- **Evaluation Summary:** clear and complete summary of evaluation results; quality of presentation and degree to which the report is compelling in motivating the proposed next steps.
- 2 -- **Reflection:** completeness, references to course materials, thoughtfulness and apparent effort
- 1 -- **Appendix (.25 per evaluation):** complete and correct use of templates to report evaluation results.


### D6: Case Study Presentation

During the Finals time slot on Monday March 19, at 12:30 pm, each team
will give a short (5-10 minute) presentation on their design project.

#### Logistics

**Assigned & Discussed in Class:** Monday, March 12

**Due:** Monday, March 19, 12:30p
_Presentations will take place during the final exam meeting._

**Turn In Directions:**
_You should upload a PPT or PDF file with your presentation in it to D2L no later than 5pm on Sunday March 18._

Everyone will present off of the computer in the room. So, I must have the
presentations in advance in order to load them on the computer.

#### Specification / Requirements

You should prepare your presentation as a human-centered design case study.

Walk the class through your project from start (research) to finish
(prototyping and evaluation). Tell us a little bit about each stage of
the project, and how you made different decisions between each part of
the design process.

You may want to review [Designing Case Studies: Showcasing a Human-Centered Design Process](https://www.smashingmagazine.com/2015/02/designing-case-studies-human-centered-design-process/) for inspiration.

**All team members should participate in the presentation.**

#### Grading
[Presentation Rubric](https://drive.google.com/open?id=1oCl-oTnZFL_tc8rZxQljmTDwwwUKDzaO)

### DF: Design Reflection

**This milestone should be completed individually.**

In this assignment, you will each reflect, individually, on your experience going through one cycle of an HCI design process, and what you’ve learned in this class.

#### Logistics

**Assigned & Discussed in Class:** Monday, March 12

**Due:** Monday, March 19, 12:30p

**Turn In Directions:** Upload a `.docx` file to D2L. **No other formats
are acceptable.**

#### Specification / Requirements

**Length**: 1000-2000 words

You should address all of the following questions:

1. **Personal Learning**
    - When you first signed up for this class, what goals did you have for this
        class? What did you hope to learn or get out of it?
    - Did your goals change at all over the quarter, or did new goals emerge?
        If so, how and why did these goals change?
    - How successful were you at achieving your goals? Why were you successful
        to this degree? What would have made you more successful?
    - What are the **three** most important things you’ve learned this quarter?
        Why are these things important to you?
    - What grade would you give yourself for this class, and how would you
        justify that grade? Are you satisfied with your performance and work in
        the class? Why or why not?
2. **HCI Design**
    - The next time you are working on a software project, will you use a human-centered process like we used in the class project? Why, how, to what extent, and/or why not?
    - Think about the different images of the HCI design process that we’ve seen this quarter – the messy cycle from Bill Moggridge’s book, the lifecycle loop from The UX Book, the hexagonal rainbow graphic from the Stanford d.school. What would your ideal HCI design process look like? Draw a picture or diagram. Explain the picture/diagram and give examples of the kinds of methods or tasks that should happen at each stage. Explain why this is your ideal process.


##### Graduate Students Only

**In addition** to the requirements above, you should also
include a third section in your document that contains a
750-word reflection on **how Human-Computer Interaction
matters for your own area of interest**. Because this course is
taught in a department in which HCI is not a specialization area,
your long term relationship to the material will depend on your
own ability to see the connections between HCI and other areas
of computer science.

MS Students: You should write this reflection with regards to
your [track](https://www.pdx.edu/computer-science/track-courses).
If you have not yet chosen your track, you should simply pick one
that you are considering, and write this reflection in terms of
that area of computer science.

PhD Students: You should write this reflection with regards to
your own current or planned area of dissertation research. If you
are not yet sure about what area of research you will pursue,
you should choose an area from the
[MS Track Listing](https://www.pdx.edu/computer-science/track-courses).

This **additional** reflection must:

- Be at least 750 words long. (So, **_your total reflection,
    should be 1750-2750 words in length_**).
- Include a clear statement of your own area of interest or work in the first
    sentence  

In addition, across your entire reflection, you must meaningfully integrate
at least three _(3) references to course materials_ and one _(1) peer-reviewed
reference_ from your own area of specialization.

#### Grading

_To be discussed in class on Monday March 12._
