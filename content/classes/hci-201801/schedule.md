---
title: Schedule
credits: <p>The structure of this course has benefited greatly from the input of <a href="http://lynndombrowski.com">Lynn Dombrowski</a>, <a href="https://jedbrubaker.com">Jed Brubaker</a>, <a href="http://www.gillianhayes.com/">Gillian Hayes</a>, <a href="http://www.dourish.com/">Paul Dourish</a>, and <a href="https://quote.ucsd.edu/lirani/">Lilly Irani</a>.</p>
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-calendar-alt"
weight: 500

menu: 
    main:
        parent: "Teaching"
        identifier: "hci-201801-schedule"
---

_Note: This schedule is subject to change. All changes will be announced and discussed in class no less than one week before an assignment is due._

Assignments are listed on the date that they are **due**. Please see further [notes](#notes) below the schedule.


| Week | Date | Topic & Readings | Assignments |
|:-----|:-----|:------|:----------|
|**1**  |**M 8-Jan**|**Intro to Course, Syllabus, Sketching Journal Discussion, Reading Strategies**|   |
|--
|   |**W 10-Jan**|**What is HCI? History & Overview**| **RR1**  |
|   |   |**Design Principles**: Hick's Law + Fitt's Law |   |
|   |   |Selections from {%reference Moggridge:2007 %} [[PDF]](https://drive.google.com/file/d/1byNqlUIf2H1R0PUuPD2atHzJb9QOOYDt/view?usp=sharing): **_Required:_** Interviews with Stu Card and Tim Mott (pp. 16-32 in PDF). **_Optional_:** Foreword by Gillian Compton Smith (pp. 3-15 in PDF)|   |
|   |   |{% reference Bodker:2006 %} |   |
|   |   |{% reference Bannon:2011 %} |   |
|   |   |**_Optional:_** {% reference Grudin:2013 %}|   |
|--
|**2**|_M 15-Jan_| _PSU Holiday: No Class Meeting_ |   |
|   |   |_**Sketching Reference:** {% reference Baskinger:2008 %}_||
|   |   |_**Sketching Reference:** {% reference Brewer:2010 %}_||
|   |   |_**Sketching Reference:** {% reference Greenberg:2012 %}_   |   |
|   |   |_**Sketching Reference:** "The Anatomy of Sketching" in {% reference Buxton:2007 %} [[PDF]](https://drive.google.com/open?id=1h33bo6bqfEpBWmezVYynKDk2IC8ByQ7e)_ ||
|--
|   |**W 17-Jan**|**Design as an Iterative Process**| **RR2**, **S1**|
|   |   |**Design Principles**: Priming + Recognition Over Recall   |   |
|   |   |"The Wheel: A Lifecycle Template" from {% reference Hartson:2012 %} [[PDF]](https://drive.google.com/open?id=1OD8wxc6tUwBRHvwhDnGxvuTFcuTS-pTr) Also available via [O'Reilly Safari](https://www.safaribooksonline.com/library/view/the-ux-book/9780123852410/)|   |
|   |   |{% reference Wilbanks:2017 %}   |   |
|   |   |**In-class**: Choose topic areas  |   |
|--
|   |_F 19-Jan_|_No class meeting. Assignment Due Date_ |_**Plagiarism Test** (5pm)_|
|--
|**3**|**M 22-Jan**|**User Research I: Observation & Contextual Inquiry**| **D1** |
|   |   |**Design Principles**: Uncertainty Principle (+ Hawthorne Effect) |   |
|   |   | {% reference Holtzblatt:2012 %} |   |
|   |   |"Field Visits: Learning from Observations" in {% reference Goodman:2012 %} [[PDF]](https://drive.google.com/open?id=12-JO03GCdN-3T9mlaW4PJy4T1DvBa7T_)|   |
|   |   |**_Project Reference:_** ["Contextual Inquiry"](https://www.wickedproblems.com/4_methods_for_research.php) and ["In Brief: Pocket Hotline, by Chap Ambrose & Scott Magee"](https://www.wickedproblems.com/4_insight_through_contextual_inquiry.php) in {% reference Kolko:2012 %}. ||
|   |   | **_Project Reference:_** Stanford D-School Understand Mixtape [[PDF]](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98) |   |
|   |   | **_Project Reference:_** SITRA Ethnography Field Guide [[PDF]](https://drive.google.com/open?id=1_i5dRT9iFCZsUyya8XoC1FYQ1lJBdLUt)  |   |
|--
|   |**W 24-Jan**|**User Research II: Interviews**| **S2**  |
|   |   |**Design Principles**: Law of Prägnaz  |   |
|   |   |"Interviewing an Informant" and "Asking Descriptive Questions" in {% reference Spradley:1979 %} [[PDF]](https://drive.google.com/open?id=1o6ByTmLN0FCWkBw9t4CA3iAhMLlGG8Tq) ||
|   |   | **_Project Reference:_** Stanford D-School Understand Mixtape [[PDF]](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98) |   |
|--
|**4**  |**M 29-Jan**|**HCI Topics: Values & Design, Accessibility, & Inclusion**| **RR3** |
|   |   |**Design Principles**:  Inattentional Blindness  |   |
|   |   |{% reference Friedman:1996 %}   |   |
|   |   |{% reference Schulman:2016 %} [[PDF]](https://drive.google.com/open?id=1I_wIhqt-6o36zVHpIaeD8PF8MbsbHmEC)   |   |
|   |   |{% reference Microsoft:2017 %}<br/> >> Download and read the [Manual (PDF)](https://download.microsoft.com/download/B/0/D/B0D4BF87-09CE-4417-8F28-D60703D672ED/INCLUSIVE_TOOLKIT_MANUAL_FINAL.pdf), and review the [Activity Cards (PDF)](https://download.microsoft.com/download/B/0/D/B0D4BF87-09CE-4417-8F28-D60703D672ED/Inclusive_Toolkit_Activities.pdf).  |   |
|   |   |**_Project Reference:_** {% reference Swan:2017 %}   |   |
|--
|   |**W 31-Jan**|**Making Sense of Data I: Managing Complexity**| **S3**, **D2**  |
|   |   |**Design Principles**:  Hierarchy |   |
|||Kolko, Jon. "Managing Complexity." *Thoughts on Interaction Design.* [[PDF]](https://drive.google.com/open?id=1VVPQM4C-07Gana9JPI45ZOz8c8gUmtcV) ||
|||**_Project Reference:_** Experience Mapping from Adaptive Path [[Website]](http://mappingexperiences.com/) and [[Guide (PDF)]](http://adaptivepath.s3.amazonaws.com/apguide/download/Adaptive_Paths_Guide_to_Experience_Mapping.pdf)||
|||**_Project Reference:_** See EMPATHY MAPS in the d.school [Understand Mixtape](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98) ||
|--
|**5**  |**M 5-Feb**|**Making Sense of Data II: Who and What**|   |
|   |   |**Design Principles**:  Not Invented Here  |   |
|   |   |{% reference Edeker:2013 %}|   |
|   |   |{% reference Pruitt:2003 %}  |   |
|   |   |**Project Reference:** SonicRim Personas Guide: <https://www.scribd.com/document/103060262/Personas-characters-from-inspirational-stories-of-real-people>   |   |
|   |   |_**Revisit the Persona Spectrum and Persona Network cards from the Microsoft Inclusive Design Activity Cards**_ |   |
|--
|   |**W 7-Feb**|**HCI Topics: Computing in Contemporary Life**| **RR4**, **S4**  |
|   |   |**Design Principles**: Aesthetic-Usability Effect   |   |
|   |   |{% reference Ames:2010 %}   |   |
|   |   |{% reference Sengers:2011 %}   |   |
|--
|**6**  |**M 12-Feb**|**Brainstorming and Ideation**| **D3**  |
|   |   |**Design Principles**: Signal to Noise Ratio   |   |
|   |   |{% reference Shedd:2013 %}   |   |
|||**_Project Reference:_** Stanford D-School Ideate Mixtape [[PDF]](https://drive.google.com/open?id=1alT56_KjggId8YA3r1DKPzgdo8Nqi_lr)||
|--
|   |**W 14-Feb**|**HCI Topics: Computing in the Workplace**| **RR5**, **S5**  |
|   |   |**Design Principles**:  Serial Position Effects  |   |
|   |   |{% reference Grudin:1990 %}|   |
|   |   |{% reference Suchman:1995 %}|   |
|   |   |**_Optional:_** {% reference Suchman:1983 %}|   |
|--
|**7**  |**M 19-Feb**|**Storyboards**|   |
|   |   |**Design Principles**: Mapping  |   |
|   |   |{% reference Truong:2006 %}   |   |
|   |   |See also: <http://adaptivepath.org/ideas/sketchboards-discover-better-faster-ux-solutions/>   |   |
|   |   |<https://uxmag.com/articles/storyboarding-in-the-software-design-process>   |   |
|--
|   |**W 21-Feb**|**HCI Topics: Human Factors**| **RR6**, **S6**  |
|   |   |**Design Principles**: Mental Model (review Fitt's Law) |   |
|   |   | {% reference Karafillis:2012 %}  |   |
|   |   | {% reference Kempton:1986 %}   |   |
|--
|**8**|**M 26-Feb**|**Wireframes & Prototypes**| **D4**  |
|   |   |**Design Principles**: Constancy + Orientation Sensitivity   |   |
|   |   | {% reference Cao:2017 %}  |   |
|   |   | Review "Low Fidelity Prototypes" activity card in the Microsoft Inclusive Design Toolkit |   |
|   |   | **_Project Tool:_** Prototyping on Paper (POP) <https://marvelapp.com/pop/?popref=1>  |   |
|--
|   |**W 28-Feb**|**HCI Topics: Affordances & Distributed Cognition**| **RR7**, **S7**  |
|   |   |**Design Principles**:  Constraint + Affordance  |   |
| | | Lecture including: affordances, physical widgets | |
|   |   | {% reference Norman:2008 %}   |   |
|   |   | {% reference Halverson:1994 %}  |   |
|   |   | **_Optional:_** {% reference Hollan:2000 %}   |   |
|--
|**9**  |**M 5-Mar**|**Evaluation & Reflection Techniques**|   |
|   |   |**Design Principles**:  Consistency  |   |
| | | Lecture including: heuristics, cognitive walkthrough, think aloud, diary study | |
|   |   | "Evaluation" in {% reference Baxter:2015 %} [PDF](https://drive.google.com/open?id=1BS5THs9jn5zkESFjZWWMd2RHQH7CHww4) |   |
|   |   | {% reference Sambasivan:2017 %}  |   |
|   |   | Review "Context and Capability" and "Situational Adaptation" activity cards in the Microsoft Inclusive Design Toolkit  |   |
|--
|   |**W 7-Mar**|**HCI Topics: Dark Patterns**| **RR8**, **S8**  |
|   |   |**Design Principles**:  Nudge + Proximity |   |
|   |   |{% reference Brownlee:2016 %}|   |
|   |   |{% reference Campbell-Dollaghan:2016 %}   |   |
|--
|**10** |**M 12-Mar**|**HCI Topics: Designing for Change** |**RR9**, **D5**   |
|   |   |**Design Principles**: Forgiveness + Desire Line |   |
|   |   |{% reference Asad:2015 %}   |   |
|   |   |{% reference Martin:2016 %}   |   |
|--
|   |**W 14-Mar**|**HCI Topics: Limits of Design and Interaction** | **RR10**, **SF**  |
|   |   |**Design Principles**: Feedback Loop (+ Jevon's Paradox)   |   |
|   |   | {% reference Baumer:2011 %}  |   |
|   |   | {% reference Taylor:2015 %}  |   |
|--
|**F**  | **19-Mar** (12:30-2:20pm)  | **Final Case Study Presentations** | **D6**, **DF**  |
|||_There is no written final exam, but attendance is mandatory at this final session, during which you will present your projects to your peers._||

## Notes
All assignments are due at the START of class on the day listed, unless
otherwise noted.

- Assignments will **_not be accepted late_**.

As a general pattern, Mondays will serve as a practicum where we will discuss the practical aspects of carrying out that week's assignment. By contrast, Wednesday lectures will focus on exposing you to current topics and open questions in HCI research and practice.

All readings are required unless noted otherwise. Other notations include:

- "Optional" -- readings will be discussed in class, but you are not required to have read them in advance of the meeting. I recommend skimming these pieces before class, and you may want to refer back to them later if you have questions after the class discussion.
- "Reference" -- reference material (often from a textbook) that may be useful for reviewing to better understand class concepts, or returning to at a later date if you are working on a project in a future class or work context. These may be referred to in class, but will not necessarily be discussed in detail.
    - Some reference materials are noted specifically as "Project Reference" or "Sketching Reference." These selections will be minimally discussed in class, but may be useful to refer to in completing your project and/or sketching assignments.
- "Design Principle" -- Each day, we will begin class by reviewing one or two design principles from the following book. You should consider purchasing this book if you would like to have your own reference to these materials.
    - {% reference Lidwell:2010 %}
