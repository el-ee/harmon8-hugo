---
title: Computing + Society
cc: true
type: "class"
layout: "subpage"
icon: "fa-book"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "cs199-201902-computing-society"

quarter: 201902
---

We will have three computing and society discussions as part of this course. These activities are designed to (a) give us all a little break time within each programming module for our brains to process what we're learning without trying to cram more knowledge in too fast (b) help you think about the bigger picture context of computer science -- how does computing interact with society? 

Each discussion will center on one shared reading, which everyone in the class will complete. In order to prepare you for discussion, you will write a brief [reading response](#reading-responses), due at the start of class, on paper.

You will sign up to be a [discussion leader](#discussion-leader) for _one_ of the discussions in class, during week 2. 

### Topics

#### C+S Discussion 1: Data and Metrics

**Discussion date:** Week 4, meeting 2.

**Reading:** Levy, Stephen. 2014. “A Spreadsheet Way of Knowledge.” _WIRED | Backchannel_, October 2014. <https://wired.com/2014/10/a-spreadsheet-way-of-knowledge/>


#### C+S Discussion 2: Automation

**Discussion date:** Week 7, meeting 2.

**Reading:** Eubanks, Virginia. 2018. "Automating Eligibility in the Heartland," Chapter 2 from _Automating Inequality_ [PDF](https://drive.google.com/open?id=1IBvtcfqS71mtKy4O-34I08z9dmtONglt) _You must be logged in to your PDX google account to access this file._


#### C+S Discussion 3: Quitting/Refusing Technology

**Discussion date:** Week 10, meeting 2.

**Reading:** Portwood-Stacer, Laura. "Care Work and the Stakes of Social Media Refusal." _New Criticals_, March 18, 2014. <http://www.newcriticals.com/care-work-and-the-stakes-of-social-media-refusal/print>



### Reading Responses

For _each_ reading, you will write a brief reading response to help prepare you for discussion. Your response should be about 300-500 words long. Do not exceed 750 words.

You should address each of the following prompts:

1. **Summarize the author's main point.** For example, you might answer any of these questions to do this: 
    - What is the central problem or question in the reading/video/podcast? What answer(s) or proposal(s) are given in response? 
    - What is the argument that the author is trying to make? What evidence is presented in support of this argument?
    - What is the socio-technical situation described in the article? Who benefits? Who is harmed? How & why?
    - _Note: Your summary should not be a list of facts from the article, but should be a higher-level overview of the **main point**._
2. **Choose one quote that stood out to you as interesting, insightful, or provocative**:
    1. Copy the quote into your document and appropriately cite it. 
    2. Write a brief (2-5 sentence) reflection on why you chose this quote, what makes it interesting, and/or how it relates to a previous class discussion.
3. **Communicate your personal opinion or takeaways** by answering two or more of the following questions:
    - What is your opinion on the subject? Do you agree or disagree with the argument presented? Why?
    - What did you learn from reading the piece? And/or what new questions do you have? What was unclear or confusing? Some of the readings are challenging and you are not expected to understand every single thing in each reading. Asking questions is not only okay, it is good!
    - How does the reading relate to your own life? How does it relate to computer science and programming? 
    - What should programmers know about the topic we're discussing this week? Is there anything from the reading that they should be thinking about when they're creating new software? 
    
    
#### General Formatting Guidelines

- Use standard US Letter paper.
- Put your name on the first page.
- Put the word count on the first page.
- Use standard and professional fonts, margins, etc, such as:
    - One inch margins on all sides.
    - Size 11-12 font
    - Line height of 1.3-1.6
    - The height of one full line between paragraphs
    - An easy to read font (Like, no Comic Sans or script fonts, yes?)
- Here is a [sample google doc](https://docs.google.com/document/d/1SrHqs81xuBJcrlgxYMXYFBBmDVE29Nd3XQujNlKxM1s/edit?usp=sharing) that is very boring and generic but easy to read.

#### Grading
Each reading response is worth 3 points. They will be graded very simply: 

| Mark | Points | Criteria Description |
|:--|:--|:--|
| ✔+ | 3 | Exceeds expectations; contains insightful and original ideas or questions that draw on your own unique experience and expertise in order to move beyond simply re-stating the content of the readings themselves; could be used as an example of an excellent reading response in future classes.|
| ✔ | 2.7 | It is clear that you completed the reading, and your commentary responds to **all three** prompts above; but your commentary does not go beyond a summary of the author's own point, or otherwise show your own substantive engagement with the topic.|
| ✔- | 2.2 | Your response is incomplete, either in that it only responds to some of the prompts above or it is so short that your answers do not fully address the prompts; or, it is questionable as to whether you actually completed the reading assignment: your commentary may be good, but there is no evidence (such as a quote or summary of a key takeaway) that shows that you read anything more than the title or first few paragraphs, or your response shows significant gaps in your understanding of the material.|

_You should expect to receive a 0 for any reading response that contains plagiarized content or is turned in late._


### Discussion Leader

When it's your turn to be a discussion leader, you will have a few additional responsibilities:
1. Facilitate a discussion with a small group of your peers in class.
2. Bring in a recent news article, related to the week's reading, to share.
3. Take notes in your group, and report back to the rest of the class about your discussion.

Each discussion group will have 2-3 leaders, and 4-5 other participants. So, you will have some help leading the discussion and taking notes. But, EACH discussion leader needs to bring in their own news article.

In-class discussion facilitation tasks:
- Introductions. Make sure everyone knows each other's names.    
- Circulate the reading responses that have been brought to class.
- After everyone has had a chance to read **two** of them (not all of them!), start the discussion by bringing up something that you thought was interesting in one of your peers' responses. 
    - Invite other people to share their ideas.
    - At a good time in the discussion, share the news article that you found that relates to the week's reading. Give everyone a brief summary of the article, since others likely have _not_ read it, and then discuss together how and why it relates to the main text (or not!).
- Split your team up to make 2 PSA/Memes based on your group's discussion (we will talk more about this in Week 4):
    - PSA flyer/Meme 1: What is the most important thing that the general public should know about this situation? 
    - PSA flyer/Meme 2: What is the most important thing that computer scientists should know about this situation?
    
In-class note-taking tasks:
- The discussion leads are jointly responsible for 3 documents that your group will create together in class (I will bring paper, or you can work in Google Docs): 
    - Each of the 2 memes
    - Brief discussion report: 
        - Names of all your group members.
        - Collect a few (1-3) interesting questions or comments that came up in your discussion that your group wants to share with the rest of the class.
        - What is the main point of the article that everyone read? Does everyone agree? If not, can you come to a consensus?
        - List of all news articles brought in by the discussion leads, with a _brief_ 1-3 sentence summary of each article.
    
#### Grading

Your discussion leader duties are worth an additional 3 points. You will be graded very simply: 

| Mark | Points | Criteria Description |
|:--|:--|:--|
| ✔+ | 3 | Good job trying to facilitate discussion, encouraged everyone to participate in the conversation; did a great job representing your group to the rest of the class and reporting out on what you talked about. You brought in a news article to share. |
| ✔- | 2.3 | You did not do a good job facilitating discussion. For example, maybe you talked the whole time without letting others get a chance to speak or you did not try to encourage others to speak; or you did not talk at all, instead just sitting there silently waiting on someone else to start the conversation; or, you did not take good notes and I can't tell from them what your group talked about; or, you did not do a good job reporting back to the rest of the class about what your group discussed. You did not bring in a news article to share. |

