---
title: Schedule
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-calendar-alt"
weight: 500

menu:
    main:
        parent: "Teaching"
        identifier: "cs199-201902-schedule"

quarter: 201902
---


| Week | T | R | S |
|:---|:---|:---|:---|
| 1 | Microbits I | Microbits II | P1 |
| 2 | JS: Intro | JS: Conditionals + Random | - |
| 3 | JS: Variables | JS: Functions | - |
| 4 | Demos  | C&S 1 | P2 |
| 5 | Py: Variables, Input  | Py: Conditionals  | - |
| 6 | Py: Functions  | Py: Loops  | - |
| 7 | Demos  | C&S 2 | P3 |
| 8 | Py: Strings  | Py: Lists  | - |
| 9 | Py: Files  | Demos + Review  | - |
| 10| Final Exam Review | C&S 3 + Closing Surveys | P4 |
| **F** | **Final Exam<br/>Thursday June 13, 10:15a-12:05p** | | |

**Finals Week:** There will be a final exam in this class. The time is [scheduled by the registrar](https://www.pdx.edu/registration/final-exams). Make plans now to attend at the proper time.


### Unit 1: Microbits

#### Wk 1, Meeting 1

Course Introduction, Microbit Introduction

- In Class:
    - [Self-Asessment Survey](http://tinyurl.com/199-intro-survey/)
    - Read. The. Course. [Policies](../policies/).
    - Microbit: Rock Paper Scissors Game
- TODO: 
    - Visit my office hours on sometime in weeks 1-3 _or_ [Schedule a 1-on-1 with me](/~harmon8/faq/#meeting-with-me) for a 15 minute block, sometime **in the first three weeks**.

#### Wk 1, Meeting 2

Microbits II

- In Class:
    - Review Rock Paper Scissors game
    - Discuss [P1 (Microbit)](../projects/#p1-microbit) in detail
    - Discuss computing + society topics + sign up 
    - Simple Dot Game: Functions

#### Deadline: Sunday, 11:59pm (April 7)
- Due:
    - [P1 (Microbit)](../programming/#p1-microbit)


### Unit 2: Graphics with Javascript

#### Wk 2, Meeting 1

Introduction to JavaScript and text-based programming, using <http://repl.it>, using the processing library, drawing shapes, coordinate grids.

- Before class:
    - Create an account on <http://repl.it>
- _New Assignments:_
    - [Project 2](../projects/#p2-javascript), Due Sunday, Week 4, 11:59pm


#### Wk 2, Meeting 2
Drawing with processing, using the documentation. Conditionals & Random Numbers.

- Before Class:
    - [Learning on Khan Academy](https://www.khanacademy.org/computing/computer-programming/programming/intro-to-programming/a/learning-programming-on-khan-academy)
    - [KA: Drawing Basics (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/drawing-basics/pt/making-drawings-with-code)
    - [KA: Coloring (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/coloring/pt/coloring-with-code)

#### Wk 3, Meeting 1
Variables

- Before Class:
    - [KA: Variables (all 6 sections)](https://www.khanacademy.org/computing/computer-programming/programming/variables/pt/intro-to-variables)

#### Wk 3, Meeting 2
Functions, functions with parameters.

- Before Class:
    - [KA: Functions (**only the first 4 parts**)](https://www.khanacademy.org/computing/computer-programming/programming/functions/pt/functions)
- New Assignment:
    - [Computing & Society Reading 1](../computing-society)
        - **Discussion date:** Week 4, meeting 2.
        - **Reading:** Levy, Stephen. 2014. “A Spreadsheet Way of Knowledge.” _WIRED | Backchannel_, October 2014. <https://wired.com/2014/10/a-spreadsheet-way-of-knowledge/>

#### Wk 4, Meeting 1

Demos for Project 2

- New Assignment:
    - **Take Home Midterm**, due Week 5, Meeting 1, start of class

#### Wk 4, Meeting 2

Computing and Society Conversation #1

- Due: 
    - **Reading Response 1**, start of class, on paper

#### Deadline: Sunday, 11:59pm (April 28)
- Due:
    - [P2 (JavaScript)](../programming/#p2-javascript)
 
    

### Unit 3: Python I

#### Wk 5, Meeting 1
Input, output, variables, and python style (resource: [PEP-8: Python Style Guide](https://www.python.org/dev/peps/pep-0008/))

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 2
    - In addition to a PDF, ePUB and Kindle version, these other options are also available:
        - If you want to try out code as you learn, use the [Interactive Version of the Textbook](https://books.trinket.io/pfe/index.html)
        - If you learn better by listening than by reading, try the [Video and Audio Lectures](https://www.py4e.com/materials)
- Due:
    - Take Home Midterm, start of class.
- _Assigned:_
    - [Project 3](../programming/#p3-python-i), Sunday, end of Week 7, 11:59p


#### Wk 5, Meeting 2
Conditionals, random numbers

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 3



#### Wk 6, Meeting 1
Functions

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 4


#### Wk 6, Meeting 2
Loops

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 5
- New Assignments:
    - [Computing & Society Reading 2](../computing-society)
        - **Discussion date:** Week 7, meeting 2.
        - **Reading:** Eubanks, Virginia. 2018. "Automating Eligibility in the Heartland," Chapter 2 from _Automating Inequality_ 

#### Wk 7, Meeting 1
Demos - Project 3

#### Wk 7, Meeting 2
Computing and Society Conversation #2

- Due:
    - Reading Response 2, on paper, start of class



#### Deadline: Sunday, 11:59pm (May 19)
- Due:
    - [P3 (Python I)](../programming/#p3-python-i)
 

### Unit 4: Python II

#### Week 8, Meeting 1
Strings

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 6
- _New Assignments:_
    - [Project 4](../projects/#p4-python-ii), Due Sunday, end of Week 10, 11:59pm


#### Week 8, Meeting 2
Lists!

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 7



#### Week 9, Meeting 1
Files, CSV format, simple data processing

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 8

        
#### Week 9, Meeting 2
Demos - Project 4

- New Assignment:
    - [Computing & Society Reading 3](../computing-society)
        - **Discussion date:** Week 10, meeting 2.
        - **Reading:** Portwood-Stacer, Laura. "Care Work and the Stakes of Social Media Refusal." _New Criticals_, March 18, 2014. <http://www.newcriticals.com/care-work-and-the-stakes-of-social-media-refusal/print>

#### Week 10, Meeting 1

Final Exam Review
    
#### Week 10, Meeting 2
Computing and Society Conversation 3 + End of term surveys & closing self-assessment

- Due:
    - Reading Response 3, start of class, on paper
    

#### Deadline: Sunday, 11:59pm (June 9)
- Due:
    - [P4 (Python II)](../programming/#p4-python-ii)

#### Final Exam

Thursday, June 13, 10:15a-12:05p

