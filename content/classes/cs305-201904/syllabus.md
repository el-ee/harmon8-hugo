---
title: Syllabus
credits: 
cc: false
type: "class"
layout: "subpage"
weight: 200
icon: "fa-info-circle"

menu: 
  main:
    parent: "Teaching"
    identifier: "cs305-201904-syllabus"

quarter: 201904

---

| | |
|--:|:-----|
| **Course** | CS 305 - Social, Ethical, and Legal Implications of Computing |
| **Meetings**| Mondays, 4:40 - 6:30, Ondine 220 |
| **Final Exam**| [TBA](https://www.pdx.edu/registration/final-exams)\* |
| **Instructor** |Dr. Ellie Harmon <br/> <ellie.harmon@pdx.edu> <br/> she / her / hers |
| **Office Hours** | M+W 2:00p - 3:00p, [Starbucks @ SW 6th & Jackson](https://goo.gl/maps/sy3iVCbZYHDycYsS7) <br/> or [by appointment](/~harmon8/faq/#meetings-with-me)|
| **Prerequisites** | None|
| **Website** | <https://web.cecs.pdx.edu/~harmon8/classes/cs305/> |
| **Google Classroom** | <https://classroom.google.com> code: x1un1u3 |
<!-- TODO: G classroom code -->

\* _Please make note of the unusual final exam time now and make plans to attend. The time is [scheduled by the registrar](https://www.pdx.edu/registration/final-exams) and I have no control over it. Although there is no final exam , we will meet during the scheduled time per [university policy](https://www.pdx.edu/registration/final-exams)._


### Hello! And Welcome.

I'm looking forward to our course this term, and I hope you are as well. All major course policies are outlined in the document that follows. Please note that all materials on the course website -- including this policy overview as well as the course schedule -- are designed to be a starting point for the course. They are subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced via email, so attend class and check your inbox. 

A series of questions are embedded throughout this document, these are used in the survey version of the syllabus. If you are taking the survey version of the syllabus, please note that you do not need to write anything down or take notes, this entire document is also available to you on the course website and is linked from the Google Classroom page.

This syllabus is adapted from the [interactive syllabus](https://interactivesyllabus.com) developed by Dr. Guy McHendry and Dr. Kathy Gonzales, both of Creighton University, and adapted by Dr. Lindsey Passenger Wieck (St. Mary's University) and Dr. Angela C. Jenks (UC Irvine). It is licensed under Creative Commons Attribution - Non-Commercial 4.0. Further acknowledgements about the content of specific sections are noted throughout the document. 


### About Dr. Harmon

My name is Dr. Ellie Harmon, and I will be your instructor this quarter. I am a Senior Instructor in the department of Computer Science, and the cluster coordinator for Freedom, Privacy, and Technology. I have a background in both computer science and social analysis of computerization, so this course is right in my wheelhouse, and I'm very exited to be teaching it this quarter. I use she/her pronouns and I thru-hiked the PCT in 2013.

Q1) What else would you like to know about me?

Q2) Please give me a brief introduction to yourself: name, pronouns, why you're here, and something interesting about you.

### Course Goals

As per the course catalog, this course covers: 

> History of computing, social context of computing, professional and ethical 
responsibilities, risks and liabilities of safety-critical systems, 
intellectual property, privacy and civil liberties, social implications of the 
Internet, computer crime, economic issues in computing.

> The course has two goals: First, the usual goal of learning the material of the course as described in the catalog entry. A higher priority goal is to instill in you an inclination to use that knowledge. The use of the content of this course is optional in the real world and our primary goal is to motivate its use. [^1]

[^1]: The quoted text is from: <https://www.pdx.edu/computer-science/cs305>

With only ten meetings, we have a limited amount of time to cover these multiple and complex issues. But, we will do our best!

Q3) What do you think about the above descriptions of this course? How do they make you feel coming into the course? Are you excited? Bored? Anxious? Annoyed?

Q4) Do you think that computer science students should have to take a course like this as part of their degree? Briefly explain why or why not.

Q5) Given that you are here, what are some things that _you_ are hoping to get out of this class? List 2-3 specific things that you'd like to learn or accomplish this quarter. 


### Specific Learning Outcomes

Upon the successful completion of this course you will be able to[^2]:

1. Identify the ethical issues that relate to computer science in real situations they may encounter.
2. Decide whether a given action is ethical as regards computer science professional ethics, and justify that decision.
3. Look up relevant ethical standards as developed by the ACM.
4. Prepare and deliver a short (8-10 minute) professional-quality talk on a topic relating to ethical, legal, and social implications of computer science.
5. Research and write a professional-quality paper about a topic relating to social, legal, and ethical implications of computer science.
6. Recognize situations in which there may be legal issues as regards computer science and related topics such as intellectual property, and know some legal principles to apply.
7. State several important impacts of computer science and related fields on contemporary society.
8. State several examples of important ethical principles as they apply to computer science related situations.

[^2]: This section contains lightly edited text from: <https://www.pdx.edu/computer-science/cs305>


Q6) To see where you're at with regards to these outcomes coming into the course, how confident are you in your ability to do each of these things _now_:

1. Identify the ethical issues that relate to computer science in real situations they may encounter.
2. Decide whether a given action is ethical as regards computer science professional ethics, and justify that decision.
3. Look up relevant ethical standards as developed by the ACM.
4. Prepare and deliver a short (8-10 minute) professional-quality talk on a topic relating to ethical, legal, and social implications of computer science.
5. Research and write a professional-quality paper about a topic relating to social, legal, and ethical implications of computer science.
6. Recognize situations in which there may be legal issues as regards computer science and related topics such as intellectual property, and know some legal principles to apply.
7. State several important impacts of computer science and related fields on contemporary society.
8. State several examples of important ethical principles as they apply to computer science related situations.

Multiple choice: 
- I cannot do this at all. 
- I can sorta do this, but not very well / not very confidently.
- I can do this pretty well, but there's always room for improvement!
- I'm really good at this, and don't feel like it's something I need to work on.

### Course Materials

#### Textbooks

There is no textbook for this course.

#### Supplementary Readings / Videos

Readings will be linked from the course schedule. Full bibliographic detail is provided for all materials, and you should attempt to locate them on your own in the event of a broken link. _**The failure of any schedule link is not an excuse to skip the assigned reading.**_

Please note that some links may require that you are on the campus network, or connected through a VPN in order to access the content for free. **You will _never_ have to pay for any readings in this class.** If you are having trouble accessing one of the readings, please visit my office hours, ask a question in class, or visit the library and ask them how to gain access to the material.

Q7) Have you ever successfully accessed materials requiring login through the library website from off campus? 
- Yes.
- No, but I will try it out this week, and ask any questions I have at our next class meeting.

#### Laptops & Phones & Tablets, Oh My!

This class requires your attention and participation in course discussions and 
activities. I understand that laptops, etc. can be useful for note taking and 
looking up information relevant to course discussions; and we will often conduct in-class projects that will be easier with a laptop. 

However, I find that they can also be quite the distraction. We all have other 
things -- families, friends, work, classes, YouTube videos, games, etc. -- that 
we might feel compelled to check in on during class.

Therefore, laptops will be allowed in class on a situational and tentative basis. If your digital technology becoms a problem for you or others around you, I may ask you to stop using it either for the rest of a class period, or if it becomes a recurring issue, for the rest of the term.

When we are doing in-class activities that will be easier to complete with a laptop, I can bring in a set of Chromebooks for use by those who do not have a laptop. So that I can be prepared for class: 

Q8) Do you have a laptop or tablet that you will bring with you to class? 
- Yes, I will bring my own. 
- No, I will need to borrow one.

_Note: If you are in the market for a low-cost laptop, Free Geek (https://www.freegeek.org/shop/free-geek-store#computers-parts-accessories) offers good prices on used/refurbished laptops out of their store in SE Portland. You can also earn a free laptop (https://www.freegeek.org/faqs#volunteering) by volunteering for them._

### Major Assignments & Grades

This class involves both individual and group assignments. Below is a brief sketch of the assignments we will complete in this class. More detailed assignment guidelines will be provided later in the quarter.

| Assignment | Quantity | Points Each | Total |
|:---|---:|---:|---:|
| Class Participation | 10 | 2 | 20 |
| Daily Writing | 6 | 3 | 18 |
| Syllabus Survey | 1 | 5 | 5 |
| CA0/1: Topic Pitch / Team Formation | 2 | 2.5 | 5 |
| CA2: Bibliography | 1 | 10 | 10 |
| CA3/5: Presentations | 2 | 5 | 10 |
| CA4: Analysis | 1 | 10 | 10 |
| CA6: Final Proposal | 1 | 15 | 15 |
| Presentation Feedback | 3 | 2 | 6 |
| Closing Self-Assessment | 1 | 5 | 5 |
| **Total** | | | **104** | 

**Attendance & Participation** This class is taught in an interactive format that includes discussion and in-class work. At each meeting, you will be asked to evaluate your own participation in that day's activities on a small exit slip. Please make sure you turn this in before leaving the classroom! _(Note: It should go without saying, but filling out a participation worksheet for another student (or lying about your own participation) is considered a form of academic misconduct.)_

**Daily Writing** Each class period (except presentation days) will begin with a 15 minute period of reflective writing, in which you will (a) ask 2-3 questions about the reading and (b) respond to a prompt given at the start of class. This assignment is designed to help you settle in, focus, and prepare for discussion as well as to demonstrate that you completed the assigned reading before class. You may prepare the questions in advance, and while writing your response, you may refer to the assigned reading plus any notes _that you created yourself_. You may _not_ refer to anyone else's notes. If you are late to class, you may miss the opportunity to complete this exercise. 

These will be graded on a simple ✔+ / ✔ / ✔- scale: 

| Mark | Grade | Description |
|---:|--:|:---|
| **✔+** | 3 | **Exceeds Expectations**: it is clear that you fully understood the reading; response contains insightful and original ideas or questions that bring your own expertise and experience to bear on the topic; response could be used as an example of an excellent reading response in future classes. |
|**✔** | 2.6 | **Satisfactory**: Response is complete; it is clear that you actually did the reading and understood most of it; your response may not show your own substantive engagement with the topic; overall fine, but not example worthy |
|**✔-** | 2.1 | **Unsatisfactory**: Your response is incomplete or lacks concrete evidence that you read beyond the title or first few paragraphs; perhaps it only responds to some of the prompts, or is too short to fully address the prompt(s), or shows significant gaps in your understanding of the material. |


**Syllabus Survey** The survey that is part of this syllabus counts for 5 points towards your final grade. It will be graded very simply for effort and completeness.

**Case Analysis** This group project will involve the preparation of a case analysis that examines the ethical dilemmas surrounding some aspect of contemporary computing. This assignment will be broken down into 7 milestones spread throughout the quarter, including two team presentations. [details](../project/)

**Presentation Feedback** On each presentation day, you will take notes and provide feedback to your classmates on their presentations.

**Closing Self-Assessment** At the end of the term, you will complete an individual self-assessment survey in which you reflect on your progress in the course. 

**Exams** There are no exams in this course.

Q9) Which assignment are you most excited about? Why? 

Q10) Which assignment worries you the most? Why?


#### Workload

One credit hour is defined by federal regulations as "One hour of classroom or direct faculty instruction and a minimum of two hours of out‐of‐class student work each week"[^credit-hour]. This is a two-hour course, which means that you should spend approximately four (4) hours on out-of-class work each week. This includes both reading assignments in preparation for class, as well as research, writing, and/or programming assignments as applicable. 

[^credit-hour]: See the Credit Hour Policy of the Northwest Commission on Colleges and Universities (this is the organization which accredits PSU) <https://www.nwccu.org/wp-content/uploads/2017/05/Credit-Hour-Policy.pdf> 

Q11) In addition to the course meetings, what blocks of time have you set aside to work on assignments for this course?

| | Morning | Afternoon | Evening |
|---|---|---|---|
| S | | | |
| M | | | |
| T | | | |
| W | | | |
| R | | | |
| F | | | |


#### Grading

Show up to class, participate in discussion, turn in all assignments,
and you will do great in this class. Skip class and assignments and you
will do poorly. 

Note in the table above that there are 104 points that you can earn in this class, though final grades will be calculated out of 100. You can use these extra points as you see fit, for example, to cover an absence from class. 

Due to the fact that there are several extra credit points built in, there will be no excused absences from class or makeup work, barring [exceptional circumstances](#exceptional-circumstances).

Letter grades will be assigned based on the following standard conversions:

| Total Points Earned | Letter Grade |
|--------------|--------------|
| 93-100       | A            |
| 90-92        | A-           |
| 87-89        | B+           |
| 83-86        | B            |
| 80-82        | B-           |
| 77-79        | C+           |
| 73-76        | C            |
| 70-72        | C-           |
| 67-69        | D+           |
| 63-66        | D            |
| 60-62        | D-           |
| Below 60     | F            |

If you earn the minimum points for a letter grade, you are guaranteed that letter grade. There will be **no** downward curve in this class. 

### Attendance & Deadlines

#### Attendance & Participation 

Reflecting on building a classroom where participation is a fundamental part of the learning experience, Emilie Pine explains:
 
 > I have tried to realise some of [my] ambitions by making my classroom a safe (and equal) space in which all of my students can take risks. Sometimes it seems that the biggest risk they can imagine is to say something out loud. I know that they are afraid of saying the wrong thing and being laughed at. But I want them to speak despite this fear. Because I worry that if students are quiet about their ideas in class perhaps they will be quiet about other things too. Things they should not be quiet about. If they cannot talk in class, how will they speak out if they get harassed, or discriminated against, or hurt? [^pine]

[^pine]: Emilie Pine. 2018. _Notes to Self_. Tramp Press.
 
Participation in this class functions as an invitation to think and take risks with one another as we build the community of the classroom. 

Q12) Why do you think I care so much about participation? 

Q13) On a scale of 1 to 5 (1 being never, 5 being frequently), how often do you participate in classes?

Q14) Please describe your attitude towards class participation. What helps and encourages you to participate? What prevents you from participating?

Q15) What are some ways you might participate in class, besides initiating a comment or question during whole-group discussions? 

#### Exceptional Circumstances 

**_Please contact me_** in the case of any exceptional or unpredictable event
that significantly impacts your ability to complete work or attend class
— such as an illness, a sick child that cannot attend school or daycare,
a family emergency, iced over roads, etc. I reserve the right to request 
documentation before granting any extensions, but they are occasionally 
possible given extenuating circumstances beyond your control. 

Please note that *traffic, TriMet delays, bridge lifts, regular work
schedules, and personal travel are not considered exceptional or
unpredictable events*. Please warn your family and friends in advance (i.e.
today) that they need to consult you regarding any future travel plans
made on your behalf (e.g., weddings, bachelorette parties, cruises,
etc.). If you choose to prioritize one of these events over your class time, 
that is perfectly fine. You are an adult and can make your own decisions. 
However, you understand that you forfeit any in-class assignments we complete 
during your departure.

#### Missing Class

When you are absent for any reason, _you (not me!) are
responsible for:_
-  Proposing a plan to catch up on any missed course material
-  Proposing a plan for any missed assignments

#### Assignment Deadlines

Since many assignments build on each-other (and often we will use
homework as material for in-class discussions or activities), as a general 
rule, **_late work will not be accepted without prior approval_**. 
You will always receive partial credit for work that is partially complete. 
Please turn in whatever you have finished at the deadline. 


Q16) Any questions about attendance, deadlines, or what might or might not constitute an exceptional circumstance?


### Communication

#### Meeting with me

My office hours will be held on Mondays and Wednesdays from 2:00p - 3:00p at the [Starbucks on the corner of SW 6th & Jackson](https://goo.gl/maps/sy3iVCbZYHDycYsS7). This is my preferred meeting time for students. You do not need an appointment, you can simply drop in. Please do visit my office hours! I enjoy working with students!

Q17) Does this time and location work for you, in general? 
- Yes
- No

If you need to meet with me, but cannot make my office hours or would like a more private meeting in my office, you can make an appointment with me via the PSU Google Calendar system ([directions](/~harmon8/faq/#meeting-with-me)).

Q18) Do you know how to view another person's schedule on Google Calendar in order to find a suitable meeting time, and create a meeting invitation? **Note: meetings scheduled outside of my drop-in office hours will take place in my office, #120-15, in the Fourth Avenue Building.**
- Yes
- No, but I will read the directions here, and ask any questions I have during the first week of class: <https://web.cecs.pdx.edu/~harmon8/faq/#meeting-with-me>

#### Google Classroom 

As you may have noticed there is no D2L shell for this course. We will be trying out Google Classroom instead, with the hope that it will be a much easier place to turn in assignments, ask questions about course content, etc. I will invite you all to the Google Classroom space on Monday morning. As this is a bit of an experiment, I would appreciate any feedback you have about the Google Classroom environment throughout the term. 

As this is a new environment for most of us, we will have to work together through the year to figure out best practices for using this new tool. However, I am hopeful that this environment will be easier to navigate and use than D2L!

Please try to ask course-related questions on the Google Classroom page before emailing me. This way everyone can benefit from the answer -- and your peers may be faster at answering than myself :)

**Important class announcements** will be posted to the Google Classroom (and you should receive email notifications about them unless you have elected to turn notifications off). Please make sure to check the Classroom site and/or your PSU email at least once a day.

Q19) How do you feel about using Google Classroom?
- I'm excited, I hate D2L.
- Sounds fine, I'm good with it.
- I am apprehensive about this.
- I don't like this. 
- I don't really care.
- Other: 

#### Email

If you need to email me directly, *you MUST include the
course number as the first word in the subject*, for example 'CS305:
Missing class due to illness, suggested plan for makeup work.'

#### Responsiveness

I will aim to answer all inquiries within **1 business day**. If you do not 
receive a reply from me within 1 day, please re-send your message. 

I do not (cannot) respond to inquiries 24/7. *Do not count
on responses from me after 6pm, before 9am, or on the weekends.*

Likewise, I will never expect a reply from you sooner than 1 business day.

### Code of Conduct

The computer science community is well-known for being an unwelcoming and toxic 
environment to many newcomers [^1]. Research shows that members of 
underrepresented groups (e.g., women, people of color, first generation college 
students) leave computer science programs and the tech industry at higher 
rates, and that this attrition is a result of environmental conditions[^4]. 
Many open source projects, professional societies, and businesses have 
recognized that the lack of diversity amongst contributors is a problem since 
they miss out on ideas, perspectives, and contributions from underrepresented 
groups[^2]. Moreover, the history and prevalence of exclusionary practices and 
cultures is an ethical problem that limits the intellectual, personal, and 
financial opportunities of members of underrepresented groups[^3]. 

To address this, many organizations and events have established community 
guidelines and codes of conduct to support communities that are more welcoming 
to new and diverse contributors. For example:

- **Contributor Covenant:** a code of conduct shared by many open source
projects, including Atom, Eclipse, Mono, Rails, Swift, and many more.
[contributor-covenant.org](http://contributor-covenant.org)
- **Mozilla Community Participation Guidelines:** community guidelines for the
makers of Firefox 
[mozilla.org/en-US/about/governance/policies/participation](http://mozilla.org/en-US/about/governance/policies/participation/)
- **PyCon Code of Conduct:** code of conduct for the major US conference for
the Python programming language 
[us.pycon.org/2018/about/code-of-conduct](https://us.pycon.org/2018/about/code-of-conduct/)
- **Ubuntu Code of Conduct:** code of conduct for the open source community
that produces the free Ubuntu operating system 
[ubuntu.com/about/about-ubuntu/conduct](http://ubuntu.com/about/about-ubuntu/conduct)

In this course, we will also have a code of conduct.

_**We will create this code of conduct together at our first class meeting. After that meeting, it will be updated and posted here.**_

Link to [code of conduct document](https://docs.google.com/document/d/1ZP-mYO0ss-zSnY0h24V7X4FzbZFDYd2_TbidJqE6qKI/edit?usp=sharing)

#### What to Do About Harassment

If you are a victim of harassment of any kind in this class, there are
several resources available to you:

-  You may [schedule a private meeting](../../../faq#meetings-with-me) to talk to me. _Be aware: I have [Title IX reporting obligations](#title-ix-reporting-obligations)._

-  Fill out the PSU [Bias Incident Report
  Form](https://goo.gl/forms/PMrV0tUbhDWqcBAy1)

-  File a [formal 
  complaint](https://www.pdx.edu/diversity/file-a-complaint-of-discriminationharassment)
  
-  Contact the Office of Global Diversity and Inclusion: Market Center
  Building, 1600 SW 4th Avenue, Suite 830

-  Contact the Dean of Student Life: <askdos@pdx.edu>



### Academic Integrity

_Note: Much of this section of the syllabus is copied (with permission) from the policy of [Nick Seaver](http://nickseaver.net); notable changes include the PSU student code of conduct, and commentary specifically relevant to computer science, such as computer programming details._

Our expressions are not our own. Humans communicate with words and
concepts — and within cultures and arguments — that are not of our own
making. Writing, like other forms of communication, is a matter of
combining existing materials in communicative ways. Different groups of
people have different norms that govern these combinations: modernist
poets and collagists, mashup artists and programmers, blues musicians
and attorneys, documentarians and physicists all abide by different sets
of rules about what counts as “originality,” what kinds of copying are
acceptable, and how one should relate to the materials from which one
draws.

In this course, you will continue to learn the norms of citation and
attribution shared by the community of scholars at Portland State
University and at other higher education institutes in the United
States. Failure to abide by these norms is considered plagiarism, as
laid out in the Student Code of Conduct[^6] with which you should
familiarize yourself:

> \(9) Academic Misconduct. Academic Misconduct is defined as, actual or
attempted, fraud, deceit, or unauthorized use of materials prohibited or
inappropriate in the context of the academic assignment. Unless
otherwise specified by the faculty member, all submissions, whether in
draft or final form, must either be the Student’s own work, or must
clearly acknowledge the source(s). Academic Misconduct includes, but is
not limited to: (a) cheating, (b) fraud, (c) plagiarism, such as word
for word copying, using borrowed words or phrases from original text
into new patterns without attribution, or paraphrasing another writer’s
ideas; (d) the buying or selling of all or any portion of course
assignments and research papers; (e) performing academic assignments
(including tests and examinations) in another person’s stead; (f)
unauthorized disclosure or receipt of academic information; (g)
falsification of research data (h) unauthorized collaboration; (i) using
the same paper or data for several assignments or courses without proper
documentation; (j) unauthorized alteration of student records; and (k)
academic sabotage, including destroying or obstructing another student’s
work.

_Any academic misconduct, including plagiarism, will result in a grade
of zero for the assignment concerned. All incidents of academic
misconduct will be reported to the PSU Conduct Office._

If you are uncertain about what constitutes plagiarism, I recommend
visiting me during my office hours and/or reviewing these online
resources:

- [Indiana University First Principles website](https://www.indiana.edu/~academy/firstPrinciples/)

- [Purdue Online Writing Laboratory (OWL)](https://owl.english.purdue.edu/owl/section/2/)

#### Attribution vs. Originality

The degree to which collaborative and derivative work is allowed in this
class may vary by assignment. 
- **_In all cases, attribution is paramount._** Because this is an educational
  setting, all copied *and derivative* computer code or written text **_must 
  be attributed_**. 
- Even if you are using public domain writings or open source code, you must
  note in your own work all places from which you draw inspiration, ideas, or
  implementation details. Even if you start with someone else’s code or text
  and then modify it significantly, you should still cite the author of the
  original work that you used to get started. **_It is always better to 
  over-attribute than under-attribute._**

In all cases, the **work you submit _as your own_ must actually be your own**. 
It is not acceptable to hand in assignments in which substantial amounts of
the work was completed by someone else.

That said, many university plagiarism policies tend to focus on the
less productive side of the issue, urging students to be “original” and
telling them what not to do (buying papers, copying text from the
internet and passing it off as one’s own, etc.). It can be helpful to take more expansive view of what
academic integrity means. _Academic integrity is not so much a matter of
producing purely original thought, but of recognizing and acknowledging
the resources on which you draw_. For example, you will notice in the
acknowledgements throughout, that this syllabus draws heavily from a
wide range of other university professors. References to others' work both 
lend your work additional authority and credibility, and also help the reader
understand what unique contributions you might have made in bringing
diverse resources together in a writing assignment or project.
Originality is not just about a singular novel idea, but may also be
about a novel combination of others' ideas.

In light of this, I do not use “plagiarism detection” services like
Turnitin. Rather than expending your energy worrying about originality,
I suggest that you think instead about what kind of citational network
you are locating yourself in. What thinkers are you thinking with? What
programmers are you coding with? Where do they come from? How might
their positions in the world inform their thoughts – and, in turn, your
thoughts? What is your position relative to these thinkers, writers, and
makers? How might you re-shape your citational network to better reflect
your priorities or ideals? How are you learning from or extending the
code or text in question? How do you think the original author would
feel about your use of their work? Are you generous in giving them
credit for the parts that are the result of their own hard work or are
you claiming it as your own?

If you are interested in these issues, I recommend these pieces:

- Ahmed, Sara. 2013. “Making Feminist Points.” *feministkilljoys*.
\<[feministkilljoys.com/2013/09/11/making-feminist-points/](http://feministkilljoys.com/2013/09/11/making-feminist-points/)\>

- Frank, Will. 2015. “IP-rimer: A Basic Explanation of Intellectual
Property.” Personal Blog Post (Medium).
\<[medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711](https://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711)\>

- This comment thread regarding proposed changes to the Stack Exchange
code licensing defaults:
\<[meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required](https://meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required?cb=1)\>

You may write a 500-word response to these pieces for extra credit. See
me in office hours for details.


##### Writing Assignments 

*In the case of any writing assignments* such as reading responses and
term papers, I expect you to quote and reference course readings, course
discussions, and external sources. Indeed, your ability to locate and
use such sources is part of the skill that you should be developing and
demonstrating as a writer. At this point in your life, you should be an
accomplished communicator and thinker. However, for full points on any
writing assignment, I will also expect you to build on these works and
move beyond simply repeating others’ ideas; to bring your own unique
experiences, critiques, and perspectives into conversation with these
authors.

##### Coding Assignments

This course does not have any coding assignments. 

Q20) Now's your chance to ask questions about plagiarism and academic integrity: 


### Take Care of Yourself!

There are many resources available to support you at PSU. I encourage
you to take advantage of them so that you will be successful at both
your academic and non-academic pursuits.

**Library:** The library is full of helpful people who can assist you in research projects, finding course books and materials, connecting with other campus resources, and getting a public library card! The types of questions that they field are endless, and they would love to be part of your college success story. Say hello! [library.pdx.edu/services/ask-a-librarian/](https://library.pdx.edu/services/ask-a-librarian/)

**We in Computer Science (WiCS):** student organization promoting
inclusivity in computer science
[wics.cs.pdx.edu](http://wics.cs.pdx.edu)

**Writing Center:** The writing center can help you improve your writing!
Visit them! [pdx.edu/writing-center](http://pdx.edu/writing-center)

**Office of Information Technology (OIT):** Campus help desk for all
things technology-related [pdx.edu/oit](http://www.pdx.edu/oit)

**C.A.R.E. Team:** Central point of contact if you *or someone you know* is
having a difficult time – mentally, financially, physically, anything!
[pdx.edu/dos/care-team](http://pdx.edu/dos/care-team)

**PSU Food Pantry**: Located in SMSU 325. <foodhelp@pdx.edu>

**Center for Student Health and Counseling (SHAC):** free, drop-in mental
and physical health care [pdx.edu/shac](http://pdx.edu/shac) For mental health, their capacity is limited to a few appointments. However, they will help you find a referral for ongoing therapy or other care.

**Financial Wellness Center**: For many college students, money is an extremely important and sometimes stressful topic. They offer coaching sessions and meetings with peer mentors: [pdx.edu/student-financial/financial-wellness-center](https://www.pdx.edu/student-financial/financial-wellness-center)

**PSU Cultural Resource Centers (CRCs)** create a student-centered inclusive environment that enriches the university experience. We provide student leadership, employment, and volunteer opportunities; student resources such as computer labs, event, lounge and study spaces; and extensive programming. All are welcome! [pdx.edu/cultural-resource-centers](https://www.pdx.edu/cultural-resource-centers), <cultures@pdx.edu>, 503-725-5351, [facebook](https://www.facebook.com/psuculturalcenters/)

See the dean of student life website for further student resources:
[pdx.edu/dos/student-resources](http://pdx.edu/dos/student-resources)

#### Additional Mental Health & Counseling Resources

College can be a stressful time for a variety of reasons, and taking care of 
your mental health is important! If SHAC isn't working out for you, these other
resources may be useful[^neera-thanks]:

[Community Counseling Clinic](https://www.pdx.edu/coun/clinic) - This is a low-cost resource for ongoing counseling, on campus, located in the grad school of education. The upside is that it is $15 per session for as long as you need, the potential downside is that the therapists are interns.

[M.E.T.A.](https://meta-trainings.com/) - This is an off-campus resource (Belmont x Cesar Chavez-ish). The cost is between $30-45, depending on the experience level of your intern and on your budget. Like the Community Counseling Clinic, it is with interns.

[Multnomah County Crisis Services](https://multco.us/mhas/mental-health-crisis-intervention) - if, at any point, you feel like you are in a mental health crisis, please call **(503) 988-488**. If this seems like a plausible scenario to you, I would put this number in your phone now. In addition to crisis services, they can also give referrals and information.

[^neera-thanks]: Resource list courtesy [Dr. Neera Malhotra](https://www.pdx.edu/profile/neera-malhotra), PSU, University Studies

Q20) Got it.
- I will take care of myself! And I will reach out to you if something comes up and I think I could use some help with this.

### Access and Inclusion for Students with Disabilities

This section is lightly edited from: [pdx.edu/drc/syllabus-statement](https://www.pdx.edu/drc/syllabus-statement)

PSU values diversity and inclusion; we are committed to fostering mutual respect and full participation for all students. My goal is to create a learning environment that is equitable, useable, inclusive, and welcoming. If any aspects of instruction or course design result in barriers to your inclusion or learning, please notify me. The Disability Resource Center (DRC) provides reasonable accommodations for students who encounter barriers in the learning environment.

If you have, or think you may have, a disability that may affect your work in this class and feel you need accommodations, contact the Disability Resource Center to schedule an appointment and initiate a conversation about reasonable accommodations. The DRC is located in 116 Smith Memorial Student Union, 503-725-4150, <drc@pdx.edu>, <https://www.pdx.edu/drc>.

- If you already have accommodations, please contact me to make sure that I have received a faculty notification letter and discuss your accommodations.
- Students who need accommodations for tests and quizzes are expected to schedule their tests to overlap with the time the class is taking the test.
- For information about emergency preparedness, please go to the Fire and Life Safety webpage <https://www.pdx.edu/environmental-health-safety/fire-and-life-safety> for information.

Q21) Do you have any accommodations that apply to this class?
- Yes
- No

Q22) If yes, what are they, and what should I know so that I can help support you in this class?
 

### Title IX Reporting Obligations

*This section is lightly edited from [this suggested syllabus statement](https://www.pdx.edu/diversity/sites/www.pdx.edu.diversity/files/Syllabus%20Statement%2062619%20for%20Title%20IX%20Reporting%20Obligations_REVISED.pdf).*

Title IX is a federal law that requires the university to appropriately respond to any concerns of sex/gender discrimination, sexual harassment or sexual violence. To assure students receive support, faculty members are required to report any instances of sexual harassment, sexual violence and/or other forms of prohibited discrimination to PSU’s Title IX Coordinator, [Julie Caron](https://www.pdx.edu/directory/name/jucaron). 

If you would rather share information about these experiences with an employee who does not have these reporting responsibilities and can keep the information confidential, please contact one of the following campus resources (or visit this link <https://www.pdx.edu/sexual-assault/get-help>): 

- Women’s Resource Center (503-725-5672) or schedule on line at <https://psuwrc.youcanbook.me>
- Center for Student Health and Counseling (SHAC): 1880 SW 6th Ave, (503) 725-2800
- Student Legal Services: 1825 SW Broadway, (SMSU) M343, (503) 725-4556
  
PSU’s Title IX Coordinator and Deputy Title IX Coordinators can meet with you to discuss how to address concerns that you may have regarding a Title IX matter or any other form of discrimination or discriminatory harassment. Please note that they cannot keep the information you provide to them confidential but will keep it private and only share it with limited people that have a need to know. You may contact the Title IX Coordinators as follows: 

- PSU’s Title IX Coordinator: Julie Caron by calling 503-725-4410, via email at <titleixcoordinator@pdx.edu> or in person at Richard and Maureen Neuberger Center (RMNC), 1600 SW 4th Ave, Suite 830 
- Deputy Title IX Coordinator: Yesenia Gutierrez by calling 503-725-4413, via email at <yesenia.gutierrez.gdi@pdx.edu> or in person at RMNC, 1600 SW 4th Ave, Suite 830
- Deputy Title IX Coordinator: Dana Walton-Macaulay by calling 503-725-5651, via email at <dana26@pdx.edu> or in person at Smith Memorial Union, Suite, 1825 SW Broadway, Suite 433 
  
For more information about the applicable regulations please complete
the required student module Creating a Safe Campus in your D2L
[pdx.edu/sexual-assault/safe-campus-module](http://pdx.edu/sexual-assault/safe-campus-module)

Q23) Got it. 
- I understand your reporting obligations, and I know who I can reach out to if I have a concern about sexual/gender harassment, discrimination, or violence.

Q24) We made it to the end! Do you have any remaining questions about the course? Is there anything else you think I should know or take into consideration as we begin the quarter?


### Notes

[^1]: See, for example, this year's news about Richard Stallman stepping down from the Free Software Foundation and MIT, or, last year's news about Linus Torvalds stepping away from maintenance of Linux as he seeks help for improving his interpersonal skills. Lawler, Richard. 2019. "GNU founder Richard Stallman resigns from MIT, Free Software Foundation: Calls to fire Stallman grew after his comments about victims of Jeffrey Epstein." _Engadget_, September 19, 2019. <https://www.engadget.com/2019/09/17/rms-fsf-mit-epstein/>; Cohen, Noam. 2018. "After Years of Abusive E-mails, the Creator of Linux Steps Aside," _The New Yorker_, September 29, 2018. <https://www.newyorker.com/science/elements/after-years-of-abusive-e-mails-the-creator-of-linux-steps-aside>

[^2]: See, e.g., Vivian Hunt et al. 2018. “Delivering through Diversity” Report by *McKenzie and Co.* January 2018. <https://www.mckinsey.com/business-functions/organization/our-insights/delivering-through-diversity>.

[^3]: See, e.g., Jane Margollis et al. 2008. *Stuck in the Shallow End: Education, Race and Computing.* MIT Press.; Steve Henn. 2014. "When Women Stopped Coding" *Planet Money*, NPR. <https://www.npr.org/sections/money/2014/10/21/357629765/when-women-stopped-coding>; Michelle Kim. 2018. "Why focusing on the “business case” for diversity is a red flag" *Quartz at WORK,* 29 March 2018. <https://work.qz.com/1240213/focusing-on-the-business-case-for-diversity-is-a-red-flag/>.

[^4]: See, e.g., J. McGrath Cohoon. 2001. Toward improving female retention in the computer science major. *Commun. ACM* 44, 5 (May 2001), 108-114.<http://dx.doi.org/10.1145/374308.374367>; Tracey Lien. 2015. “Why are women leaving the tech industry in droves?” *LA Times*. <http://www.latimes.com/business/la-fi-women-tech-20150222-story.html>.

[^6]: PSU Student Code of Conduct: <https://www.pdx.edu/dos/psu-student-code-conduct>
