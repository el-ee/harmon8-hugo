---
title: Reading Reflections
layout: "subpage"
weight: 1000
icon: "fa-book"

menu:
    main:
        parent: "Teaching"
        identifier: "cs305-202001-readings"

---

In order to prepare for our class discussions you will write a brief reading reflection before each class meeting (except presentation days) and bring it to class _on paper_. If you email your response to me by _noon_, I will print it for you. Otherwise, you are responsible for printing it. I will only grade the printed version. There is no need to email it to me unless you just need me to print it.

This reflection must include three parts:

1. _**Summary**_ (100-300 words): Identify the central question or problem of each reading and summarize the author's answer and the main pieces of evidence they use to back up their argument. This portion of the response should convince me that you actually _did the reading_.
2. _**Ethical Reflection**_ (200-400 words): Identify _one_ ethical dilemma relevant to the reading and take a stand on what you think should be done in response to this dilemma. Refer to course materials, your own experiences and prior knowledge, and/or the ACM code to justify your proposed action.
3. _**Questions**_: List 2-3 questions that you would like to discuss in class.

Each class period (except presentation days) will begin with a 15 minute period of reflective writing. During this time, you will exchange reading reflections with someone else and write a brief response either to their ethical reflection or to one of their questions. This brief period of reflective writing is designed to help you settle in, focus, and prepare for discussion. You may refer to the assigned reading and/or any notes you have while writing; it is not a quiz. _If you are late to class, you will miss the opportunity to complete this exercise._


The combined prepared reading reflection + in-class writing response will be graded together on a simple ✔+ / ✔ / ✔- scale:

| Mark | Grade | Description |
|---:|--:|:---|
| **✔+** | 3 | **Exceeds Expectations**: it is clear that you fully understood the reading. Both the prepared response and the in-class writing contain insightful and original ideas or questions that bring your own expertise and experience to bear on the topic. The response could be used as an example of an excellent reading response in future classes. |
|**✔** | 2.6 | **Satisfactory**: Response is complete and it is clear that you actually did the reading and understood most of it. However, your response may not show your own substantive engagement with the topic. |
|**✔-** | 2.1 | **Unsatisfactory**: Your response is incomplete or lacks concrete evidence that you read beyond the title or first few paragraphs. Perhaps you did not complete the in-class portion, your prepared response only responds to some of the prompts, is too short to fully address the prompt(s), or shows significant gaps in your understanding of the material. |
