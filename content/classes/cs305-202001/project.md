---
title: Case Analysis
layout: "subpage"
weight: 1000
icon: "fa-edit"

menu:
    main:
        parent: "Teaching"
        identifier: "cs305-202001-case-analysis"

---

In this class, you will work in teams of 3 to research a topic related to the social, ethical, and legal implications of computing. In preparation for the final paper due on **Friday** of finals week, there are several milestones:

- [Week 2: Intros + Topics](#week-2-intros--topics)
- [Week 2: Team Memo](#week-2-team-memo)
- [Week 5: Annotated Bibliography](#week-5-annotated-bibliography)
- [Week 6: 5-minute presentation](#week-6-5-minute-presentation)
- [Week 8: Case Analysis](#week-8-case-analysis)
- [Finals Week: 10-minute presentation](#week-10-10-minute-presentation)
- [Finals Week: Final Proposal](#finals-week-final-proposal)

Excepting the in-class introductions & topic pitches in week 2, all assignments are team assignments. Only 1 member of your team should turn in a file.

Please review the below case analysis about pharmaceutical pricing in order to see an example of what each section would look like in a well-written and high quality analysis.

- Cwik, Bryan. (forthcoming) "Of Pills and Prices: Ethics Issues in Pharmaceutical Pricing" to appear in _Business Cases in Ethical Focus_. [PDF](https://drive.google.com/file/d/1_KveKJNJOSeYFQ5RWYKE8alRiINNvrtO/view?usp=sharing) *Only available to students in this course. You must be logged in with your PDX google account to access.*

### General Formatting Guidelines

- All documents should be turned in in `.pdf` or `.docx` format.
- Put your team name and all individuals' names on the first page.
- Use headings as appropriate.
- Be consistent! Use the style feature of modern word processors!
- Use standard and professional fonts, margins, etc, such as:
    - One inch margins on all sides.
    - Size 11-12 font
    - Line height of 1.3-1.6
    - The height of one full line between paragraphs
    - An easy to read font (Like, no Comic Sans or script fonts, yes?)
    - Page numbers! They are great.
- If your document is very long with many sections, consider a table of contents.
- I recommend [Zotero](https://zotero.org) for keeping your references neat and correct.
- Here is a [sample google doc](https://docs.google.com/document/d/1SrHqs81xuBJcrlgxYMXYFBBmDVE29Nd3XQujNlKxM1s/edit?usp=sharing) that is very boring and generic but easy to read.


### Week 2: Intros + Topics

**Deadline:** in-class activity, week 2

**How to turn in:** show up to class and participate

At the start of class, you will each briefly introduce yourself to the class and pitch a topic for the paper/project. During this time you should make notes of who you might want to work with. There will be some time at the end of class to discuss with your classmates, and then you will sign up for a topic in a Google Doc, and create a team contract.

If you cannot or do not want to decide who to work with, or cannot form a complete group of 3 people, I will assign you to a topic and team.

#### Specification

Please be prepared to share each of these four things:

1. What is your name?
2. Optional: What pronoun should the class use to refer to you?
3. One thing about you – such as, where you work, where you are from, what you like to do outside of school, what part of computer science you care about most, etc.
4. A proposal for a topic area:
    - **What’s the topic area?** Almost anything is okay, but it must be somehow related to the social, legal, and/or ethical implications of computing. For now it can be broad. You should focus in as you do your research.
    - **Why should the class care?** What makes your topic important to computer scientists and/or society at large? Why should people want to work on it with you?

**Your brief introduction and topic pitch should be _no more than 90 seconds_. Ideally, one minute.**


#### Grading Rubric

This assignment is worth 2 points. It will be graded very simply:

| Mark | Points | Criteria Description |
|:--|:--|:--|
| ✔+ | 2.5 | Introduced yourself, shared your topic area, and seemed prepared. Did not go over 90 seconds. Hit all the required points.|
| ✔- | 1.5 | Shared topic with class, but was not prepared -- either missed some required points, or ran significantly over time.|

_If you do not participate or are not present in class, you will receive a 0 for this assignment._


### Week 2: Team Memo

**Deadline:** Tuesday, January 21, 11:59p

**How to turn in:** A single well-organized `.pdf` or `.docx` file, on D2L.

Your team should prepare a brief memo outlining your work plan for the term. This memo should be no more than 2 pages.

 **You should also schedule a [meeting with me](/~harmon8/faq/#meetings-with-me) for some time in weeks 3 or 4.**

#### Specification

- **A team name**.
- **All team member names** and PDX email addresses.
- **Topic** What's your general topic area, in 2 sentences or less.

- **A communication plan**: how will you communicate with each-other in between classes and course meetings? Will you make a multi-person private chat on Facebook? Will you use email? Will you create a group SMS? Will you set up a Discord server or Slack channel somewhere? I don't care what it is, but I want to know all team members are on the _same_ page.
- **A collaboration plan**:
    - Have you discussed each other's work, school, and family schedules? If not, do that now and then answer this with a Yes.
    - Does anyone on your team have a major time commitment this quarter that may make it hard for them to contribute to the project during some period of time (e.g., a week long work trip, a best friend's wedding, a really important midterm in a really hard class)? How will you work around these commitments?
    - When/how will you work outside of class? Will you have a regular meeting or schedule meetings as needed? Will you meet in person or remotely (Note! PSU offers free use of Zoom meetings now!)
    - What tools will you use to collaborate on writing: e.g., will you use Google Docs to draft text, and provide feedback to each other there? If not, what is your alternative plan?
    - Who will be your designated turn-in person to upload the file to D2L? How will they let the rest of you know it is complete?
- **Project Timeline** including individual responsibilities.  What are all the things that need to be done so that you will be ready to turn in your bibliography on February 6? Who will take the lead on each of these tasks? Who will take the lead on motivating and coordinating the group, making sure everyone else has done their part?

#### Grading Rubric

This assignment is worth 2 points. It will be graded very simply:

| Mark | Points | Criteria Description |
|:--|:--|:--|
| ✔+ | 2.5 | Hit all the required points.|
| ✔- | 1.5 | Incomplete.|

_If you do not participate or are not present in class, you will receive a 0 for this assignment._

### Week 5: Annotated Bibliography

For this milestone, you will compile an annotated bibliography with at least **six** references in it that you will use in your final paper. (Your final paper will need to include at least 9 or 12 references (see [Final Proposal](#finals-week-final-proposal)).

Preparing an annotated bibliography should be a useful step on the way to writing your final paper. If you do it well, you should be able to save yourself the time later of going back to re-read any of your sources when it's time to actually write. Think of your future self and your team-mates as the audience for your entries. What will you want to reference from this source when you're writing in the future? What should your team-mates know about it?

**Deadline:** Thursday, February 6, 11:59p.

**How to turn in:** A single well-organized `.pdf` or `.docx` file, to D2L


#### Specification

Prepare a single `.pdf` or `.docx` that includes:

1. **Memo**: _Try to keep this to a page or less._
    1. **Team name**
    2. **Team member names and pdx emails**
    3. **Topic statement:** What is the topic you are working on? (_1-5 sentences. This can be very short! I just want to know what I'm supposed to be reading about._)
    4. **Contribution summary:** What did each person do for this assignment?
    5. **Self-Assessment:** What grade do you believe you have earned on this   assignment? Why? (_1-3 sentences._)
    6. **Request for feedback:** What do you need help on? What questions do you have for me? (_1-3 sentences._)
    7. **What's next?** What's your team's timeline and plan for moving forward with the project? What do you need to do to have your case analysis done in 3 weeks? (_Maybe in list format?_)
2. **The Annotated Bibliography** with at least 6 (_six_) entries.
    - The reference set must include:
        - At least 4 (_four_) peer-reviewed scholarly works (see, e.g., <https://dl.acm.org/> )
        - At least 4 (_four_) references that are not from the course schedule.
    - _Each entry_ in the annotated bibliography should include:
        1. _A full bibliographic entry_ for the reference in a standard reference format (e.g., ACM, MLA, Chicago, etc.). This means that you include, at minimum: author name(s), date of publication, title of publication, publication venue (if article, journal name; if book, the press), link to official online version (no random homepages, no SSRN links). I strongly recommend using a citation manager, such as [Zotero](https://www.zotero.org/) or [BibTeX](https://texfaq.org/FAQ-usebibtex). We will discuss Zotero in Week 1.
        2. A 3-5 sentence _summary_ of the source. What is the author's point. None of your own evaluation here. Just. Summarize.
        3. A 3-5 sentence _reflection_. How is this source useful for your project? How does it relate to other sources in your bibliography? Does it agree with or contradict them?
        4. A selection of _direct quotes_ or _key statistics_ (1-5) that might be useful when you are working on the next parts of this project.
3. **Draft Ethical Questions**: 2-4 ethical questions that you will explore in the rest of this project. Your case analysis should delve into the complexities of these questions, and your final proposal should take a stand on one or more of them. It is critical that these questions are ethical or political in nature, and are not informational questions, are not 'what to do' questions, and do not have a factual right/wrong answer. These should be questions to which there are multiple legitimate answers depending on your own moral/ethical/political stance. We will talk more about this in class!

#### Resources

You can find more information about annotated bibliographies here:

- Overview: <https://owl.english.purdue.edu/owl/resource/614/01/>
- Examples: <https://owl.english.purdue.edu/owl/resource/614/03/>

_Note:_ You only have to write "summary" and "reflection" paragraphs for this assignment. You do not have to write the "assessment" paragraph that is shown in the OWL directions and examples. You _DO_, however, need to additionally include any direct quotes that you may want to use later.

#### Grading

This assignment is worth 10 points. You can earn these points as follows:  

- Base Grade:
    - **0**: not completed, or contains plagiarized content.
    - **5**: A low quality bibliography that meets some of the requirements. For example, half or more of your entries are missing reflection paragraphs (or summary paragraphs); or, you only included 4 bibliography entries.
    - **6.5**: An okay response that meets most of the requirements. For example there are 5 entries instead of 6; some entries are missing a reflection paragraph; fewer than 4 of your sources were located on your own (i.e., not part of the course reqdings).
    - **8**: A good response that meets all requirements (unless listed below as a separate grade).
- Additional points, earned regardless of your base grade:
    - **+.6**: Summaries are high quality and clearly _summarize_ the listed article. A good summary should not talk _about_ the article, but should instead actually _convey the main point(s)_. It may be helpful to think of this as akin to an abstract.
    - **+0.2**: Direct quotes are actually quote-worthy. Why are these specific words important?
    - **+0.2**: Ethical questions are actually ethical questions (and not, e.g., factual questions).
    - **+0.2**: Bibliography includes at least _FOUR_ peer-reviewed sources, and they are truly peer-reviewed.
    - **+0.2**: All citations are _complete_, correct, and in a _consistent_, standard format.
    - **+0.2**: Document is reasonably & consistently formatted. See [General Formatting Guidelines](#general-formatting-guidelines) above.
    - **+0.4**: Exceptionally high quality work; could be used as an example. You can earn these points by, for example, writing particularly insightful reflections, or writing very high quality succinct but complete summaries.

### Week 6: 5-minute presentation

In class in week 6, each team will present their in-progress research work. You will then have an opportunity to get feedback and questions from your peers in advance of turning in your Case Analysis in week 8.

**Deadline:** 4p, Thursday, February 13.

**How to turn in:**

1. Upload your slide deck in `.pdf` or `.pptx` format to D2L.
2. Deliver your presentation in class.

Each team will be allotted 10 minutes including questions/feedback. Any time you spend plugging in laptops, waiting on websites to load, etc, will count towards this 10 minute slot.

#### Specification

- Aim for 5 minutes. Anything in the 4-6 minute range is okay.
- **Everyone** in your team must meaningfully participate.
- Your presentation should include each of the following pieces of information:
    1. Who are you? Introduce yourselves.
    2. What is your topic area? Why is it interesting or important?
    3. What background is important for people to know in order to understand your topic?
    4. What problems or concerns have you identified in your topic space, as related to the social, ethical, or legal implications of computing?
        - Who is harmed? How and why?
        - Who benefits? How and why?
        - What is the history of the situation? How and why did these problems or concerns arise?
    5. Where and how is your knowledge limited? What do you need to learn about in order to:
        - fully understand the nature and scope of the situation (by week 7)
        - and make a compelling argument about how this situation should be addressed (by week 10)

_Your presentation should not include an argument for taking any particular action._


#### Grading

This assignment is worth 5 points total:

|.95|.85|.75|.65|
|:--|:--|:--|:--|
|**Mechanics** <br/> Presentation lasted 4-6 minutes. Slides were turned in by 4p. All team members participate. | Slides not turned in by 4p. | Over time by > 2 minutes. | Not even 4 minutes -or- all team members do not participate in a meaningful way. |
|**Preparation** <br/> You manage your own time. You seem prepared. Perhaps you have notes. You do not ramble. | . | Presentation ends up fine, but you do not seem well-prepared or rehearsed. | . |
|**Delivery & Organization** <br/> Strong focus & structure. Clear speaking. Engaging delivery. Increases audience _interest_.| Well-organized, clear delivery, somewhat interesting. | Audience can follow the big picture story but some details are confusing. |  Uninteresting or unclear delivery. Audience struggled to follow your point.|
|**Content** <br/> Increases audience _knowledge_. Hits all five requirements (who are you, topic, background, problem, what's next?)| Overall a fine presentation, but misses one of the key requirements. | Content doesn't match the audience. We are unable to follow your main point because you talk above our heads or don't provide sufficient context. | Fails to fulfill multiple requirements. |
|**Quality** <br/> Graphics and/or charts are used liberally and to good effect. Text is presented in small easily digestible chunks. Slides are useful as visual aids for the audience and do not distract from your speaking. | There are at least three _meaningful_ graphics and/or charts, and no more than one or two lengthy bulleted lists. | There are many lengthy bulleted lists. | There are whole paragraphs of text. Graphics are distracting / extraneous / non-existent. (NB: If you never, in the course of speaking, refer to your graphics, they are probably extraneous.) |

- **+0.25** point is reserved for exceptionally high quality work, such as especially well-designed slides, a particularly engaging presentation, or especially thought-provoking content.
- If you do not give your presentation, but you do turn in a complete slide deck, you will receive 1 point.

### Week 8: Case Analysis

**Deadline:** Thursday, February 27, 11:59p.

**How to turn in:** Turn in a single, well-organized `.pdf` or `.docx` to D2L.

Please review the below case analysis about pharmaceutical pricing in order to see an example of what each section would look like in a well-written and high quality analysis.

- Cwik, Bryan. (forthcoming) "Of Pills and Prices: Ethics Issues in Pharmaceutical Pricing" to appear in _Business Cases in Ethical Focus_. [PDF](https://drive.google.com/file/d/1_KveKJNJOSeYFQ5RWYKE8alRiINNvrtO/view?usp=sharing) *Only available to students in this course. You must be logged in with your PDX google account to access.*


#### Specification

**Length:**

- Three person groups: Your complete case analysis must be at least 1200 words.
- Four person groups: Your complete case analysis must be at least 1600 words.
- No case analyses should exceed 2500 words.

Prepare a single `.pdf` or `.docx` that includes:

1. **Memo**: _Try to keep this to a page or less._
    1. **Team name**
    2. **Team member names and pdx emails**
    3. **Word counts:** for background and analysis sections. Please list separately.
    4. **Contribution summary:** What did each person do for this assignment?
    5. **Self-Assessment:** What grade do you believe you have earned on this assignment? Why? (_1-3 sentences._)
    6. **Request for feedback:** What do you need help on? What questions do you have for me? (_1-3 sentences._)
    7. **What's next?** What's your team's timeline and plan for moving forward with the project? What do you need to do to have your final proposal done in 3 weeks? (_Maybe in list format?_)
2. **Case Analysis**: Your case analysis should have the following three sections:
    - **Background (400-600 words)**: A concise summary of the technology/situation you are examining, including relevant historical information. This section should present 'just the facts' _without_ any analysis from you.
    - **Analysis (750-1500 words)**: Identify _and analyze_ 2-4 key social, ethical, and/or legal issues related to your technology/situation. This section should present the issues and identify tradeoffs. _It should not move ahead to arguing for taking any particular action._ You will do this for the [Final Proposal](#finals-week-final-proposal)
    - **Ethical Questions (2-4 questions)**: Summarize your analysis in 2-4 ethical questions that need resolution in order to respond to the social, ethical, and/or legal issues identified in your analysis. These may be the same as the questions you turned in with your annotated bibliography, but it is also fine if you need to change them based on my feedback and/or your own furthe research and writing. They must follow directly from your analysis, and should make sense as a summative ending point for the document. Consequently, each question should take no more than 2 sentences to clearly communicate. For now, just pose the questions. You will answer them later in your final document.

3. **Citations/References:**
    - You must meaningfully integrate at least _9 (3-person groups) or 12 (4 person groups)_ references in your analysis.
        - At least 6 must be sources from outside the course.
        - At least 4 must be peer-reviewed, scholarly sources.
    - You may use any citation format you like, but it must be standard and consistent.
    - In addition to in-text citations, you should include a bibliography/works cited list at the end of your document. This bibliography does _**not**_ need to be annotated.



### Finals Week: 10-minute presentation

**Deadline:** 4p, Thursday, March 19

**How to turn in:**

1. Upload your slide deck in `.pdf` or `.pptx` format to D2L.
2. Deliver your presentation in class.

#### Specification

- Aim for 10 minutes. Anything in the 8-12 minute range is okay.
- **Everyone** in your team must meaningfully participate.
- Your presentation should include each of the following pieces of information:
    1. Briefly review your topic area, but remember that the class has already heard you give one presentation.
    2. Pose the ethical questions/challenges that you will respond to.
    3. Propose some action/response/answer to the questions you identify.
    4. Make a compelling case! Convince the class that this is the right action to take.

#### Grading Rubric

|1.9|1.7|1.5|1.3|
|:--|:--|:--|:--|
|**Mechanics** <br/> Presentation lasted 8-12 minutes. Attempted to upload video no later than 1 hour into exam period. All team members participate. | Excessive delay on video upload. | Less than 8 minutes / more than 12 minutes. | All team members do not participate in a meaningful way. |
|**Preparation** <br/> You manage your own time. You seem prepared. Perhaps you have notes. You do not ramble. | . | Presentation ends up fine, but you do not seem well-prepared or rehearsed. | . |
|**Delivery & Organization** <br/> Strong focus & structure. Clear speaking. Engaging delivery. Increases audience _interest_.| Well-organized, clear delivery, somewhat interesting. | Audience can follow the big picture story but some details are confusing.  | Uninteresting or unclear delivery. Audience struggled to follow your point. You do not engage the audience.|
|**Content** <br/> Increases audience _knowledge_. Hits all five requirements (review topic, pose questions, propose action, make a _compelling case_)| Overall a fine presentation, but misses one of the key requirements. | Content doesn't match the audience. We are unable to follow your main point because you talk above our heads or don't provide sufficient context. | Fails to fulfill multiple requirements. |
|**Quality** <br/> Graphics and/or charts are used liberally and to good effect. Text is presented in small easily digestible chunks. Slides are useful as visual aids for the audience and do not distract from your speaking. | There are at least three _meaningful_ graphics and/or charts, and no more than one or two lengthy bulleted lists. | There are many lengthy bulleted lists. | There are whole paragraphs of text. Graphics are distracting / extraneous / non-existent. (NB: If you never, in the course of speaking, refer to your graphics, they are probably extraneous.) |

- **+0.5** point is reserved for exceptionally high quality work, such as especially well-designed slides, a particularly engaging presentation, or especially thought-provoking content.


### Finals Week: Final Proposal

In this assignment you will revise your case analysis as needed based on feedback from the prior assignment, and additionally make an argument that responds to the questions you identified in your case analysis taking a stand on the ethical issues and proposing some action/response to the situation.

**Deadline:** Friday, March 20, 11:59p

**How to turn in:** Upload a single, well-organized `.pdf` or `.docx` to D2L.

#### Specification

**Length:**

- Three person groups: Your complete case analysis and proposal must be at least 1800 words combined.
- Four person groups: Your complete case analysis and proposal must be at least 2200 words combined.
- No case analyses and proposals should exceed 3500 words combined.

Prepare a single `.pdf` or `.docx` that includes:

1. **Memo**: _Try to keep this to a page or less._
    1. **Team name**
    2. **Team member names and pdx emails**
    3. **Word counts:** for background, analysis, and proposal sections. Please list separately.
    4. **Contribution summary:** What did each person do for this assignment?
    5. **Self-Assessment:** What grade do you believe you have earned on this assignment? Why? (_1-3 sentences._)
    6. **Request for feedback:** What do you need help on? What questions do you have for me? (_1-3 sentences._)
    7. **What's next?** What's your team's timeline and plan for moving forward with the project? What do you need to do to have your final proposal done in 3 weeks? (_Maybe in list format?_)
2. **Case Analysis**: Your case analysis should have the following three sections:
    - **Background (400-600 words)**: A concise summary of the technology/situation you are examining, including relevant historical information. This section should present 'just the facts' _without_ any analysis from you.
    - **Analysis (750-1500 words)**: Identify _and analyze_ 2-4 key social, ethical, and/or legal issues related to your technology/situation. This section should present the issues and identify tradeoffs. _It should not move ahead to arguing for taking any particular action._ You will make an argument in the Proposal section.
    - **Questions (2-4 questions)**: Summarize your analysis in 2-4 underlying questions that need resolution in order to respond to the social, ethical, and/or legal issues related to your technology/situation. Because these questions should follow directly from your previous analysis, each question should take no more than 2 sentences to communicate. You will respond to these questions in the next section.
3. **Proposal (500-1000 words)**: Take a stand! Answer the questions you posed at the end of the case analysis. Now that you've thoroughly examined this particular ethical case, make a proposal for what should be done, who should do it, how, and why. Make a strong case, using evidence to support your arguments.
4. **Citations/References:**
    - You must meaningfully integrate at least _9 (3-person groups) or 12 (4 person groups)_ references in your analysis.
        - At least 6 must be sources from outside the course.
        - At least 4 must be peer-reviewed, scholarly sources.
    - You may use any citation format you like, but it must be standard and consistent.
    - In addition to in-text citations, you should include a bibliography/works cited list at the end of your document. This bibliography does _**not**_ need to be annotated.

#### Grading Rubric

[PDF](https://drive.google.com/file/d/1Hyrf8RkWT_41kzVyhvpn6uTBp3jve5l4/view?usp=sharing)

