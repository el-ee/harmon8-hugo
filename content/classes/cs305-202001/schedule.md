---
title: Schedule
layout: "subpage"
weight: 300
icon: "fa-calendar-alt"

menu: 
  main:
    parent: "Teaching"
    identifier: "cs305-202001-schedule"

---

| Week | Topic | Milestones|
|:--|:--|:--|
| [1](#week-1) | Introduction / ACM Code | |
| [2](#week-2) | Why Ethics? <sup>RR1</sup> | _In-class: Intros + Pitches<br/> Friday: Team Memo_ |
| [3](#week-3) | No class. | _Team Meetings_ |
| [4](#week-4) | Privacy, Speech, Data <sup>RR2</sup> |  |
| [5](#week-5) | Intellectual Property & Licensing <sup>RR3</sup> | _Due: Bibliography_ |
| [6](#week-6) | _Presentation I_ | _Due: 4pm, Slide Deck_ |
| [7](#week-7) | Work & Automation <sup>RR4</sup> |  |
| [8](#week-8) | Security & Reliability <sup>RR5</sup> |  _Due: Case Analysis_ |
| [9](#week-9) | Taking Action <sup>RR6</sup> |
| [10](#week-10) | _Presentation II_ | _Due: 4pm, Slide Deck_ |
| [Finals](#finals-week) | Thursday, 5:30p <br/> _Presentation II_ + Closing |_Due: Final Proposal, Wednesday_ |

### Week 1

- Read before class: n/a
- In Class:
    - Technologies that have had an impact
    - Syllabus
    - Start of term [Self-Assessment](https://portlandstate.qualtrics.com/jfe/form/SV_3KINvIzR1xItlxr)
    - What is an Ethical Question?
    - ACM Code
- Homework:
    - Read for next class
    - Prepare for 30-second personal [intro & topic pitch](../project#week-2-intros-topics)


### Week 2

Why Ethics?

**Due: 30-second intro + topic pitch, in class**

- Read before class:
    - _Everyone:_ Aaron Swartz. 2006. "Code and Other Laws of Wikipedia." _aaronsw.com (Personal Blog)_ <http://www.aaronsw.com/weblog/wikicodeislaw>
    - _And choose 1 more_:
        - _Choice A:_ Watch/listen to: Jarmul, Katherine. 2017. “If Ethics Is Not None.” At _EuroPython_ Conference. <http://youtu.be/FtRbAePXUoI?t=1m26s>
        - _Choice B:_ Read: Green, Ben. 2019. "'Good' isn’t good enough." AI for Social Good workshop at _Neural Information Processing Systems 2019_ <https://www.benzevgreen.com/wp-content/uploads/2019/11/19-ai4sg.pdf>
        - _Choice C:_ Read: Ochigame, Rodrigo. 2019. “The Invention of ‘Ethical AI’: How Big Tech Manipulates Academia to Avoid Regulation.” _The Intercept_. December 20, 2019. <https://theintercept.com/2019/12/20/mit-ethical-ai-artificial-intelligence/>.

- In Class:
    - Intros/Topic Pitches
    - Discuss Readings
    - Form Groups
- Homework:
    - Read for next class
    - [Team Memo](../project#week-2-team-memo) (due ~~Sunday~~ Tuesday, 11:59p)
    - Schedule a 30 minute team meeting with me _during week 3 or 4_. See [FAQ: Meethings with Me](../../faq#meetings-with-me) for how-to.
    - Start working on [Annotated Bibliography](../project#week-5-annotated-bibliography) (due week 5)



### Week 3

No class meeting. I will be at an NSF workshop in Denver.

- [Team Memo](../project#week-2-team-memo) due ~~Sunday~~ **Tuesday**, 11:59p

- Your team should schedule a meeting with me to discuss your planned project and make sure you are on track for a successful final paper. This meeting should be completed by the end of Week 4. See [FAQ: Meethings with Me](../../faq#meetings-with-me) for how-to.
- Continue working on [Annotated Bibliography](../project#week-5-annotated-bibliography) (due week 5)
- Consider looking at Zotero for your citation management. [General Docs](https://www.zotero.org/support/start) + [Collaborating with a Group](https://www.zotero.org/support/groups)

### Week 4

Privacy, Speech, Data _+ Zotero_

- **Choose one area**, and read/watch everything in that sublist [Complete a reading response before class](../readings/):
    - Privacy (Read both):
        - Warren, Samuel D., and Louis D. Brandeis. 1890. “The Right to Privacy.” _Harvard Law Review_, vol. 4, no. 5. pp. 193–220. <http://jstor.org/stable/1321160> or <http://groups.csail.mit.edu/mac/classes/6.805/articles/privacy/Privacy_brand_warr2.html>
        - Rejouis, Gabrielle M. 2019. “Why Is It OK for Employers to Constantly Surveil Workers?” _Slate Magazine_, September 2, 2019. <https://slate.com/technology/2019/09/labor-day-worker-surveillance-privacy-rights.html>
    - Speech (Listen to):
        - Mike Ananny, Tarleton Gillespie, Kate Klonick. "Freedom in Moderation: Platforms, Press, and the Public" Data & Society Podcast: <https://listen.datasociety.net/freedom-in-moderation-platforms-press-and-the-public/>
    - Data (_choose any 3_):
        - Joy Buolamwini, “AI, Ain’t I A Woman?” YouTube. <https://www.youtube.com/watch?v=QxuyfWoVV98>
        - Powles, Julia & Helen Nissenbaum. 2018. “The Seductive Diversion of ‘solving’ Bias in Artificial Intelligence.” Medium: Artificial Intelligence (Blog). <https://medium.com/s/story/the-seductive-diversion-of-solving-bias-in-artificial-intelligence-890df5e5ef53>
        - Jonas Lerman. 2013. “Big Data and Its Exclusions.” Stanford Law Review 66 (September), pp. 55–63. <http://stanfordlawreview.org/online/privacy-and-big-data-big-data-and-its-exclusions>
        - _new additional option!_ Gooblar, David. 2019. "A Teacher’s New Year’s Resolution: Stop Fixating on the Data." _Chronicle itae (blog)_. <https://chroniclevitae.com/news/2293-a-teacher-s-new-year-s-resolution-stop-fixating-on-the-data?cid=VTEVPMSED1> 
- Homework:
    - Read for next class
    - Continue working on [Annotated Bibliography](../project#week-5-annotated-bibliography) (due week 5)
    - Zotero References: [General Docs](https://www.zotero.org/support/start) + [Collaborating with a Group](https://www.zotero.org/support/groups)



### Week 5

Intellectual Property & Licensing

**Due: Annotated Bibliography, 11:59p**

- Read before class (choose _any 2_) and [complete a reading response before class](../readings/):
    - "Will Frank. 2015. “IP-Rimer: A Basic Explanation of Intellectual Property.” Medium (Personal Blog), November 2015. <http://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711>
    - Review: <https://opensource.org/faq>
    - Cherry, Miriam A. 2014. “A Eulogy for the EULA.” Duquesne University Law Review 52 (2): 335–44. Official version: <https://issuu.com/duquesnelaw/docs/52.2?e=15059800/58167323>  Also  available at: <https://scholarship.law.slu.edu/cgi/viewcontent.cgi?article=1012&context=faculty>

- Homework:
    - Read for next class
    - Start working on [Presentation I](../project#week-6-5-minute-presentation) (due week 6)
    - Start working on [Case Analysis Draft](../project#week-8-case-analysis) (due week 8)




### Week 6

**Due: Presentation I, upload slides by 4pm**

- Read before class: n/a
- In class:
    - Presentations
    - Give feedback to peers
- Homework:
    - Read for next class
    - Continue working on [Case Analysis Draft](../project#week-8-case-analysis) (due week 8)


### Week 7

Work & Automation

- Read before class (choose one area, then read/watch that sublist as directed) and [complete a reading response before class](../readings/):
    - **Area Choice 1: Automation** (Choose _one_. They're the same. Book chapter version or podcast/presentation version.):
        - The reading option: Eubanks, Virginia. 2018. “Automating Eligibility in the Heartland,” chapter 2 from Automating Inequality [PDF](https://drive.google.com/open?id=1MfmLlVFaBg80OkXnW1wnudnGgtPNpKOe)
        - The video option: Eubanks, Virginia. 2018. _Automating Inequality_,  Databite No. 106: Virginia Eubanks, Alondria Nelson, Julia Angwin. <https://datasociety.net/events/databite-no-106-automating-inequality-virginia-eubanks-in-conversation-with-alondra-nelson-and-julia-angwin/>
    - **Area Choice 2: Temping, Gigging and Contracting** (Read all three):
        - Wong, Julia Carrie. 2018. “Revealed: Google’s ‘two-Tier’ Workforce Training Document.” The Guardian, December 12, 2018, sec. Technology. <https://www.theguardian.com/technology/2018/dec/11/google-tvc-full-time-employees-training-document>.
        - Wong, Julia Carrie. 2019. “Google Reportedly Targeted People with ‘dark Skin’ to Improve Facial Recognition.” The Guardian, October 3, 2019, sec. Technology. <https://www.theguardian.com/technology/2019/oct/03/google-data-harvesting-facial-recognition-people-of-color>.
        - Vinokour, Maya. 2019. "Gig Authoritarians" Public Books Essay <https://www.publicbooks.org/gig-authoritarians/>
    - **Area Choice 3: Workplace Diversity** (Read all three):
        - Dare Obasanjo. 2016. “The Big Lie: Tech Companies and Diversity Hiring.” Don’t Panic, Just Hire / 42 Hire (Medium Blog), July 15, 2016. <http://42hire.com/the-big-lie-tech-companies-and-diversity-hiring-f52fb82abfbf>
        - Yonaten Zunger. 2017. “So, about This Googler’s Manifesto.” Personal Blog (Medium), August 5, 2017. <http://medium.com/@yonatanzunger/so-about-this-googlers-manifesto-1e3773ed1788>
        - "Corporate Diversity: Beyond the Pipeline Problem" (pp. 19-26) in West, S.M., Whittaker, M. and Crawford, K. (2019). _Discriminating Systems: Gender, Race and Power in AI_. AI Now Institute. <https://ainowinstitute.org/discriminatingsystems.pdf>
- Homework:
    - Read for next class
    - Continue working on [Case Analysis Draft](../project#week-8-case-analysis) (due week 8)


### Week 8

Security & Reliability

**Due: Case Analysis Draft, 11:59p**

- Read before class (2 total, one from each list) and [complete a reading response before class](../readings/):
    - Choose 1 from this list:
        - Nancy G. Leveson. 2017. “The Therac-25: 30 Years Later.” _IEEE Computer_. pp. 8-11. <http://ieeexplore.ieee.org/iel7/2/8102264/08102762.pdf>
        - Travis, Gregory. “How The Boeing 737 Max Disaster Looks To A Software Developer - IEEE Spectrum.” _IEEE Spectrum: Technology, Engineering, and Science News_, April 18, 2019. <https://spectrum.ieee.org/aerospace/aviation/how-the-boeing-737-max-disaster-looks-to-a-software-developer>.
    - _And_ choose 1 from this list:
        - Blaze, Matt. 2017. “Cybersecurity of Voting Machines.” Testimony to US House Committee on Oversight & Government Reform, November 2017. <http://oversight.house.gov/wp-content/uploads/2017/11/Blaze-UPenn-Statement-Voting-Machines-11-29.pdf>
        - Schneier, Bruce. 2018. “Patching Software Is Failing as a Security Strategy.” Excerpt from _Click Here to Kill Everybody: Security & Survival in a Hyperconnected World_ as published in _Motherboard_. <https://motherboard.vice.com/en_us/article/439wbw/patching-is-failing-as-a-security-paradigm>
        - Fazzini, Kate. 2019. "Cisco settles with cybersecurity whistleblower, setting a precedent." _CNBC_, July 31, 2019. <https://www.cnbc.com/2019/07/31/cisco-settles-with-cybersecurity-whistleblower-setting-a-precedent.html>
        - Watch/listen to: Schneier, Bruce. 2018. “Click Here to Kill Everybody.” _Talks at Google_. <https://www.youtube.com/watch?v=GkJCI3_jbtg>

- Homework:
    - Read for next class
    - Begin working on [Presentation II](../project#week-10-10-minute-presentation) (due week 10)
    - Begin working on [Final Proposal](../project##finals-week-final-proposal) (due finals week)

### Week 9

Taking Action

- Read before class (_Choose any two_) and [complete a reading response before class](../readings/):
    - Everton Bailey Jr. 2020. "Portland considering strictest ban on facial recognition technology in the U.S." _The Oregonian_, February 21, 2020. <https://www.oregonlive.com/portland/2020/02/portland-considering-strictest-ban-on-facial-recognition-technology-in-the-us.html>
        - Recommended accompaniment: Hector Dominguez and Kevin Martin. 2020. "Opinion: A ban on facial-recognition software protects privacy, civil rights" _The Oregonian_, February 12, 2020. <https://www.oregonlive.com/opinion/2020/02/opinion-a-ban-on-facial-recognition-software-protects-privacy-civil-rights.html>
    - Joan C. Williams and Sky Mihaylo. 2019. "How the Best Bosses Interrupt Bias on Their Teams" _Harvard Business Review_ <https://hbr.org/2019/11/how-the-best-bosses-interrupt-bias-on-their-teams>
    - Lou Moore. 2017. “Engineering Principles at Code for America.” _Medium (Code For America Blog)_, July 2017. <http://medium.com/code-for-america/engineering-principles-at-code-for-america-bda7b99740de>.
    - Interviews by Cameron Bird, Sean Captain, Elise Craig, Haley Cohen Gilliland and Joy Shan. 2019. “The Tech Revolt.” _California Sunday Magazine_. <https://story.californiasunday.com/tech-revolt>
    - Giancarlo Valdes. 2018. “How developers can reduce toxicity in online communities.”” _Rolling Stone_. March 20, 2018. <http://rollingstone.com/glixel/features/how-devs-can-reduce-toxicity-in-online-communities-w518104>  
    - AI Now Institute. 2018. “After a Year of Tech Scandals, Our 10 Recommendations for AI.” _Medium Blog_. <https://medium.com/@AINowInstitute/after-a-year-of-tech-scandals-our-10-recommendations-for-ai-95b3b2c5e5>
    - Jessie Daniels. 2019. "Why Now is the Time for Racial Literacy in Tech" _Data & Society Podcast_ (Podcast: 12 minutes) <https://listen.datasociety.net/why-now-is-the-time-for-racial-literacy-in-tech/>
    - Sarah Kessler (editor), Andy Wright & Sarah Emerson (interviewers). 2020. "Google Engineers, Uber Drivers, and the Voices of a New Tech Labor Revolution", _OneZero (Medium blog)_, Feburay 23, 2020. <https://onezero.medium.com/google-lyft-uber-workers-speak-out-7fdabaf4b348>

- Homework:
    - Read for next class
    - Continue working on [Presentation II](../project#week-10-10-minute-presentation) (due week 10)
    - Continue working on [Final Proposal](../project##finals-week-final-proposal) (due finals week)


### Week 10

- Read before class: n/a
- In Class:
    - Closing self-assessment, course reflection
    - Thing from the future exercise
- Homework:
    - Continue working on [Presentation II](../project#week-10-10-minute-presentation) and  [Final Proposal](../project##finals-week-final-proposal) (due finals week)


### Finals Week

#### Thu, Mar 19

**Due: Presentation II, upload slides by 4pm**

There is no final exam, but we will use the final exam period for presentations.

- Read before class: n/a
- In Class:
    - [Presentation II](../project#week-10-10-minute-presentation)
    - Closing self-assessment
- Homework: n/a

#### Fri, Mar 20

**Due: Final Proposal, 11:59p**
