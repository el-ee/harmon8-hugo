---
title: Sketchbook
cc: true
type: "class"
layout: "subpage"
icon: "fa-pencil-paintbrush"
weight: 200

quarter: 201901

menu:
    main:
        parent: "Teaching"
        identifier: "hci-201901-sketchbook"

---

Sketching is a common practice used by designers that fosters the ability to think critically about existing objects and interactions, and generate ways of improving them.

As part of this class, you will keep a sketchbook, in which you will think about objects or interactions in your daily life and sketch situations that are problematic or frustrating and ideas for how they could be improved.[^1]

### Assignment Overview

Each week, for weeks 2-9, you will need to make at least **three** entries in your sketching journal. At least one entry should be related to the week's theme. The other two can be on any topic related to HCI.

"Related to HCI" is meant to be understood broadly. You might sketch ideas for new interactions, processes, things, objects, or interfaces. You might sketch observations of people using a technology. Your sketches can focus on an entire system (e.g., how to follow a recipe) or one specific input/interaction (e.g., redesigning an oven knob).

Each sketch should tell a story or help you communicate something. It is ok to add some annotations, but you should not write lengthy paragraphs alongside your sketches.

**The focus is on the quantity of sketches and communication of your ideas, not the quality of your drawing skills.** The best way to have a good idea is to have lots of ideas. Futuristic, off-the-wall, and original ideas are welcome and encouraged!

### Logistics

All sketches should be entered in a single notebook.

- Each sketch should be on the front of a separate page.
- On the back of the page, please write the date, but leave everything else blank.

You must bring your sketchbook to class every Wednesday/Thursday (whichever is the second meeting of the week for you). During this period, you will break into groups and exchange feedback and critique.

- Group members will sign the back of your 3 chosen sketches for the week.
- Take notes (on the back of the sketch would be fine) about what feedback you receive. You will need this for the final reflection assignment.

 At the end of the quarter, you will write a 1000-word reflection essay about your sketching experience:

- usefulness in the identification of problems and design opportunities
- usefulness for the generation of design ideas
- what you learned from feedback during the group critique sessions
- whether you enjoyed the experience
- whether you gained confidence in your ideas (and perhaps drawing skills ;))

#### Resources

- Buxton, Bill. 2007. "Anatomy of Sketching." In *Sketching User
Experiences: Getting the Design Right and the Right Design*. Morgan
Kaufmann. [PDF (GDrive)](https://drive.google.com/open?id=1h33bo6bqfEpBWmezVYynKDk2IC8ByQ7e)
- Rohde, Mike. 2011. "Sketching: The Visual Thinking Power Tool." *A List
Apart* (blog). January 25, 2011.
<http://alistapart.com/article/sketching-the-visual-thinking-power-tool>.

#### Sketch Grading

There are two main parts to this assignment. Together, they count 16 points towards your final grade.

1. 8 weekly sketching assignments: 1 point each, for a total of 8 points towards your final grade.
2. Final reflection: 8 points towards your final grade.

Each weeks' sketching assignment is worth 1 point towards your final grade, for a total of 8 points for all 8 weeks of sketching. To earn this point each week, you must simply have all three sketches complete, signed, and dated from the Wednesday activity. If you do not have three sketches complete, or you do not have your sketching notebook with you you will receive zero points for the week. It's only worth one point; there will be no partial credit. I will circulate during class to check off everyone's sketches for the week.

### Sketching topics


#### S1: Objects and interactions in daily life

**Due in class, 2nd meeting, Week 2**

Find a public location where you can observe people for your D0 milestone. As part of your observation work, make some sketches about what you see. Please do put these in your sketchbook. You can take photos to integrate in your D0 turn-in.

Anything is fair game here. Some prompts to get you started:

- What do you notice people around you doing? How are they accomplishing these activities?
- What kind of technologies are people using?
- Who/what are people interacting with? How are they interacting?
- How could you make it easier? How could you make it more fun?
- How might this scene be different in ten years?
- What kind of new technologies would change this situation? How?


#### S2: Objects and Interactions in Daily Life II

**Due in class, 2nd meeting, Week 3**

What kind of new interactions or new things would change your daily life as a student at Portland State?

To help you get started, you might want to think about questions like:

- What kinds of problems do you have during the day? What frustrates you? What would make these frustrating situations better?
- Complete this sentence "wouldn't it be great if ..."
- What do you imagine campus will look like in 20 years? How might classes be different? How might people communicate differently? How might people pay for things differently? How might people travel differently?

The [antplanner website](https://antplanner.appspot.com/) is a great example of something that you might come up with if you were thinking "wouldn't it be great if I could see these classes I'm thinking about registering for in a calendar format instead of just a list?" Try to capture moments like this -- when you or someone you know is frustrated and find yourself imagining a way to improve something you deal with as part of your regular day.

#### S3: Experiencing Your Surroundings    

**Due in class, 2nd meeting, Week 4**

For this sketching topic, pay attention to how people around you are experiencing (or not experiencing) their surroundings.

Some questions to get you thinking:

Are people texting while walking? Is it sometimes a problem? What would make it better? Do you wish you could more easily send a message to someone while you’re on the move? How could you do that? Do you notice event fliers posted around campus? What would make you notice them more? OR would help you ignore them if they’re bothersome? What do you know about the places where you are walking – do you know their history, who else has been there? What things would you want to know when you’re out and about? Do you get lost riding bikes? Do you know where you’re allowed (and not allowed) to skateboard?

_If you have a sketch that you would be willing to share with the whole class, please post to the Twist board in the sketching channel._

#### S4: Mobile Working

**Due in class, 2nd meeting, Week 5**

Your sketches can have anything to do with mobile working or studying.

But, if you are not sure where to begin, here are some things other students in class are thinking about:

- How do people find locations to study?
- How do people find people to study with?
- What kinds of technologies do people use while studying?
- Are technologies like laptops or phones helpful when studying? Why? How could they be more helpful?
- Are technologies distracting from studying? When? Why? How could they be less (or more?) distracting?

_If you have a sketch that you would be willing to share with the whole class, please post to the Twist board in the sketching channel._

#### S5: Time Management

**Due in class, 2nd meeting, Week 6**

For this sketching topic, you might try to pay attention to things like: individual or group scheduling, calendaring, keeping track of tasks and chores, coordinating plans with other people, etc.



#### S6: Natural Environment

**Due in class, 2nd meeting, Week 7**

For this sketching topic, you should pay attention to your own and others'
relationships to the natural environment: How is
trash, recycling, to-go containers managed? How is waste managed in
a restaurant? In the university? On public streets and sidewalks?

What kinds of things are discarded? When and where?

How do you buy produce, meat, other food? What are the pathways between
a farm/source of food and yourself?

What kinds of things do people do outside? What are the ways that people
experience nature, parks, forests, lakes, oceans? What barriers to experience
exist?


#### S7: Sketch your project area!

**Due in class, 2nd meeting, Week 8**

You should be moving towards making a decision about what to prototype for your final project this week. Sketch some of your ideas for possible solutions.


#### S8: Technology Non-Use

**Due in class, 2nd meeting, Week 9**

For this weeks' sketching assignment you should try to focus on times or settings in which people are _not_ using digital computing technology.
- Where are these places? Why is digital technology not used? 
- Are there other non-digital / non-computation technologies that people are using? What are they? 
- What are those interactions like? 
- Are there digital technologies that you think could or should be added to these spaces/interactions/times?

-- AND/OR -- 

- Are there places in which you see a lot of digital technology use, but think that the experience of the place/time/interaction would be improved if technology was taken away? 
- What would interactions look like with a lessened technological presence? 

_Challenge:_ In addition to using sketching to demonstrate new ideas that you think are good; think this week about how you can also use sketching to communicate how/why a [current or new] technology might be a _bad_ idea.


## Final Reflection

Review your sketches, and spend some time thinking about how your relationship with sketching has changed (or not) over the duration of the quarter. Then write a reflection essay. The objective here is to thoughtfully reflect on your experience overall.

#### Logistics

**Due:** Sunday, March 17, 10:00am

**Turn In Directions:** You should upload a single `.docx` `.doc` or `.pdf` file to D2L.

- **If you upload a PDF:** you must note the word count at the top of the first page. Do NOT include the title, your name, the word count line, etc. in this count, ONLY include the main body of your reflection. **Do not lie about how long your reflection is. Lying about word count is a form of academic dishonesty and is grounds for an F on the assignment.**

#### Specification / Requirements

Your reflection essay should be 750-1200 words long.

At minimum, this reflection must include:

- A scan of your favorite and least favorite sketch; a summary of the feedback you received on these sketches during critique, and an explanation of why you chose them.

- Answers to all of the following questions:
    - Did you ever do any sketching before taking this class?
    - How did you initially feel about sketching?
        - How has that feeling changed?
    - What have you learned through sketching?
    - When was sketching most beneficial? When wasn't it?
    - Will you ever using sketching again in the future? Why or why not?
    - Were there any surprising moments?
    - What kind of feedback did you get in class?
        - What kind of feedback was helpful?
        - What kind of feedback wasn't helpful?
        - What did you learn about critique from this assignment?
    - Are there things that could improve the sketching component of this course?

#### Grading

The final reflection counts 8 points towards your final grade, and will be graded as follows:

- **1 point**: Grammar and writing clarity
- **1 point**: Specified length & includes 2 requested images
- **3 points**: Addresses questions in the prompt
- **3 points**: Expresses student opinion and contains thoughtful reflection on own learning


[^1]: This sketching project is modeled very closely on Jed Brubaker's <a href="http://www.jedbrubaker.com/teaching/inf132-sp2013/sketching-project/">sketching project</a> for Informatics 132, as taught in the Spring of 2013.
