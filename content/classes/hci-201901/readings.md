---
title: Reading Assignments
credits: <p>The structure of this course has benefited greatly from the input of <a href="http://lynndombrowski.com">Lynn Dombrowski</a>, <a href="https://jedbrubaker.com">Jed Brubaker</a>, <a href="http://www.gillianhayes.com/">Gillian Hayes</a>, <a href="http://www.dourish.com/">Paul Dourish</a>, and <a href="https://quote.ucsd.edu/lirani/">Lilly Irani</a>.</p>
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-book"
weight: 30

quarter: 201901

menu:
    main:
        parent: "Teaching"
        identifier: "hci-201901-readings"

---

You are expected to come to each class meeting prepared for engaged discussion. There will be a reading assignment for every class period. Unless there is a note specifying otherwise, you should read _all_ readings listed on a certain day.

**Readings for Weekly Meeting 1:** These readings focus on the practical skills that you need for your quarter-long project. I may **not** cover their entire contents in the 2 hour meeting, nonetheless you are still responsible for the content. Please **come to class prepared with any questions you may have after doing the readings**. Failure to read and comprehend these readings will likely result in your failure to succeed on the project.

**Readings for Weekly Meeting 2:** These readings are selected in order to help you understand the breadth of the field of HCI, the ways that design matters in the real world, and current and historical topics in HCI research. Each of these readings offers an opportunity for you to complete a reading response to prepare for discussion and demonstrate your comprehension of the material.

### Logistics

- There are **eleven (11)** opportunities to complete a reading response. You must complete at least **eight (8)**. If you complete more than 8, then the highest 8 grades will be kept, and the lowest grades dropped.
- **Submission:** You will submit your reading response by filling out the corresponding survey linked from the class [schedule](../schedule/). I recommend writing answers in a document first so that you can have a saved copy. You can then quickly copy and paste each answer into the survey fields to turn in.
- **Deadline:** In order to receive any credit at all for the reading response, you must complete it **before the start of class** on the day that we will discuss the reading. This means **before 2:00pm** (not at 2:01, nor 2:05, nor 2:15).


#### Reading Response Grading

Each reading response is worth up to three (3) points. Together they account for 24% on your total grade in the class.

They will be graded on a very simple scale:

| Points | Criteria |
|---:|:---|
| **3** | Moves beyond simply repeating others’ ideas to bring your own unique experiences, critiques, and perspectives into conversation with these authors. Could be used as an example of excellent work in a future class. |
| **2.8** | Correctly and completely answers each question in the reading response, demonstrating comprehension of the reading material. |
| **2.6** | All questions are completely answered, and most are correct. There may be minor misunderstandings in 1 or 2 of your answers, but it is clear that you mostly understood the reading. |
| **2.2** | Answers are complete, but more than 1 or 2 answers are clearly wrong, showing an incomplete understanding of the material. |
| **0** | Shows little to no understanding of the material, and/or incomplete. |

#### Extra Credit Opportunity

For each reading response completed above the minimum (8) and on which you also score at least a 2.6, you will receive one point of extra credit on your final grade.

- For example, if you complete 6 reading responses with a grade of 3, 4 reading responses with a grade of 2.6 and one reading response with a grade of 2.2, you would score:
    - 6 x 3 + 2 x 2.6 = 23.2 points for your reading response grade (the highest 8 scores)
    - 2 x 1 = 2 points of extra credit added at the end (one point for each other reading response with a score of 2.6 or more)

You can earn a maximum of 3 extra points on your final score if you complete all reading responses, and earn at least a 2.6 on each one.


### Reading questions

#### RR1: History

**Due**: Before class, Week 1, Meeting 2

**Survey Link**: <https://goo.gl/forms/1gfRLWXutSk7fpZC3>

A complete answer to each question should take about 2-5 sentences.

1. Before starting this class, what did you think "HCI" was all about? What did you think this class would be about?

2. Based on the interviews from the Moggridge book, why was Stu Card frustrated with precursors to HCI like 'Human Factors'? What did he want to do differently?

3. Based on the interviews from the Moggridge book, what was Tim Mott's initial reaction to the POLOS system when he first flew out to visit PARC? How did he go about deciding how to change the system?

4. What does Taylor mean by "world making" and how does it relate to design? Or, beyond 'the interface,' what work does design do?

5. What is the one key takeaway for you, after reading the Taylor article? -- OR -- What is one new question you have after reading the Taylor article?

6. Alex Taylor's article was written over 30 years after the pioneering work of Card and Mott discussed in the Moggridge book. Over these years, the concerns of HCI researchers have changed and shifted. We will discuss this more in class, but just from what you know right know (1) what do you think the core methods, goals, or questions of HCI were when the field was just starting? (b) what do you think are the core methods, goals, or questions today?

7. How do you think the field of HCI has impacted your own life? What would be different without it?

8. Thinking across all of the readings for today, and the first class meeting, has your question to answer 1 changed? If so, how?


#### RR2: Research Ethics

**Due**: Before class, Week 3, Meeting 2

**Survey Link**: <https://goo.gl/forms/cPHtizoCxG7OQvmY2>

A complete answer to each question should take about 2-5 sentences.

1. Identify 2-3 criteria from Mortensen's article that you think are most important. Explain why each is important to you.
2. Identify 2-3 criteria from Mortensen's article that you think would be most difficult to achieve. Explain why each is difficult or challenging.
3. Reflect on the project that you have begun working on in light of Mortensen's criteria. Evaluate your own work in terms of Mortensen's criteria. Explain how you are or are not meeting each criteria. Are there things that you will change moving forward, or if you were to do a similar project again?
4. Suchman focuses her paper around a concept of 'work' and 'work practice.' What does 'work practice' mean for Suchman?
5. What does it mean to make 'work practice' visible? Why is that something that might need to be done?
6. What are some of the effects of making something visible [that previously was not visible]?
7. What is the power of maps according to Wood (see p. 61) and how does that relate to the representations of work that Suchman is talking about?
8. How do Suchman's concerns relate to Mortensen's seven criteria? Do they raise any new questions or challenges?


#### RR3: Problematizing

**Due**: Before class, Week 4, Meeting 1

**Survey Link**: <https://goo.gl/forms/zEw0eTBDIuKQHiGA2>

1. Reading only the abstract, identify (1) what the researchers did (what was there general area of research and what methods did they use) and (2) what the researchers found.
2. Reading only the introduction, describe why this research project is important to do and relevant to the CHI community
3. Read through the related work section. Identify one contribution to the literature that this paper will make. To answer this you might think about things like: What work do the authors say they will build on? Where do the authors show that there are gaps or weaknesses in the current literature that their work will fill in?
4. Read through the methods section. How did the researchers decide who to interview for their study?
5. What kinds of topics did they ask about in their interviews?
6. Choose one of the three sociotechnical practices that workers use to try to address wage theft. Explain the role of technology in that situation.
7. Describe one limitation or challenge of trying to intervene in low-wage work through technology design.
8. Describe one way that the authors suggest technology could be useful in helping to address wage theft.



#### RR4: Including I

**Due**: Before class, Week 5, Meeting 2

**Survey Link**: <https://goo.gl/forms/pBFytugNOMu7HSdD2>

**Readings**:

- Rachlin, Benjamin. 2018. "Sailing Blind: How Technology Helps Visually Impaired Athletes Navigate New Waters." *Pacific Standard*, December 18, 2018. <https://psmag.com/social-justice/sailing-blind>.

- Abreu, Amelia. 2018. "Why I Won't 'Try on' Disability to Build Empathy in the Design Process (and You Should Think Twice...." *Prototypr (Medium)*, May 1, 2018. <https://blog.prototypr.io/why-i-wont-try-on-disability-to-build-empathy-in-the-design-process-and-you-should-think-twice-7086ed6202aa>.
- Microsoft. n.d. "Inclusive: A Microsoft Design Toolkit." Accessed December 28, 2017. <https://www.microsoft.com/design/inclusive/>.
    - Review the [Manual (PDF)](https://download.microsoft.com/download/b/0/d/b0d4bf87-09ce-4417-8f28-d60703d672ed/inclusive_toolkit_manual_final.pdf) _(link updated 2/4)_
    - Review the [Activities (PDF)](https://download.microsoft.com/download/b/0/d/b0d4bf87-09ce-4417-8f28-d60703d672ed/inclusive_toolkit_activities.pdf) _(link updated 2/4)_


1. According to Rachlin, what about sailing originally made it inclusive for blind people?
2. How have new technologies changed blind sailing?
3. According to Abreu, what are some examples of ways that designers try to build empathy with disabled people?
4. What does Abreu argue about 'trying on' disability as part of a design process? Include at least 2 of her reasons for why she takes her stance.
5. How does Abreau think we should include the concerns of disabled persons in our design process?
6. Give a short summary, in your own words, of each of the three inclusive design principles central to Microsoft's toolkit.
7. Are any of these readings directly relevant to your project? If so, how will you integrate their suggestions in your work this quarter?


#### RR5: Including II

**Due**: Before class, Week 6, Meeting 2

**Survey Link**: <https://goo.gl/forms/u4gGr9LGieH5DuXR2>

**Readings**:

- Schulman, Sarah. 2016. "Redefining the Politics of Inclusion with George and Dustin." *The Journal of Design Strategies* 8 (1): 50--55. <http://sds.parsons.edu/jds>. [Alternate PDF (GDrive)](https://drive.google.com/open?id=1I_wIhqt-6o36zVHpIaeD8PF8MbsbHmEC)
- Martin, Courtney. 2016. "The Reductive Seduction of Other People's Problems." *BRIGHT Magazine (Medium)*, January. <https://brightthemag.com/the-reductive-seduction-of-other-people-s-problems-3c07b307732d>.

1. Schulman writes about trying to change some of the vocabulary of design. For each of these three shifts that Schulman says we should make, describe what the shift is really about. That is, beyond just a word change, what change in design practice does each vocabulary shift entail?
    1. Imagining to Immersing
    2. Designing For to Making With
    3. Public to Private
2. Summarize the main argument that Courtney Martin makes in "The Reductive Seduction of Other People’s Problems."
3. What are two reasons that Martin argues "reductive seduction" is dangerous?
4. Thinking across these two articles, and the articles from last week, what are two main takeaways _for you_ after reading these four pieces? (Your answer should be at least 5 sentences long and must engage the content of at least 1 article from each week in a substantive way.)

#### RR6: Values

**Due**: Before class, Week 7, Meeting 2

**Survey Link**: <https://goo.gl/forms/WyACbRHhoo5jnPFO2>

**Readings**:

- Sengers, Phoebe. 2011. "What I Learned on Change Islands." *Interactions* 18 (2). <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1925820.1925830>
- Menegus, Bryan, and Kate Conger. 2018. "Microsoft Employees Pressure Leadership to Cancel ICE Contract." *Gizmodo*, June 19, 2018. <https://gizmodo.com/microsoft-employees-pressure-leadership-to-cancel-ice-c-1826965297>.
- Mansky, Jackie. 2018. "W.E.B. Du Bois' Visionary Infographics Come Together for the First Time in Full Color." Smithsonian. November 15, 2018. <https://www.smithsonianmag.com/history/first-time-together-and-color-book-displays-web-du-bois-visionary-infographics-180970826/>.

1. In the Gizmodo article, Mat Marquis is quoted as saying: "It would be easy to think of coding as neutral—we solve puzzles... Put a semicolon in the right place and an error goes away, make the website a little faster than it was yesterday; in a vacuum, it’s innocuous work. It’s important, though, to consider the bigger picture for the things we help to build—how can it be misused, who am I supporting with it, who benefits from it and who bears the costs?"
    - What did you think about this stance? Is technology neutral? Why or why not?
2. How does the article about W.E.B.Du Bois' visualizations contribute to your understanding of how design and culture might interact?
3. In the first page of her article, Sengers writes that design is part of an "ongoing process of modernization." Name and briefly describe all three of the values that Sengers identifies as related to this larger process.
4. Explain the relationships between IT and the cultural logics Sengers explores in this article. Does IT create these logics?
5. Phoebe Sengers ends her article with a question, “Can IT design as an influence compete with a pervasive cultural atmosphere of overwork and overload?” All three articles today examine the power and limitations of design. Thinking across all three articles for today, how would you answer Sengers' question? What do you think would be the best tactics or strategies for changing the cultural atmosphere she writes about?
6. Brainstorm a list of values that are important to your project for this class. List at least **4 values**.
7. For one of the values listed in the last answer, reflect on the meaning of the value in the context of your project. Your reflection should address all of the following questions:
    1. What does the value mean in the context of your project?
    2. Who is the value is important to?
    3. Does the value have different meanings or are there multiple ways to enact it?
    4. How is technology related?

 _Example:  My project is about family communication. In family life, family members often value ‘togetherness.’ This value is about spending time or being with another family member, and feeling connected. People sometimes enact this value by turning off their phones around the dinner table in order to feel more present and in the moment with other family members. Other times people enact this value by using their phones to share a photo with someone who cannot be present. The same technology (a phone) can be both supportive of this value (when people use it to share pictures), and can threaten this value (when people feel it is distracting)._


#### RR7: Human Factors

**Due**: Before class, Week 8, Meeting 2

**Survey Link**: <https://goo.gl/forms/uxlATsSw0zWcTeSm2>

**Readings**:

- Karafillis, Anastasios. 2012. "When You Shouldn't Use Fitts Law To Measure User Experience." *Smashing Magazine (Online Magazine)*, December.
<https://www.smashingmagazine.com/2012/12/fittss-law-and-user-experience/>.
- Gaver, William W. 1991. "Technology Affordances." In *Proceedings of the SIGCHI Conference on Human Factors in Computing Systems*, 79--84. CHI '91. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/108844.108856>.


1. Explain Fitt's law.
2. In the Smashing Magazine article, they divide Fitt's law into 4 'rules.' Choose two of these rules and explain the pros and cons of following them in different circumstances.
3. Give an example of a _good_ use of Fitt's law.
4. Explain what an affordance is.
5. Give an example of an affordance from your daily life that is **different** from the examples in the article (i.e. no door handles, onscreen buttons, etc...). Explain _why_ it is an affordance. _-- OR --_ Give an example of a time when you or someone you know had trouble using some kind of technology and the situation could have been improved with a better-designed affordance. Explain _how_ a better-designed affordance would make the situation better.
6. Explain how & why affordances are relevant to design. Why should designers care about this concept developed by psychologist JJ Gibson?
7. Could either Fitt's law or the concept of an affordance e used to improve the design of your final project? Explain why or why not.

#### RR8: Transforming

**Due**: Before class, Week 9, Meeting 1

**Survey Link**: <https://goo.gl/forms/FuXyhw0WZeo0Ypyu2>

**Readings:**

- Choose _one_:
    - Asad, Mariam, and Christopher A. Le Dantec. 2015. "Illegitimate Civic
    Participation: Supporting Community Activists on the Ground." In *ACM
    Conference on Computer Supported Cooperative Work & Social Computing*,
    1694--1703. CSCW '15. Vancouver, BC, Canada.
    <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2675133.2675156>.
    - Raval, Noopur, and Paul Dourish. 2016. "Standing Out from the Crowd:
    Emotional Labor, Body Labor, and Temporal Labor in Ridesharing." In
    *Proceedings of the 19th ACM Conference on Computer-Supported
    Cooperative Work & Social Computing*, 97--107. CSCW '16. New York, NY,
    USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2818048.2820026>.

**If you chose Asad & Le Dantec**:

1. Describe what Asad and LeDantec mean by "illegitimate civic participation."
2. Review the section ICTs and Civic Participation. Describe how ICTs relate to the three models of democracy that Asad and LeDantec highlight in reviewing van Dijk's prior work.
3. Review the background and methods section. Give a short (2-5 sentence) overview of the methods used by Asad and LeDantec, and the site where they conducted their research.
4. In the 'Illegitimacy in Action' section, Asad and LeDantec describe three OOHA practices that involved engaging with ICTs. Briefly explain each of these three practices.
5. In the 'design orientations' section, Asad and LeDantec highlight two examples of design approaches that might better support activist work. Give a brief explanation of each approach.
6. What new questions do you have after reading this article and/or what did you learn from it?
7. How do the arguments raised in this paper relate to prior course readings or discussions?

**If you chose Raval & Dourish**:

1. Describe what Raval and Dourish mean by "emotional labor."
2. Review the section 'Feminist Political Economy.' Briefly summarize / or give an example of each of their three main concerns (1-2 sentences each).
3. Review the methods section. Give a short (2-5 sentence) overview of the methods used by Raval and Dourish, and the site where they conducted their research.
4. In the "The work of rideshare" section, Raval and Dourish highlight two main findings from their research. Briefly describe the main points from each of these two sections.
5. In the "Discussion" section, Raval and Dourish make two propositions for the CSCW/HCI community. Briefly explain each proposition. What is the key takeaway that they are trying to communicate with each one?
6. What new questions do you have after reading this article and/or what did you learn from it?
7. How do the arguments raised in this paper relate to prior course readings or discussions?


#### RR9: Persuading

**Due**: Before class, Week 9, Meeting 2

**Survey Link**: <https://goo.gl/forms/T25UvymuJtWkc09v2>

**Readings**:

- Seaver, Nick. 2018. "Captivating Algorithms: Recommender Systems as Traps." *Journal of Material Culture*, December, 1359183518820366.
<https://doi-org.proxy.lib.pdx.edu/10.1177/1359183518820366>.
- AND, choose _one_:
    - Stark, Luke. 2018. "How Computer Science's View of the Brain as Just Another Machine Gave Rise to Cambridge Analytica." *Slate Magazine*, March 18, 2018. <https://slate.com/technology/2018/03/cambridge-analytica-and-the-long-history-of-computer-science-and-psychology.html>.
    - Brownlee, John. 2016. "Why Dark Patterns Won't Go Away." *Co.Design (Fast Company)*, August. <https://www.fastcodesign.com/3060553/why-dark-patterns-wont-go-away>.
    - Campbell-Dollaghan, Kelsey. 2016. "The Year Dark Patterns Won." *Co.Design (Fast Company)*, December. <https://www.fastcodesign.com/3066586/the-year-dark-patterns-won>.

1. Describe what 'captology' means as developed by BJ Fogg and his HCI lab at Stanford.
2. What understanding of an animal trap allows us to reframe it as a "persuasive technology" as opposed to merely a weapon?
3. How did the goal of recommender systems change over time? What did they originally set out to do? How did metrics for judging algorithmic success change over time?
4. At the start of his article, Seaver writes that 'the anthropology of trapping helpfully sidesteps this framing, providing a model for thinking that does not depend a strict dichotomy between the voluntary and coerced, the mental and the material, or the cultural and the technical.' By the time you got to the end of the article, do you agree or disagree with this proposition -- i.e., did it work? Explain why or why not.
5. What new questions do you have after reading this article and/or what did you learn from it?
6. For whichever article you chose as a companion piece (Stark, Brownlee, or Campbell-Dollaghan), summarize its central argument.
7. Explain how the arguments raised in Seaver's article relate to your chosen companion article.


#### RR10: Speculating

**Due**: Before class, Week 10, Meeting 1

**Survey Link**: <https://goo.gl/forms/RLp7wlGrgDkM2K7V2>

**Readings**:

- Wong, Richmond Y., and Vera Khovanskaya. 2018. "Speculative Design in HCI: From Corporate Imaginations to Critical Orientations." In *New Directions in Third Wave Human-Computer Interaction: Volume 2 - Methodologies*, edited by Michael Filimowicz and Veronika Tzankova, 175--202. Human--Computer Interaction Series. Cham: Springer International Publishing. <https://doi.org/10.1007/978-3-319-73374-6_10>.  [Author Copy (PDF)](http://people.ischool.berkeley.edu/~richmond/docs/wong-khovanskaya-2018-chapter-speculative-design-in-hci.pdf)

1. Based on the Wong & Khovanskaya article, describe what 'third wave' HCI is.
2. Briefly summarize the history of Speculative Design as a critical practice. Who were some of the key practitioners of critical design? What were their goals?
3. Is Speculative Design about predicting the future? If yes, explain why and how. If not, explain what it is about instead. Draw on one of the two examples of speculative design in HCI in your answer.
4. Briefly summarize the common story about Speculative Design and its relationship to third wave HCI. In this story, what made SD a third wave methodology/practice?
5. Briefly summarize the alternate origin story of Speculative Design that Wong and Khovanskaya outline. Who were the key players? What was the role of Speculative Design in these settings?
6. View one of the corporate concept videos linked in footnote 1 on page 187. After reviewing the video yourself, draw on examples from it to explain whether or not (and WHY) you agree with Wong and Khovanskaya's argument in section 10.5.
7. Summarize the three recommendations that Wong & Khovanskaya propose at the end of the article.

#### RR11: Troubling Design

**Due**: Before class, Week 10, Meeting 2

**Survey Link**: <https://goo.gl/forms/wkpc0S1fvWzzLd503>

**Reading**:

- Choose _one_:
    - Baumer, Eric P S, and M Six Silberman. 2011. "When the Implication Is Not to Design (Technology)." In *Proc. CHI 2011*, 2271--2274. CHI '11. Vancouver, BC, Canada: ACM. <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1978942.1979275>.
    - Irani, Lilly C., and M. Six Silberman. 2016. "Stories We Tell About Labor: Turkopticon and the Trouble with 'Design.'" In *Proceedings of the 2016 CHI Conference on Human Factors in Computing Systems*, 4573--4586. CHI '16. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2858036.2858592>.

**If you chose Baumer & Silberman:**

1. Summarize Baumer & Silberman’s argument for why we should not always frame design in terms of problems and solutions.
2. In “When the Implication is Not to Design (Technology),” Eric Baumer and Six Silberman present a set of three specific questions.
    1. State & then answer the first of these questions with regards to the design project you’ve been working on this quarter.
    2. State & then answer the second of these questions with regards to the design project you’ve been working on this quarter.
    3. State & then answer the third of these questions with regards to the design project you’ve been working on this quarter.
3. In the closing section, "Arguments for Practice," Baumer and Silberman conclude with 5 implications for HCI practitioners. Choose one of these, summarize it, and explain why or how it might be applied to some real world technology that you are familiar with.
4. What new questions do you have after reading this article and/or what did you learn from it?
5. How do the arguments raised in this paper relate to prior course readings or discussions?

**If you chose Irani & Silberman:**

1. Describe Turkopticon, practically. What is it? What does it let people do?
2. Describe some of the differences between depictions of Turkers (AMT Workers) and the Turkopticon designers in public discourse about Turkopticon. Use at least one example from the paper.
3. Give one example of how Irani and Silberman tried to respond to the "failure" of Turkopticon. In your opinion, how successful was their response? Explain why.
4. The title of this paper is Stories We Tell About Labor: Turkopticon and the Trouble with "Design." How would you summarize the 'trouble' with design, according to Irani & Silberman?
5. What new questions do you have after reading this article and/or what did you learn from it?
6. How do the arguments raised in this paper relate to prior course readings or discussions?
