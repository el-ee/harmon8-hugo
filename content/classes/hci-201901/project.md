---
title: Design Project
cc: true
type: "class"
layout: "subpage"
icon: "fa-shapes"
weight: 200

quarter: 201901

menu:
    main:
        parent: "Teaching"
        identifier: "hci-201901-design"

---


The design project is meant to give you hands-on experience with a variety of techniques for HCI design. By the end of the quarter you will have engaged in substantive individual as well as group work, and should have material of appropriate quality for including in a portfolio to demonstrate your knowledge of HCI methods.

In week 1, you will conduct observations of people using technology individually as a basis for inspiring your project.

In weeks 2-5, you will work with a team to conduct further research on a shared topic, and identify a problem that you will address through a design project.

In weeks 6-10, you will develop a response to your problem through prototyping and initial user evaluation.

During the scheduled finals time slot, you will present your work to the rest of the class.

Each of, you, individually, will close the term by writing a brief reflection on your experience working through a user-centered design process for the first time.

_Note: Directions for each milestone are subject to change until the date that
the milestone is assigned and discussed in class._

| Milestone | Individual/Group | Due Date | Points |
|:---|:---|:---|:--|
| [D0: Observation Report](#d0-observation-report) | Individual | Sun, Jan 13, 11:59p | 5 |
| [D1: Framing Your Design Challenge](#d1-framing-your-design-challenge) | Group | Sun, Jan 20, 11:59p | 5 |
| [D2: Research + Problem Statement](#d2-research-problem-statement) | Group | Sun, Feb 10, 11:59p| 10 |
| [D3: Design Proposal](#d3-design-proposal) | Group | Sun, Feb 24, 11:59p  | 10 |
| [D4: Evaluation Plan](#d4-evaluation-plan) | Group | Sun, Mar 3, 11:59p | 2 |
| [D5: Final Design Report](#d5-final-design-report) | Group | Sun, Mar 17, 11:59p | 8 |
| [D6: Case Study Presentation](#d6-case-study-presentation) | Group | In Class, Final Exam Period | 5 |
| [FR: Final Reflection](#individual-reflection) | Individual | Fri, Mar 22, 5p | 10 |


### D0: Observation Report

**This milestone should be conducted individually**

For this milestone, you will conduct at least 40 minutes of observation in public, in order to learn about how people use technology in a context that you find interesting. The goal of this milestone is to get you thinking about how some technology is really used in the world, and to inspire ideas of topics for your final project.

#### Logistics

**Assigned & Discussed in Class:** Monday, January 7

**Due:** Sunday, January 13, 11:59p

**Turn In:** A _**single**_, _**well-organized**_ `.docx` file to the appropriate folder on D2L.

#### Directions

You should conduct two periods of observation in a public location. Each period should be at least 20 minutes long. You should vary either the time or location of your 2 observation periods meaningfully.

Because this is a very short class assignment, and you are a novice, you should
not conduct any observations in a location where people would have a
reasonable expectation of privacy. **Exception:** _**With permission**_, you may observe someone else in this class if you want to study something that does not occur in a public location -- e.g., if you want to observe how someone follows a recipe to cook something in their kitchen. You must get my okay in advance for what you plan to observe.

- For example, you might observe for 20 minutes at the food trucks at lunch time, and come back and conduct observations again at the same location for 20 minutes at 5pm.
- Or, you might observe people at the bus stop near city hall from 4:45-5:15 on one day, and observe people at Urban Plaza on PSU campus from 12:00-12:20 on another day.

You may take field notes in a medium of your choice (e.g., on your phone, laptop, or in a small notebook).

- Field notes should total **at least 4 pages** in length. It's okay if they are really messy. Field notes will not be graded for grammar, etc. We just want to see that you took notes in the field. I should see things like time stamps, written notes, small sketches or maps of the location. You can see examples of my own field notes in the lecture slides from January 7.

- You should make **at least 4 sketches** while conducting observations, depicting scenes from your field site that are relevant to your understanding of how people are using (or not using) some technology.

#### Report Specification

Your report will contain two parts: a 1-2 page memo about your observations, and an appendix containing the raw materials documenting your work.

##### Memo

- **Summary (1-3 paragraphs):**
    - Date, times, and location of observations. How and why did you choose these locations/days/times?
    - List 3-5 interesting things you noticed. Explain why they are interesting.
    - Did you notice any problems that people were having with technologies or notice situations or experiences that you think could be better? Do you have any new ideas for a design project?

- **Reflection (1-3 paragraphs)**: Reflect on the experience of conducting observations.
    - Have you ever conducted observations before?
    - What new things did you notice that you have not paid attention to before?
    - What distracted you? Did you get bored?
    - What parts of this assignment were easy? What was challenging?
    - What would you do differently the next time you want to conduct observations?

##### Appendix

- **A copy of your field notes.**
    - If you only have hand-written notes, please scan these or take high resolution photographs so that you can embed them in your report. Do not turn in multiple images.
    - These should be at least 4 letter-size pages long (for example, if you took notes in a very small 3"x5" notebook, you should have ~16 pages).
- **The 4 sketches** you created while conducting observations.
    - Each sketch should be captioned with a brief (1-4 sentence) description of what it is showing.
    - Again, you must integrate these images into your report document. Do not upload separate images.
    - If sketches are already integrated as part of your field notes that is fine, you do not need to include them twice. However, please do include brief descriptions of each sketch. You can include these on an additional page following the field notes.


#### Grading

This assignment is worth five points. It will be graded fairly simply.

- **0 points**: Not turned in
- **3 points**: Only partially fulfills the requirements. Some sections are significantly lacking.
- **~~3.75~~ 4 points**: Mostly meets requirements, but something silly is wrong, e.g., only included 3 sketches instead of 4.
- **~~4.25~~ 4.5 points**: All three sections (summary, reflection, appendix) are complete and fully meet the requirements.
- **5 points**: Assignment goes beyond the basic requirements, and demonstrates a thoughtful engagement with the process of conducting observations. Reflection includes references to course readings and/or discussions.


### D1: Framing Your Design Challenge

This is your first group milestone, and should be a very short turn in. It is mostly just about team logistics, and making sure you are all set up for a successful quarter. Should be 5 very easy points.

#### Logistics

**Assigned & Discussed in Class:** Monday, January 14

**Due:** Sunday, January 20, 11:59p

**Turn In:** A _**single**_, _**well-organized**_ `.docx` file to the appropriate folder on D2L.

The memo should include:

- A team name.
- All team member names.

- A _brief_ (2-5 sentence) summary of your design challenge. You may use the [IDEO project framing template](https://drive.google.com/open?id=19mIcIXQWi2po8vULP6KRCijHGD2PLZvl), but it is not required. See <http://www.designkit.org/resources/1> for a full copy of the book this is from.

- A Team Contract:
    - A list of all team member names and email addresses.
    - A communication plan: how will you communicate with each-other? Will you make a multi-person private chat on Twist? Will you use email? Will you create a group SMS or Facebook message? I don't care what it is, but I want to know all team members are on the _same_ page.
    - A work plan:
        - Have you discussed each other's work and school schedules? If not, do that now and then answer this question with a Yes.
            - Does anyone on your team have a major time commitment this quarter that may make it hard for them to contribute to the project during some period of time (e.g., a week long work trip, a best friend's wedding, a really important midterm in a really hard class)? How will you work around these commitments?
        - When/how will you work outside of class? Will you have a regular meeting or schedule meetings as needed?
        - What tools will you use to collaborate: e.g., will you use Google Docs to draft text, and provide feedback to each other there? If not, what is your alternative plan?
    - An outline of individual responsibilities and project timeline: What are all the things that need to be done in each of the next 2 weeks so that you will be ready to turn in your D2 assignment on February 10? Who will take the lead on each of these tasks? Who will take the lead on motivating and coordinating the group, making sure everyone else has done their part?
    - An outline of individual contributions so far. What has each person done to contribute to this first milestone? (_You will write a brief summary of individual contributions with each milestone. Please know that I read these, and will adjust grades accordingly._)
- Request for Feedback:
    - What kind of feedback do you want from me right now? What questions do you have?

#### Grading

This is worth 5 points towards your final grade. All group members will receive the same grade.

- **0: Not turned in or contains plagiarized content.**
- **3: Turned in something.**
- **4: Memo addresses all items in the list above, at least superficially.**
- **5: Memo is not only complete but also thoughtful. It could be used as an example of excellent work in a future class.**


### D2: Research + Problem Statement

For this milestone, your team will conduct at least 4 interviews with stakeholders relevant to your design challenge. You will summarize your initial research findings, and revise your original design challenge into an actionable problem statement.

This is a group project.

**Due:** Sun, Feb 10, 11:59p

**Turn in Directions:** You should upload to D2L:

- 4 audio files or other proof of completing the interviews
- a _**single**_, _**well-organized**_ `.docx` file containing all other required contents.

**Grading:** This project is worth up to 10 points. All team members will receive the same grade unless there is a major work discrepancy, in which case I may lower the grades of those who do not contribute to the assignment.

#### Directions

1. Conduct at least 1 additional 20-minute observation.
2. Conduct at least 4 interviews. You should complete these interviews no later than **the start of Week 4**
3. Create an affinity diagram in class in Week 4 to begin analyzing your data.
4. Create one additional diagram of your choice.
5. Refine your initial design challenge into a short 1-2 paragraph problem statement. (We will discuss in class in Week 4.)
6. Create two personas / persona stories / modular personas. (We will discuss in class at the start of Week 5.)


##### Interview Requirements

- At least 3 of your interviews must be at least 10 minutes long. (You can have one failure! But after that, you need to redo them if they aren't lasting at least 10 minutes.)
- Prepare an interview guide with at least 10 questions on it before the first interviews.
    - Revise the interview guide between interviews
- Record the interview. You MUST SECURE EXPLICIT PERMISSION from the interviewee before starting the recording!
- No more than 1 interviewee may be in this class (across both sections).
- At least 1 interviewee must be someone who is NOT a computer science major.


##### A list of diagramming & mapping techniques:

- Experience Mapping from Adaptive Path [[Website]](http://mappingexperiences.com/) and [[Guide]](http://ellieharmon.com/wp-content/uploads/Adaptive_Paths_Guide_to_Experience_Mapping.pdf)
- IDEO HCD Toolkit: [Frameworks (Journey Maps, Relational Maps, 2x2s)](https://drive.google.com/open?id=11qd21HlISl2m57mkTW1YitRtT_NmIdEF)
- Universal Methods of Design: [Cognitive Mapping](http://ellieharmon.com/wp-content/uploads/UMD-Cognitive-Mapping.pdf)
- Universal Methods of Design: [Concept Mapping](http://ellieharmon.com/wp-content/uploads/UMD-Concept-Mapping.pdf)
- Universal Methods of Design: [Thematic Networks](http://ellieharmon.com/wp-content/uploads/UMD-Thematic-Networks.pdf)
- Universal Methods of Design: [Stakeholder Maps](http://ellieharmon.com/wp-content/uploads/UMD-Stakeholder-Maps.pdf)
- EMPATHY MAPS in the d.school [Understand Mixtape](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98)
- Please ask on Twist if you have another idea of a map or diagram you want to use to make sense of your data.


#### Document Specification

You should turn in a single document which includes, in an organized fashion:

1. **Cover Page:**
    - Project Title
    - All team member names & PSU Emails
    - **Contribution Summary:** What has each person done to contribute to this first major team milestone?
    - **Feedback Request:** What specific feedback would you like on this project? What questions do you have about moving forward with your project?

2. **Research Summary:**
    - **Observation Summary:**
        - 1-3 paragraphs summarizing your new observation.
            - Date, time, and location of observation.
            - How and why did you choose this location/day/time?
            - 3-5 key findings -- what did you learn and why is it relevant?
        - If there were any relevant findings from the observations that any team members completed for the D0 milestone, please briefly review those findings in a bulleted list.
    - **Interview Summaries:** For **each** interview, write a short (2-3 paragraph) summary of the interview. This should include:
        - The setting of the interview
        - The length of the interview
        - A general background / brief bio of the interviewee.
            - Is the interviewee in this class?
            - What is your relationship to this person (e.g., friend, family, roommate, classmate, stranger?)
            - What is their major?
            - Why did you choose to interview this person? What about them makes them a good candidate for an interview?
            - **Do NOT include any personally identifiable information (e.g. no names)**.
        - 3-5 key findings -- what did you learn and why is it relevant?
    - **Research Reflection:** 2-3 paragraphs reflecting on your experience conducting research for this milestone.
        - This reflection should include answers to questions such as: What did you do differently in this round of observations? Do you feel your observation skills have improved? What would you do differently the next time you conduct user research -- observations and research both? What kinds of things did you learn from interviewing vs. observation? What parts of this research were easy/challenging, and why? How and why were each of your four interviews different?

3. **Data Analysis Diagrams**
    1. A photo of the affinity diagram you made in class.
    2. A photo of the second diagram your team chose to make.
    3. A short reflection that answers each of these questions:
        - What 2nd diagram did your team choose to make?
        - Why did you chose this diagram?
        - Who participated in the 2nd diagramming activity?
        - How did this activity differ from affinity diagramming?
        - Were these diagramming activities useful? Why or why not?
        - What would you do differently next time?

4. **Problem Statement**
    - Refine your topic area into a clear problem statement that explains the problem that you will tackle in your project. This should be 1-2 paragraphs long, and meet the criteria discussed in class in Week 4.

5. **Personas / persona stories / modular personas**
    - Explain the problem you want to solve from the perspective of 2 prospective users. Each persona story should include at least 1/2 page of text as well as one image or visual, and **follow the guidelines discussed in class on Monday February 5**.

6. **Appendix**
    - **Interview Questions**
        - A photo or scan of the interview guide you used from each interview.
        - The TA must be able to tell from this photo/scan that you took
        notes as you went through the interview.
        - The TA should also be able to tell from this document that you
        revised your questions at least once.

#### Grading

- **0/10: Not turned in or contains plagiarized content.**
- **5/10: Turned in something that is partially complete.**
- **+1 point: Cover page & Document Organization** - Cover page includes all listed elements, and document is well-organized.
- **+1 point: Research components** - Research sections are complete and show evidence of completing 4 interviews + 1 observation period.
    - Half of the grade for the research component is based on your reflection quality: Is it thoughtful? Does it show evidence of your engagement with the course readings and/or other materials and of your learning?
- **+1 point: Data Analysis** - Both required diagrams are present and show evidence of completing the task.
    - Half of the grade for the data analysis component is based on your reflection quality: Is it thoughtful? Does it show evidence of your engagement with the course readings and/or other materials and of your learning?
- **+1 point: Problem Statement** - Problem statement is complete, and _meets criteria discussed in Week 4_: it is grounded in data and/or research, it is framed in a way that invites creative solutions (but does not specify a solution), it is specific about where/when/why/how a problem is occurring.
- **+1 point: Personas** - 2 personas are included, each includes some kind of image or diagram, each is grounded in data/research, each one tells a _story_.
    - A quarter of your personas grade is a subjective measure of presentation quality: how well have you designed your persona presentation? How well are they communicating your ideas? Does it appear that you put some thought into their visual layout or did you just throw a stock image on a page and write a paragraph underneath?

### D3: Design Proposal

For this milestone, you will come up with multiple solutions to your design problem, and propose one of them for moving forward with.

This is a group project.

**Due:** Sun, Feb 24, 11:59p

**Turn In Directions:** You should upload a _**single**_, **_well-organized_** `.docx` or `.pdf` file to D2L, containing all required components. **Just one file per group**

**Grading:** This project is worth up to 10 points. All team members will receive the same grade unless there is a major work discrepancy, in which case I may lower the grades of those who do not contribute to the assignment.

#### Directions

**Part 1: Brainstorming**

We will have a brainstorming session in class in Week 6 to get you started on this milestone.

You should aim to complete all of your brainstorming work **before** the first class meeting in Week 7.

1. **Identify Key User Needs**: You may already have these from your problem statement, but if not, spend 10 minutes brainstorming the key criteria for a solution. What are the important requirements that any design must meet in order to successfully respond to your problem? What is it that your users need/desire to be different? You might think of this in terms of the 'how might we' questions from last time. From the d.school mixtape:
    > What do you (and your team) know about this challenge that no one else has considered? Not in terms of your long-held expertise, but instead unique user-centered insights. What’s your specific vision (not solution) about how you want change people’s lives? Another way to think about this is: what assumption are you going to disrupt on your way to a successful solution?

    - Once you have a good sized list, work with your team to narrow it down to 3-5 critical needs/requirements. You will need to use these later to compare your various design alternatives.

2. **Brainstorming session 1**: Solutions I
    - Start in class, dedicate at least 20 minutes to this, with your **entire** team.
    - Thinking about your identified problem area, and _using your how-might-we questions as prompts,_ **identify at least 25 potential design solutions or innovations** with your team mates. These can be far-fetched ideas -- in fact, wild ideas are encouraged at this stage of the process!

3. **Brainstorming session 2**: Solutions II
    - Outside of class, with _at least 2 people not on your team_ and at least 1 team mate present
    - Facilitate a brainstorming session with people who aren't just group members. Identify at least **10 additional design solutions** -- or iterations on how you might implement one of your chosen solutions.

_**After the two brainstorming sessions**: Decide on 3 solutions that you will storyboard next week and consider in more depth. These should be sufficiently **different** from one another._

**Part 2: Storyboarding**

_We will discuss storyboards in class on the first meeting of Week 7. I recommend waiting
until then to start this portion of the milestone._

I am personally a big fan of simple paper sketches. However, you may prefer to use some online tool like, [StoryboardThat](http://www.storyboardthat.com)

For **each** of your three proposed design solutions, you should create **two** storyboards of 3-5 frames each. Each storyboard should depict a key use case for that design solution. Your storyboards must meet the requirements discussed in class in Week 7!

After creating the six storyboards (2 per design alternative x 3 design alternatives), you should decide, as a group, which one you are going to move forward with. In making this decision, you should consider how well each solution is likely to solve the problem you identified in D2, and how well it responds to the user needs/requirements you selected at the start of your brainstorming sessions.

#### Document Specification

Turn in a single, well-organized file, with all of the following sections:

1. **Cover Page:**
    - Project Title
    - All team member names & PSU Emails
    - **Contribution Summary:** What has each person done to contribute to this first major team milestone?
    - **Feedback Request:** What specific feedback would you like on this project? What questions do you have about moving forward with your project?

2. **User Needs Summary:** List your 3-5 most important how-might-we questions and/or your set of 3-5 key user needs. Describe how each question or specified user need relates to your prior research and your design problem (1-2 sentences each). Why are these questions or user needs the most important things to keep in mind as you transition into storyboarding and prototyping?

3. **Brainstorming Documentation:** A photo of your two brainstorming sessions. With each photo, include a note stating who facilitated the session and who participated in coming up with ideas.

4. **Design Alternatives:**
    _If you have revised your problem since D2, please re-state your revised problem at the start of the design alternatives section, before presenting any of your alternatives._
    - For _each_ of your three design alternatives:
        - Give a brief description or overview of the solution (2-3 sentences).
        - Include an image of your 2 storyboards for this solution. You should include a short textual description of the use case beneath each storyboard (1-3 sentences).
            - One of your storyboards may be created to depict the situation without your new technology, and the second storyboard to depict the situation _after_ your designed intervention. If you choose this route, make sure it is clear to the reader what's going on!
        - Explain how well this solution addresses the problem you identified in D3 (1-3 sentences).
        - Explain how this solution does or does not satisfy your 3-5 key user needs (2-5 sentences).

5. **Design Choice:** Write a 1-3 paragraph explanation about which design solution you will move forward with (or how you will combine your different solutions together). This paragraph should clearly explain:
    - what the design solution is
    - how it addresses your design problem
    - and *why* you chose this particular solution over the others. Reference your user research, problem statement, and key user needs.

6. **Reflection:** A one to three paragraph reflection on this milestone. You must, _at minimum_, respond to these questions:
    - How did the the brainstorming session with your team differ from the one with outsiders?
        - Which session produced the ideas that you chose for your project?
        - Which method was more valuable, or did they each produce different results?
        - What would you do differently the next time you need to brainstorm design ideas?
    - How did you decide what three design ideas to storyboard?
    - What did you learn about your project from creating the storyboards? Do you have different ideas now about your users? Did you use your personas to think about how to create the storyboards?
    - How did you decide which one solution to move forward with after your storyboarding? Was it challenging to choose which design alternative you wanted to move forward with?
    - What did you learn from this milestone? What would you do differently in the future?


#### Grading

- **0/10: Not turned in or contains plagiarized content.**
- **5/10: Turned in something that is partially complete.**
- **+.5 point: Cover page & Document Organization** - Cover page includes all listed elements, and document is well-organized.
- **+.5 point: User Needs Summary** - User needs summary is complete, and includes a thoughtful explanation of _why_ these user needs/requirements are especially important
- **+.5 point: Brainstorming** - Report includes documentation of 2 brainstorming sessions, including information about participants.
- **+1.5 points: Design Alternatives** - For EACH of your three design alternatives, you include:
    - a description of the solution
    - two storyboards showing this solution in context
    - an evaluation of how well this solution addresses the problem
    - an evaluation of how well this solution meets user needs
- **+.5 point: Design Choice** - After presenting all three possible alternatives, you make a compelling case for why you chose the solution you will move forward with.
- **+1.5 point: Reflection + Overall Quality** -  The reflection is comprehensive and, at minimum, responds to all questions listed above. For full credit, your report should show evidence throughout of your engagement with course readings/materials, and of your _learning_ even if all elements are not perfectly executed.
    - Because much of design work is about communication, half of this grade is devoted to overall presentation quality. For example, do your storyboards look neat and professional? Are they doing a good job of communicating your solution-in-context? Have you proof-read your text sections? Are they easy to read and understand?  Do you use things like headers and page breaks to separate content?

### D4: Evaluation Plan

This is a group project. It is worth 2 points.

This is a brief turn in to make sure you are on track with an evaluation and prototyping plan. This is adapted from the "Determine what to Prototype" activity in the IDEO UCD book.

#### Logistics

**Assigned & Discussed in Class:** Week 8, Meeting 1

**Due:** Sunday, March 3, 11:59p

**Turn In:** A _**single**_, _**well-organized**_ `.docx` or `.pdf` file to the appropriate folder on D2L.

#### Document Specification

The memo should include:

- Your team name.
- All team member names.
- Contribution summary for this milestone. Who did what?

- A _brief_ (2-5 sentence) summary of each of the following:
    - Your problem statement.
    - Your proposed solution.
    - A list of the key elements of your idea. What are all the pieces you _could_ test? This list might include particular interface features or questions about use-in-context.
    - Choose 2-3 questions you want to answer with your evaluation. _What part_ of your project do you want to test.
    - What evaluations will you conduct to answer these questions?
    - Think through what kind of prototype you need to build to answer these questions and conduct these types of evaluations. What will you build and how will you build it? _Hint: It may be easier to build multiple smaller prototypes, each targeted for a particular evaluation, rather than build one giant prototype that you try to use for everything._
- A list of responsibilities with a timeline:
    - Lead person for prototype building.
    - Date that prototype will be complete.
    - For each evaluation you will conduct:
        - What is it (think aloud, heuristic, cognitive walkthrough)?
        - Who is going to do it?
        - When will it be done?
        - When will the report/form be completed for that evaluation?
- Request for Feedback:
    - What kind of feedback do you want from me right now? What questions do you have?

#### Grading

This is worth 2 points towards your final grade. All group members will receive the same grade.

- **0: Not turned in or contains plagiarized content.**
- **1: Turned in something.**
- **2: Complete and high quality work.**

### D5: Final Design Report

For this milestone, you will create paper or other low-fi prototypes for your chosen design solution, and conduct some simple evaluations of the system.

This is a group project. It is worth 8 points.


#### Logistics

**Assigned & Discussed in Class:** Week 8, Meeting 1.

**Due:** Sun, Mar 17, 11:59p

**Turn In Directions:** You should upload a _**single**_, **_well-organized_** `.docx` or `.pdf` to D2L, containing all required components.

- THIS IS A GROUP PROJECT. Only one member of your group should upload a file to D2L. _Please reduce image sizes when you generate your final file._

##### Prototyping Tools

I recommend paper. Here is an example of using [paper prototyping for website development](http://alistapart.com/article/paperprototyping)

For mobile apps, you might consider using a tool like [POP App](https://marvelapp.com/pop/) to make your paper prototypes come alive!

You can do something similar with presentation software like [PowerPoint](http://boxesandarrows.com/interactive-prototypes-with-powerpoint/), also see this article on [PowerPoint](http://keynotopia.com/guides-ppt/).

You may also use specialized tools like [Sketch](https://www.sketchapp.com/), [UX Pin](https://www.uxpin.com/), [Balsamiq](http://balsamiq.com/) or [Mockingbird](https://gomockingbird.com/mockingbird/#). It will take you longer. You have been warned.

Some people like to use diagramming tools like Visio or OmniGraffle.


#### Directions

1. Create a prototype! See prototyping tools section above. Your prototype must be detailed enough to allow you to examine at least one of the use cases described in your storyboards turned in for the last milestone. If you have a major issue with this, you should meet with me about what is an appropriate scope for your prototype.
2. At least **two** of your team-members should **separately** conduct **one** self-reflective evaluation. You may choose to conduct either a cognitive walk-through or a heuristic evaluation. Each team mate can do the same one, or you can do different ones.
    - **Heuristics Details**: Consider [Jakob Neilsen's famous list](http://www.nngroup.com/articles/ten-usability-heuristics/) or others that we discussed in class, or another of your choosing. If you choose your own, you **must explain where you found it, why you chose it, and why I should believe it's a legit list.**
        - You should use this form for recording your heuristic evaluations: Usability Aspect Report [UAR Word Template](https://drive.google.com/open?id=1sv3dMNvk3_e9wv8-qJz-GlaRor2gXzm3)
    - **Cognitive Walkthrough Details**: These two write-ups give detailed examples of using the method: [Wharton et al 1994](https://drive.google.com/open?id=1Mk9txWxZB92AEghQNqZjX0v4JHFbyCba) OR [Spencer et al 2000](https://drive.google.com/open?id=1L8Aio1Ijve1tAccVVbWK_A8bPqM_x95L). You may follow the style of either one (they are different!). Regardless, you should use the form linked in the document specification below to record your findings.
        - Use this cognitive walk-through form for recording your evaluation: [CW Word Template](https://drive.google.com/open?id=1UWjvjp8xFHU0mYyRdzYNEzTBoJYwH6vk)

3. You should also recruit at least 2 people to participate in 2 separate short think-aloud exercises. Review the relevant sections in [this chapter](https://drive.google.com/open?id=1e23Pt4SyuuE2tNzrUa1iHg01P7-xuDzi) from Bill Buxton's sketching book for a detailed explanation. You will give them a task or two to do and then you will observe how they carry it out. You should use the [Usability Aspect Report](https://drive.google.com/open?id=1sv3dMNvk3_e9wv8-qJz-GlaRor2gXzm3) to record your observations.


#### Document Specification

Turn in A SINGLE well-organized file containing:

1. **Pictures & Description of your prototype**: include an explanation of what features or use cases your prototype allows you to text, a description of what prototyping methods/tools you chose, and why you made both of those decisions.
2. **Evaluation Summary & Next Steps** Based on all of your evaluations, write a brief report:
    - What evaluations did you conduct?
    - What were the key problems that you identified?
    - If you had more time to work on this project:
        - What would you change in response do feedback?
        - What would you not change about your design?
        - Would you want to conduct more evaluations before iterating the design or are you ready to change things now?
3. **Reflection** (3-6 paragraphs)
    - A reflection on the experience of prototyping
        - What worked well?
        - What did not work well?
    - Reflection on the evaluation process
        - At a high level, what kind of problems did each evaluation surface? Were they similar or different? Why do you think that is?
        - Which evaluation was most helpful (or were they all equally helpful)? Why?
    - What would you do differently next time regarding **both** prototyping and evaluation?
    - Did you learn anything from this exercise? If so, what? If not, why not?
4. **Appendix**: One evaluation report for each of your **four** evaluations (2x self-reflective evaluation + 2x think aloud):
    - [UAR Word Template](https://drive.google.com/open?id=1sv3dMNvk3_e9wv8-qJz-GlaRor2gXzm3) -- used for think aloud and heuristic evaluations
    - [CW Word Template](https://drive.google.com/open?id=1UWjvjp8xFHU0mYyRdzYNEzTBoJYwH6vk) -- used only for cognitive walkthrough

#### Grading

- 1 -- **Format and Presentation:** correct format and quality of presentation style
- 1 -- **Prototype:** images fully document the prototype; description answers all questions, throughly and clearly communicating the proposed solution, and what aspects of that solution can be tested with the prototype; compelling explanation of _why_ prototyping decisions were made.
- 2 -- **Evaluation Summary:** clear and complete summary of evaluation results; quality of presentation and degree to which the report is compelling in motivating the proposed next steps.
- 2 -- **Reflection:** completeness, includes references to course materials, is thoughtful and shows clear effort and evidence of learning
- 2 -- **Appendix (.5 per evaluation):** complete and correct use of templates to report evaluation results.



### D6: Case Study Presentation

During the Finals time slot, each team will give a short (5-10 minute)
presentation on their design project.

#### Logistics

**Assigned & Discussed in Class:** Week 9

**Due:** Slides must be turned in 24 hours before your presentation time. Presentations will take place during the final exam meeting.

**Turn In Directions:**
_Upload a `.ppt`, `.pptx`, or `.pdf` file with your presentation in it to D2L._

Your slides MUST be in PDF or PowerPoint format. No, you absolutely may NOT present from Google Slides or Prezi or any other online service.

Everyone will present off of the computer in the room.

#### Specification / Requirements

You should prepare your presentation as a human-centered design case study.

Walk the class through your project from start (research) to finish
(prototyping and evaluation). Tell us a little bit about each stage of
the project, and how you made different decisions between each part of
the design process.

You may want to review [Designing Case Studies: Showcasing a Human-Centered Design Process](https://www.smashingmagazine.com/2015/02/designing-case-studies-human-centered-design-process/) for inspiration.

**All team members should participate in the presentation.**

#### Grading

[Presentation Rubric](https://drive.google.com/file/d/1BUZBXkNnomtG07wQSA1Wg5EJ3BXHbDGp/view?usp=sharing)


### Individual Reflection

**This milestone should be completed individually. It is worth 10 points.**

In this assignment, you will each reflect, individually, on your experience going through one cycle of an HCI design process, and what you’ve learned in this class.

If you are a graduate student, make sure you attend to the additional requirements noted at the end.

#### Logistics

**Assigned & Discussed in Class:** Week 10

**Due:** Friday, March 22, 11:59pm

**Turn In Directions:** Upload a `.docx` or `.pdf` file to D2L. **No other formats are acceptable.**

- **If you upload a PDF:** you must note the word count at the top of the first page. Do NOT include the title, your name, the word count line, etc. in this count, ONLY include the main body of your reflection. **Do not lie about how long your reflection is. Lying about word count is a form of academic dishonesty and is grounds for an F on the assignment.**


#### Specification / Requirements

**Length**: 1000-2000 words

You should address all of the following questions, and in so doing, meaningfully reference at least 3 readings from the course.

1. **Personal Learning**
    - When you first signed up for this class, what goals did you have for this
        class? What did you hope to learn or get out of it?
    - Did your goals change at all over the quarter, or did new goals emerge?
        If so, how and why did these goals change?
    - How successful were you at achieving your goals? Why were you successful
        to this degree? What would have made you more successful?
    - What are the **three** most important things you’ve learned this quarter?
        Why are these things important to you?
    - What grade would you give yourself for this class, and how would you
        justify that grade? Are you satisfied with your performance and work in
        the class? Why or why not?
2. **HCI Design**
    - The next time you are working on a software project, will you use a human-centered process like we used in the class project? Why, how, to what extent, and/or why not?
    - Think about the different images of the HCI design process that we’ve seen this quarter – the messy cycle from Bill Moggridge’s book, the lifecycle loop from The UX Book, the hexagonal rainbow graphic from the Stanford d.school. What would your ideal HCI design process look like? Draw a picture or diagram. Explain the picture/diagram and give examples of the kinds of methods or tasks that should happen at each stage. Explain why this is your ideal process.


##### Graduate Students Only

**In addition** to the requirements above, you should also
include a third section in your document that contains a
750-word reflection on **how Human-Computer Interaction
matters for your own area of interest**. Because this course is
taught in a department in which HCI is not a specialization area,
your long term relationship to the material will depend on your
ability to see the connections between HCI and other areas
of computer science.

MS Students: You should write this reflection with regards to
your [track](https://www.pdx.edu/computer-science/track-courses).
If you have not yet chosen your track, you should simply pick one
that you are considering, and write this reflection in terms of
that area of computer science.

PhD Students: You should write this reflection with regards to
your own current or planned area of dissertation research. If you
are not yet sure about what area of research you will pursue,
you should choose an area from the
[MS Track Listing](https://www.pdx.edu/computer-science/track-courses).

This **additional** reflection must:

- Be at least 750 words long. (So, **_your total reflection,
    should be 1750-2750 words in length_**).
- Include a clear statement of your own area of interest or work in the first
    sentence  
- Reference at least _1_ peer-reviewed article/book from your own area of specialization.

#### Grading

- [Graduate Rubric](https://drive.google.com/open?id=1Ho8WWJ5cHdlEV65c7w775axIvGwHRJUn)
- [Undergraduate Rubric](https://drive.google.com/open?id=1HtdqvDrW_-u0NOZU-D3TYAaCM6SNP12D)
