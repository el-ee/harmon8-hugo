---
title: Schedule
credits: <p>The structure of this course has benefited greatly from the input of <a href="http://lynndombrowski.com">Lynn Dombrowski</a>, <a href="https://jedbrubaker.com">Jed Brubaker</a>, <a href="http://www.gillianhayes.com/">Gillian Hayes</a>, <a href="http://www.dourish.com/">Paul Dourish</a>, and <a href="https://quote.ucsd.edu/lirani/">Lilly Irani</a>.</p>
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-calendar-alt"
weight: 20

quarter: 201901

menu:
    main:
        parent: "Teaching"
        identifier: "hci-201901-schedule"

---

| Week | Meeting 1 | Meeting 2 | Sunday, 10a |
|:--|:--|:--|:--|
| [1](#week-1) | Observing | History [RR1](https://goo.gl/forms/1gfRLWXutSk7fpZC3) | [D0](../project#d0-observation-report) |
| [2](#week-2) | Designing | Interviewing [S1](../sketching/#s1-objects-and-interactions-in-daily-life) | [D1](../project#d1-framing-your-design-challenge) |
| [3](#week-3) | - NO CLASS -  | Research Ethics [RR2](https://goo.gl/forms/cPHtizoCxG7OQvmY2) [S2](../sketching/#s2-objects-and-interactions-in-daily-life-ii) |  |
| [4](#week-4) | Problematizing [RR3](https://goo.gl/forms/zEw0eTBDIuKQHiGA2) | Data! What [S3](../sketching/#s3-experiencing-your-surroundings) | |
| [5](#week-5) | Data! Who | Including I [RR4](https://goo.gl/forms/pBFytugNOMu7HSdD2) [S4](../sketching/#s4-mobile-working) | [D2](../project#d2-research-problem-statement) |
| [6](#week-6) | Brainstorming | Including II [RR5](https://goo.gl/forms/u4gGr9LGieH5DuXR2) [S5](../sketching/#s5-time-management) | |
| [7](#week-7) | Storyboarding | Values [RR6](https://goo.gl/forms/WyACbRHhoo5jnPFO2) [S6](../sketching/#s6-natural-environment) | [D3](../project#d3-design-proposal) |
| [8](#week-8) | Prototyping + Evaluating  | Human Factors [RR7](https://goo.gl/forms/uxlATsSw0zWcTeSm2) [S7](../sketching/#s7-sketch-your-project-area) | [D4](../project#d4-evaluation-plan) |
| [9](#week-9) | Transforming [RR8](https://goo.gl/forms/FuXyhw0WZeo0Ypyu2) | Persuading [RR9](https://goo.gl/forms/T25UvymuJtWkc09v2) [S8](../sketching/#s8-technology-non-use) | |
| [10](#week-10) | Speculating [RR10](https://goo.gl/forms/RLp7wlGrgDkM2K7V2) | Troubling Design [RR11](https://goo.gl/forms/wkpc0S1fvWzzLd503) | [D5](../project#d5-final-design-report), [SF](../sketching/#final-reflection) |
| [Finals](#finals-week) | Final Meeting: [Presentation (D6)](../project#d6-case-study-presentation) | Friday: [Final Reflection](../project#individual-reflection) | |

**Finals Week:** The final meeting is [scheduled by the registrar and is considered an integral part of the course](https://www.pdx.edu/registration/final-exams). Make plans now to attend at the proper time for your section.

## Details

The class will follow a regular pattern each week.

**Meeting 1 (Monday or Tuesday)**: we will focus on a practical skill that you will need in your project.

**Meeting 2 (Wednesday or Thursday)**: we will discuss a topic of importance within the field of HCI.

There are two sections of this class on offer this quarter. **You may only attend the section for which you are registered**.

**Deadlines**: Reading responses are due promptly at the start of class. Most other deadlines will be on **Sundays at 11:59p**.


### Week 1

#### Meeting 1: Observing

... and Sketching!

- Read Before Class:
    - nothing!
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1pcMbhpsT6wSN9jptYAfA7enDk_GoqdMc)
    - Review the [course policies](../policies/)
- Resources for Sketching Journal:
    - Buxton, Bill. 2007. "Anatomy of Sketching." In *Sketching User Experiences: Getting the Design Right and the Right Design*. Morgan Kaufmann. [PDF (GDrive)](https://drive.google.com/open?id=1h33bo6bqfEpBWmezVYynKDk2IC8ByQ7e)
    - Rohde, Mike. 2011. "Sketching: The Visual Thinking Power Tool." *A List Apart* (blog). January 25, 2011. <http://alistapart.com/article/sketching-the-visual-thinking-power-tool>.
- Resources for Observations:
    - Helsinki Design Lab / Sitra. n.d. *Ethnography Fieldguide*. <https://www.helsinkidesignlab.org/files/731/Field%20Guide%20English.pdf>.

- New Assignments:
    - RR1 due at the **start** of next class. See [Meeting 2](#meeting-2-history)
    - [D0: Observation Report](../project#d0-observation-report) **due Sunday, January 13, 11:59p**
    - Begin your [Sketchbook](../sketching), first sketch **due Meeting 2, week 2**

#### Meeting 2: History

- Read Before Class:
    - Moggridge, Bill. 2007. "Interviews with Stu Card and Tim Mott." In *Designing Interactions*, 1st ed. MIT Press. [PDF (GDrive)](https://drive.google.com/open?id=1xmvq9cxq3kwwTA8Iv2v-udZ3_CLmGTK1)
    - Taylor, Alex. 2015. "After Interaction." *Interactions* 22 (5): 48--53. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2809888>.
    - [Reading Response 1](https://goo.gl/forms/1gfRLWXutSk7fpZC3) ([directions](../readings))
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1BdI4wa5TslO3h2_qaVn9Tw0CXAow5_HT)

#### <span class="fal fa-calendar-day"></span> Sun, Jan 13, 11:59p

- **Due:**
    - [D0: Observation Report](../project#d0-observation-report)

### Week 2

#### Meeting 1: Designing

What are we doing in this class anyway? What is UX Design & Research? What kinds of problems can we use design to address?

- Read Before Class:
    - UX Booth Editorial Team. 2018. "Complete Beginner's Guide to Interaction Design." *UX Booth* (blog). August 15, 2018. <https://www.uxbooth.com/articles/complete-beginners-guide-to-interaction-design/>.
    - Armstrong, Ian. 2018. "The Evolution of UX Process Methodology." *UX Planet* (blog), March 3, 2018. <https://uxplanet.org/the-evolution-of-ux-process-methodology-47f52557178b?ref=uxdesignweekly>.
- In Class:
    - Form project groups
    - [Slides (PDF)](https://drive.google.com/open?id=1hnBN8BDCsbrC8H9BrIpNdOJulFv8ejW4)
- New Assignments:
    - [D1: Framing Your Design Challenge](../project#d1-framing-your-design-challenge) (**Due Sunday, January 20, 11:59p**)

#### Meeting 2: Interviewing
<!-- https://www.mindtheproduct.com/2018/08/cognitive-biases-the-questions-you-shouldnt-be-asking-by-cindy-alvarez/ -->
- Read Before Class:
    - Spradley, James P. 1979. "'Interviewing an Informant' and 'Asking Descriptive Questions.'" In *The Ethnographic Interview*. Holt, Rinehart and Winston. [PDF (GDrive)](https://drive.google.com/open?id=1o6ByTmLN0FCWkBw9t4CA3iAhMLlGG8Tq)
- In Class:
    - [Sketch Journal 1](../sketching)
        - Share with project group & refine your project ideas!
    - [Slides (PDF)](https://drive.google.com/open?id=1N0WnVEFDfUcz2C8t-9QWd8g0PvPviDsR)
- New Assignments:
    - [D2: Research + Problem Statement](../project#d2-research-problem-statement) (**Due Sunday, February 10, 11:59p**)

#### <span class="fal fa-calendar-day"></span> Sun, Jan 20, 11:59p

- **Due:**
    - [D1: Framing Your Design Challenge](../project#d1-framing-your-design-challenge)


### Week 3

#### Meeting 1: NO CLASS

- No regular meeting on Monday or Tuesday:
    - University is closed for MLK Day on Monday
    - I will have office hours during the Tuesday class period (2:00-3:50), open to students from both sections. FAB 120-15.

#### Meeting 2: Research Ethics

- Read Before Class:
    - Suchman, Lucy. 1995. "Making Work Visible." *Commun. ACM* 38 (9): 56--64. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/223248.223263>.
    - Mortensen, Ditte. 2018. "Conducting Ethical User Research." *The Interaction Design Foundation* (blog). July 24, 2018. <https://www.interaction-design.org/literature/article/conducting-ethical-user-research>.
    - [Reading Response 2](https://goo.gl/forms/cPHtizoCxG7OQvmY2) <!-- TODO: create and link RR2 --> ([directions](../readings))
- In Class:
    - [Sketch Journal 2](../sketching)
    - [Slides (PDF)](https://drive.google.com/open?id=1Y5I-TWUEHQd0p9RxSYu9G5Rd5XTb0VIO)
- New Assignment:
    - Make sure you are starting to work on your team project in earnest. **You will need to have interview data to work with in meeting 2 next week.**


### Week 4

#### Meeting 1: Problematizing

- Read Before Class:
    - Dombrowski, Lynn, Adriana Alvarado Garcia, and Jessica Despard. 2017. "Low-Wage Precarious Workers' Sociotechnical Practices Working Towards Addressing Wage Theft." In *Proceedings of the 2017 CHI Conference on Human Factors in Computing Systems*, 4585--4598. CHI '17. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/3025453.3025633>.
    - [Reading Response 3](https://goo.gl/forms/zEw0eTBDIuKQHiGA2)  ([directions](../readings))
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1v8-js8qP71PM0sTGrW6nMESDHonuVkxE)
- Project Reference:
    - Gygi, Craig, Bruce Williams, Neil DeCarlo, and Stephen Covey. n.d. "How to Write a Problem Statement for Six Sigma." *Dummies (blog)*. Accessed January 5, 2019. <https://www.dummies.com/careers/project-management/six-sigma/how-to-write-a-problem-statement-for-six-sigma/>.


#### Meeting 2: Data! What?

- Read Before Class:
    - Kolko, Jon. 2011. "Managing Complexity." In *Thoughts on Interaction
    Design*, 2nd ed. Morgan Kaufmann. [PDF (GDrive)]<https://drive.google.com/open?id=1VVPQM4C-07Gana9JPI45ZOz8c8gUmtcV>
    - IDEO HCD Toolkit Excerpts: [Download Ideas, etc.](https://drive.google.com/open?id=1DLIMEBGehOOZWpH0CeQouUAkAhpD_AdN)
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1Kxuijkb-4eKKj8FezLxzZfiHP_5R3USz)
    - [Sketch Journal 3](../sketching)

### Week 5

#### Meeting 1: Data! Who?

- Read Before Class:
    - Edeker, Kyra, and Jan Moorman. 2013. "Love, Hate, and Empathy: Why We
    Still Need Personas." *UX Magazine*, February.
    <https://uxmag.com/articles/love-hate-and-empathy-why-we-still-need-personas>.
- Project Resources:
    - Pruitt, John, and Jonathan Grudin. 2003. “Personas: Practice and Theory.” In Proceedings of the 2003 Conference on Designing for User Experiences  - DUX ’03, 1. San Francisco, California: ACM Press. <https://doi.org/10.1145/997078.997089>
    - SonicRim, "Personas: characters from inspirational stories of real people" <https://www.scribd.com/document/103060262/Personas-characters-from-inspirational-stories-of-real-people>
    - Bolt|Peters on Personas: [Trailer](https://vimeo.com/37941863) + [Slide Deck](https://speakerdeck.com/boltpeters/avoiding-bullshit-personas)
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1B43QdkP2_DFyyvRUC_9eaq62rVlhfVCf)


#### Meeting 2: Accessibility

- Read Before Class:
    - Rachlin, Benjamin. 2018. "Sailing Blind: How Technology Helps Visually Impaired Athletes Navigate New Waters." *Pacific Standard*, December 18, 2018. <https://psmag.com/social-justice/sailing-blind>.
    - Abreu, Amelia. 2018. "Why I Won't 'Try on' Disability to Build Empathy in the Design Process (and You Should Think Twice...." *Prototypr (Medium)*, May 1, 2018. <https://blog.prototypr.io/why-i-wont-try-on-disability-to-build-empathy-in-the-design-process-and-you-should-think-twice-7086ed6202aa>.
    - Microsoft. n.d. "Inclusive: A Microsoft Design Toolkit." Accessed December 28, 2017. <https://www.microsoft.com/design/inclusive/>.
        - Review the [Manual (PDF)](https://download.microsoft.com/download/b/0/d/b0d4bf87-09ce-4417-8f28-d60703d672ed/inclusive_toolkit_manual_final.pdf) _(link updated 2/4)_
        - Review the [Activities (PDF)](https://download.microsoft.com/download/b/0/d/b0d4bf87-09ce-4417-8f28-d60703d672ed/inclusive_toolkit_activities.pdf) _(link updated 2/4)_
    - [Reading Response 4](https://goo.gl/forms/pBFytugNOMu7HSdD2) ([directions](../readings))
- In Class:
    - [Sketch Journal 4](../sketching)
    - [Slides (PDF)](https://drive.google.com/open?id=1BAzuiIqgp6MlnwOmW_hz7NP-kkAgy2og)



#### <span class="fal fa-calendar-day"></span> Sun, Feb 10, 11:59p

- Due:
    - [D2: Team Problem Statement](../project#d2-team-problem-statement)

### Week 6

#### Meeting 1: Brainstorming

- Read Before Class:
    - Shedd, Catriona. 2013. "Tips for Structuring Better Brainstorming Sessions." *InspireUX (Online Blog)*, July. <http://www.inspireux.com/2013/07/18/tips-for-structuring-better-brainstorming-sessions/>.
    - [IDEO Brainstorming Tips](https://drive.google.com/open?id=1BCYlxkv-MJUxWc2hqrsJUk5G5LePTa1S)
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1B4ZFY5dU7qlX0T04AzPdv72skgoioZ1T)
- New Assignments:
    - [D3: Design Proposal](../project#d3-design-proposal) (**Due Sunday, Feb 24, 11:59p**)

#### Meeting 2: Inclusion II

... and Other People's Problems
<!--  add CFA software engineering thing -->
- Read Before Class:
    - Schulman, Sarah. 2016. "Redefining the Politics of Inclusion with George and Dustin." *The Journal of Design Strategies* 8 (1): 50--55. <http://sds.parsons.edu/jds>. [Alternate PDF (GDrive)](https://drive.google.com/open?id=1I_wIhqt-6o36zVHpIaeD8PF8MbsbHmEC)
    - Martin, Courtney. 2016. "The Reductive Seduction of Other People's Problems." *BRIGHT Magazine (Medium)*, January. <https://brightthemag.com/the-reductive-seduction-of-other-people-s-problems-3c07b307732d>.
    - [Reading Response 5](https://goo.gl/forms/u4gGr9LGieH5DuXR2) ([directions](../readings))
- In Class:
    - [Sketch Journal 5](../sketching)
    - [Slides (PDF)](https://drive.google.com/open?id=1hyx6g171mekgkshAka67IvnbOBjkU9IV)

### Week 7

#### Meeting 1: Storyboarding

- Read Before Class: <!-- Storyboards, IDEO Example next time -->
    - Little, Ambrose. 2013. "Storyboarding in the Software Design Process." UX Magazine (blog). February 8, 2013. <https://uxmag.com/articles/storyboarding-in-the-software-design-process>.
    - Schauer, Brandon. 2007. "Sketchboards: Discover Better + Faster UX Solutions." Adaptive Path (blog). December 14, 2007. <https://adaptivepath.org/ideas/sketchboards-discover-better-faster-ux-solutions/>.
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1FgvslOEbUHNQfYrZ-1sBrEfPOnbSlnkq)


#### Meeting 2: Values

- Read Before Class:
    - Sengers, Phoebe. 2011. "What I Learned on Change Islands." *Interactions* 18 (2). <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1925820.1925830>
    - Menegus, Bryan, and Kate Conger. 2018. "Microsoft Employees Pressure Leadership to Cancel ICE Contract." *Gizmodo*, June 19, 2018. <https://gizmodo.com/microsoft-employees-pressure-leadership-to-cancel-ice-c-1826965297>.
    - Mansky, Jackie. 2018. "W.E.B. Du Bois' Visionary Infographics Come Together for the First Time in Full Color." Smithsonian. November 15, 2018. <https://www.smithsonianmag.com/history/first-time-together-and-color-book-displays-web-du-bois-visionary-infographics-180970826/>.
    - [Reading Response 6](https://goo.gl/forms/WyACbRHhoo5jnPFO2) ([directions](../readings))
- In Class:
    - [Sketch Journal 6](../sketching)
    - [Slides (PDF)](https://drive.google.com/file/d/1BO0jJ0XSz_qtg_6SWbQYAbkUpnWFYPlK/view?usp=sharing) <!-- TODO: what stories does your object tell? -->

#### <span class="fal fa-calendar-day"></span> Sun, Feb 24, 11:59p

- Due:
    - [D3: Design Proposal](../project#d3-design-proposal)

### Week 8

#### Meeting 1: Prototyping

- Prototyping References:
    - Cao, Jerry. 2016. "Paper Prototyping: The 10-Minute Practical Guide." *Studio by UXPin* (blog). January 27, 2016. <https://www.uxpin.com/studio/blog/paper-prototyping-the-practical-beginners-guide/>.
    - Moggridge, Bill. 2007. "Prototypes." In *Designing Interactions*, 1st ed. MIT Press. [\[PDF\]](https://drive.google.com/file/d/1BRVfqvUsJKakP0IFnZ9iZ9_OSgjDwBgF/view?usp=sharing)
- Evaluation References:
    - Think Aloud:
        - Buxton, Bill. 2007. "Interacting with Paper." In *Sketching User
        Experiences: Getting the Design Right and the Right Design*. Morgan Kaufmann. [\[PDF\]](https://drive.google.com/file/d/1BPNKwTtvQUX21I_NNB5PRJHNE-YiZ14-/view?usp=sharing)
    - Cognitive Walkthrough:
        - Wharton, Cathleen, et al. 1994. "The Cognitive Walkthrough Method: A Practitioners' Guide." Chapter 5 in J. Nielsen and R. Mack (eds.), _Usability Inspection Methods_. Wiley. [\[PDF\]](https://drive.google.com/open?id=1Mk9txWxZB92AEghQNqZjX0v4JHFbyCba)
        - Spencer, Rick. 2000. "The Streamlined Cognitive Walkthrough Method, Working Around Social Constraints Encountered in a Software Development Company" _CHI 2000_. [\[PDF\]](https://drive.google.com/file/d/1L8Aio1Ijve1tAccVVbWK_A8bPqM_x95L/view?usp=sharing)
- In Class:
    - [Slides (PDF)](https://drive.google.com/file/d/1BeJ53q6bqk2F9_wg8R3atkd6PLOddLf-/view?usp=sharing)
- New Assignments:
    - [D4: Evaluation Plan](../project#d4-evaluation-plan) (**due Sunday, March 2, 11:59p**)

#### Meeting 2: Human Factors

- Read Before Class:
    - Karafillis, Anastasios. 2012. "When You Shouldn't Use Fitts Law To Measure User Experience." *Smashing Magazine (Online Magazine)*, December. <https://www.smashingmagazine.com/2012/12/fittss-law-and-user-experience/>.
    - Gaver, William W. 1991. "Technology Affordances." In *Proceedings of the SIGCHI Conference on Human Factors in Computing Systems*, 79--84. CHI '91. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/108844.108856>.
    - [Reading Response 7](https://goo.gl/forms/uxlATsSw0zWcTeSm2)  ([directions](../readings))
- In Class:
    - [Sketch Journal 7](../sketching)
    - [Slides (PDF)](https://drive.google.com/open?id=1FvG5B8UK5PIG1QmiRFppTVfAZabzI1JB)

#### <span class="fal fa-calendar-day"></span> Sun March 2, 11:59p

- Due:
    - [D4: Evaluation Plan](../project#d4-evaluation-plan)

### Week 9

#### Meeting 1: Transforming Society

- Read Before Class:
    - Choose _one_:
        - Asad, Mariam, and Christopher A. Le Dantec. 2015. "Illegitimate Civic     Participation: Supporting Community Activists on the Ground." In *ACM     Conference on Computer Supported Cooperative Work & Social Computing*,     1694--1703. CSCW '15. Vancouver, BC, Canada.     <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2675133.2675156>.
        - Raval, Noopur, and Paul Dourish. 2016. "Standing Out from the Crowd:     Emotional Labor, Body Labor, and Temporal Labor in Ridesharing." In     *Proceedings of the 19th ACM Conference on Computer-Supported     Cooperative Work & Social Computing*, 97--107. CSCW '16. New York, NY,     USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2818048.2820026>.
    - [Reading Response 8](https://goo.gl/forms/FuXyhw0WZeo0Ypyu2) ([directions](../readings))
- In Class:
    - No Slides
- New Assignments:
    - [D5: Final Design Report](../project#d5-final-design-report) (**due Sunday March 17, 11:59p**)
    - [D6: Case Study Presentation](../project#d6-case-study-presentation) (**to be presented during Final Exam Block**)

#### Meeting 2: Persuading

- Read Before Class:
    - Seaver, Nick. 2018. "Captivating Algorithms: Recommender Systems as Traps." *Journal of Material Culture*, December, 1359183518820366. <https://doi-org.proxy.lib.pdx.edu/10.1177/1359183518820366>.
    - AND, choose _one_:
        - Stark, Luke. 2018. "How Computer Science's View of the Brain as Just     Another Machine Gave Rise to Cambridge Analytica." *Slate Magazine*,     March 18, 2018.     <https://slate.com/technology/2018/03/cambridge-analytica-and-the-long-history-of-computer-science-and-psychology.html>.
        - Brownlee, John. 2016. "Why Dark Patterns Won't Go Away." *Co.Design     (Fast Company)*, August.     <https://www.fastcodesign.com/3060553/why-dark-patterns-wont-go-away>.
        - Campbell-Dollaghan, Kelsey. 2016. "The Year Dark Patterns Won."     *Co.Design (Fast Company)*, December.     <https://www.fastcodesign.com/3066586/the-year-dark-patterns-won>.
    - [Reading Response 9](https://goo.gl/forms/T25UvymuJtWkc09v2)  ([directions](../readings))
- In Class:
    - [Sketch Journal 8](../sketching)
    - No Slides
- New Assignments:
    - [Sketching Reflection](../sketching#final-reflection) (**due Sunday March 17, 11:59p**)


### Week 10

#### Meeting 1: Speculating

- Read Before Class:
    - Wong, Richmond Y., and Vera Khovanskaya. 2018. "Speculative Design in HCI: From Corporate Imaginations to Critical Orientations." In *New Directions in Third Wave Human-Computer Interaction: Volume 2 - Methodologies*, edited by Michael Filimowicz and Veronika Tzankova, 175--202. Human--Computer Interaction Series. Cham: Springer International Publishing. <https://doi.org/10.1007/978-3-319-73374-6_10>.  [Author Copy (PDF)](http://people.ischool.berkeley.edu/~richmond/docs/wong-khovanskaya-2018-chapter-speculative-design-in-hci.pdf)
    - [Reading Response 10](https://goo.gl/forms/RLp7wlGrgDkM2K7V2)  ([directions](../readings))
- In Class:
    - [Slides (PDF)](https://drive.google.com/open?id=1I1JyawWaNdMRT_1qg8yt22wJyg_WUoNE)


#### Meeting 2: Troubling Design

- Read Before Class:
    - Choose _one_:
        - Baumer, Eric P S, and M Six Silberman. 2011. "When the Implication Is     Not to Design (Technology)." In *Proc. CHI 2011*, 2271--2274. CHI '11.     Vancouver, BC, Canada: ACM. <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1978942.1979275>.
        - Irani, Lilly C., and M. Six Silberman. 2016. "Stories We Tell About     Labor: Turkopticon and the Trouble with 'Design.'" In *Proceedings of     the 2016 CHI Conference on Human Factors in Computing Systems*,     4573--4586. CHI '16. New York, NY, USA: ACM.     <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2858036.2858592>.
    - [Reading Response 11](https://goo.gl/forms/wkpc0S1fvWzzLd503) ([directions](../readings))
- In Class:
    - [Slides](https://drive.google.com/open?id=1I1cIsXLQOjmtrQOh1mjI5hLjYAcjnCQS)
- New Assignments:
    - [Individual Design Reflection](../project#individual-reflection) (**due Friday March 22, 5pm**)

#### <span class="fal fa-calendar-day"></span> Sun, Mar 17, 11:59p

- Due:
    - [D5: Final Design Report](../project#d5-final-design-report)
    - [SF: Sketching Reflection](../sketching#final-reflection)


### Finals Week

#### Final Exam Period

There is no final exam in this course. Instead, the final meeting will be used for final presentations. The time of this meeting is [scheduled by the registrar and is considered an integral part of the course](https://www.pdx.edu/registration/final-exams). You must plan to attend at the scheduled time.

- MW Class:
    - Weds, March 20, 12:30-2:20
- TR Class:
    - Mon, March 18, 10:15-12:05
- In Class:
    - [D6: Case Study Presentation](../project#d6-case-study-presentation)

#### <span class="fal fa-calendar-day"></span> Friday, March 22, 11:59p

- Due:
    - [Individual Design Reflection](../project#individual-reflection)
