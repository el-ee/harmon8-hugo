---
title: Schedule
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-calendar-alt"
weight: 500

menu:
    main:
        parent: "Teaching"
        identifier: "cs199-201901-schedule"

quarter: 201901
---


| Week | T | R | S |
|:---|:---|:---|:---|
| 1 | Microbits I | Microbits II | P1 |
| 2 | JS: Intro | JS: Conditionals + Random | - |
| 3 | JS: Variables | JS: Functions | RR1 |
| 4 | _JS Bonus: Arrays_  | Demos + C&S 1  | P2 |
| 5 | Py: Variables, Input  | Py: Conditionals  | - |
| 6 | Py: Functions  | Py: Loops  | RR2 |
| 7 | _Py Bonus: Recursion_  | Demos + C&S 2  | P3 |
| 8 | Py: Strings  | Py: Lists  | - |
| 9 | Py: Files  | Demos + Review  | RR3 |
| 10| Final Exam Review | C&S 3 + Closing Surveys | P4 |
| **F** | **Final Exam<br/>Tue Mar 19, 5:30p - 7:20p** | | |

**Finals Week:** There will be a final exam in this class. The time is [scheduled by the registrar](https://www.pdx.edu/registration/final-exams). Make plans now to attend at the proper time.


### Unit 1: Microbits

#### T 8-Jan

Course Introduction, Microbit Introduction

- In Class:
    - [Self-Asessment Survey](http://tinyurl.com/199-intro-survey/)
    - Read. The. Course. [Policies](../policies/).
    - ~~Discuss computing + society topics~~
    - Microbit: Rock Paper Scissors Game
    - [Overview Slides (PDF)](https://drive.google.com/open?id=1TlURzEbDTCIuVH08w8Zbph5o8Wz_NQLS)
    - [In-class handout (PDF)](https://drive.google.com/open?id=1S010prK_wqpF2ROCokjW-70pd4EOH3QP)
- TODO: 
    - Visit my office hours on Mon Jan 14 or Wed Jan 16 _or_ [Schedule a 1-on-1 with me](/~harmon8/faq/#meeting-with-me) for a 15 minute block, sometime **during week 2**.

#### R 10-Jan

Microbits II

- In Class:
    - Review Rock Paper Scissors game
    - Discuss [P1 (Microbit)](../projects/#p1-microbit) in detail
    - Discuss computing + society topics + sign up 
    - Simple Dot Game: Functions

#### Deadline: S 13-Jan, midnight
- Due:
    - [P1 (Microbit)](../projects/#p1-microbit)


### Unit 2: Graphics with Javascript

#### T 15-Jan

Introduction to JavaScript and text-based programming, using <http://repl.it>, using the processing library, drawing shapes, coordinate grids.

- Before class:
    - Create an account on <http://repl.it>
- _New Assignments:_
    - [Project 2](../projects/#p2-javascript), Due Sunday, 3-Feb, midnight


#### R 17-Jan


- Before Class:
    - [Learning on Khan Academy](https://www.khanacademy.org/computing/computer-programming/programming/intro-to-programming/a/learning-programming-on-khan-academy)
    - [KA: Drawing Basics (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/drawing-basics/pt/making-drawings-with-code)
    - [KA: Coloring (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/coloring/pt/coloring-with-code)
- In Class:
    - Drawing with processing, using the documentation. 

#### T 22-Jan


- Before Class:
    - [KA: Variables (all 6 sections)](https://www.khanacademy.org/computing/computer-programming/programming/variables/pt/intro-to-variables)
- In Class:
    - Variables

#### R 24-Jan

- Before Class:
    - [KA: Functions (**only the first 4 parts**)](https://www.khanacademy.org/computing/computer-programming/programming/functions/pt/functions)
- In Class: 
    - Functions, functions with parameters.
- New Assignments:
    - Computing and Society Reading 1: Bitcoin & Blockchain
        - Read (Choose one): 
            - Both of these articles:
                - Wagner, Christian. 2013 “A Bitcoin FAQ.” Accessed January 15, 2019. <https://brokenlibrarian.org/bitcoin/>.
                - Weaver, Nicholas. 2018. “Risks of Cryptocurrencies.” Commun. ACM 61 (6): 20–24. <https://doi.org/10.1145/3208095>.
            - Or, this one article:
                - Swartz, Lana. 2017. “Blockchain Dreams: Imagining Techno-Economic Alternatives After Bitcoin.” In Another Economy Is Possible, edited by Manuel Castells, 24. Polity. [Author PDF](http://llaannaa.com/papers/Swartz_Blockchain_Dreams.pdf)
        - Provocation Group: [Provocations](../readings/), due Sunday, 27-Jan, midnight
        - Response Group: [Substantive comment](../readings/) in response to someone else's provocation, due, 2pm, Thurs 31-Jan.

#### Deadline: S 27-Jan
- Due: 
    - Reading Provocation, midnight

#### T 29-Jan

- In class:
    - Bonus: Arrays
- New Assignment:
    - **Take Home Midterm**, due Tuesday 5-Feb, start of class


#### R 31-Jan

- In class:
    - Computing and Society Conversation #1
    - Javascript project demos
- Due: 
    - Provocation Response, 2pm


#### Deadline: S 3-Feb 
- Due:
    - [Project 2](../projects/#p2-javascript), midnight
    

### Unit 3: Python I

#### T 5-Feb

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 2
- In Class:
    - Input, output, variables
    - [In-class worksheet](https://drive.google.com/open?id=19EIZRb9BFQjusvSJx5buAoIFF-CZQI2A)
    - [PEP-8: Python Style Guide](https://www.python.org/dev/peps/pep-0008/)
- Due:
    - Take Home Midterm, start of class.
- _Assigned:_
    - [Project 3](../projects/#p3-python-i), due Sunday 24-Feb, midnight


#### R 7-Feb

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 3
- In Class:
    - Conditionals, random numbers


#### T 12-Feb  

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 4
- In Class:
    - Functions


#### R 14-Feb

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 5
- In Class:
    - Loops
- New Assignments:
    - Computing and Society Reading 2: Data Analysis + Smartphone Impacts
        - Read: Orben, Amy, and Andrew K. Przybylski. 2019. “The Association between Adolescent Well-Being and Digital Technology Use.” Nature Human Behaviour, January, 1. https://doi.org/10.1038/s41562-018-0506-1. [PDF (Google Drive)](https://drive.google.com/open?id=1jg9KXCNNbPMeoDNOn0Rly8yDztqh-r3e)
        - Provocation Group: [Provocations](../readings/), due Sunday, 17-Feb, midnight
        - Response Group: [Substantive comment](../readings/) in response to someone else's provocation, due, 2pm, Thurs 21-Feb.

#### Deadline: S 17-Feb
- Due: 
    - Reading Provocation, midnight

#### T 19-Feb

- In Class: 
    - Loops Review
    - Bonus: Recursion

#### Thu 21-Feb
- In Class:
    - Computing and Society Conversation #2
    - Python Project I Demos
- Due:
    - Provocation Responses, 2pm.


#### Deadline: S 24-Feb

- Due:
    - [Project 3](../projects/#p3-python-i), midnight


### Unit 4: Python II

#### T 26-Feb

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 6

- In Class:
    - Strings


#### R 28-Feb

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 7
- In Class:
    - Lists 



#### T 5-Mar

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 8
- In Class:
    - Files, CSV format, data processing
        
#### R 7-Mar
- In Class:
    - Python II Demos
- New Assignments:
    - Computing and Society Reading 3: Security & Privacy
        - Read: 
            - Naughton, John. 2019. “‘The Goal Is to Automate Us’: Welcome to the Age of Surveillance Capitalism.” _The Observer_, January 20, 2019, sec. Technology. <https://www.theguardian.com/technology/2019/jan/20/shoshana-zuboff-age-of-surveillance-capitalism-google-facebook>.
            - Schneier, Bruce. 2018. “Why an Insecure Internet Is Actually in Tech Companies’ Best Interests.” _ideas.ted.com_ (blog). October 26, 2018. https://ideas.ted.com/why-an-insecure-internet-is-actually-in-tech-companies-best-interests/.
        - Provocation Group: [Provocations](../readings/), due Sunday, 10-Mar, midnight
        - Response Group: [Substantive comment](../readings/) in response to someone else's provocation, due, 2pm, Thurs 14-Mar.


#### Deadline: S 10-Mar
- Due:
    - Reading Provocation, midnight

#### T 12-Mar

- In Class:
    - Exam Review
    
#### R 14-Mar

- In Class:
    - Computing and Society Conversation 3
    - End of term surveys, including closing Self-Assessment
- Due:
    - Provocation Response, 2pm.

#### Deadline: S 17-Mar
- Due:
    - [P4 - Python](../projects/#p4-python-ii)

#### Final Exam

Tuesday, March 19, 5:30p-7:20p, ASRC 230

- In class:
    - Final Exam

