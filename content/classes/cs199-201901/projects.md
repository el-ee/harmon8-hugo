---
title: Programming Projects
cc: true
type: "class"
layout: "subpage"
icon: "fa-laptop-code"
weight: 1000

menu:
    main:
        parent: "Teaching"
        identifier:  "cs199-201901-projects"
        
quarter: 201901
---

Four programming projects will offer you the opportunity to practice and develop your skills this quarter.


### P1: Microbit

In this project, you will explore the microbit environment in order to familiarize yourself with some basic programming concepts.


#### Logistics

**Deadline:** Sunday, January 13, 11:59p

**What to turn in:**
1. **Your Code:** a `.hex` file that you save from the microbit website with your code*
1. **Your Report:** in either `.docx` or `.pdf` format

**How to turn it in:** D2L, in the folder **P1: Project 1 (Microbit)**




#### Resources & Key concepts

In this project, you will explore some of these core programming concepts:

> input (events), output, on start block, random numbers, variables, sprites, coordinate grid, forever loop, conditionals, functions

#### Specification: Program

In this project, you will create a simple program in the micro:bit environment that demonstrates your own creativity and explores some programming concepts that we will be reinforcing throughout the terms. There are only a few key requirements:

- You demonstrate the use of at least 6 of the 10 concepts in the list 
- Your program is different from the programs made in class
- Your program has a _point_. It does _something_ that you can explain in words that makes sense (i.e., you did not just drag random blocks onto the screen and say look it does a thing, but who knows what).
- Your program is something novel created by _you_ and not copied off of a micro:bit demo site or implemented by following a YouTube video, etc.


#### Specification: Report


1. Your Program:
    - What does your program do? (Explain it from the user's point of view.)
    - Why did you choose to make this program? 
    - What grade would you give yourself on this project, and how would you justify that grade?
3. Your Experience:
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During week 1, how much time did you spend, outside of class, working on this project, and all other activities related to this class? 
3. External Resources:
    - What other resources did you use in completing this project, if any?
        - Since you cannot easily leave comments in the microbit code environment, please use this part of the report to give credit and acknowledge any work done by others that you integrated in your project. If you looked anything up online, or used any other resources in completing this project, please additionally respond to these three prompts from the syllabus:
            - How much have I done myself? Is the entire assignment copied from someone else, or did I add in my own work in places? How much did I change the original code?
            - Did I learn from the code that I used as a starting point or revised? Or, have I used others’ code in a way that undermined my own learning?
            - Could I re-create the code if parts of the assignment were included as questions in a closed-book test?    
4. Core Concepts: List each of the 10 concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of microbit code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I could find a loop in the microbit side bar and correctly fill in the block)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when I would need to seek out this construct and how to use it to accomplish some other goal

[^1]:  Note: your answers to this last part of the assignment will not impact your grade; I am just trying to get a better grasp of how much all these different concepts really make sense to you! You should answer the questions honestly! I am trying to get some insight into how confident you feel about your programming development.

#### Grading

This assignment is worth 7.5 points total. You can earn these points by:

- **0 / 7.5**: Not completed or contains plagiarized content
- **4.5 / 7.5**: Something is turned in that shows some evidence of student work
- **5.5 / 7.5**: All requirements for 4.5 points, and also:
    - The files turned in include your microbit code in `.hex` format.
        - The code works and includes at least 3 programming concepts.
    - The files turned in include a report in either `.docx` or `.pdf` format
        - All questions are answered, at least superficially.
- **6.5 / 7.5**: All requirements for 5.5 points, and also:
    - Your code demonstrates at least 6 of the listed programming concepts.
- **7.5 / 7.5**: All requirements for 6.5 points, and also:
    - Program embodies a creative use of micro:bit features to do something interesting, fun, and challenging.
    - Report contents show evidence of significant student effort, seriousness, thoughtfulness, and reflection.

### P2: JavaScript

In this project you will create a program that creates a visual composition using the JavaScript Processing Library.

This assignment is adapted from Intro CS coursework by Vera Khovanskaya <courses.cit.cornell.edu/info1100_2017su/>.

#### Logistics

**Deadline:**: Sunday, February 3

**What to turn in:**
1. **Your Code:** a `.zip` file that you save from repl.it with all your code
1. **Your Report:** in either `.docx` or `.pdf` format

**How to turn it in:** D2L, in the folder **P2: Project 2 (JavaScript)**


#### Resources & Key concepts

In this project, you will practice using these concepts:

> setup function, draw function, random numbers, loops, variables, coordinate grid, conditionals, your own functions, comments

You may choose to investigate these additional concepts:

> [events (input)](https://p5js.org/reference/#group-Events), [text display](https://p5js.org/reference/#group-Typography)

#### Specification: Program

In this project you will use the processing library and JavaScript to create an interesting visual composition. Your composition may be a still image or an animation. It may be representative of something that exists in the world or it may be abstract. The only requirements are that you demonstrate your skills in each of these areas:

1. Good usage of the setup and draw functions.
    - Think about what is different between these two functions. Is your code in the right place?
2. Good usage of comments.
    - Are there comments throughout your code that explain what you are instructing the computer to do?
    - Did you comment out any broken code with a note about why it is commented out?
3. Conditionals (if statements).
    - For full credit you should use at least one conditional somewhere in your code. You might use this in an animation to test if something is off the screen, or you might use this in connection with the random number function to make a decision about what to draw, what color to use, etc.
4. Good use of loops for abstraction.
    - For full credit, you should use at least one loop (other than the draw function) to handle repetition in your code.
5. Good use of functions for abstraction.
    - For full credit, you should create at least one custom function and use it to draw something in your program.
    - Your custom function should take at least one argument.
    - There should be a humanly noticeable effect that occurs when the argument value(s) change(s) (e.g., some part of the composition changes color; shapes appear in different locations)
    - You should document your function's arguments using standard comments. Make sure to specify any rules for the arguments. For example, if you accept a color as one of the input variables, and it must be a color that processing understands, then include a link to the list of colors.

##### Extra Enrichment

If you get all of the above working, look at the [events (input)](https://p5js.org/reference/#group-Events) and/or [text display](https://p5js.org/reference/#group-Typography) sections of the P5.js documentation. See if you can figure out how to incorporate one of these features into your program.
- You may earn up to 1 bonus point for getting either working. You **MUST** document this in your report and in comments in the program itself.

##### Recommended Procedure
1. Start by planning on paper. What kind of a composition might be interesting and fun for you to work on? Sketch out what you want to make. Think about how it will overlay on the coordinate grid.
3. Build up a simple composition without variables or functions, using different drawing commands. Add one command at a time and test your code each time to make sure that it works.
4. Once you have a static image you like, find a piece of it that you might want to repeat (or notice some repetition that you already have). Define a function to draw this piece of the composition.
    1. Move all the code for that part of the composition into your function. And make sure to call the function in your program. Test it to make sure everything still works.
    2. Substitute variables for some of the numbers inside your function. Each time you put a variable in, add it to the argument list in your function definition and substitute the variable name in for its place in your code. Again, work step by step and test after each change.
    3. Play with the variable assignments. Vary them and see what effect this has on your composition. When you find an outcome that you like, note the value of the argument in a comment in your code.
5. You might decide, as you work, to change your code. _**If so, go step-by-step and test each time to make sure things still work.**_
6. Before submitting, review the list of requirements to make sure you have fulfilled all of them.
7. Go through your code and add comments anywhere where it might not be clear to anyone reading your code.

#### Specification: Report


1. Your Program:
    - What does your program do? (Explain it from the user's point of view.)
    - Why did you choose to make this program? 
    - What grade would you give yourself on this project, and how would you justify that grade?
3. Your Experience:
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During weeks 2, 3, and 4, how much total time did you spend, outside of class, working on this project, and all other activities related to this class? 
    - How is programming in JavaScript different from using the micro:bit system? Which one did you like better?
    - Are you glad you worked with micro:bit first, or do you wish you had jumped straight to JavaScript. Why?
3. External Resources:
    - What other resources did you use in completing this project, if any? If you looked anything up online, or used any other resources in completing this project, please additionally respond to these three prompts from the syllabus:
            - How much have I done myself? Is the entire assignment copied from someone else, or did I add in my own work in places? How much did I change the original code?
            - Did I learn from the code that I used as a starting point or revised? Or, have I used others’ code in a way that undermined my own learning?
            - Could I re-create the code if parts of the assignment were included as questions in a closed-book test?    
4. Core Concepts: List each of the concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of JavaScript code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I write the code for a loop.)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when I would need to seek out this construct and how to use it to accomplish some other goal


#### Grading

This assignment is worth 15 points total. You can earn these points by:

- **0 / 15**: Not completed or contains plagiarized content
- **9 / 15**: Something is turned in
    - Serious errors show clear effort, but serious gaps in understanding, such as code that does not run and is not close to being correct.
- **+1 point**: Partially Complete Program
    - Some bigger errors, but with potential to develop into a competent response. For example, the code may not run, but with a few small syntactical fixes it would properly fulfill the assignment; the code may work but is not well-commented and is therefore confusing to read; or the code completely misses on one or two of the requirements, but still shows some understanding of the unit's material.
- **+2.5 points**: Complete and functional program, meeting all requirements
    - **+0.5 point**: Good usage of the setup and draw functions.
    - **+0.5 point**: Good usage of comments.
    - **+0.5 point**: Good usage of conditionals (if statements).
    - **+0.5 point**: Good use of loops for abstraction.
    - **+0.5 point**: Good use of functions for abstraction.
- **+1 point**: Overall Program Quality
    - Program embodies a creative use of p5js features to do something interesting, fun, and challenging.
- **+1.5 points**: Report Contents and Quality
    - Report includes answers to all questions.
    - Report contents show evidence of significant student effort, seriousness, thoughtfulness, and reflection. It could be used as an example of excellent work in a future class.
    - Report formatting is organized and professional. The writing is of high quality and conforms to the conventions of edited, revised English.
- **+1 point**: Extra Enrichment (bonus)
    - Integrated events and/or text in an interesting way.
    - Extra enrichment is documented with comments in the code and described in the report.

(Maximum score is 16 but will be calculated out of 15 points. Up to one bonus point is available for event / text extra enrichment.)



### P3: Python I

In this project, you will create a program to play _either_ MadLibs or a simplified Oregon Trail / text-based dungeon game. 

Parts of this assignment are adapted from Intro CS coursework by Tammy VanDeGrift <http://sites.up.edu/sigcse2015/>

#### Logistics

**In Class Demos:** Thursday, February 21

**Deadline:** Sunday, February 24, 11:59p

**What to turn in:**
1. **Your Code:** a `.py` file that you save from repl.it with your code \*
1. **Your Report:** in either `.docx` or `.pdf` format

**How to turn it in:** D2L, in the folder **P3: Project 3 (Python I)**

\* _Note: Do **not** turn in more than one Python file for this assignment. You should put all code in a single Python file. Simply comment out any code that is not functional._

#### Key concepts

In this project, you will practice using these concepts:

> variables, input/output, conditionals (if statements or 'logic'), comments, comparisons (e.g., equal to, greater than, less than), string concatenation

You may choose to use these additional concepts, but it is not required:

> functions, random numbers, while loops

#### Spec: MadLib (Choice 1)

- At the start of the program, you should greet the user and give a short informative message about the game
- You should then prompt the user to enter words and phrases of certain types. For example: noun, famous person, adjective, verb ending in -ing, adverb, animal, food, etc..
- Once the user has entered a word/phrase for each type, your program should print the MadLib story that includes the entered words and phrases.
- You must prompt the user for at least 8 different words.
- The core requirements for the program are that you demonstrate your ability to use each of these Python concepts:
    - variables, input/output, conditionals (if statements or 'logic'), comments, comparisons (e.g., equal to, greater than, less than), string concatenation
- Extra Enrichment: use a function in a meaningful way. 
- Extra Enrichment: give the user the ability to restart the program at the end. 
- Extra Enrichment: Add some error checking -- what do you do if the user enters an invalid response (Like, if they hit enter without typing in anything? Or give you some input you weren't expecting?)

#### Spec: Text-based Game (Choice 2)

- At the start of the program, you should greet the user and give a short informative message about the game. 
- During the game, you should alternate between prompting the user for information, and printing out new lines in a game sequence. 
- If you have never played a text based game, you can find some sample games here <https://classicreload.com/oregon-trail.html> and <https://www.makeuseof.com/tag/browser-text-based-games/>
    - Your game does not need to be as complex as these! But, this will give you the idea. 
- Your game should have at least 8 prompts before it ends.
- The main requirements for the program are that you demonstrate your ability to use each of these Python concepts:
    - variables, input/output, conditionals (if statements or 'logic'), comments, comparisons (e.g., equal to, greater than, less than), string concatenation / substitution
- Extra Enrichment: use a function in a meaningful way. 
- Extra Enrichment: give the user ability to restart the program at the end. 
- Extra Enrichment: Add some error checking -- what do you do if the user enters an invalid response (Like, if they hit enter without typing in anything? Or give you some input you weren't expecting?) 

#### Spec: Report

Try to keep this to no more than a page. We don't need you to write a book!

1. Your Program:
    - What does your program do? (Explain it from the user's point of view.)
    - Did you complete any of the extra enrichment portions of the assignment? If so, how and why have you satisfied the extra enrichment requirement?
    - Why did you choose to make this program? 
    - What grade would you give yourself on this project, and how would you justify that grade? 
2. Your Experience: 
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During each of the last 3 weeks, about how much time did you spend, outside of class, working on this project, and doing all other activities related to this class? 
    - How is programming in Python different from using JavaScript? Which one did you like better?
3. External Resources:
    - What other resources did you use in completing this project, if any? If you looked anything up online, or used any other resources in completing this project, please additionally respond to these three prompts from the syllabus:
        - How much have I done myself? Is the entire assignment copied from someone else, or did I add in my own work in places? How much did I change the original code?
        - Did I learn from the code that I used as a starting point or revised? Or, have I used others’ code in a way that undermined my own learning?
        - Could I re-create the code if parts of the assignment were included as questions in a closed-book test?    
4. Core Concepts: List each of the concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of JavaScript code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I write the code for a loop.)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when I would need to seek out this construct and how to use it to accomplish some other goal    

_Note: your answers to this last part of the assignment will not impact your grade; I am just trying to get a better grasp of how much all these different concepts really make sense to you! You should answer the questions honestly! I am trying to better understand how students' learning is progressing over the quarter._


#### Grading 
It doesn't matter whether you chose to create a MadLib program or a text based game. Either one will be graded based on this same set of guidelines. 

This assignment is worth 15 points total. You can earn these points by:

- **0 / 15**: Not completed or contains plagiarized content
- **9 / 15**: Something is turned in
    - Serious errors show clear effort, but serious gaps in understanding, such as code that does not run and is not close to being correct.
- **+1 point**: Partially Complete Program
    - May have some bigger errors, but with potential to develop into a competent response. For example, the code may not run, but with a few small syntactical fixes it would properly fulfill the assignment; the code may work but is not well-commented and is therefore confusing to read; or the code completely misses on one or two of the requirements, but still shows some understanding of the unit's material.
- **+2.5 point**: Complete and Functional Program, meeting all requirements:
    - **+0.5 point**: Successfully prompts the user at least 8 times.
    - **+0.5 point**: Good usage of variables to store information.
    - **+0.5 point**: Good usage of at least one conditional (if statement) with a comparison (equal to, greater than, less than, etc.).
    - **+0.5 point**: Good use of comments to document your code.
    - **+0.5 point**: Good use of string concatenation to combine user input with other data, and then printing the result out for the user to see.
- **+1 point**: Overall Program Quality
    - Program embodies a creative use of python features to do something interesting, fun, and challenging.
- **+1.5 points**: Report Contents and Quality
    - Report includes answers to all questions.
    - Report contents show evidence of significant student effort, seriousness, thoughtfulness, and reflection. It could be used as an example of excellent work in a future class.
    - Report formatting is organized and professional. The writing is of high quality and conforms to the conventions of edited, revised English.
- **+1 point**: Extra Enrichment (bonus, up to 1 point maximum)
    - **Note: Extra enrichment attempts must be documented with comments in the code AND described in the report for any extra points.**
    - Successfully integrated a function in a meaningful way. (.5 points)
    - Successfully added the ability for the user to re-start the program at the end (.5 points)
    - Successfully added some error checking to the program (.5 points)

(Maximum score is 11 but will be calculated out of 10 points. One bonus point (total) available for the extra enrichment.)

### P4: Python II

For your final project, you will create one of two programs:
- a simple Python program to extract and process data from a .csv file
- a word-guessing game like wheel of fortune

#### Logistics

**In Class Demos:** Thursday, March 7

**Deadline:** Sunday, March 17 
_There is more time than usual between demos and the final project. My goal is
to allow you some extra time on this project so that you can manage your 
last homework in this class as well as studying for finals. I would recommend 
turning the project in early! Don't over-think it!_

**What to turn in:**
1. **Your code:** a `.zip` file that you save from repl.it with all of your code and your dataset (if applicable) \*
2. **Your report** in `.docx` or `.pdf` format

**How to turn it in:** D2L in the **P4: Project 4 (Python II)** folder

\* _Note: Do **not** turn in more than one `.py` file for this assignment. You should put all Python code in a single Python file. Simply comment out any code that is not functional. **Please do make sure to include any external files necessary for your program to run (e.g., `.csv` files)**_

#### Key Concepts

In this project, you will practice using these concepts:

> variables, input/output, conditionals (if statements, logic), comments, functions, loops, lists, and _maybe_ files!

#### Option 1: Word Guessing Game
In this project, you will create a simple word or phrase-guessing game that
works like hangman or a simplified Wheel of Fortune. 

1. Basic Program:
    - Uses variables hard-coded into the program to store a list of words
        or phrases that the user will try to guess
    - Randomly chooses a word/phrase from this list
    - Prints out a brief explanation of the game, and asks the user to guess
        a letter
    - After a guess, reports to the user as to whether or not that letter
        is in the word
2. Complete Program: 
    - In addition to the above, 
        - The program displays the user's progress towards guessing the 
        whole word
        - The program repeats until the user has guessed all letters
    - _See sample interaction sequence below._
3. Extra Enrichment: 
    - Complete **three** for the full extra credit point.
        - Use a text file to store the list of words or phrases. 
            At the start of your program, read in the list of words, 
            and then pick a random one from the list.
        - Create a way for the user to say they're ready to guess the
            whole word (e.g., enter a letter to proceed as before, 
            or type GUESS to skip ahead to guessing the whole word/phrase)
        - Create a way for the user to play again -- with a new
            word or phrase.
        - Keep track of previously guessed letters. Don't let the 
            user guess something they've already guessed. 
        - Keep score -- however you like, just explain in a print 
            statement at the start of your program how it works. 
            For example, the user could earn points for guessing
            consonants, and have to spend points to guess a 
            vowel (like wheel of fortune). Or, users could have a 
            limited number of guesses (e.g., 10) before the game 
            ends (like hangman). 

##### Sample Interaction Sequence - Option 1 - Complete:
```
In this game, you guess letters until you figure out the word:
_________
Guess a new letter:	a
Yes that letter is in the word!
a____a___

Guess a new letter:	l
Yes that letter is in the word!
all__a___

Guess a new letter:	i
Yes that letter is in the word!
alli_a___

Guess a new letter:	g
Yes that letter is in the word!
alliga___

Guess a new letter:	t
Yes that letter is in the word!
alligat__

Guess a new letter:	r
Yes that letter is in the word!
alligat_r

Guess a new letter:	o
Yes that letter is in the word!
alligator

You win!
```            


#### Option 2: Data Exploration with Python
1. Choose one dataset to work with:
    - There are several ready to use in the “DataSets for Project 1” folder at this link:
        - <https://www.dropbox.com/sh/fburagb6keb97hv/AACaOGDE0yLyZCuBWCiLf_Rda?dl=0>
    - If there is another data set that you would like to use, please run it by me first.
2. Create a new python program on repl.it, and upload the `.csv` file you will use in your project. 
3. Write a python program to open your dataset file, and explore its contents in order **to answer at least two questions about the dataset**. For example, you could count the number of days in a certain year with at least 1 inch of rain, and compute an average rainfall amount for a certain month or year.
    - **Your program should have a comment at the top of the file that explains what it does, including what 2 questions it answers about the dataset.** The questions must be more complicated than simply counting the number of lines in the file.
    - When run, you program should display a message to the user that explains what it does.
    - When run, the program should display a message that states the answer to each question you have explored in the dataset.
    - _See sample program interaction sequence below._
4. **Extra Enrichment**: Allow the user to specify which question 
    in the survey they want information about. 
    _See sample interaction sequeence below_.
4. **See report specification below.**

##### Sample Interaction Sequence — Option 2 - Complete:  
```
This program analyzes the November 2016 Pew Dataset about Information Engagement.

The number of total respondents in the survey was: 3015

Of the total, 2640 (87.56218905472637 %) use the internet.
Of the total, 2508 (83.18407960199005 %) use the internet 
    through a mobile device at least occasionally.
```

##### Sample Interaction Sequence — Option 2 - Extra:  
```
This program allows you to query for summary statistics about the 
Nov 2016 Pew Dataset. Please refer to the dataset for a list of 
question and answer codes.

Enter a question code:
	q1a
Enter an answer code:
	1

The number of total respondents in the survey was: 3015
935 respondents (31.01160862354892 %) answered 1 to question q1a.
```



#### Report Specification
Try to keep this to no more than a page. We don't need you to write a book!


1. Your Program:
    - Did you choose option 1 or option 2? Why? 
    - What does your program do? (Explain it from the user's point of view.)
    - Did you complete any of the extra enrichment portions of the assignment? If so, how and why have you satisfied the extra enrichment requirement?
    - What grade would you give yourself on this project, and how would you justify that grade? 
2. Your Experience: 
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During each of the last 3 weeks, about how much time did you spend, outside of class, working on this project, and doing all other activities related to this class? 
    - How did this project compare to prior projects in this class? Which one did you like the best? Why?
3. External Resources:
    - What other resources did you use in completing this project, if any? If you looked anything up online, or used any other resources in completing this project, please additionally respond to these three prompts from the syllabus:
        - How much have I done myself? Is the entire assignment copied from someone else, or did I add in my own work in places? How much did I change the original code?
        - Did I learn from the code that I used as a starting point or revised? Or, have I used others’ code in a way that undermined my own learning?
        - Could I re-create the code if parts of the assignment were included as questions in a closed-book test?    
4. Core Concepts: List each of the concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of Python code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I write the code for a loop.)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when I would need to seek out this construct and how to use it to accomplish some other goal    

_Note: your answers to this last part of the assignment will not impact your grade; I am just trying to get a better grasp of how much all these different concepts really make sense to you! You should answer the questions honestly! I am trying to better understand how students' learning is progressing over the quarter._




#### Grading 
It doesn't matter whether you chose option 1 or option 2. Either one will be graded based on this same set of guidelines. 

This assignment is worth 10 points total. You can earn these points by:

- **0 / 15**: Not completed or contains plagiarized content
- **9 / 15**: Something is turned in
    - Serious errors show clear effort, but serious gaps in understanding, such as code that does not run and is not close to being correct.
- **+1 point**: Partially Complete Program
    - May have some bigger errors, but with potential to develop into a competent response. For example, the code may not run, but with a few small syntactical fixes it would properly fulfill the assignment; the code may work but is not well-commented and is therefore confusing to read; or the code completely misses on one or two of the requirements, but still shows some understanding of the unit's material.
- **+1 point**: Basic Features/Functionality
    - Option 1: program meets requirements for basic features
    - Option 2: program answers at least 1 question about the dataset
- **+1.5 points**: Complete Feature/Functionality Requirements
    - Option 1: program meets requirements for complete features
    - Option 2: program answers 2 different questions about the dataset
- **+1 point**: Program Quality
    - Program demonstrates a creative use of python features to do something interesting, unique, and/or challenging
    - Program has a unique and engaging interaction sequence. You 
    do something more interesting than my examples in terms of how 
    you communicate with the user.
    - Program is high quality, with good comments, etc.
- **+1.5 points**: Report Contents and Quality
    - Report includes answers to all questions.
    - Report contents show evidence of significant student effort, seriousness, thoughtfulness, and reflection. It could be used as an example of excellent work in a future class.
    - Report formatting is organized and professional. The writing is of high quality and conforms to the conventions of edited, revised English.
- **+1 point**: Extra Enrichment/Challenge


(Maximum score is 11 but will be calculated out of 10 points. One bonus point (total) available for the extra enrichment.)
