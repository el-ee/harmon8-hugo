---
title: Reading Responses
cc: true
type: "class"
layout: "subpage"
icon: "fa-book"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "cs199-201901-readings"
        
quarter: 201901
---

At three points in the quarter, we will devote half of a class period to a discussion about the social, ethical, and/or legal implications of computing. 

Once in the quarter you will be tasked with writing a provocation for the rest of the class to respond to. 
- Each provocation should be posted to the class Twist forum in a new discussion thread, no later than **_10am_ on the _Sunday_ before the in-class discussion**.

For the other two discussions, you will be asked to read your classmates provocations, and then compose a response. 
- Responses should be posted no later than **2pm on the day of our class meeting**. 
- Your response should address the questions posed by the original post, or raised later in the discussion thread _and_ contribute your own insight from both the readings and your own personal experience.

_Note: We will choose topics in class on January 8, and you will sign up for a provocation week in class on January 10._


#### Provocation Directions

Your goal in writing a provocation is to raise a question for discussion related to the assigned reading. 
- Good discussion questions are things that are open-ended and do not have clear answers. You should not ask an informational question that has a clear and short answer.
- You should ground your question in something specific: a takeaway from the article, an important quote, or a question that the author raised.
- Your provocation may take up the same concerns as the author, or relate the authors' concern to something in your own life.
- You do _not_ need to write a summary of the entire article in your post; you may assume everyone has read it. However, you should give the reader enough context that they can understand your question, and how it is related to the assigned reading. Also, it should be clear to the grader that _you_ have read the assigned article. You should cite specific passages or claims from the article in formulating your question.
- Your provocation should be about 200-250 words long. 

Here is an example:

> **From Ease of Use to Increased Obligation**

> In “A Spreadsheet Way of Knowledge,” Steven Levy recounts the history of the spreadsheet, and discusses their impacts on business and social life. 

> The advent of spreadsheets changed business in many ways. Jobs previously dedicated to manual calculation disappeared and organizational structures shifted; complex modeling tasks planned in advance and carried out on specialized mainframe computers, were replaced by on-the-fly calculations by anyone with spreadsheet skills and a PC; decision makers could ask new questions about quantitative data -- and could more easily explore multiple ‘what-if’ scenarios. 

> Levy also notes that these changes came along with new obligations for business to ask more quantitative questions and to run more ‘what if’ scenarios. Because all of these new number-based practices were so ‘quick’ and ‘easy’ to do with the spreadsheet system, people were soon obligated to take advantage of them. As Levy puts it, “instant hard figures” went from being a “luxury” to a “necessity.” 

> _If a technology makes something easier, but also creates a new obligation for people, is there a net win for individuals overall? Are there other technologies that have made something both easier, but also more necessary? What are some of the bigger picture effects of new technologies designed to make certain kinds of activities easier?_

If you're feeling a little stuck, thinking through some of these questions may help you come up with a good provocation:

1. What is the question or problem that prompted the author to write this article?
2. What is the answer to that question that the author presents? Or, what is it that they think should happen in response to the problem? 
3. What evidence does the author present to convince you of their answer/solution? Or, how do they make their argument? Is it convincing? Why or why not?
4. Who benefits from the situation described in the article?
5. Who is harmed or stands to lose from the situation described in the article?
6. What do you think should be done after reading the article? Anything?
7. How does the assigned reading relate to recent news article you have read, a previous class discussion, or something you have encountered in your own life?

It might also be helpful to review this excellent explanation of [what makes a good discussion question](https://docs.google.com/document/d/12gVLnUdf8ua2ILvknlRbJlJy_DtAEy5lEuGhPN_94kY/) by Dr. Robin James.

#### Response Directions

When it's your turn to write a response, you should compose a thoughtful response to one of the provocations made by your classmates. 

Your response should be about 150-200 words long.

You should contribute something new to the conversation, either by bringing another aspect of the assigned reading to the discussion, or linking the conversation to other topics in the news, or your own life.

You should post your response no later than 2pm on the day of our class discussion.

Again, you do _not_ need to summarize the article in your post. However, it should be clear to the grader that you actually completed the assigned reading. 


### Grading

#### Provocation

Your reading provocation is worth 7 point towards your final grade. You can earn these points by: 

- **0 points**: Not completed, or 
- **4.5 points**: It is not clear from the post that you actually completed the assignment. Your question may be good, but it is not well-grounded in the assigned reading.
- **6 points**: It is clear that you completed the reading, but your discussion question is either too much of an information question (i.e. something with a single or obvious answer), or it does not show your own substantive engagement with the readings beyond a repetition of the author's own argument. 
- **7 points**: Excellent discussion question; demonstrates that you read the assigned reading; goes beyond the ideas presented by the author and contributes your own insight from both the readings and your own personal experience; could be used as an example in future classes.

#### Responses

Each response you post is worth 4 points towards your final grade. You can earn these points by: 

- **0 points**: Not completed
- **3 points**: It is not clear from your post that you actually completed the assignment. Your commentary may be good, but there is no evidence (such as a quote or summary of a key takeaway) that shows that you read anything more than the provocation posts.
- **3.5 points**: It is clear that you completed the reading, but your commentary does not go beyond a summary of the author's own point, or otherwise show your own substantive engagement with the topic.
- **4 points**: Excellent commentary; demonstrates that you read the assigned reading; goes beyond the ideas presented by the author and in the posted provocations; contributes your own knowledge and expertise; could be used as an example in future classes.

