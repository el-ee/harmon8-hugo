---
title: Admin + Policies
cc: true
type: "class"
layout: "subpage"
icon: "fa-info-circle"
weight: 200

menu: 
    main:
        parent: "Teaching"
        identifier: "cs199-201901-policies"
        
quarter: 201901
---


| | |
|--:|:-----|
| **Course** | CS 199 - Introduction to Programming and Problem Solving: A First Exploration |
| **Meetings**| TR 4:40-6:30p, ASRC 230 |
| **Final Exam**| T March 19, 5:30-7:20, ASRC 230 |
| **Instructor** |Dr. Ellie Harmon <br/> <ellie.harmon@pdx.edu> <br/> she / her / hers |
| **Office Hours** | MW 4:00 - 5:00 <br/>or, [by appointment](/~harmon8/faq/#meetings-with-me)<br/>FAB [\#120-15](/~harmon8/faq/#how-to-find-my-office)|
| **TCSS** | TBD |
| **TCSS Office Hours** | TBD <br/>FAB \#120 (The CS "Fishbowl")|
| **Prerequisites** | None|
| **Course Goals** | See the [Course Home Page](../) |
| **Website** | <https://web.cecs.pdx.edu/~harmon8/classes/cs199/> |

### This Website is a Living Document

This website is a starting point for the course. It is subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced via email, so attend class and check your inbox. 

### Course Materials

#### Textbooks

We will be using the free online textbook _Python for Everybody_ in the second half of the quarter. This book is available in several formats, including a [PDF](http://do1.dr-chuck.com/pythonlearn/EN_us/pythonlearn.pdf), and an [interactive website](https://books.trinket.io/pfe/index.html). For more options, including print purchasing options, see the links here: <https://www.py4e.com/book.php>.

#### Supplementary Readings / Videos

Supplementary readings will be linked from the course schedule. Full bibliographic detail is provided for all materials, and you should attempt to locate them on your own in the event of a broken link. _**The failure of any schedule link is not an excuse to skip the assigned reading.**_

Please note that some links may require that you are on the campus network, or connected through a VPN in order to access the content for free. You will _never_ have to pay for any readings in this class. If you are having trouble accessing one of the readings, you should visit my office hours, ask a question in class, or visit the library and ask them how to gain access to the material.

#### Laptops

This class will be taught in an interactive and hands on format. If you have a laptop, please bring it to class every day. If you do not have a laptop, you will be able to check out a Chromebook from me for use during the class period, but you cannot take it home with you. 

If you are in the market for a low-cost laptop, [Free Geek](https://www.freegeek.org/shop/free-geek-store#computers-parts-accessories) offers good prices on used/refurbished laptops out of their store in SE Portland. You can also earn a [free laptop](https://www.freegeek.org/faqs#volunteering) by volunteering for them.

### Major Assignments & Grades

Show up to class, participate in discussion, turn in all assignments,
and you will do great in this class. Skip class and assignments and you
will do poorly. 

Your grade in this class will be based on the following assignments

| Assignment | Quantity | Points Each | Total |
|:---|---:|---:|---:|
| Project 1 | 1 | 7.5 | 7.5 |
| Projects 2-4 | 3 | 15 | 45 |
| Reading Provocation | 1 | 7 | 7 |
| Comments/Discussion | 2 | 4 | 8 |
| Demo | 1 | 5 | 5 |
| Self-Assessments | 2 | 5 | 10 |
| Midterm | 3 | 2.5 | 7.5 |
| Final | 4 | 2.5 | 10 |
| **Total** | | | **100** | 
| Extra Credit Opportunities | | | + 3 |

#### Exams

We will also have 2 comprehensive exams in this class:
- A take-home midterm handed out in week 4 and due at the start of week 5
- A regular final exam during the registrar-scheduled finals time-slot


#### Conversion to Letter Grades

Letter grades will be assigned based on the following standard conversions:

| Total Points Earned | Letter Grade |
|--------------|--------------|
| 93-100       | A            |
| 90-92        | A-           |
| 87-89        | B+           |
| 83-86        | B            |
| 80-82        | B-           |
| 77-79        | C+           |
| 73-76        | C            |
| 70-72        | C-           |
| 67-69        | D+           |
| 63-66        | D            |
| 60-62        | D-           |
| Below 60     | F            |

If you earn the minimum points for a letter grade, you are guaranteed that letter grade. There will be **no** downward curve in this class. For example, once you earn 93 points over the course of the term, you are guaranteed an A in the class.

However, I reserve the right to raise grades to reflect outstanding contributions or effort. For example, if you end the term having earned only 89 points, but were a particularly active and regular contributor to classroom discussions, I may bump your grade up from a B+ to an A-. _Note: You may **not** petition me for a grade you did not earn; any adjustments will be made at my sole discretion._


#### Extra Credit: Attendance & Participation

You may receive extra credit for excellent class attendance and participation:

- 3 points: Attended all but one (1) class meetings
- 2 points: Attended all but two (2) class meetings
- 1 point: Attended all but three (3) class meetings
- 0 points: Missed four (4) or more class meetings

**Because attendance and participation is strictly extra credit, there will be no excused absences.**


### Attendance + Participation

This class is taught in an interactive format that includes discussion
and in-class work. You will find it very challenging to succeed in the class 
if you do not show up!

Disruptive behavior such as disrespecting a member of the class, sleeping, text messaging, web browsing, holding personal conversations, disrespecting another member of our community, or doing work for other classes cannot be tolerated. I reserve the right to ask students to leave the classroom or to drop disruptive students from the course if disruptive behavior persists, in particular if a verbal warning is ineffective at eliminating the behavior or if a single incident is particularly egregious. If asked to leave for violating the course or University policies, you will forfeit any in-class assignments we complete after your departure. Remember that our community does not end at the classroom door, but extends to our D2L and Twist spaces and all other out-of-class environments used for our course interactions. [^jenn-olive]

[^jenn-olive]: https://jenniferolive.com/gender-identity-inclusive-classroom-best-practices/


#### Missing Class

When you are absent for any reason, _you (not me!) are
responsible for:_

-   Initiating a plan to catch up on any missed course material
-   Initiating a plan for any missed assignments

When you miss class, you are _not_ entitled to a private recap of the entire 
course meeting in my office nor via email. You may ask me specific questions,
but you should not expect a detailed reply to any generic "what did I miss?"
questions. I have over 90 students in 3 classes this quarter, it simply
is not realistic for me to deliver course content multiple times.


#### Assignment Deadlines

Since many assignments build on each-other (and often we will use
homework as material for in-class discussions or activities), as a general 
rule, **_late work will not be accepted_**. You will receive partial credit for
work that is partially complete. Please turn in whatever you have finished at 
the deadline. 

#### Exceptional Circumstances

**Please contact me** in the case of any exceptional or unpredictable event
that significantly impacts your ability to complete work or attend class
— such as an illness, a sick child that cannot attend school or daycare,
a family emergency, iced over roads, etc. I reserve the right to request 
documentation before granting any extensions, but they are occasionally 
possible given extenuating circumstances beyond your control. 

Please note that *traffic, TriMet delays, bridge lifts, regular work
schedules, and personal travel are not considered exceptional or
unpredictable events*. Please warn your family and friends in advance (i.e.
today) that they need to consult you regarding any future travel plans
made on your behalf (e.g., weddings, bachelorette parties, cruises,
etc.).

Please make note of the final exam time now and make plans to attend. This 
meeting will be at a different time than our other class meetings. The time is 
[scheduled by the registrar](https://www.pdx.edu/registration/final-exams) and 
I have no control over it. We will meet during the scheduled time.

### Communication

#### Meeting with me

My office hours are Monday and Wednesday from 4:00-5:00 in [FAB 120-15](/~harmon8/faq/#how-to-find-my-office). This is my preferred meeting time for students. You do not need an appointment, you can simply drop in. Please do visit my office hours! I enjoy working with students!

If you need to meet with me, but cannot make my office hours, you can make an appointment with me via the PSU Google Calendar system ([directions](/~harmon8/faq/#meeting-with-me)).

#### Twist

The primary means of communication for this class outside of the classroom will be the [Twist](https://twist.com) application. We will discuss this on the first day of class. Please visit my office hours ASAP if you have any questions or concerns. 

You may have used similar tools such as Slack or Piazza in other classes, but I am moving away from these because:

- Slack is set up as a real-time synchronous chat room. This design makes it hard to keep track of common questions or organize information. 
- Piazza serves _you_ lots of annoying ads. (Interestingly, I did not know this for literally years, as they intentionally do not serve any ads in the interface that instructors use!)

By contrast, the asynchronous-friendly thread-based organization of Twist is far better suited to classroom purposes; it does not serve you ads; and it has a clear easy-to-use interface.

Twist is a great place **to ask questions** publicly so that your classmates can benefit from the answers, too. Also, sometimes your classmates know the answers to your questions and can respond more quickly than I can. _**Unless you have a question of a personal nature, all questions should be posted to the Twist forum.**_  

**Important class announcements** will be posted to the announcements channel. Please check it once a day.

Twist will also be used for your [computing and society reading responses](../readings).

You may also use the Twist forum to post any other relevant commentary about classwork such as news articles, or continuations of in-class discussions.

#### Email

I will **_not_** answer any questions sent
to my email that should have been posted to Twist. Please ask your questions there so that everyone may benefit from both the asking and the answering!

If you must email me about a personal question, *you MUST include the
course number as the first word in the subject*, for example 'CS199:
Final Exam Conflict.''

Do **_not_** send messages to me via D2L.

#### Responsiveness

I will aim to answer all inquiries within **one business day**. 

I do not (cannot) respond to inquiries 24/7. *Do not count
on responses from me after 6pm, before 9am, or on the weekends.*
Instead, use this course as an opportunity to develop your time
management skills. Start work well in advance of deadlines so that you have
time to ask questions.

Likewise, I will never expect a reply from you sooner than 1 business day.

### Code of Conduct

_With permission from the author, this section is adapted from Dr. Jennifer Parham-Mocello, Oregon State University <https://classes.engr.oregonstate.edu/eecs/fall2017/cs161-001/syllabus/community.html> with additional credit to Dr. Susan Shaw, Oregon State University_

The computer science community is well-known for being an unwelcoming and toxic 
environment to many newcomers [^1]. Research shows that members of 
underrepresented groups (e.g., women, people of color, first generation college 
students) leave computer science programs and the tech industry at higher 
rates, and that this attrition is a result of environmental conditions[^4].  
Many open source projects, professional societies, and businesses have 
recognized that the lack of diversity amongst contributors is a problem since 
they miss out on ideas, perspectives, and contributions from underrepresented 
groups[^2]. Moreover, the history and prevalence of exclusionary practices and 
cultures is an ethical problem that limits the intellectual, personal, and 
financial opportunities of members of underrepresented groups[^3].  

To address this, many organizations and events have established community 
guidelines and codes of conduct to support communities that are more welcoming 
to new and diverse contributors. For example:

**Contributor Covenant:** a code of conduct shared by many open source
projects, including Atom, Eclipse, Mono, Rails, Swift, and many more.
[contributor-covenant.org](http://contributor-covenant.org)

**Mozilla Community Participation Guidelines:** community guidelines for the
makers of Firefox  
[mozilla.org/en-US/about/governance/policies/participation](http://mozilla.org/en-US/about/governance/policies/participation/)

**PyCon Code of Conduct:** code of conduct for the major US conference for
the Python programming language  
[us.pycon.org/2018/about/code-of-conduct](https://us.pycon.org/2018/about/code-of-conduct/)

**Ubuntu Code of Conduct:** code of conduct for the open source community
that produces the free Ubuntu operating system  
[ubuntu.com/about/about-ubuntu/conduct](http://ubuntu.com/about/about-ubuntu/conduct)

In this course, we will also have a set of community guidelines. These
guidelines start from the premise that every student should feel safe
and welcome to contribute. As the instructor, I will try
to establish this tone whenever possible, but ultimately the
responsibility for cultivating a safe and welcoming community belongs to
the students — that means you!

Fortunately, being part of a safe and welcoming community is not too
hard. A good place to start is to recognize (and continually remind
yourself) of the following facts:

-   Your classmates come from a variety of cultural, economic, and
    educational backgrounds. Something that is obvious to you may not be
    obvious to them.

-   Your classmates are human beings with intelligence and emotions.
    This applies even when sending emails or posting messages online.

-   Your classmates are here to learn. They have the right to pursue
    their education without being distracted by others’ disruptive
    behavior and without being made uncomfortable by inappropriate jokes
    or unwanted sexual interest.

If each of us remembers these facts and act with corresponding decency,
respect, and professionalism, the course will be better for everyone.

Some students might be inclined to shrug this off and perhaps crack a
joke about safe spaces or political correctness. If that’s you, please
also know that if you make a fellow student uncomfortable by mocking
them, making inappropriate jokes, or making unwanted advances, that is
harassment and will be taken seriously. (If you are a victim of
harassment, please see the brief list of resources in the section [What
To Do About Harassment](#what-to-do-about-harassment)).

However, I hope that we can all approach this positively and
professionally. Treat your classmates as respected colleagues, support
each other when needed, have fun without spoiling it for anyone else,
and everybody wins.

#### Course Guidelines

-   Make a personal commitment to learning about, understanding, and
    supporting your peers and instructor.

-   Think through and/or re-read your comments before presenting them.

-   Never make derogatory comments toward another person in the class,
    including the instructor or assistants.

-   Do not make sexist, racist, homophobic, or victim-blaming comments
    at all.

-   Disagree with ideas, but do not make personal attacks.

-   Assume the best of others in the class and expect the best from
    them.

-   Acknowledge the impact of sexism, racism, ethnocentrism, classism,
    heterosexism, ageism, and ableism on the lives of class members.

-   Recognize and value the experiences, abilities, and knowledge each
    person brings to class. Value the diversity of the class.

-   Pay close attention to what your classmates say. Ask clarifying
    questions, when appropriate. These questions are meant to probe and
    shed new light, not to minimize or devalue comments.

-   Be open to being challenged or confronted on your ideas or
    prejudices.

-   Challenge others with the intent of facilitating growth. Do not
    demean or embarrass others.

-   Encourage others to develop and share their ideas.

-   Participate actively in the discussions, having completed the
    readings and thought about the issues.

-   Be willing to change.

#### What to Do About Harassment

If you are a victim of harassment of any kind in this class, there are
several resources available to you:

-   You may schedule a private meeting to talk to me. _Be aware: I have [Title IX reporting obligations](#title-ix-reporting-obligations)._

-   Fill out the PSU [Bias Incident Report
    Form](https://goo.gl/forms/PMrV0tUbhDWqcBAy1)

-   File a [formal 
    complaint](https://www.pdx.edu/diversity/file-a-complaint-of-discriminationharassment)
    
-   Contact the Office of Global Diversity and Inclusion: Market Center
    Building, 1600 SW 4th Avenue, Suite 830

-   Contact the Dean of Student Life: \<<askdos@pdx.edu>\>



### Academic Integrity

Much of this section of the syllabus is copied (with permission) from the policy of [Nick Seaver](http://nickseaver.net); notable changes include the PSU student code of conduct, and commentary specifically relevant to computer science, such as computer programming details.

Our expressions are not our own. Humans communicate with words and
concepts — and within cultures and arguments — that are not of our own
making. Writing, like other forms of communication, is a matter of
combining existing materials in communicative ways. Different groups of
people have different norms that govern these combinations: modernist
poets and collagists, mashup artists and programmers, blues musicians
and attorneys, documentarians and physicists all abide by different sets
of rules about what counts as “originality,” what kinds of copying are
acceptable, and how one should relate to the materials from which one
draws.

In this course, you will continue to learn the norms of citation and
attribution shared by the community of scholars at Portland State
University and at other higher education institutes in the United
States. Failure to abide by these norms is considered plagiarism, as
laid out in the Student Code of Conduct[^6] with which you should
familiarize yourself:

> \(9) Academic Misconduct. Academic Misconduct is defined as, actual or
attempted, fraud, deceit, or unauthorized use of materials prohibited or
inappropriate in the context of the academic assignment. Unless
otherwise specified by the faculty member, all submissions, whether in
draft or final form, must either be the Student’s own work, or must
clearly acknowledge the source(s). Academic Misconduct includes, but is
not limited to: (a) cheating, (b) fraud, (c) plagiarism, such as word
for word copying, using borrowed words or phrases from original text
into new patterns without attribution, or paraphrasing another writer’s
ideas; (d) the buying or selling of all or any portion of course
assignments and research papers; (e) performing academic assignments
(including tests and examinations) in another person’s stead; (f)
unauthorized disclosure or receipt of academic information; (g)
falsification of research data (h) unauthorized collaboration; (i) using
the same paper or data for several assignments or courses without proper
documentation; (j) unauthorized alteration of student records; and (k)
academic sabotage, including destroying or obstructing another student’s
work.

_Any academic misconduct, including plagiarism, will result in a grade
of zero for the assignment concerned. All incidents of academic
misconduct will be reported to the PSU Conduct Office._

If you are uncertain about what constitutes plagiarism, I recommend
visiting me during my office hours and/or reviewing these online
resources:

- [Indiana University First Principles website](https://www.indiana.edu/~academy/firstPrinciples/)

- [Purdue Online Writing Laboratory (OWL)](https://owl.english.purdue.edu/owl/section/2/)

#### Attribution vs. Originality

Admittedly, many university plagiarism policies tend to focus on the
less productive side of the issue, urging students to be “original” and
telling them what not to do (buying papers, copying text from the
internet and passing it off as one’s own, etc.). While you should follow
these rules, I encourage you to take a more expansive view of what
academic integrity means. _Academic integrity is not a matter of
producing purely original thought, but of recognizing and acknowledging
the resources on which you draw_ (for example, you will notice in the
acknowledgements throughout, that this syllabus draws heavily from a
wide range of other university professors). Such references both lend
your work additional authority and credibility, and also help the reader
to understand what unique contributions you might have made in bringing
diverse resources together in a writing assignment or project.
Originality is not just about a singular novel idea, but may also be
about a novel combination of others' ideas.

In light of this, I do not use “plagiarism detection” services like
Turnitin. Rather than expending your energy worrying about originality,
I suggest that you think instead about what kind of citational network
you are locating yourself in. What thinkers are you thinking with? What
programmers are you coding with? Where do they come from? How might
their positions in the world inform their thoughts – and, in turn, your
thoughts? What is your position relative to these thinkers, writers, and
makers? How might you re-shape your citational network to better reflect
your priorities or ideals? How are you learning from or extending the
code or text in question? How do you think the original author would
feel about your use of their work? Are you generous in giving them
credit for the parts that are the result of their own hard work or are
you claiming it as your own?

If you are interested in these issues, I recommend these pieces:

- Ahmed, Sara. 2013. “Making Feminist Points.” *feministkilljoys*.
\<[feministkilljoys.com/2013/09/11/making-feminist-points/](http://feministkilljoys.com/2013/09/11/making-feminist-points/)\>

- Frank, Will. 2015. “IP-rimer: A Basic Explanation of Intellectual
Property.” Personal Blog Post (Medium).
\<[medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711](https://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711)\>

- This comment thread regarding proposed changes to the Stack Exchange
code licensing defaults:
\<[meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required](https://meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required?cb=1)\>

You may write a 500-word response to these pieces for extra credit. See
me in office hours for details.

#### Derivative Works

The degree to which collaborative and derivative work is allowed in this
class will vary by assignment. **_In all cases, attribution is paramount._**
Because this is an educational setting, all copied *and derivative*
computer code or written text **_must be
attributed_**. Even if you are using public domain writings or open
source code, you must note in your own work all places from which you
draw inspiration, ideas, or implementation details. Even if you start
with someone else’s code or text and then modify it significantly, you
should still cite the author of the original work that you used to get
started. **_It is always better to over-attribute than under-attribute._**

##### Writing Assignments 

*In the case of any writing assignments* such as reading responses and
term papers, I expect you to quote and reference course readings, course
discussions, and external sources. Indeed, your ability to locate and
use such sources is part of the skill that you should be developing and
demonstrating as a writer. At this point in your life, you should be an
accomplished communicator and thinker. However, for full points on any
writing assignment, I will also expect you to build on these works and
move beyond simply repeating others’ ideas; to bring your own unique
experiences, critiques, and perspectives into conversation with these
authors.

##### Coding Assignments

*In the case of any coding projects*, the degree to which you may use
external sources at all will vary. I will endeavor to make this clear in
the specifications for each assignment. Sometimes it may be appropriate
and beneficial for you to build on the work of others; but when you are
still learning to code, sometimes it can be more beneficial for you to
work through some problems by yourself. Once you have done all the
attribution work on a coding project, I would recommend asking yourself
the following questions:

-   How much have I done myself? Is the entire assignment copied from
    someone else, or did I add in my own work in places? How much did I
    change the original code?

-   Did I learn from the code that I used as a starting point or
    revised? Or, have I used others’ code in a way that undermined my
    own learning?

-   Could I re-create the code if parts of the assignment were included
    as questions in a closed-book test?

**_If, at any point in the term, you have questions about what is or is
not acceptable with regards to reusing or modifying existing work,
please come see me in my office hours!_** I am more than happy to talk
about your projects; and would very much prefer to help guide you
through the work up front rather than scold you for plagiarizing after
the fact.

In all cases, the **_work you submit as your own must be your own_**. It is
not acceptable to hand in assignments in which substantial amounts of
the material was done by someone else. **_I may ask any student to explain
their assignments verbally._**

### Take Care of Yourself!

There are many resources available to support you at PSU. I encourage
you to take advantage of them so that you will be successful at both
your academic and non-academic pursuits.

**Writing Center:** The writing center can help you improve your writing!
Visit them! [pdx.edu/writing-center](http://pdx.edu/writing-center)

**Office of Information Technology (OIT):** Campus help desk for all
things technology-related [pdx.edu/oit](http://www.pdx.edu/oit)

**We in Computer Science (WiCS):** student organization promoting
inclusivity in computer science
[wics.cs.pdx.edu](http://wics.cs.pdx.edu)

**Center for Student Health and Counseling (SHAC):** free, drop-in mental
and physical health care [pdx.edu/shac](http://pdx.edu/shac)

**C.A.R.E. Team:** Central point of contact if you *or someone you know* is
having a difficult time – mentally, financially, physically, anything!
[pdx.edu/dos/care-team](http://pdx.edu/dos/care-team)

**PSU Food Pantry**: Located in SMSU 325. <foodhelp@pdx.edu>

You can find *even more* resources on the dean of student life website:
[pdx.edu/dos/student-resources](http://pdx.edu/dos/student-resources)

### Access and Inclusion for Students with Disabilities

_This section is lightly edited from: [pdx.edu/drc/syllabus-statement](https://www.pdx.edu/drc/syllabus-statement)_

PSU values diversity and inclusion; we are committed to fostering mutual
respect and full participation for all students. My goal is to create a
learning environment that is equitable, useable, inclusive, and
welcoming. If any aspects of instruction or course design result in
barriers to your inclusion or learning, please notify me. The Disability
Resource Center (DRC) provides reasonable accommodations for students
who encounter barriers in the learning environment.

If you have, or think you may have, a disability that may affect your
work in this class and feel you need accommodations, contact the
Disability Resource Center to schedule an appointment and initiate a
conversation about reasonable accommodations. The DRC is located in 116
Smith Memorial Student Union, 503-725-4150, <drc@pdx.edu>,
[pdx.edu/drc](https://www.pdx.edu/drc)

-   If you already have accommodations, **you must contact me** to make
    sure that I have received a faculty notification letter and to discuss 
    your accommodations.

-   Students who need accommodations for the final exam are expected
    to schedule their test to overlap with the time the class is taking
    the test. You should [schedule this **now**](https://www.pdx.edu/shac/testing-drc) as finals slots fill up quickly.

-   Please be aware that the accessible tables or chairs in the room
    should remain available for students who find that standard
    classroom seating is not useable.

-   For information about emergency preparedness, please go to the Fire
    and Life Safety webpage for information  
    [pdx.edu/environmental-health-safety/fire-and-life-safety](http://pdx.edu/environmental-health-safety/fire-and-life-safety)

### Title IX Reporting Obligations

_This section is lightly edited from the suggested syllabus statement available here:    [pdx.edu/sexual-assault/faculty-staff-resources-responding-to-students-in-distress](https://www.pdx.edu/sexual-assault/faculty-staff-resources-responding-to-students-in-distress)_

Portland State University is committed to supporting students’ safe
access to their education. Sexual assault, sexual/gender-based
harassment, dating violence, domestic violence and stalking are all
prohibited at PSU. Students have many options for accessing support,
both on and off campus.

As an instructor, one of my responsibilities is to help create a safe
learning environment for my students and for the campus as a whole.
Please be aware that federal, state, and PSU policies require faculty
members to report any instances of sexual harassment, sexual violence
and/or other forms of prohibited discrimination. Similarly, PSU faculty
are required to file a report if they have reasonable cause to believe
that a child with whom they come into contact has suffered abuse, or
that any person with whom they come into contact has abused a child.

*If you would rather share information about these experiences with an
employee who does not have these reporting responsibilities and can keep
the information confidential,* please contact one of the following
campus resources:

**PSU’s Sexual Misconduct Options Website**
[pdx.edu/sexual-assault/get-help](http://pdx.edu/sexual-assault/get-help)

**Women’s Resource Center**  
(503) 725-5672 [pdx.edu/wrc/contact](http://pdx.edu/wrc/contact)

**Queer Resource Center**  
(503) 725-9742 [pdx.edu/queer](http://pdx.edu/queer/)

**Center for Student Health and Counseling (SHAC)**  
(503) 725-2800 [pdx.edu/shac](http://pdx.edu/shac)

**Student Legal Services**  
(503) 725-4556 [pdx.edu/sls](http://pdx.edu/sls)

For more information about the applicable regulations please complete
the required student module Creating a Safe Campus in your D2L
[pdx.edu/sexual-assault/safe-campus-module](http://pdx.edu/sexual-assault/safe-campus-module)



### Notes

[^1]: See, for example, the recent news about Linus Torvalds stepping away from maintenance of Linux as he seeks help for improving his interpersonal skills. Cohen, Noam. 2018. "After Years of Abusive E-mails, the Creator of Linux Steps Aside," _The New Yorker_, September 29, 2018. <https://www.newyorker.com/science/elements/after-years-of-abusive-e-mails-the-creator-of-linux-steps-aside>

[^2]: See, e.g., Vivian Hunt et al. 2018. “Delivering through Diversity”
    Report by *McKenzie and Co.* January 2018.
    \<[mckinsey.com/business-functions/organization/our-insights/delivering-through-diversity](https://www.mckinsey.com/business-functions/organization/our-insights/delivering-through-diversity)\>

[^3]: See, e.g., Jane Margollis et al. 2008. *Stuck in the Shallow End:
    Education, Race and Computing.* MIT Press.; Steve Henn. 2014. "When
    Women Stopped Coding" *Planet Money, NPR.
    \<*<https://www.npr.org/sections/money/2014/10/21/357629765/when-women-stopped-coding>*\>*;
    Michelle Kim. 2018. "Why focusing on the “business case” for
    diversity is a red flag" *Quartz at WORK,* 29 March 2018.
    *\<*<https://work.qz.com/1240213/focusing-on-the-business-case-for-diversity-is-a-red-flag/>*\>.*

[^4]: See, e.g., J. McGrath Cohoon. 2001. Toward improving female
    retention in the computer science major. *Commun. ACM* 44, 5 (May
    2001), 108-114.
    \<[dx.doi.org/10.1145/374308.374367](http://dx.doi.org/10.1145/374308.374367)\>;
    Tracey Lien. 2015. “Why are women leaving the tech industry in
    droves?” *LA Times.
    \<*[latimes.com/business/la-fi-women-tech-20150222-story.html](http://www.latimes.com/business/la-fi-women-tech-20150222-story.html)*\>*



[^6]: PSU Student Code of Conduct:
    <https://www.pdx.edu/dos/psu-student-code-conduct>

