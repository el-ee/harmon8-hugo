---
title: "CS 199 (Fall 2019)"
date: 2019-08-01
courseNumber: "CS 199"
courseTitle: "Introduction to Programming and Problem Solving: A First Exploration"
sidebarItems:

meetings: "TR 10:00-11:50"
location: "EB 103"

shortDescription: |
    Introduction to programming and computer science, for students with _no prior programming experience_.

    This course is designed to be a pre-CS 161 course for students who are not computer science majors, might benefit from a more accessible pace, do not have a strong math or computing background, and/or want to explore programming and computer science.

type: "class"
layout: "class-home"

menu: 
    main:
        parent: "Teaching"
        
quarter: 201904    
---
Introduction to programming and computer science, for students with _no prior programming experience_.

An increasingly important skill, computer coding can let you harness the power of your computer in new and creative ways. Coding can be a useful tool in areas as diverse as healthcare, art, journalism, biology, chemistry, sociology, and more.

We will learn to use JavaScript and Python to manipulate text and graphics. You do not need a strong math background to succeed in this class.

Throughout the course, we will also explore the history of computing and some of its impacts on contemporary society: from Facebook addiction and filter bubbles to the impacts of automation and big data on the future of work.

This course is designed to be a pre-CS 161 course for students who:

* are not computer science majors
* might benefit from a more accessible pace
* do not have a strong math or computing background
* want to explore programming and computer science


### This Website is a Living Document

This website is a starting point for the course. It is subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced via email, so attend class and check your inbox.

