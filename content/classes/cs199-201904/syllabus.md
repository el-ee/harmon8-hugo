---
title: Syllabus
credits: 
cc: false
type: "class"
layout: "subpage"
weight: 200
icon: "fa-info-circle"

menu: 
  main:
    parent: "Teaching"
    identifier: "cs199-201904-syllabus"

quarter: 201904
---

| | |
|--:|:-----|
| **Course** | CS 199 - Introduction to Programming & Problem Solving, a First Exploration |
| **Meetings**| Tuesdays + Thursdays, 10:00 - 11:50, EB 103 |
| **Final Exam**| [Tuesday, December 10, 10:15-12:05](https://www.pdx.edu/registration/final-exams)\* |
| **Instructor** |Dr. Ellie Harmon <br/> <ellie.harmon@pdx.edu> <br/> she / her / hers |
| **Office Hours** | M+W 2:00p - 3:00p, [Starbucks @ SW 6th & Jackson](https://goo.gl/maps/sy3iVCbZYHDycYsS7) <br/> or [by appointment](/~harmon8/faq/#meetings-with-me)|
| **Prerequisites** | None|
| **Website** | <https://web.cecs.pdx.edu/~harmon8/classes/cs199/> |
| **Google Classroom** | <https://classroom.google.com> code: 9mu0kl3 |

\* _There will be a final exam in this course. Please make note of the unusual time now and make plans to attend. The time is [scheduled by the registrar](https://www.pdx.edu/registration/final-exams) and I have no control over it._


### Hello! And Welcome.

I'm looking forward to our course this term, and I hope you are as well. All major course policies are outlined in the document that follows. Please note that all materials on the course website -- including this policy overview as well as the course schedule -- are designed to be a starting point for the course. They are subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced via email, so attend class and check your inbox.

A series of questions are embedded throughout this document, these are used in the survey version of the syllabus. If you are taking the survey version of the syllabus, please note that you do not need to write anything down or take notes, this entire document is also available to you on the course website and is linked from the Google Classroom page.

This syllabus is adapted from the [interactive syllabus](https://interactivesyllabus.com) developed by Dr. Guy McHendry and Dr. Kathy Gonzales, both of Creighton University, and adapted by Dr. Lindsey Passenger Wieck (St. Mary's University) and Dr. Angela C. Jenks (UC Irvine). It is licensed under Creative Commons Attribution - Non-Commercial 4.0. Further acknowledgements about the content of specific sections are noted throughout the document.


### About Dr. Harmon

My name is Dr. Ellie Harmon, and I will be your instructor this quarter. I am a Senior Instructor in the department of Computer Science, and the coordinator for the University Studies cluster, Freedom, Privacy, and Technology.

I started college as an Earth and Atmospheric Sciences major, but discovered that I liked computer science when taking a required CS1 course in my Freshman year. Unlike my Earth Sciences class which required lots of memorization and seemed really boring, my computer science courses offered me the opportunity to develop my creative and problem solving skills. Over the years, I have used my computing skills in everything from artistic collaborations with a microbiologist, to research about new forms of work conducted for a German labor union. I think computing is something that everyone can and should learn to work with, and I am very excited to be working with you this quarter.

I use she/her pronouns and I thru-hiked the PCT in 2013.

Q0) What else would you like to know about me?

Q1) Please tell me a little bit about yourself: name, pronouns, why you're here, and something interesting about you.

### Course Description

An increasingly important skill, computer coding can let you harness the power of your computer in new and creative ways. Coding can be a useful tool in areas as diverse as healthcare, art, journalism, biology, chemistry, sociology, and more.

This course is a work-in-progress designed to offer a different, and hopefully less intimidating, first course in Computer Science to students who:

- may not be computer science majors
- might benefit from a more accessible pace
- do not have a strong math or computing background
- want to explore programming and computer science, but might only take one or two classes

We will start out with some programming in the blocks-based micro:bit environment that will allow you to get up and running with computer code quickly.

We will also learn to use the JavaScript and Python languages to manipulate text and graphics. I have explicitly designed this course to avoid math-based problem sets, so that you can develop your computing skills even if you do not feel like you have a strong math background.

Throughout the course, we will also explore the history of computing and some of its impacts on contemporary society from Facebook addiction and filter bubbles to the impacts of automation and big data on the future of work.

Q2) Why are you taking this course? How did you find out about it?

#### My goals for you

By the end of this course, I hope that you will be able to:

- ... see computing as a potential future pursuit for you. Like an introductory writing class Will not transform you into a Pulitzer prize winning author immediately, this course will not make you into a rockstar programmer overnight. However, I hope that you leave this course with the confidence and competence to pursue future study if you so desire.
- ... write simple programs to express yourself creatively and/or answer questions about the world, using basic programming concepts such as loops, conditionals, and functions.
- ... learn new computing concepts and/or programming languages in future coursework or through independent study.
- ... explain some of the interactions between computing technology and social life, and offer thoughtful assessments and critiques of computing-in-practice.

Q3) What do you think about the above course description and set of goals? How does it make you feel about the course? Are you excited? Anxious? Something else?

Q4) What are some things that _you_ are hoping to get out of this class? List 2-3 specific things that you'd like to learn or accomplish this quarter.

Q5) Tell me a little bit about your background with computing. What kind of skills do you already have (e.g., do you blog, make videos, play games, create websites, etc.)? What kind of things do you want to work on?

### Course Materials

#### Textbook

In the first half of the term, I will ask you to work through some free online lessons on Khan Academy in preparation for class work. These will be linked in the class schedule.

In the second half of the term, we will use the freely available textbook _Python for Everybody_ (Py4E). I have chosen this book for three reasons:

1. It is freely available in [interactive HTML](https://books.trinket.io/pfe/index.html) and [PDF](http://do1.dr-chuck.com/pythonlearn/EN_us/pythonlearn.pdf) formats. You can also buy a hardcopy if you prefer that; see <https://www.py4e.com/book.php> for details on different format options.
2. All chapters are also available in video-lecture format. I will not lecture in this class, but if you prefer that method of content delivery, you can watch the online lectures in lieu of reading before class.
3. It was designed for classes like ours, where students may or may not choose to pursue computer science as a major.

Q6) Do you have any questions about accessing Khan Academy or the Py4E textbook?

#### Supplementary Readings / Videos

We will have a couple of computing and society discussions this quarter. There will a reading and writing assignment realated to each of these discussions.

Readings will be linked from the course schedule. Full bibliographic detail is provided for all materials, and you should attempt to locate them on your own in the event of a broken link. _**The failure of any schedule link is not an excuse to skip the assigned reading.**_

Please note that some links may require that you are on the campus network, or connected through a VPN in order to access the content for free. **You will _never_ have to pay for any readings in this class.** If you are having trouble accessing one of the readings, please visit my office hours, ask a question in class, or visit the library and ask them how to gain access to the material.

Q7) Have you ever successfully accessed materials requiring login through the library website from off campus?

- Yes.
- No, but I will try it out this week, and ask any questions I have at our next class meeting.

#### Laptops!

I teach this class in an interactive and hands on format. If you have a laptop, please bring it to class every day. If you do not have a laptop, you will be able to check out a Chromebook from me for use during the class period, but you cannot take it home with you.

In my experience, iPad and Android tablet devices do not work well for what we are doing. A Microsoft surface tablet should be totally fine. Everything we are doing will be web-based. For this class, it does not make a difference if you have a Windows/Linux/Mac/Chrome OS computer.

Q8) Do you have a laptop that you will bring with you to class?

- Yes, I will bring my own laptop.
- No, I will need to borrow a Chromebook.

_If you are in the market for a low-cost laptop, [Free Geek](https://www.freegeek.org/shop/free-geek-store#computers-parts-accessories) offers good prices on used/refurbished laptops out of their store in SE Portland. You can also earn a [free laptop](https://www.freegeek.org/faqs#volunteering) by volunteering for them. I would recommend any Mac, Linux, or Windows device over a Chromebook._

### Major Assignments & Grades

This class involves primarily individual assignments. Below is a brief sketch of the assignments we will complete. More detailed assignment guidelines will be provided later in the quarter.

**Attendance & Participation** This class is taught in an interactive format. There will be no lectures. At each meeting, you will be asked to evaluate your own participation in that day's activities on a small exit slip. Please make sure you turn this in before leaving the classroom! _(Note: It should go without saying, but filling out a participation worksheet for another student (or lying about your own participation) is considered a form of academic misconduct.)_

**Syllabus Survey** The survey that is part of this syllabus counts for 5 points towards your final grade. It will be graded very simply for effort and completeness.

**Projects 1-4** There will be four coding projects in this course, due at the end of weeks 1, 4, 7, and 10. These assignments are opportunities for you to explore and apply the concepts we are learning in class to your own projects. They make up about half of your grade in the class. Extra credit opportunities will be detailed for each of projects 2, 3, and 4.

**Demo** At least once this quarter, you will need to demo one of your projects for the rest of the class. This demo is designed to be a learning opportunity that happens _before the project is due._ You are not expected to have a fully functioning project for your demo. To the contrary, I hope you will use the demo to ask questions about where you might be stuck, and draw on the collective wisdom of the class in overcoming those obstacles! As long as you participate in the demo in good faith, you will receive full points for this. However, if you procrastinate to the very end of the term, and we run out of time for everyone to demo at the last available opportunity, you forfeit these points.

**Computing and Society (C+S) Activities** We will have three computing and society discussions as part of this course. These activities are designed to (a) give us all a little break time within each programming module for our brains to process what we're learning without trying to cram more knowledge in too fast (b) help you think about the bigger picture context of computer science -- how does computing interact with society? Each discussion will center on one shared reading, which everyone in the class will complete. In order to prepare you for discussion, you will write a brief [reading response](../computing-society#reading-responses), due at the start of class, on paper. You will sign up to be a [discussion leader](../computing-society#discussion-leaders) for _one_ of the discussions in class, during week 2.

**Exams** We will have 2 comprehensive exams in this class: (1) A take-home midterm handed out in week 4 and due at the start of week 5 (2) A regular final exam during the registrar-scheduled finals time-slot. Both exams contribute only a nominal percentage towards your final grade. Writing computer code on paper is a particularly odd skill to develop, and my primary goal in giving you exams in this class is to help you prepare for future computer science courses more than it is to evaluate your learning. I will be working closely with you in class, and will have a pretty good idea of your mastery of course materials before you ever set pencil to paper on the final exam.

**Closing Self-Assessment** At the end of the term, you will complete an individual self-assessment reflecting on your accomplishments in the course.


Q9) Which assignment are you most excited about? Why?

Q10) Which assignment worries you the most? Why?


#### Workload

One credit hour is defined by federal regulations as "One hour of classroom or direct faculty instruction and a minimum of two hours of out‐of‐class student work each week"[^credit-hour]. This is a four-hour course, which means that you should spend approximately eight (8) hours on out-of-class work each week. This includes both reading assignments in preparation for class, as well as research, writing, and/or programming assignments as applicable.

[^credit-hour]: See the Credit Hour Policy of the Northwest Commission on Colleges and Universities (this is the organization which accredits PSU) <https://www.nwccu.org/wp-content/uploads/2017/05/Credit-Hour-Policy.pdf>

Q11) In addition to the course meetings, what blocks of time have you set aside to work on assignments for this course?

| | Morning | Afternoon | Evening |
|---|---|---|---|
| S | | | |
| M | | | |
| T | | | |
| W | | | |
| R | | | |
| F | | | |


#### Grading

Show up to class, participate in discussion, turn in all assignments,
and you will do great in this class. Skip class and assignments and you
will do poorly.


| Assignment | Quantity | Points Each | Total |
|:---|---:|---:|---:|
| Class Participation | 20 | 0.5 | 10 |
| Syllabus Survey | 1 | 4 | 4 |
| Project 1 | 1 | 7.5 | 7.5 |
| Projects 2, 3, 4 | 3 | 15 | 45 |
| Demo | 1 | 4 | 4 |
| C+S: Discussion Lead | 1 | 4 | 4 |
| C+S: Reading Response | 3 | 3 | 9 |
| Midterm | 1 | 4.5 | 4.5 |
| Final | 1 | 9 | 9 |
| Closing Self-Assessment | 1 | 5 | 5 |
| Extra Credit Opportunities on Projects | 3 | 1 | 3 |
|  **Total**  |   |   |  **105**  |  


Note in the table above that there are 105 points that you can earn in this class, though final grades will be calculated out of 100. You can use these extra points as you see fit, for example, to cover an absence from class.

Due to the fact that there are several extra credit points built in, there will be no excused absences from class or makeup work, barring [exceptional circumstances](#exceptional-circumstances).

Letter grades will be assigned based on the following standard conversions:

| Total Points Earned | Letter Grade |
|--------------|--------------|
| 93-100       | A            |
| 90-92        | A-           |
| 87-89        | B+           |
| 83-86        | B            |
| 80-82        | B-           |
| 77-79        | C+           |
| 73-76        | C            |
| 70-72        | C-           |
| 67-69        | D+           |
| 63-66        | D            |
| 60-62        | D-           |
| Below 60     | F            |

If you earn the minimum points for a letter grade, you are guaranteed that letter grade. There will be **no** downward curve in this class.

### Attendance & Deadlines

#### Attendance & Participation

Reflecting on building a classroom where participation is a fundamental part of the learning experience, Emilie Pine explains:

 > I have tried to realise some of [my] ambitions by making my classroom a safe (and equal) space in which all of my students can take risks. Sometimes it seems that the biggest risk they can imagine is to say something out loud. I know that they are afraid of saying the wrong thing and being laughed at. But I want them to speak despite this fear. Because I worry that if students are quiet about their ideas in class perhaps they will be quiet about other things too. Things they should not be quiet about. If they cannot talk in class, how will they speak out if they get harassed, or discriminated against, or hurt? [^pine]

[^pine]: Emilie Pine. 2018. _Notes to Self_. Tramp Press.

Participation in this class functions as an invitation to think and take risks with one another as we build the community of the classroom.

Q12) Why do you think I care so much about participation?

Q13) On a scale of 1 to 5 (1 being never, 5 being frequently), how often do you participate in classes?

Q14) Please describe your attitude towards class participation. What helps and encourages you to participate? What prevents you from participating?

Q15) What are some ways you might participate in class, besides initiating a comment or question during whole-group discussions?

#### Exceptional Circumstances

**_Please contact me_** in the case of any exceptional or unpredictable event
that significantly impacts your ability to complete work or attend class
— such as an illness, a sick child that cannot attend school or daycare,
a family emergency, iced over roads, etc. I reserve the right to request
documentation before granting any extensions, but they are occasionally
possible given extenuating circumstances beyond your control.

Please note that *traffic, TriMet delays, bridge lifts, regular work
schedules, and personal travel are not considered exceptional or
unpredictable events*. Please warn your family and friends in advance (i.e.
today) that they need to consult you regarding any future travel plans
made on your behalf (e.g., weddings, bachelorette parties, cruises,
etc.). If you choose to prioritize one of these events over your class time,
that is perfectly fine. You are an adult and can make your own decisions.
However, you understand that you forfeit any in-class assignments we complete
during your departure.

#### Missing Class

When you are absent for any reason, _you (not me!) are
responsible for:

- Proposing a plan to catch up on any missed course material
- Proposing a plan for any missed assignments

#### Assignment Deadlines

Since many assignments build on each-other (and often we will use
homework as material for in-class discussions or activities), as a general
rule, **_late work will not be accepted without prior approval_**.
You will always receive partial credit for work that is partially complete.
Please turn in whatever you have finished at the deadline.


Q16) Any questions about attendance, deadlines, or what might or might not constitute an exceptional circumstance?


### Communication

#### Meeting with me

My office hours will be held on Mondays and Wednesdays from 2:00p - 3:00p at the [Starbucks on the corner of SW 6th & Jackson](https://goo.gl/maps/sy3iVCbZYHDycYsS7). This is my preferred meeting time for students. You do not need an appointment, you can simply drop in. Please do visit my office hours! I enjoy working with students!

Q17) Does this time and location work for you, in general?

- Yes
- No

If you need to meet with me, but cannot make my office hours or would like a more private meeting in my office, you can make an appointment with me via the PSU Google Calendar system ([directions](/~harmon8/faq/#meeting-with-me)).

Q18) Do you know how to view another person's schedule on Google Calendar in order to find a suitable meeting time, and create a meeting invitation? **Note: meetings scheduled outside of my drop-in office hours will take place in my office, #120-15, in the Fourth Avenue Building.**

- Yes
- No, but I will read the directions here, and ask any questions I have during the first week of class: <https://web.cecs.pdx.edu/~harmon8/faq/#meeting-with-me>

#### Google Classroom

As you may have noticed there is no D2L shell for this course. We will be trying out Google Classroom instead, with the hope that it will be a much easier place to turn in assignments, ask questions about course content, etc.  As this is a bit of an experiment, I would appreciate any feedback you have about the Google Classroom environment throughout the term.

As this is a new environment for most of us, we will have to work together through the year to figure out best practices for using this new tool. However, I am hopeful that it will be easier to navigate and use than D2L!

Please try to ask course-related questions on the Google Classroom page before emailing me. This way everyone can benefit from the answer -- and your peers may be faster at answering than myself :)

**Important class announcements** will be posted to the Google Classroom (and you should receive email notifications about them unless you have elected to turn notifications off). Please make sure to check the Classroom site and/or your PSU email at least once a day.

Q19) How do you feel about using Google Classroom?

- I'm excited, I hate D2L.
- Sounds fine, I'm good with it.
- I am apprehensive about this.
- I don't like this.
- I don't really care.
- Other / Comments:

#### Email

If you need to email me directly, *you MUST include the
course number as the first word in the subject*, for example 'CS305:
Missing class due to illness, suggested plan for makeup work.'

#### Responsiveness

I will aim to answer all inquiries within **1 business day**. If you do not
receive a reply from me within 1 day, please re-send your message.

I do not (cannot) respond to inquiries 24/7. *Do not count
on responses from me after 6pm, before 9am, or on the weekends.*

Likewise, I will never expect a reply from you sooner than 1 business day.

### Code of Conduct

The computer science community is well-known for being an unwelcoming and toxic
environment to many newcomers [^1]. Research shows that members of
underrepresented groups (e.g., women, people of color, first generation college
students) leave computer science programs and the tech industry at higher
rates, and that this attrition is a result of environmental conditions[^4].
Many open source projects, professional societies, and businesses have
recognized that the lack of diversity amongst contributors is a problem since
they miss out on ideas, perspectives, and contributions from underrepresented
groups[^2]. Moreover, the history and prevalence of exclusionary practices and
cultures is an ethical problem that limits the intellectual, personal, and
financial opportunities of members of underrepresented groups[^3].

To address this, many organizations and events have established community
guidelines and codes of conduct to support communities that are more welcoming
to new and diverse contributors. For example:

- **Contributor Covenant:** a code of conduct shared by many open source
projects, including Atom, Eclipse, Mono, Rails, Swift, and many more.
[contributor-covenant.org](http://contributor-covenant.org)
- **Mozilla Community Participation Guidelines:** community guidelines for the
makers of Firefox
[mozilla.org/en-US/about/governance/policies/participation](http://mozilla.org/en-US/about/governance/policies/participation/)
- **PyCon Code of Conduct:** code of conduct for the major US conference for
the Python programming language
[us.pycon.org/2018/about/code-of-conduct](https://us.pycon.org/2018/about/code-of-conduct/)
- **Ubuntu Code of Conduct:** code of conduct for the open source community
that produces the free Ubuntu operating system
[ubuntu.com/about/about-ubuntu/conduct](http://ubuntu.com/about/about-ubuntu/conduct)

In this course, we will also have a code of conduct.

_**We will create this code of conduct together at our first class meeting. After that meeting, it will be updated and posted here.**_

Link to [code of conduct document](https://docs.google.com/document/d/1ZP-mYO0ss-zSnY0h24V7X4FzbZFDYd2_TbidJqE6qKI/edit?usp=sharing)

#### What to Do About Harassment

If you are a victim of harassment of any kind in this class, there are
several resources available to you:

- You may [schedule a private meeting](../../../faq#meetings-with-me) to talk to me. _Be aware: I have [Title IX reporting obligations](#title-ix-reporting-obligations)._

- Fill out the PSU [Bias Incident Report
  Form](https://goo.gl/forms/PMrV0tUbhDWqcBAy1)

- File a [formal
  complaint](https://www.pdx.edu/diversity/file-a-complaint-of-discriminationharassment)
  
- Contact the Office of Global Diversity and Inclusion: Market Center
  Building, 1600 SW 4th Avenue, Suite 830

- Contact the Dean of Student Life: <askdos@pdx.edu>



### Academic Integrity

_Note: Much of this section of the syllabus is copied (with permission) from the policy of [Nick Seaver](http://nickseaver.net); notable changes include the PSU student code of conduct, and commentary specifically relevant to computer science, such as computer programming details._

Our expressions are not our own. Humans communicate with words and
concepts — and within cultures and arguments — that are not of our own
making. Writing, like other forms of communication, is a matter of
combining existing materials in communicative ways. Different groups of
people have different norms that govern these combinations: modernist
poets and collagists, mashup artists and programmers, blues musicians
and attorneys, documentarians and physicists all abide by different sets
of rules about what counts as “originality,” what kinds of copying are
acceptable, and how one should relate to the materials from which one
draws.

In this course, you will continue to learn the norms of citation and
attribution shared by the community of scholars at Portland State
University and at other higher education institutes in the United
States. Failure to abide by these norms is considered plagiarism, as
laid out in the Student Code of Conduct[^6] with which you should
familiarize yourself:

> \(9) Academic Misconduct. Academic Misconduct is defined as, actual or
attempted, fraud, deceit, or unauthorized use of materials prohibited or
inappropriate in the context of the academic assignment. Unless
otherwise specified by the faculty member, all submissions, whether in
draft or final form, must either be the Student’s own work, or must
clearly acknowledge the source(s). Academic Misconduct includes, but is
not limited to: (a) cheating, (b) fraud, (c) plagiarism, such as word
for word copying, using borrowed words or phrases from original text
into new patterns without attribution, or paraphrasing another writer’s
ideas; (d) the buying or selling of all or any portion of course
assignments and research papers; (e) performing academic assignments
(including tests and examinations) in another person’s stead; (f)
unauthorized disclosure or receipt of academic information; (g)
falsification of research data (h) unauthorized collaboration; (i) using
the same paper or data for several assignments or courses without proper
documentation; (j) unauthorized alteration of student records; and (k)
academic sabotage, including destroying or obstructing another student’s
work.

_Any academic misconduct, including plagiarism, will result in a grade
of zero for the assignment concerned. All incidents of academic
misconduct will be reported to the PSU Conduct Office._

If you are uncertain about what constitutes plagiarism, I recommend
visiting me during my office hours and/or reviewing these online
resources:

- [Indiana University First Principles website](https://www.indiana.edu/~academy/firstPrinciples/)

- [Purdue Online Writing Laboratory (OWL)](https://owl.english.purdue.edu/owl/section/2/)

#### Attribution vs. Originality

The degree to which collaborative and derivative work is allowed in this
class may vary by assignment.

- **_In all cases, attribution is paramount._** Because this is an educational
  setting, all copied *and derivative* computer code or written text **_must
  be attributed_**.
- Even if you are using public domain writings or open source code, you must
  note in your own work all places from which you draw inspiration, ideas, or
  implementation details. Even if you start with someone else’s code or text
  and then modify it significantly, you should still cite the author of the
  original work that you used to get started. **_It is always better to
  over-attribute than under-attribute._**

In all cases, the **work you submit _as your own_ must actually be your own**.
It is not acceptable to hand in assignments in which substantial amounts of
the work was completed by someone else.

That said, many university plagiarism policies tend to focus on the
less productive side of the issue, urging students to be “original” and
telling them what not to do (buying papers, copying text from the
internet and passing it off as one’s own, etc.). It can be helpful to take more expansive view of what
academic integrity means. _Academic integrity is not so much a matter of
producing purely original thought, but of recognizing and acknowledging
the resources on which you draw_. For example, you will notice in the
acknowledgements throughout, that this syllabus draws heavily from a
wide range of other university professors. References to others' work both
lend your work additional authority and credibility, and also help the reader
understand what unique contributions you might have made in bringing
diverse resources together in a writing assignment or project.
Originality is not just about a singular novel idea, but may also be
about a novel combination of others' ideas.

In light of this, I do not use “plagiarism detection” services like
Turnitin. Rather than expending your energy worrying about originality,
I suggest that you think instead about what kind of citational network
you are locating yourself in. What thinkers are you thinking with? What
programmers are you coding with? Where do they come from? How might
their positions in the world inform their thoughts – and, in turn, your
thoughts? What is your position relative to these thinkers, writers, and
makers? How might you re-shape your citational network to better reflect
your priorities or ideals? How are you learning from or extending the
code or text in question? How do you think the original author would
feel about your use of their work? Are you generous in giving them
credit for the parts that are the result of their own hard work or are
you claiming it as your own?

If you are interested in these issues, I recommend these pieces:

- Ahmed, Sara. 2013. “Making Feminist Points.” *feministkilljoys*.
\<[feministkilljoys.com/2013/09/11/making-feminist-points/](http://feministkilljoys.com/2013/09/11/making-feminist-points/)\>

- Frank, Will. 2015. “IP-rimer: A Basic Explanation of Intellectual
Property.” Personal Blog Post (Medium).
\<[medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711](https://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711)\>

- This comment thread regarding proposed changes to the Stack Exchange
code licensing defaults:
\<[meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required](https://meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required?cb=1)\>

You may write a 500-word response to these pieces for extra credit. See
me in office hours for details.


##### Writing Assignments

*In the case of any writing assignments* such as reading responses and
term papers, I expect you to quote and reference course readings, course
discussions, and external sources. Indeed, your ability to locate and
use such sources is part of the skill that you should be developing and
demonstrating as a writer. At this point in your life, you should be an
accomplished communicator and thinker. However, for full points on any
writing assignment, I will also expect you to build on these works and
move beyond simply repeating others’ ideas; to bring your own unique
experiences, critiques, and perspectives into conversation with these
authors.


##### Coding Assignments

I certainly expect you to reference to course readings, official
language documentation, and in-class assignments in your homework projects.

However, it is not a good idea to look too far beyond these resources. In
introductory computing courses, in particular, we are not trying to produce
amazing programs, we are trying to develop our problem solving and
computational thinking skills. This often requires a bit of 'reinventing the
wheel.' A reliance on external sources in completing homework has two effects:

1. You will get confused because undoubtedly existing solutions will use more complex programming constructs than we have learned. This will make programming seem harder and more challenging than it actually is.
2. You will miss out on the opportunity to develop your own problem solving skills. You are likely already quite capable of searching for completed programs, copying them into a new file, and making some minor customizations. Do not waste the time and money your are spending on a university class to merely hone this skill further. Instead, try visiting my office hours or asking a question in class when you get stuck!

We are trying to learn the structure and logic of computing programs in this
class. If you are  starting with someone else's structure and logic but going
through and customizing things like variable names, the contents of the strings
that are printed to the screen, comments in the file, etc, then you are not
practicing the skills that this class aims to teach, and **you should expect a
zero on the assignment**.

All coding projects include a required written report. When you turn in your report, you must respond to each of the three questions listed below. Any deviations from a full and true account of your work practices in response to these questions is also grounds for a zero on the assignment.

- How much have I done myself? Did I use an external resource to answer a
    targeted question, or did I begin my project by starting with a nearly
    complete solution? Have I done substantive problem solving work or
    superficial editing work?

- Did I learn from the code that I integrated? Or, have I used others’ code
    in a way that undermined my own learning?

- Could I re-create the code if parts of the assignment were included
    as questions in a closed-book test?

**_If, at any point in the term, you have questions about what is or is
not acceptable with regards to reusing or modifying existing work,
please come see me in my office hours!_** I am more than happy to talk
about your projects; and would very much prefer to help guide you
through the work up front rather than scold you for plagiarizing after
the fact.

To summarize:

- When working on your homework, it is perfectly acceptable to:
    - Review course materials
    - Look things up in the [p5js](https://p5js.org/reference/) or
        [python](https://docs.python.org/3.7/library/) documentation
    - Work with a study group made up of people who are learning the same thing
        as you,
    - Look up specific questions online, such as when you forget the structure
        of a for loop in javascript.
- Do **not**, under any circumstances, begin a homework project by:
    - looking for completed code which you can then modify
    - asking a more experienced friend, family member, or stranger to outline a solution for you

I may ask you to explain your assignment verbally. If you cannot satisfactorily
explain what your code does, and answer questions about why you wrote it in a
particular way, then you should also expect a zero.


Q20) Do you have any questions about any of this right now?


Q21) Do you agree to ask me right away if you ever have questions about any of this in the future?

- Yes
- No

### Take Care of Yourself!

There are many resources available to support you at PSU. I encourage
you to take advantage of them so that you will be successful at both
your academic and non-academic pursuits.

**Library:** The library is full of helpful people who can assist you in research projects, finding course books and materials, connecting with other campus resources, and getting a public library card! The types of questions that they field are endless, and they would love to be part of your college success story. Say hello! [library.pdx.edu/services/ask-a-librarian/](https://library.pdx.edu/services/ask-a-librarian/)

**We in Computer Science (WiCS):** student organization promoting
inclusivity in computer science
[wics.cs.pdx.edu](http://wics.cs.pdx.edu)

**Writing Center:** The writing center can help you improve your writing!
Visit them! [pdx.edu/writing-center](http://pdx.edu/writing-center)

**Office of Information Technology (OIT):** Campus help desk for all
things technology-related [pdx.edu/oit](http://www.pdx.edu/oit)

**C.A.R.E. Team:** Central point of contact if you *or someone you know* is
having a difficult time – mentally, financially, physically, anything!
[pdx.edu/dos/care-team](http://pdx.edu/dos/care-team)

**PSU Food Pantry**: Located in SMSU 325. <foodhelp@pdx.edu>

**Center for Student Health and Counseling (SHAC):** free, drop-in mental
and physical health care [pdx.edu/shac](http://pdx.edu/shac) For mental health, their capacity is limited to a few appointments. However, they will help you find a referral for ongoing therapy or other care.

**Financial Wellness Center**: For many college students, money is an extremely important and sometimes stressful topic. They offer coaching sessions and meetings with peer mentors: [pdx.edu/student-financial/financial-wellness-center](https://www.pdx.edu/student-financial/financial-wellness-center)

**PSU Cultural Resource Centers (CRCs)** create a student-centered inclusive environment that enriches the university experience. We provide student leadership, employment, and volunteer opportunities; student resources such as computer labs, event, lounge and study spaces; and extensive programming. All are welcome! [pdx.edu/cultural-resource-centers](https://www.pdx.edu/cultural-resource-centers), <cultures@pdx.edu>, 503-725-5351, [facebook](https://www.facebook.com/psuculturalcenters/)

See the dean of student life website for further student resources:
[pdx.edu/dos/student-resources](http://pdx.edu/dos/student-resources)

#### Additional Mental Health & Counseling Resources

College can be a stressful time for a variety of reasons, and taking care of
your mental health is important! If SHAC isn't working out for you, these other
resources may be useful[^neera-thanks]:

[Community Counseling Clinic](https://www.pdx.edu/coun/clinic) - This is a low-cost resource for ongoing counseling, on campus, located in the grad school of education. The upside is that it is $15 per session for as long as you need, the potential downside is that the therapists are interns.

[M.E.T.A.](https://meta-trainings.com/) - This is an off-campus resource (Belmont x Cesar Chavez-ish). The cost is between $30-45, depending on the experience level of your intern and on your budget. Like the Community Counseling Clinic, it is with interns.

[Multnomah County Crisis Services](https://multco.us/mhas/mental-health-crisis-intervention) - if, at any point, you feel like you are in a mental health crisis, please call **(503) 988-488**. If this seems like a plausible scenario to you, I would put this number in your phone now. In addition to crisis services, they can also give referrals and information.

[^neera-thanks]: Resource list courtesy [Dr. Neera Malhotra](https://www.pdx.edu/profile/neera-malhotra), PSU, University Studies

Q22) Got it.

- I will take care of myself! And I will reach out to you if something comes up and I think I could use some help with this.

### Access and Inclusion for Students with Disabilities

This section is lightly edited from: [pdx.edu/drc/syllabus-statement](https://www.pdx.edu/drc/syllabus-statement)

PSU values diversity and inclusion; we are committed to fostering mutual respect and full participation for all students. My goal is to create a learning environment that is equitable, useable, inclusive, and welcoming. If any aspects of instruction or course design result in barriers to your inclusion or learning, please notify me. The Disability Resource Center (DRC) provides reasonable accommodations for students who encounter barriers in the learning environment.

If you have, or think you may have, a disability that may affect your work in this class and feel you need accommodations, contact the Disability Resource Center to schedule an appointment and initiate a conversation about reasonable accommodations. The DRC is located in 116 Smith Memorial Student Union, 503-725-4150, <drc@pdx.edu>, <https://www.pdx.edu/drc>.

- If you already have accommodations, please contact me to make sure that I have received a faculty notification letter and discuss your accommodations.
- Students who need accommodations for tests and quizzes are expected to schedule their tests to overlap with the time the class is taking the test.
- For information about emergency preparedness, please go to the Fire and Life Safety webpage <https://www.pdx.edu/environmental-health-safety/fire-and-life-safety> for information.

Q23) Do you have any accommodations that apply to this class?

- Yes
- No

Q24) If yes, what are they, and what should I know so that I can help support you in this class?


### Title IX Reporting Obligations

*This section is lightly edited from [this suggested syllabus statement](https://www.pdx.edu/diversity/sites/www.pdx.edu.diversity/files/Syllabus%20Statement%2062619%20for%20Title%20IX%20Reporting%20Obligations_REVISED.pdf).*

Title IX is a federal law that requires the university to appropriately respond to any concerns of sex/gender discrimination, sexual harassment or sexual violence. To assure students receive support, faculty members are required to report any instances of sexual harassment, sexual violence and/or other forms of prohibited discrimination to PSU’s Title IX Coordinator, [Julie Caron](https://www.pdx.edu/directory/name/jucaron).

If you would rather share information about these experiences with an employee who does not have these reporting responsibilities and can keep the information confidential, please contact one of the following campus resources (or visit this link <https://www.pdx.edu/sexual-assault/get-help>):

- Women’s Resource Center (503-725-5672) or schedule on line at <https://psuwrc.youcanbook.me>
- Center for Student Health and Counseling (SHAC): 1880 SW 6th Ave, (503) 725-2800
- Student Legal Services: 1825 SW Broadway, (SMSU) M343, (503) 725-4556
  
PSU’s Title IX Coordinator and Deputy Title IX Coordinators can meet with you to discuss how to address concerns that you may have regarding a Title IX matter or any other form of discrimination or discriminatory harassment. Please note that they cannot keep the information you provide to them confidential but will keep it private and only share it with limited people that have a need to know. You may contact the Title IX Coordinators as follows:

- PSU’s Title IX Coordinator: Julie Caron by calling 503-725-4410, via email at <titleixcoordinator@pdx.edu> or in person at Richard and Maureen Neuberger Center (RMNC), 1600 SW 4th Ave, Suite 830
- Deputy Title IX Coordinator: Yesenia Gutierrez by calling 503-725-4413, via email at <yesenia.gutierrez.gdi@pdx.edu> or in person at RMNC, 1600 SW 4th Ave, Suite 830
- Deputy Title IX Coordinator: Dana Walton-Macaulay by calling 503-725-5651, via email at <dana26@pdx.edu> or in person at Smith Memorial Union, Suite, 1825 SW Broadway, Suite 433
  
For more information about the applicable regulations please complete
the required student module Creating a Safe Campus in your D2L
[pdx.edu/sexual-assault/safe-campus-module](http://pdx.edu/sexual-assault/safe-campus-module)

Q25) Got it.

- I understand your reporting obligations, and I know who I can reach out to if I have a concern about sexual/gender harassment, discrimination, or violence.

Q26) We made it to the end! Do you have any remaining questions about the course? Is there anything else you think I should know or take into consideration as we begin the quarter?


### Notes

[^1]: See, for example, this year's news about Richard Stallman stepping down from the Free Software Foundation and MIT, or, last year's news about Linus Torvalds stepping away from maintenance of Linux as he seeks help for improving his interpersonal skills. Lawler, Richard. 2019. "GNU founder Richard Stallman resigns from MIT, Free Software Foundation: Calls to fire Stallman grew after his comments about victims of Jeffrey Epstein." _Engadget_, September 19, 2019. <https://www.engadget.com/2019/09/17/rms-fsf-mit-epstein/>; Cohen, Noam. 2018. "After Years of Abusive E-mails, the Creator of Linux Steps Aside," _The New Yorker_, September 29, 2018. <https://www.newyorker.com/science/elements/after-years-of-abusive-e-mails-the-creator-of-linux-steps-aside>

[^2]: See, e.g., Vivian Hunt et al. 2018. “Delivering through Diversity” Report by *McKenzie and Co.* January 2018. <https://www.mckinsey.com/business-functions/organization/our-insights/delivering-through-diversity>.

[^3]: See, e.g., Jane Margollis et al. 2008. *Stuck in the Shallow End: Education, Race and Computing.* MIT Press.; Steve Henn. 2014. "When Women Stopped Coding" *Planet Money*, NPR. <https://www.npr.org/sections/money/2014/10/21/357629765/when-women-stopped-coding>; Michelle Kim. 2018. "Why focusing on the “business case” for diversity is a red flag" *Quartz at WORK,* 29 March 2018. <https://work.qz.com/1240213/focusing-on-the-business-case-for-diversity-is-a-red-flag/>.

[^4]: See, e.g., J. McGrath Cohoon. 2001. Toward improving female retention in the computer science major. *Commun. ACM* 44, 5 (May 2001), 108-114.<http://dx.doi.org/10.1145/374308.374367>; Tracey Lien. 2015. “Why are women leaving the tech industry in droves?” *LA Times*. <http://www.latimes.com/business/la-fi-women-tech-20150222-story.html>.

[^6]: PSU Student Code of Conduct: <https://www.pdx.edu/dos/psu-student-code-conduct>
