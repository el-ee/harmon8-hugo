---
title: Schedule
cc: true
page-class: schedule
type: "class"
layout: "subpage"
icon: "fa-calendar-alt"
weight: 500

menu:
    main:
        parent: "Teaching"
        identifier: "cs199-201904-schedule"

quarter: 201904
---


| Week | T | R | S |
|:---|:---|:---|:---|
| 1 | Microbits I | Microbits II | P1 |
| 2 | JS: Intro | JS: Conditionals + Random | - |
| 3 | JS: Variables | JS: Functions | - |
| 4 | Demos  | C&S 1 | P2 |
| 5 | Py: Variables, Input  | Py: Conditionals  | - |
| 6 | Py: Functions  | Py: Loops  | - |
| 7 | Demos | C&S 2 | P3 |
| 8 | Py: Strings  | Py: Lists  | - |
| 9 | Py: Files  | No Class  | - |
| 10| Demos + Review | C&S 3 + Closing Surveys | P4 |
| **F** | **Final Exam<br/> Tuesday, December 10, 10:15-12:05** | | |

**Finals Week:** There will be a final exam in this class. The time is [scheduled by the registrar](https://www.pdx.edu/registration/final-exams). Make plans now to attend at the proper time.

_All programming projects are due by midnight on the Sunday at the end of the week specified._

### Unit 1: Microbits

#### Wk 1, Meeting 1

Course Introduction, Microbit Introduction

- In Class:
    - Microbit: Rock Paper Scissors Game
- TODO:
    - Complete the [syllabus survey](https://tinyurl.com/CS199-Fall2019)
    - Visit my office hours on sometime in weeks 1-3 _or_ [Schedule a 1-on-1 with me](/~harmon8/faq/#meeting-with-me) for a 15 minute block, sometime **in the first three weeks**.
    - Think about topics you are interested in reading about for the computing & society days.

#### Wk 1, Meeting 2

Microbits II

- In Class:
    - Discuss [P1 (Microbit)](../projects/#p1-microbit) in detail
    - Discuss computing + society topics + sign up
    - Simple Dot Game: Functions

#### Deadline: Sunday, 11:59pm (October 6)

- Due:
    - [P1 (Microbit)](../programming/#p1-microbit)


### Unit 2: Graphics with Javascript

#### Wk 2, Meeting 1

Introduction to JavaScript and text-based programming, using <http://repl.it>, using the processing library, drawing shapes, coordinate grids.

- Before class:
    - Create an account on <http://repl.it>
- _New Assignments:_
    - [Project 2](../projects/#p2-javascript), Due Sunday, Week 4, 11:59pm


#### Wk 2, Meeting 2

Drawing with processing, using the documentation. Conditionals & Random Numbers.

- Before Class:
    - [Learning on Khan Academy](https://www.khanacademy.org/computing/computer-programming/programming/intro-to-programming/a/learning-programming-on-khan-academy)
    - [KA: Drawing Basics (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/drawing-basics/pt/making-drawings-with-code)
    - [KA: Coloring (all 5 sections)](https://www.khanacademy.org/computing/computer-programming/programming/coloring/pt/coloring-with-code)

#### Wk 3, Meeting 1

Variables

- Before Class:
    - [KA: Variables (all 6 sections)](https://www.khanacademy.org/computing/computer-programming/programming/variables/pt/intro-to-variables)

#### Wk 3, Meeting 2

Functions, functions with parameters.

- Before Class:
    - [KA: Functions (**only the first 4 parts**)](https://www.khanacademy.org/computing/computer-programming/programming/functions/pt/functions)
- New Assignment:
    - [Computing & Society Reading 1](../computing-society)
        - **Discussion date:** Week 4, meeting 2.
        - **Reading:**
            - Newman, Lily Hay. 2018. “Get a Password Manager. Here’s Where to Start.” Wired, January 3, 2018. <https://www.wired.com/story/password-manager-autofill-ad-tech-privacy/>
            - Newman, Lily Hay. 2019. “How Incognito Google Maps Protects You—and How It Doesn’t.” Wired, October 2, 2019. <https://www.wired.com/story/google-maps-incognito-mode/>
            - Schneier, Bruce. 2018. “Patching Software Is Failing as a Security Strategy.” Excerpt from Click Here to Kill Everybody: Security & Survival in a Hyperconnected World as published in _Motherboard_. <https://motherboard.vice.com/en_us/article/439wbw/patching-is-failing-as-a-security-paradigm>

#### Wk 4, Meeting 1

Demos for Project 2

- New Assignment:
    - **Take Home Midterm**, due Week 5, Meeting 1, start of class

#### Wk 4, Meeting 2

Computing and Society Conversation #1

- Due:
    - **Reading Response 1**, start of class, on paper

#### Deadline: Sunday, 11:59pm (October 27)

- Due:
    - [P2 (JavaScript)](../programming/#p2-javascript)


### Unit 3: Python I

#### Wk 5, Meeting 1

Input, output, variables, and python style (resource: [PEP-8: Python Style Guide](https://www.python.org/dev/peps/pep-0008/))

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 2
    - In addition to a PDF, ePUB and Kindle version, these other options are also available:
        - If you want to try out code as you learn, use the [Interactive Version of the Textbook](https://books.trinket.io/pfe/index.html)
        - If you learn better by listening than by reading, try the [Video and Audio Lectures](https://www.py4e.com/materials)
- Due:
    - Take Home Midterm, start of class.
- _Assigned:_
    - [Project 3](../programming/#p3-python-i), Sunday, end of Week 7, 11:59p


#### Wk 5, Meeting 2

Conditionals, random numbers

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 3



#### Wk 6, Meeting 1

Functions

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 4


#### Wk 6, Meeting 2

Loops

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 5
- _New Assignments_:
    - [Computing & Society Reading 2](../computing-society)
        - **Discussion date:** Week 7, meeting 2.
        - **Reading:**
            - Mitchell, Melanie. 2019. “How Do You Teach a Car That a Snowman Won’t Walk across the Road?” Aeon, May 31, 2019. <https://aeon.co/ideas/how-do-you-teach-a-car-that-a-snowman-wont-walk-across-the-road>
            - Mitchell, Melanie. 2019. “AI Can Pass Standardized Tests—But It Would Fail Preschool.” Wired, September 10, 2019. <https://www.wired.com/story/ai-can-pass-standardized-testsbut-it-would-fail-preschool/>
            - Powles, Julia, and Helen Nissenbaum. 2018. “The Seductive Diversion of ‘Solving’ Bias in Artificial Intelligence.” Medium | Artificial Intelligence (blog). December 7, 2018. <https://medium.com/s/story/the-seductive-diversion-of-solving-bias-in-artificial-intelligence-890df5e5ef53>


#### Wk 7, Meeting 1

Demos - Project 3

#### Wk 7, Meeting 2

Computing and Society Conversation #2

- Due:
    - Reading Response 2, on paper, start of class



#### Deadline: Sunday, 11:59pm (November 17)

- Due:
    - [P3 (Python I)](../programming/#p3-python-i)



### Unit 4: Python II

#### Week 8, Meeting 1

Strings

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 6
- _New Assignments:_
    - [Project 4](../projects/#p4-python-ii), Due Sunday, end of Week 10, 11:59pm


#### Week 8, Meeting 2

Lists!

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 7



#### Week 9, Meeting 1

Files, CSV format, simple data processing

- Before Class:
    - Read [Py4E](https://www.py4e.com/book), Chapter 8
- New Assignment:
    - [Computing & Society Reading 3](../computing-society)
        - **Discussion date:** Week 10, meeting 2.
        - **Reading:**
            - Option 1: Surveillance
                - Rejouis, Gabrielle M. 2019. “Why Is It OK for Employers to Constantly Surveil Workers?” Slate, September 2, 2019. <https://slate.com/technology/2019/09/labor-day-worker-surveillance-privacy-rights.html>
                - Ram, Natalie, Christi J. Guerrini, and Amy L. McGuire. 2018. “Genealogy Databases and the Future of Criminal Investigation.” Science 360 (6393): 1078–79. <https://doi.org/10.1126/science.aau1083>
            - Option 2: Gene Editing
                - Dolgin, Elie. 2018. “Scientists Downsize Bold Plan to Make Human Genome from Scratch.” Nature 557 (May): 16–17. <https://doi.org/10.1038/d41586-018-05043-x>
                - Agapakis, Christina. 2018. The story of synthetic biology is still being written—don’t leave the women out this time. SynBioBeta (blog). September 12, 2018. <https://synbiobeta.com/the-story-of-synthetic-biology-is-still-being-written-dont-leave-the-women-out-this-time/>


#### Week 9, Meeting 2

No Meeting. Thanksgiving.


#### Week 10, Meeting 1

Demos - Project 4

Final Exam Review

#### Week 10, Meeting 2

Computing and Society Conversation 3 + End of term surveys & closing self-assessment

- Due:
    - Reading Response 3, start of class, on paper


#### Deadline: Sunday, 11:59pm (December 8)

- Due:
    - [P4 (Python II)](../programming/#p4-python-ii)

#### Final Exam

**Tuesday, December 10, 10:15-12:05**

