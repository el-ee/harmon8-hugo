---
title: Programming Projects
cc: true
type: "class"
layout: "subpage"
icon: "fa-laptop-code"
weight: 1000

menu:
    main:
        parent: "Teaching"
        identifier: "cs199-201904-programming"

quarter: 201904
---

Four programming projects will offer you the opportunity to practice and develop your skills this quarter.

## Pro Tips

1. **Start small and simple.** There's nothing wrong with not doing the extra enrichment. People are starting at lots of different places in this class. Regardless, don't _start_ with the extra enrichment. One of the most important things you can learn in this class is how to break down a problem into smaller pieces, and build your program up from functional smaller building blocks. You can't code everything at once! I have tried to outline a set of suggested steps for most of the projects. Follow them! Don't come to my office hours looking for help on step 7 when you haven't gotten step 1 working yet!
2. **Skip the YouTube how tos.** Inevitably, these will involve concepts and techniques that are different and more complicated from what we are doing in class. You will get confused! Keep it simple. Everything you need to know for an assignment has already been covered in class.
3. **Find a study group.** You will have many opportunities in class to work with a partner. Take advantage of this to meet different people in the class and find some people you like to work with. Unlike YouTube videos, these folks are learning the same things as you, and can be an invaluable resource for helping you figure out answers to your questions, or clear up confusions from class.

## Academic integrity reminder

Please review the [coding section](../policies#coding-projects) of the course academic integrity policy before beginning these assignments. Some highlights:

- When working on your homework, it is perfectly acceptable to:
    - Review course materials
    - Look things up in the [p5js](https://p5js.org/reference/) or
        [python](https://docs.python.org/3.7/library/) documentation
    - Work with a study group made up of people who are learning the same thing
        as you,
    - Look up specific questions online, such as when you forget the structure
        of a for loop in javascript.
- Do **not**, under any circumstances, begin a homework project by:
    - looking for completed code which you can then modify
    - asking a more experienced friend or family member to outline a solution
        for you

We are trying to learn the structure and logic of computing programs in this
class. If you are  starting with someone else's structure and logic but going
through and customizing things like variable names, the contents of the strings
that are printed to the screen, comments in the file, etc, then you are not
practicing the skills that this class aims to teach, and **you should expect a
zero on the assignment**.

I may ask you to explain your assignment verbally. If you cannot satisfactorily
explain what your code does, and answer questions about why you wrote it in a
particular way, then you should also expect a zero.

### P1: Microbit

In this project, you will explore the microbit environment in order to familiarize yourself with some basic programming concepts.


#### Logistics

**Deadline:** Sunday, October 6, 11:59p

**What to turn in:**
1. **Your Code:** a `.hex` file that you save from the microbit website with your code*
2. **Your Report:** in `.pdf` or Google Docs format

**How to turn it in:** Google Classroom **P1: Project 1 (Microbit)**




#### Resources & Key concepts

In this project, you will explore some of these core programming concepts:

> input (events), output, on start block, random numbers, variables, sprites, coordinate grid, forever loop, conditionals, functions

#### Specification: Program

In this project, you will create a simple program in the micro:bit environment that demonstrates your own creativity and explores some programming concepts that we will be reinforcing throughout the terms. There are only a few key requirements:

- You create your own program, from scratch.
- You demonstrate the use of at least 6 of the 10 concepts in the list.
- Your program has a _point_. It does _something_ that you can explain in words
    that makes sense (i.e., you did not just drag random blocks onto the screen
    and say look it does a thing, but who knows what).
- Your program is something novel created by _you_:
    - It is not copied off of a micro:bit demo site, implemented by following a YouTube video, etc
    - It is not just a minor modification of something we did in class.


#### Specification: Report

_Note: This report is **required** and you will receive a zero on the
assignment if you do not turn one in._

Try to keep your report to no more than one page.

1. Your Program:
    - What does your program do? (Explain it from the user's point of view.)
    - Why did you choose to make this program?
    - What grade would you give yourself on this project, and how would you justify that grade?
3. Your Experience:
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During week 1, how much time did you spend, outside of class, working on this project, and all other activities related to this class?
3. External Resources:
    - What other resources did you use in completing this project, if any?
        - Since you cannot easily leave comments in the micro:bit code environment, please use this part of the report to give credit and acknowledge any work done by others that you integrated in your project. If you looked anything up online, or used any other resources in completing this project, you must respond to these three questions from the course syllabus:
        -   How much have I done myself? Did I use an external resource to
            answer a targeted question, or did I begin my project by starting
            with a nearly complete solution? Have I done substantive problem
            solving work or superficial editing work?
        -   Did I learn from the code that I integrated? Or, have I used
            others’ code in a way that undermined my own learning?
        -   Could I re-create the code if parts of the assignment were included
            as questions in a closed-book test?  
4. Core Concepts: List each of the 10 concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of microbit code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I could find a loop in the microbit side bar and correctly fill in the block)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when to seek out this construct and how to use it to accomplish another goal

_Note: your answers to the Core Concepts part of the assignment will not impact your grade; I am just trying to get a better grasp of how much all these different concepts really make sense to you! You should answer the questions honestly! I am trying to better understand how students' learning is progressing over the quarter._

#### Grading

This assignment is worth 7.5 points total. You can earn these points by:

- **0 / 7.5**: Not completed, not your own work, contains plagiarized material,
    written report not completed, or written report not completed truthfully.
- **+4.5 points**: Something is turned in that shows compelling evidence of
    independent work, even if the program is not fully functional.
- **+0.5 point**: Your code functions as per your description in the report.    
- **+0.75 point**: Your code demonstrates at least 3 of the programming concepts
    listed above.
- **+0.75 point**: Your code demonstrates at least 6 of the programming concepts
    listed above.
- **+0.5 point**: Report is complete, and shows evidence of significant student effort, seriousness, thoughtfulness, and reflection
- **+0.5 point**: Program embodies a creative use of the micro:bit features to do something interesting, fun, and challenging.


### P2: JavaScript

In this project you will create a program that creates a visual composition using the JavaScript Processing Library.

This assignment is adapted from [Intro CS coursework by Vera Khovanskaya](courses.cit.cornell.edu/info1100_2017su/).

#### Logistics

**Deadline:**: Sunday, October 27, 11:59p

**What to turn in:**
1. **Your Code:** a `.zip` file that you save from repl.it with all your code
1. **Your Report:** in `.pdf` format

**How to turn it in:** Google Classroom


#### Resources & Key concepts

In this project, you will practice using these concepts:

> setup function, draw function, random numbers, loops, variables, coordinate grid, conditionals, your own functions, comments

You may choose to investigate these additional concepts:

> [events (input)](https://p5js.org/reference/#group-Events), [text display](https://p5js.org/reference/#group-Typography)

#### Specification: Program

In this project you will use the processing library and JavaScript to create an interesting visual composition. Your composition may be a still image or an animation. It may be representative of something that exists in the world or it may be abstract. The only requirements are that you demonstrate your skills in each of these areas:

1. Good usage of the setup and draw functions.
    - Think about what is different between these two functions. Is your code in the right place?
2. Good usage of comments.
    - Are there comments throughout your code that explain what you are instructing the computer to do?
    - Did you comment out any broken code with a note about why it is commented out?
3. Conditionals (if statements).
    - For full credit you should use at least one conditional somewhere in your code. You might use this in an animation to test if something is off the screen, or you might use this in connection with the random number function to make a decision about what to draw, what color to use, etc.
4. Good use of loops for abstraction.
    - For full credit, you should use at least one loop (other than the draw function) to handle repetition in your code.
5. Good use of functions for abstraction.
    - For full credit, you should create at least one custom function and use it to draw something in your program.
    - Your custom function should take at least one argument.
    - There should be a humanly noticeable effect that occurs when the argument value(s) change(s) (e.g., some part of the composition changes color; shapes appear in different locations)
    - You should document your function's arguments using standard comments. Make sure to specify any rules for the arguments. For example, if you accept a color as one of the input variables, and it must be a color that processing understands, then include a link to the list of colors.

##### Extra Enrichment

If you get all of the above working, look at the [events (input)](https://p5js.org/reference/#group-Events) and/or [text display](https://p5js.org/reference/#group-Typography) sections of the P5.js documentation. See if you can figure out how to incorporate one of these features into your program.
- You may earn up to 1 bonus point for getting either working. You **MUST** document this in your report and in comments in the program itself.

##### Recommended Procedure
1. Start by planning on paper. What kind of a composition might be interesting and fun for you to work on? Sketch out what you want to make. Think about how it will overlay on the coordinate grid.
3. Build up a simple image without variables, functions, or animations using different drawing commands. Add one command at a time and test your code each time to make sure that it works.
4. Once you are satisfied with your image, find a piece of it that you might want to repeat (or notice some repetition that you already have). Define a function to draw this piece of the composition.
    1. Move all the code for that part of the composition into your function. Make sure to call the function in your program where that code used to be. Test it to make sure everything still works.
    2. Substitute variables for some of the numbers inside your function. Each time you put a variable in, add it to the argument list in your function definition and substitute the variable name in for its place in your code. Again, work step by step and test after each change.
    3. Play with the variable assignments. Vary them and see what effect this has on your composition. When you find an outcome that you like, note the value of the argument in a comment in your code.
5. You might decide, as you work, to change your code. _**If so, go step-by-step and test each time to make sure things still work.**_
6. Before submitting, review the list of requirements to make sure you have fulfilled all of them.
7. Go through your code and add comments anywhere where it might not be clear to anyone reading your code.

#### Specification: Report

_Note: This report is **required** and you will receive a zero on the
assignment if you do not turn one in._

Try to keep your report to no more than one page.

<!-- TODO: photo of a plan/sketch -->

1. Your Program:
    - What does your program do? (Explain it from the user's point of view.)
    - Why did you choose to make this program?
    - Did you complete any of the extra enrichment portions of the assignment? If so, how and why have you satisfied the extra enrichment requirement?
    - What grade would you give yourself on this project, and how would you justify that grade?
3. Your Experience:
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During weeks 2, 3, and 4, how much total time did you spend, outside of class, working on this project, and all other activities related to this class?
    - How is programming in JavaScript different from using the micro:bit system? Which one did you like better?
3. External Resources:
    - What other resources did you use in completing this project, if any?
        -   How much have I done myself? Did I use an external resource to
            answer a targeted question, or did I begin my project by starting
            with a nearly complete solution? Have I done substantive problem
            solving work or superficial editing work?
        -   Did I learn from the code that I integrated? Or, have I used
            others’ code in a way that undermined my own learning?
        -   Could I re-create the code if parts of the assignment were included
            as questions in a closed-book test?     
4. Core Concepts: List each of the concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of JavaScript code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I write the code for a loop.)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when I would need to seek out this construct and how to use it to accomplish some other goal


#### Grading

This assignment is worth 15 points total. You can earn these points by:

- **0 / 15**: Not completed, not your own work, contains plagiarized material,
    written report not completed, or written report not completed truthfully.
- **+9 points**: You turned in some evidence of independent work:
    - Your program may not run, and may show serious gaps in understanding, but
        shows evidence of clear effort.
- **+1 point**: Your program is at least partially complete.
    - Some bigger errors, but with potential to develop into a competent
        response. For example, the code may not run, but with a few small
        syntactical fixes it would properly fulfill the assignment; the code
        may work but is not well-commented and is therefore confusing to read;
        or the code completely misses on one or two of the requirements, but
        still shows some understanding of the unit's material.
- **+3 points**: Complete and functional program, meeting all requirements
    - **+0.6 point**: Good usage of the setup and draw functions.
    - **+0.6 point**: Good usage of comments.
    - **+0.6 point**: Good usage of conditionals (if statements).
    - **+0.6 point**: Good use of loops for abstraction.
    - **+0.6 point**: Good use of functions for abstraction.
- **+1 point**: Report is complete, and shows evidence of significant student
    effort, seriousness, thoughtfulness, and reflection
- **+1 point**: Program embodies a creative use of the Processing features to
    do something interesting, fun, and challenging.
- **+1 point**: Extra Enrichment (bonus)
    - Integrated events and/or text in an interesting way.
    - Extra enrichment is documented with comments in the code and described in the report.

(Maximum score is 16 but will be calculated out of 15 points. Up to one bonus point is available for event / text extra enrichment.)



### P3: Python I

In this project, you will create a program that allows a user to play a simple game. In your program you must demonstrate your knowledge of user input, print statements, variables, string concatenation, and if statements (conditionals).

A few suggestions include:
- A MadLibs type game, where the user enters words, and you fill in a story based on their words. You will need to be creative about how you can incorporate a conditional in a meaningful way.
- A fortune teller, where you ask the user a series of questions, and then generate a fortune based on their answers.
- A **simplified** Oregon Trail / text-based dungeon game. It is easy to make this choice over-complicated. For you to be successful, you will need to keep your game small and simple.
- If you have another idea, please run it by me during office hours.

_Parts of this assignment are adapted from [Intro CS coursework by Tammy VanDeGrift](http://sites.up.edu/sigcse2015/)._


#### Logistics

**In Class Demos:** Tuesday, Week 7

**Deadline:** Sunday, November 17, 11:59p

**What to turn in:**
1. **Your Code:** a `.py` file that you save from repl.it with your code
1. **Your Report:** in `.pdf` format

**How to turn it in:** Google Classroom

#### Key concepts

In this project, you will practice using these concepts:

> variables, input/output, conditionals (if statements or 'logic'), comments, comparisons (e.g., equal to, greater than, less than), string concatenation

You may choose to use these additional concepts, but it is not required:

> functions, random numbers, while loops

#### Specification:

- At the start of the program, you should greet the user and give a short informative message about the game you made
- You should prompt the user to enter information relevant to your game, and then respond in turn.
- When the game ends, you should tell the user it's over before exiting.
- General requirements:
    - You must prompt the user for input at least 6 times.
    - You must use the print command to communicate back with the user in a meaningful way.
    - You must use at least one conditional in a meaningful way.
    - You must use string concatenation with user input. That is, you must somehow combine the text that the user enters with some other pre-determined text that is part of your program.
- Extra enrichment:
    - Use the try/catch pattern to protect your program from errors (and re-prompt the user for valid input).
    - Give the user the ability to restart the program at the end.
    - Use a function in a _meaningful_ way.

#### Spec: Report

_Note: This report is **required** and you will receive a zero on the
assignment if you do not turn one in._

Try to keep your report to no more than one page.

1. Your Program:
    - What does your program do? (Explain it from the user's point of view.)
    - Why did you choose to make this program?
    - Did you complete any of the extra enrichment portions of the assignment? If so, how and why have you satisfied the extra enrichment requirement?
    - What grade would you give yourself on this project, and how would you justify that grade?
2. Your Experience:
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During each of the last 3 weeks, about how much time did you spend, outside of class, working on this project, and doing all other activities related to this class?
    - How is programming in Python different from using JavaScript? Which one do you like better?
3. External Resources:
    - What other resources did you use in completing this project, if any? If you used anything beyond the textbook or in-class notes, then also answer these questions:
        -   How much have I done myself? Did I use an external resource to
            answer a targeted question, or did I begin my project by starting
            with a nearly complete solution? Have I done substantive problem
            solving work or superficial editing work?
        -   Did I learn from the code that I integrated? Or, have I used
            others’ code in a way that undermined my own learning?
        -   Could I re-create the code if parts of the assignment were included
            as questions in a closed-book test?     
4. Core Concepts: List each of the concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of python code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I write the code for a loop.)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when I would need to seek out this construct and how to use it to accomplish some other goal    

_Note: your answers to the Core Concepts part of the assignment will not impact your grade; I am just trying to get a better grasp of how much all these different concepts really make sense to you! You should answer the questions honestly! I am trying to better understand how students' learning is progressing over the quarter._


#### Grading

This assignment is worth 15 points total. You can earn these points by:

- **0 / 15**: Not completed, not your own work, contains plagiarized material,
    written report not completed, or written report not completed truthfully.
- **+9 points**: You turned in some evidence of independent work:
    - Your program may not run, and may show serious gaps in understanding, but
        shows evidence of clear effort.
- **+1 point**: Your program is at least partially complete.
    - Some bigger errors, but with potential to develop into a competent
        response. For example, the code may not run, but with a few small
        syntactical fixes it would properly fulfill the assignment; the code
        may work but is not well-commented and is therefore confusing to read;
        or the code completely misses on one or two of the requirements, but
        still shows some understanding of the unit's material.
- **+3 points**: Complete and functional program, meeting all requirements
    - **+0.6 point**: Good usage of comments.
    - **+0.6 point**: Prompts the user at least 6 times.
    - **+0.6 point**: Uses the print statement to communicate with users.
    - **+0.6 point**: Uses at least one conditional in a meaningful way.
    - **+0.6 point**: Uses string concatenation with user input and pre-determined text.    
- **+1 point**: Report is complete, and shows evidence of significant student
    effort, seriousness, thoughtfulness, and reflection
- **+1 point**: Program embodies a creative use of Python features to
    do something interesting, fun, and challenging.
- **+1 point**: Extra Enrichment (bonus, up to 1 point maximum)
    - **Note: Extra enrichment attempts must be documented with comments in the code AND described in the report for any extra points.**
    - Integrated a function in a meaningful way. (.5 points)
    - The user can re-start the program at the end as many times as they want (.5 points)
    - Added some error checking to the program, including the use of the try/catch pattern (.5 points)

The maximum score is 16 but will be calculated out of 15 points. One bonus point (total) available for the extra enrichment.


### P4: Python II

For your final project, you have three choices:
1. A word-guessing game like a simplified hangman or wheel of fortune
2. A simple Python program to extract and process data from a .csv file
3. Revise your P3/Python I project to use a list or external file

#### Logistics

**In Class Demos:** Tuesday, Week 10

**Deadline:** Sunday, December 8, 11:59p

**What to turn in:**
1. **Your code:** a `.zip` file that you save from repl.it with all of your code and your dataset/necessary files (if applicable)
2. **Your report** in `.pdf` format

**How to turn it in:** Google Classroom

#### Key Concepts

In this project, you will practice using these concepts:

> variables, input/output, conditionals (if statements, logic), comments, functions, loops, lists, and _maybe_ files!

#### Option 1: Word Guessing Game
In this project, you will create a simple word or phrase-guessing game that
works like hangman or a _simplified_ Wheel of Fortune.

1. Basic Program (**START HERE**):
    - Uses a variable hard-coded into the program to store a list of words
        or phrases that the user will try to guess
    - Randomly chooses a word/phrase from this list
    - Prints out a brief explanation of the game, and asks the user to guess
        a letter
    - After a guess, reports to the user as to whether or not that letter
        is in the word
    - That's all! Then just exit!
2. Intermediate Program:
    - **Do not proceed to these features until the basic program is fully functional**.
    - In addition to the above, the program displays the user's progress towards guessing the word before exiting.
2. Complete Program:
    - **Do not proceed to these features until the intermediate program is fully functional**.
    - In addition to the above, the program repeats until the user has guessed all letters
    - _See sample interaction sequence below._
3. Extra Enrichment:
    - Complete **three** for the full extra credit point.
        - Use a text file to store the list of words or phrases.
            At the start of your program, read in the list of words,
            and then pick a random one from the list.
        - Create a way for the user to say they're ready to guess the
            whole word (e.g., enter a letter to proceed as before,
            or type GUESS to skip ahead to guessing the whole word/phrase)
        - Create a way for the user to play again -- with a new
            word or phrase.
        - Keep track of previously guessed letters. Don't let the
            user guess something they've already guessed.
        - Keep score -- however you like, just explain in a print
            statement at the start of your program how it works.
            For example, the user could earn points for guessing
            consonants, and have to spend points to guess a
            vowel (like wheel of fortune). Or, users could have a
            limited number of guesses (e.g., 10) before the game
            ends (like hangman).

##### Sample Interaction Sequence - Option 1 - Complete:
```
In this game, you guess letters until you figure out the word:
_________
Guess a new letter:	a
Yes that letter is in the word!
a____a___

Guess a new letter:	l
Yes that letter is in the word!
all__a___

Guess a new letter:	i
Yes that letter is in the word!
alli_a___

Guess a new letter:	g
Yes that letter is in the word!
alliga___

Guess a new letter:	t
Yes that letter is in the word!
alligat__

Guess a new letter:	r
Yes that letter is in the word!
alligat_r

Guess a new letter:	o
Yes that letter is in the word!
alligator

You win!
```            


#### Option 2: Data Exploration with Python
1. Choose one dataset to work with:
    - There are several ready to use in the “DataSets for Project 1” folder at this link:
        - <https://www.dropbox.com/sh/fburagb6keb97hv/AACaOGDE0yLyZCuBWCiLf_Rda?dl=0>
    - If there is another data set that you would like to use, please run it by me first.
2. Create a new python program on repl.it, and upload the `.csv` file you will use in your project.
3. Write a python program to open your dataset file, and explore its contents in order **to answer at least two questions about the dataset**. For example, you could count the number of days in a certain year with at least 1 inch of rain, and compute an average rainfall amount for a certain month or year.
    - **Your program should have a comment at the top of the file that explains what it does, including what 2 questions it answers about the dataset.** The questions must be more complicated than simply counting the number of lines in the file.
    - When run, you program should display a message to the user that explains what it does.
    - When run, the program should display a message that states the answer to each question you have explored in the dataset.
    - _See sample program interaction sequence below._
4. **Extra Enrichment**: Allow the user to specify which question
    in the survey they want information about.
    _See sample interaction sequence below_.
4. **See report specification below.**

##### Sample Interaction Sequence — Option 2 - Complete:  
```
This program analyzes the November 2016 Pew Dataset about Information Engagement.

The number of total respondents in the survey was: 3015

Of the total, 2640 (87.56218905472637 %) use the internet.
Of the total, 2508 (83.18407960199005 %) use the internet
    through a mobile device at least occasionally.
```

##### Sample Interaction Sequence — Option 2 - Extra:  
```
This program allows you to query for summary statistics about the
Nov 2016 Pew Dataset. Please refer to the dataset for a list of
question and answer codes.

Enter a question code:
	q1a
Enter an answer code:
	1

The number of total respondents in the survey was: 3015
935 respondents (31.01160862354892 %) answered 1 to question q1a.
```

#### Option 3: Revise your P3 program, using a list or file.

For this option, you need to revise the program you created for P3:

1. **Add at least 1 new feature:** this should be meaningful from a user's perspective. That is, when someone goes to use your program, something different happens in terms of the program's execution and flow.
2. **Improve the code in some way:** such as abstracting out some repeated code into a loop or function, adding better comments, re-organizing your code so that it makes more sense to read, or improving the way you use variables.
3. **Use _either_ a list or a file in a meaningful way.**
4. **Document all of your changes/improvements in comments within the python file:** The top of your file should contain an overview comment that highlights the major changes which fulfill the above requirements. For example, "I added a new function (see lines 6-10) to replace code that was previously repeated in the original program."
5. **Extra enrichment:** Incorporate both a list and an external file in a meaningful way.

#### Report Specification

_Note: This report is **required** and you will receive a zero on the
assignment if you do not turn one in._

Try to keep your report to no more than one page.

1. Your Program:
    - Did you choose option 1, 2, or 3? Why?
    - What does your program do? (Explain it from the user's point of view.)
    - Did you complete any of the extra enrichment portions of the assignment? If so, how and why have you satisfied the extra enrichment requirement?
    - What grade would you give yourself on this project, and how would you justify that grade?
2. Your Experience:
    - What are you most proud of about this project? Why?
    - What challenges did you run into while completing this assignment? What would have made you more successful?
    - What would have made this assignment better?
    - During each of the last 3 weeks, about how much time did you spend, outside of class, working on this project, and doing all other activities related to this class?
    - How did this project compare to prior projects in this class? Which one did you like the best? Why?
3. External Resources:
    - What other resources did you use in completing this project, if any?
        -   How much have I done myself? Did I use an external resource to
            answer a targeted question, or did I begin my project by starting
            with a nearly complete solution? Have I done substantive problem
            solving work or superficial editing work?
        -   Did I learn from the code that I integrated? Or, have I used
            others’ code in a way that undermined my own learning?
        -   Could I re-create the code if parts of the assignment were included
            as questions in a closed-book test?     
4. Core Concepts: List each of the concepts that can be found under the ‘Resources and Key Concepts’ header at the top of this section. Rate your level of understanding of each one using these terms:
    - Not sure — I don’t remember this one.
    - Remember — I remember this term, and could recognize it in an image of Python code
    - Comprehend — I can describe what this term means in simple English
    - Apply — I can use this concept in a program (e.g., If someone said ‘use a loop’ I write the code for a loop.)
    - Analyze / Synthesize — If given a programming challenge, I think I could know when I would need to seek out this construct and how to use it to accomplish some other goal    

_Note: your answers to the Core Concepts part of the assignment will not impact your grade; I am just trying to get a better grasp of how much all these different concepts really make sense to you! You should answer the questions honestly! I am trying to better understand how students' learning is progressing over the quarter._




#### Grading

This assignment is worth 15 points total. You can earn these points by:

- **0 / 15**: Not completed, not your own work, contains plagiarized material,
    written report not completed, or written report not completed truthfully.
- **+9 points**: You turned in some evidence of independent work:
    - Your program may not run, and may show serious gaps in understanding, but
        shows evidence of clear effort.
- **+1 point**: Partially Complete Program
    - May have some bigger errors, but with potential to develop into a competent response. For example, the code may not run, but with a few small syntactical fixes it would properly fulfill the assignment; the code may work but is not well-commented and is therefore confusing to read; or the code completely misses on one or two of the requirements, but still shows some understanding of the unit's material.
- **+1 point**:
    - Option 1: program meets requirements for basic features
    - Option 2: clear attempt to answer a question about the dataset, but your code is not accurate (e.g., pulled wrong data from file)
    - Option 3: the code quality is meaningfully _improved_ from the P3 turn in, and this is documented with comments
- **1 point**: 
    - Option 1: program meets requirements for intermediate features
    - Option 2: program correctly answers at least 1 question about the dataset
    - Option 3: program has at least one major new feature compared to P3 and this is documented with comments
- **+1 point**: Complete Feature/Functionality Requirements
    - Option 1: program meets requirements for complete features
    - Option 2: program correctly answers 2 different questions about the dataset
    - Option 3: program successfully incorporates either a list or file in a meaningful way
- **+1 point**: Report is complete, and shows evidence of significant student
    effort, seriousness, thoughtfulness, and reflection
- **+1 point**: Program embodies a creative use of Python features to
    do something interesting, fun, and challenging.
- **+1 point**: Extra Enrichment is completed as specified in the specs for the option you chose.


(Maximum score is 16 but will be calculated out of 15 points. One bonus point (total) available for the extra enrichment.)
