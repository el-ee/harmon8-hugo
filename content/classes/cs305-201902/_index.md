---
title: "CS 305 (Spring 2019)" 
date: 2019-03-01
courseNumber: "CS 305" 
courseTitle: "Social, Ethical, and Legal Implications of Computing " 

shortDescription: "This course includes a survey of topics related to the social, ethical, and legal implications of computing, including: the history of computing, the social context of computing, professional and ethical responsibilities, the risks and liabilities of safety-critical systems, intellectual property, privacy and civil liberties, the social implications of the Internet, computer crime, and economic issues in computing."

meetings: "R 4:40-6:30"
location: "FAB 48"

type: "class"
layout: "class-home"

menu: 
    main:
        parent: "Teaching"

quarter: 201902 

---

## Course Description

History of computing, social context of computing, professional and ethical 
responsibilities, risks and liabilities of safety-critical systems, 
intellectual property, privacy and civil liberties, social implications of the 
Internet, computer crime, economic issues in computing. [^1]

[^1]: This section is unedited text from: <https://www.pdx.edu/computer-science/cs305>

## Course Goals

The course has two goals: First, the usual goal of learning the material of the course as described in the catalog entry. A higher priority goal is to instill in you an inclination to use that knowledge. The use of the content of this course is optional in the real world and our primary goal is to motivate its use.

Upon the successful completion of this course students will be able to:

1. Identify the ethical issues that relate to computer science in real situations they may encounter.
2. Decide whether a given action is ethical as regards computer science professional ethics, and justify that decision.
3. Look up relevant ethical standards as developed by the ACM.
4. Prepare and deliver a short (8-10 minute) professional-quality talk on a topic relating to ethical, legal, and social implications of computer science.
5. Research and write a professional-quality paper about a topic relating to social, legal, and ethical implications of computer science.
6. Recognize situations in which there may be legal issues as regards computer science and related topics such as intellectual property, and know some legal principles to apply.
7. State several important impacts of computer science and related fields on contemporary society.
8. State several examples of important ethical principles as they apply to computer science related situations.[^2]

[^2]: This section is unedited text from: <https://www.pdx.edu/computer-science/cs305>



