---
title: Readings
cc: false
type: "class"
layout: "subpage"
weight: 1000

menu: 
    main:
        parent: "Teaching"
        identifier: "cs305-201902-readings"

quarter: 201902
---

On weeks 2, 3, 4, 6, 7, 8 and 9, there will be a reading assignment to prepare you for class discussion. As part of this assignment, you will write a brief reading response to demonstrate your understanding of the material, communicate your opinion, and prepare for the discussion.

**You only need to complete _6_ of the 7 reading responses**. Think about when in the quarter you will most need a break! And plan to give yourself a break!

**Deadline:** Start of class.  
**How to Turn in:** Choose 1:
- Bring to class, on paper.
- Turn in on D2L, no later than **noon** and I will print it for you.

_We will circulate and read these at the start of class. Think of your classmates as your audience when you are writing them._

## Specification

Reading responses should be around 300-500 words and address all readings for the day. **Do not** exceed 750 words.

In each reading response, you should do all of the following:

1. **Summarize the main point of _each_ reading/video/podcast.** For example, you might answer any of these questions to do this: 
    - What is the central problem or question in the reading/video/podcast? What answer(s) or proposal(s) are given in response? 
    - What is the argument that the author is trying to make? What evidence is presented in support of this argument?
    - What is the socio-technical situation described in the article? Who benefits? Who is harmed? How & why?
    - _Note: Your summary should not be a list of facts from the article, but should be a higher-level overview of the **main point**._
2. **Further reflect on the reading in the context of our class**, by answering one or more of the following questions:
    - If there are multiple readings, identify linkages across the set of readings for the day. While not always on exactly the same topic, the readings are meant to be read together, and you should be able to draw some kind of connection between them. How do they speak to each other? 
    - Identify linkages between the current readings and topics or readings from prior classes. How do the readings relate to prior discussions? How do the readings relate to the specific computer systems we are learning to use?
    - Ask questions about new concepts or things that were not clear. Some of the readings are challenging and you are not expected to understand every single thing in each reading. Asking questions is not only okay, it is good!
3. **Communicate your personal opinion or takeaways** by answering one or more of the following questions:
    - What is your opinion on the subject? Do you agree or disagree with the argument presented? Why?
    - What did you learn from reading the piece? And/or what new questions do you have?
    - How does the reading relate to your own area of study, work, or everyday life?
    - Is the argument compelling? Why/not? Is the evidence presented sufficient? Why/not?
4. **Identify one recent news story _from a reputable source_ related to the week's topic**. Include all of the following:
    - A full citation including a link if it is available online. 
    - A brief summary of the main point
    - A brief explanation of why you chose it / how it relates to the week's reading(s).

**Please prepare your response with attention to detail and presentation**:
- Appropriately reference the author and title/subject of the piece in the response (i.e., Do not write things like "In the first reading the author raised a question about ..." Instead, you should write things like this: "In an article about internet censorship, AuthorName raised a question about ..."). Practice using Zotero, BibTeX, or some other tool to insert citations as appropriate.
- Follow standard English punctuation and grammar rules. These are not formal essays or term papers, and I am not going to grade for every grammar or spelling error. Nonetheless, if grammatical and spelling errors are so common that it is difficult to understand your writing, then you will lose some points here.
    

### General Formatting Guidelines

- Use standard US Letter paper.
- Put your name on the first page.
- Put the word count on the first page.
- Use standard and professional fonts, margins, etc, such as:
    - One inch margins on all sides.
    - Size 11-12 font
    - Line height of 1.3-1.6
    - The height of one full line between paragraphs
    - An easy to read font (Like, no Comic Sans or script fonts, yes?)
- Here is a [sample google doc](https://docs.google.com/document/d/1SrHqs81xuBJcrlgxYMXYFBBmDVE29Nd3XQujNlKxM1s/edit?usp=sharing) that is very boring and generic but easy to read.

## Grading

Each reading response is worth 3 points. They will be graded very simply: 

| Mark | Points | Criteria Description |
|:--|:--|:--|
| ✔+ | 3 | Exceeds expectations; contains insightful and original ideas or questions that draw on your own unique experience and expertise in order to move beyond simply re-stating the content of the readings themselves; could be used as an example of an excellent reading response in future classes.|
| ✔ | 2.7 | It is clear that you completed the reading, and your commentary responds to **all four** prompts above; but your commentary does not go beyond a summary of the author's own point, or otherwise show your own substantive engagement with the topic.|
| ✔- | 2.2 | Your response is incomplete, either in that it only responds to some of the prompts above or it is so short that your answers do not fully address the prompts; or, it is questionable as to whether you actually completed the reading assignment: your commentary may be good, but there is no evidence (such as a quote or summary of a key takeaway) that shows that you read anything more than the title or first few paragraphs, or your response shows significant gaps in your understanding of the material.|

_You should expect to receive a 0 for any reading response that contains plagiarized content or is turned in late._
