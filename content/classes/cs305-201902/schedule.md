---
title: Schedule
credits: <p>This course -- including the schedule, homework assignments, and lecture slides -- has been only slightly modified from a previous version developed by <a href="http://web.cecs.pdx.edu/~wuchi/#teaching">Wu-chi Feng</a>.</p><p>Some reading assignments from Dr. Casey Fiesler’s Information Ethics &amp; Policy class, offered at CU Boulder, Fall of 2016. <a href="https://informationethicspolicy.wordpress.com/">https://informationethicspolicy.wordpress.com/</a></p>
cc: false
type: "class"
layout: "subpage"
weight: 500
icon: "fa-calendar-alt"

menu:
    main:
        parent: "Teaching"
        identifier: "cs305-201902-schedule"

quarter: 201902
---

| Week | Topic | Milestones|
|:--|:--|:--|
| [1](#week-1) | Introduction / ACM Code | |
| [2](#week-2) | Why Ethics? <sup>RR1</sup> | _In-class: Intros + Pitches<br/> End of class: Team Signups_ |
| [3](#week-3) | Privacy + Speech <sup>RR2</sup> |
| [4](#week-4) | Intellectual Property <sup>RR3</sup> | _Thursday, 11:59p: Bibliography_  |
| [5](#week-5) | Security + Reliability <sup>RR4</sup> |
| [6](#week-6) | _Presentation I_ | _Thursday, noon, slide deck_ |
| [7](#week-7) | Automation + Data + Bias <sup>RR5</sup> | ~~_Thursday, 11:59p: Case Analysis_~~ |
| [8](#week-8) | Work + Workplaces <sup>RR6</sup> |  _Tuesday, 11:59p: Case Analysis_ |
| [9](#week-9) | Taking Action <sup>RR7</sup> |
| [10](#week-10) | _Presentation II_ | _Thursday, noon, slide deck_ |
| [Finals](#finals-week) | _Presentation II_ + Closing |_~~Monday~~ Thursday, 11:59p, Final Proposal_ |

_[Reading Responses](../readings/) are due on paper at the start of class. If it is difficult for you to print them, you may turn it in to D2L by **noon** and I will print it and bring to class. All other assignments due via D2L._

#### Week 1

Course Introduction, ACM Code of Ethics, How to: Zotero + Google Scholar

- Read:
    - - . _
- Due:
    - - . _
- New Assignments:
    - _In class: [Start of Term Self-Assessment](https://forms.gle/q7ZX48zkjB6KBw7i9)_
    - 30-second personal intro + topic pitch (week 2)
    - Reading Responses (weeks 2, 3, 4, 6, 7, 8 and 9 )

#### Week 2

Why Ethics?

- Read:
    - Watch/listen to: Jarmul, Katherine. 2017. “If Ethics Is Not None.” At EuroPython Conference. <http://youtu.be/FtRbAePXUoI?t=1m26s>
    - Aaron Swartz. 2006. "Code and Other Laws of Wikipedia." _aaronsw.com (Personal Blog)_ <http://www.aaronsw.com/weblog/wikicodeislaw>
- Due:
    - Reading Response 1, _**start of class, paper**_
    - _In-class: 30-second lightning introductions to you and a proposed topic_
- New Assignments:
    - _In-class assignment: sign up for a team_
    - Annotated Bibliography (week 4)

#### Week 3

Civil Liberties: Privacy + Speech

- Read:
    - Choice A:
        - Warren, Samuel D., and Louis D. Brandeis. 1890. “The Right to Privacy.” Harvard Law Review, vol. 4, no. 5. pp. 193–220. <http://jstor.org/stable/1321160> or <http://groups.csail.mit.edu/mac/classes/6.805/articles/privacy/Privacy_brand_warr2.html>
    - Choice B (read / listen to both):
        - Tufekci, Zeynep. 2018. “It’s the (Democracy-Poisoning) Golden Age of Free Speech.” Wired | Business, January 16, 2018. <http://wired.com/story/free-speech-issue-tech-turmoil-new-censorship/>
        - "Free Speech, Limited?" Mozilla IRL Podcast. Available as a podcast to listen to or transcript to read at: <https://irlpodcast.org/episode7/>
- Due:
    - Reading Response 2, _**start of class, paper**_
- New Assignments:
    - - . _

#### Week 4

Intellectual Property

- Read:
    - Will Frank. 2015. “IP-Rimer: A Basic Explanation of Intellectual Property.” _Medium (Personal Blog)_, November 2015. <http://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711>
    - Review: https://opensource.org/faq
- Due:
    - Reading Response 3, _**start of class, paper**_
    - Annotated Bibliography, _D2L, 11:59pm_
- New Assignments:
    - Presentation I (week 6)
    - Case Analysis (week 7)

#### Week 5

Reliability & Security

- Read:
    - **Required:**
        - Nancy G. Leveson. 2017. “The Therac-25: 30 Years Later.” IEEE Computer. pp. 8-11. <http://ieeexplore.ieee.org/iel7/2/8102264/08102762.pdf>
    - **And choose 1 more:**
        - Matt Blaze. 2017. “Cybersecurity of Voting Machines.” _Testimony to US House Committee on Oversight & Government Reform_, November 2017. <http://oversight.house.gov/wp-content/uploads/2017/11/Blaze-UPenn-Statement-Voting-Machines-11-29.pdf>
        - Cherry, Miriam A. 2014. “A Eulogy for the EULA.” _Duquesne University Law Review_ 52 (2): 335–44. Official version: <https://issuu.com/duquesnelaw/docs/52.2?e=15059800/58167323> Also available at: <https://scholarship.law.slu.edu/cgi/viewcontent.cgi?article=1012&context=faculty>
        - Schneier, Bruce. 2018. “Patching Software Is Failing as a Security Strategy.” Excerpt from _Click Here to Kill Everybody: Security & Survival in a Hyperconnected World_ as published in _Motherboard_. <https://motherboard.vice.com/en_us/article/439wbw/patching-is-failing-as-a-security-paradigm>.
        - Watch/listen to: Schneier, Bruce. 2018. “Click Here to Kill Everybody.” _Talks at Google_. <https://www.youtube.com/watch?v=GkJCI3_jbtg>.
- Due:
    - Reading Response 4, _**start of class, paper**_
- New Assignments:
    - - . _

#### Week 6

Presentations I: Case Analysis

- Read before class:
    - - . _
- Due:
    - Slide Deck, in PDF format, _**noon, D2L**_
- New Assignments:
    - _In-class assignment: presentation feedback (due at end of class)_

#### Week 7

Automation, Data, Bias
<!-- NEXT TIME: https://modelviewculture.com/pieces/quantify-everything-a-dream-of-a-feminist-data-future -->

- Read before class:
    - Choice A:
        - Eubanks, Virginia. 2018. "Automating Eligibility in the Heartland," chapter 2 from _Automating Inequality_ [PDF](https://drive.google.com/file/d/1IBvtcfqS71mtKy4O-34I08z9dmtONglt/view?usp=sharing)
    - Choice B (read both):
        - Jonas Lerman. 2013. “Big Data and Its Exclusions.” _Stanford Law Review_ 66 (September), pp. 55–63. <http://stanfordlawreview.org/online/privacy-and-big-data-big-data-and-its-exclusions>.
        - Powles, Julia & Helen Nissenbaum. 2018. "The Seductive Diversion of 'solving' Bias in Artificial Intelligence." _Medium: Artificial Intelligence (Blog)_. <https://medium.com/s/story/the-seductive-diversion-of-solving-bias-in-artificial-intelligence-890df5e5ef53>.
- Due:
    - Reading Response 5, _**start of class, paper**_
    - ~~Case Analysis, _D2L, 11:59pm_~~ (extended to Tuesday May 21)
- New Assignments:
    - Presentation II (Week 10)
    - Final Proposal (Week 11, **~~Monday~~Thursday**)

#### Week 8

Work &  Workplaces

- Read before class:
    - Required:
        - Rosenblat, Alex. 2018. "Introduction," chatper 1 of _Uberland: How Algorithms are Rewriting the Rules of Work_. <https://content.ucpress.edu/chapters/13610.intro.pdf>
    - And choose 1 more:
        - Dare Obasanjo. 2016. “The Big Lie: Tech Companies and Diversity Hiring.” _Don’t Panic, Just Hire / 42 Hire (Medium Blog)_, July 15, 2016. <http://42hire.com/the-big-lie-tech-companies-and-diversity-hiring-f52fb82abfbf>.
        - Yonaten Zunger. 2017. “So, about This Googler’s Manifesto.” _Personal Blog (Medium)_, August 5, 2017. <http://medium.com/@yonatanzunger/so-about-this-googlers-manifesto-1e3773ed1788>
        - As told to Joy Shan and Elise Craig. 2018. "The Pipeline." _California Sunday_. <https://story.californiasunday.com/tech-pipeline-diversity>
- Due:
    - Case Analysis, _D2L, 11:59pm_, **TUESDAY**
    - Reading Response 6, _**start of class, paper**_
- New Assignments:
    - - . _

#### Week 9

Taking Action

- Read before class:
    - **Choose any two:**
        - Lou Moore. 2017. “Engineering Principles at Code for America.” _Medium (Code For America Blog)_, July 2017. <http://medium.com/code-for-america/engineering-principles-at-code-for-america-bda7b99740de>.
        - Interviews by Cameron Bird, Sean Captain, Elise Craig, Haley Cohen Gilliland and Joy Shan. 2019. "The Tech Revolt." _California Sunday Magazine._ <https://story.californiasunday.com/tech-revolt>
        - Giancarlo Valdes. 2018. "How developers can reduce toxicity in online communities."" _Rolling Stone_. March 20, 2018. <http://rollingstone.com/glixel/features/how-devs-can-reduce-toxicity-in-online-communities-w518104>
        - AI Now Institute. 2018. "After a Year of Tech Scandals, Our 10 Recommendations for AI." _Medium Blog_. <https://medium.com/@AINowInstitute/after-a-year-of-tech-scandals-our-10-recommendations-for-ai-95b3b2c5e5>
- Due:
    - Reading Response 7, _**start of class, paper**_
- New Assignments:
    - - . _

#### Week 10

Presentations II, Day 1

- Read before class:
    - - . _
- Due:
    - Slide Deck, in PDF format, _**noon, D2L**_
- New Assignments:
    - _In-class assignment: presentation feedback (due at end of class)_

#### Finals Week

Presentations II, Day 2

_Class meeting is Thursday, June 13, 5:30p-7:20p_

Closing & Self Assessment

- Read before class:
    - - . _
- Due:
    - _**Thursday**, 11:59p, Final Proposal, D2L_
- New Assignments:
    - _In-class assignment: presentation feedback (due at end of class)_
    - _In-class assignment: self-assessment (due at end of class)_
