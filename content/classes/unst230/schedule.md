---
title: Schedule
credits: 
cc: false
type: "class"
layout: "subpage"
weight: 300
icon: "fa-calendar-alt"

menu: 
  main:
    parent: "Teaching"
    identifier: "unst230-201904-schedule"

quarter: 201904

---

<!-- TODO: next time, how to learn, start the quarter with chapter 7 of Ambrose -->

### Week 1

#### Monday, September 30

- Read before class: n/a
- Homework:
    - Read for next class
    - Syllabus Survey <http://tinyurl.com/FPTSINQ-Fall2019>
- Monday mentor
    - Introductions, what is UNST, FPT cluster

#### Wednesday, October 2

- Read before class:
    - Le Guin, Ursula K. 2004. “A Rant About ‘Technology.’” Post on Personal Website. 2004. Note: original no longer available; see archive at: <https://web.archive.org/web/20190430182241/http://www.ursulakleguin.com/Note-Technology.html>.
- Homework:
    - Read for next class
    - Think about topics for the research project
- Wednesday mentor
    - Topic selection, form groups

### Week 2

#### Monday, October 7

- Read before class:
    - Winner, Langdon. 1980. “Do Artifacts Have Politics?” _Daedalus_ 109 (1): 121–36. <http://www.jstor.org/stable/20024652>.
- Homework:
    - Read for next class
    - Work on [brain dump](../report/#week-3-brain-dump) (due Monday, Oct 14)
- Monday mentor
    - Topic selection, form groups


#### Wednesday, October 9

- Read before class:
    - Kling, Rob. 1991. “Computerization and Social Transformations.” _Science, Technology & Human Values_ 16 (3): 342–67. <https://doi.org/10.1177/016224399101600304>.
- Homework:
    - Read for next class
    - Continue working on [brain dump](../report/#week-3-brain-dump) (due Monday, Oct 14)
- Wednesday mentor
    - Introductions, what is UNST, FPT cluster

### Week 3

#### Monday, October 14

**Due: [brain dump](../report/#week-3-brain-dump)**

- Read before class:
    - Shapin, Steven. 1989. “The Invisible Technician.” _American Scientist_ 77 (6): 554–63. <http://www.jstor.org/stable/27856006>.
- Homework:
    - Read for next class
    - Find resources for your research project
- Monday mentor
    - library + research help

#### Wednesday, October 16

- Read before class:
    - (Watch video) Nafus, Dawn. 2016. “Who Asks the Questions?” presented at the _2015 Quantified Self Public Health Symposium_, January 24. <https://medium.com/quantified-self-public-health/who-asks-the-questions-1700abd42a5e>.
    - Agapakis, Christina. 2014. “Conversations with Evelyn Fox Keller.” _Method: Science in the Making_, November 2014. <http://www.methodquarterly.com/2014/11/conversations-with-evelyn-fox-keller/>.
- Homework:
    - Read for next class
    - Find resources for your research project
- Wednesday mentor
    - library + research help

### Week 4

#### Monday, October 21

- Read before class:
    - (Listen to) NPR TED Radio Hour. 2019. “Digital Manipulation.” <https://one.npr.org/i/740833056:740851531>.
- Homework:
    - Read for next class
    - Start compiling your [Annotated Bibliography](../report/#week-5-annotated-bibliography), due 10/28
- Monday mentor
    - Campus Surveillance Map

#### Wednesday, October 23

- Read before class:
    - Brayne, Sarah. 2017. “Big Data Surveillance: The Case of Policing.” _American Sociological Review_ 82 (5): 977–1008. <https://doi.org/10.1177/0003122417725865>.
- Homework:
    - Read for next class
    - Continue working on your [Annotated Bibliography](../report/#week-5-annotated-bibliography), due 10/28
- Wednesday mentor
    - Campus Surveillance Map

### Week 5

#### Monday, October 28

**Due: [Annotated Bibliography](../report/#week-5-annotated-bibliography)**

- Read before class:
    - Rejouis, Gabrielle M. 2019. “Why Is It OK for Employers to Constantly Surveil Workers?” _Slate Magazine_, September 2, 2019. <https://slate.com/technology/2019/09/labor-day-worker-surveillance-privacy-rights.html>.
    - (Watch/Listen to) Neville, Jennifer. 2019. “Recommendation and Learning to Improve Personal Productivity.” presented at the _Microsoft Faculty Summit_ 2019, July 17. <https://youtu.be/gKNoYQtDR-E?t=1171>. _[Note:  There are multiple presentations in this video, but you only need to watch Jennifer Neville’s. The link is set up to start playing at 19:31 when her talk begins, and you can stop watching when questions for her are over at approximately 41:55.]_
- In class:
    - Choose section of _Automating Inequality_
- Homework:
    - Read for next class
    - Start working on your [First Draft](../report/#week-7-first-draft), due November 13
- Monday mentor
    - Peer feedback on annotated bibliographies

#### Wednesday, October 30

- Read before class:
    - MIT Media Lab. 2019. “Project Overview ‹ AttentivU.” MIT Media Lab. 2019. <https://www.media.mit.edu/projects/attentivu/overview/>. _[Note: read page, including FAQ; watch embedded three and a half minute video.]_
    - (Watch video) Near Future Laboratory. 2014. _Curious Rituals: A Digital Tomorrow_. <https://vimeo.com/92328805>.
- Homework:
    - Read for next class
    - Continue working on your [First Draft](../report/#week-7-first-draft), due November 13
    - Finish up your [Midterm Portfolio](../portfolios/#week-6-midterm-portfolio), due Monday, November 4
- Wednesday mentor
    - Peer feedback on annotated bibliographies

### Week 6

#### Monday, November 4

**Due: [Midterm Portfolio](../portfolios/#week-6-midterm-portfolio)**

- Read before class:
    - No reading. We will be doing a data analysis activity in class that will replace your writing response grade.

- Homework:
    - Read for next class
    - Continue working on your [First Draft](../report/#week-7-first-draft), due November 13
- Monday mentor
    - group work time

#### Wednesday, November 6

- Read before class:
    - You will read _one_ chapter from one of the following books. It has been assigned & shared with you on Google Classroom:
        - Eubanks, Virginia. 2018. _Automating Inequality: How High-Tech Tools Profile, Police, and Punish the Poor_. St. Martin’s Press.
        - Benjamin, Ruha. 2019. _Race After Technology_. Polity.
- Homework:
    - Read for next class
    - Continue working on your [First Draft](../report/#week-7-first-draft), due November 13
- Wednesday mentor
    - group work time

### Week 7

#### Monday, November 11

No class! And no mentor session. Thank a Veteran for their service.


#### Wednesday, November 13

**Due: [First Draft](../report/#week-7-first-draft)**

Stories about Artificial Intelligence

- Read before class (choose 1):
    - Klee, Miles. 2016. “​Interference.” Vice (blog). October 20, 2016. <https://www.vice.com/en_us/article/nz7qad/interference>.
    - Sloan, Robin. 2015. “​The Counselor.” Vice (blog). May 25, 2015. <https://www.vice.com/en_us/article/wnjakz/the-counselor>.
    - Worthington, Leah. 2019. “Warning! AI Is Heading for a Cliff (Interview with Stuart Russell, UC Berkeley CS Professor).” Cal Alumni Association (blog). September 11, 2019. <https://alumni.berkeley.edu/california-magazine/fall-2019/berkeley-expert-warns-ai-is-heading-for-a-cliff>.

- Homework:
    - Read for next class
    - **Sign up for a time for your group (at least 3/4 people) to meet with me between now and Nov. 27.** Please book a 20 minute appointment on my [calendar](../../../faq#meetings-with-me). I am available during the regular class block on November 27, but I would like you to still make an appointment if you want to meet during that time so I know who to expect when.
- Wednesday mentor
    - Peer review / feedback

### Week 8

#### Monday, November 18

AI critiques

- Read before class (Choose 2):
    - Mitchell, Melanie. 2019. “How Do You Teach a Car That a Snowman Won’t Walk across the Road?” _Aeon_, May 31, 2019. <https://aeon.co/ideas/how-do-you-teach-a-car-that-a-snowman-wont-walk-across-the-road>.
    - Mitchell, Melanie. 2019. “AI Can Pass Standardized Tests—But It Would Fail Preschool.” _Wired_, September 10, 2019. <https://www.wired.com/story/ai-can-pass-standardized-testsbut-it-would-fail-preschool/>.
    - elish,  m c. (2018, January 17). Don’t Call AI Magic. Retrieved November 8, 2019, from Medium website: <https://points.datasociety.net/dont-call-ai-magic-142da16db408>
    - Powles, Julia, and Helen Nissenbaum. 2018. “The Seductive Diversion of ‘Solving’ Bias in Artificial Intelligence.” Medium | Artificial Intelligence (blog). December 7, 2018. <https://medium.com/s/story/the-seductive-diversion-of-solving-bias-in-artificial-intelligence-890df5e5ef53>.
- In Class:
    - Sign up for a section of _Towards a Fairer Gig Economy_
- Homework:
    - Read for next class
    - Start working to incorporate feedback, revise draft. [Final Draft](../report/#finals-final-draft) due December 12.
- Monday mentor
    - Peer review / feedback

#### Wednesday, November 20

- Read before class:
    - Gray, Mary L. 2019. “Ghost Work.” _Data & Society Podcast Series_. May 8, 2019. <https://listen.datasociety.net/ghost-work/>.
- Homework:
    - Read for next class
    - Continue working to incorporate feedback, revise draft. [Final Draft](../report/#finals-final-draft) due December 12.
- Wednesday mentor
    - Presentation Advice

### Week 9

#### Monday, November 25

- Read before class:
    - Graham, Mark, and Joe Shaw, eds. 2017. _Towards a Fairer Gig Economy_. Meatspace Press. <http://ia800605.us.archive.org/26/items/Towards-a-Fairer-Gig-Economy/Towards_A_Fairer_Gig_Economy.pdf>. [Note: You will sign up to read a selection from this book on 11/18.]
- Homework:
    - Read for next class
    - Continue working to incorporate feedback, revise draft. [Final Draft](../report/#finals-final-draft) due December 12.
- Monday mentor
    - Presentation Advice

#### Wednesday, November 27

No class. And no mentor session.  
**Last day to meet with me for draft feedback.**  

### Week 10

#### Monday, December 2

- Read before class:
    - Karpf, David. 2018. “25 Years of WIRED Predictions: Why the Future Never Arrives.” _Wired_, September 18, 2018. <https://www.wired.com/story/wired25-david-karpf-issues-tech-predictions/>.
    - Bird, Cameron, Sean Captain, Elise Craig, Haley Cohen Gilliland, and Joy Shan. 2019. “The Tech Revolt.” _The California Sunday Magazine_, January 23, 2019. <https://story.californiasunday.com/tech-revolt>.
- Homework:
    - Read for next class
    - Work on [end of term portfolio](../portfolios/#week-10-final-portfolio), due December 6
    - Continue working to incorporate feedback, revise draft. [Final Draft](../report/#finals-final-draft) due December 12.

- Monday mentor
    - Group work time

#### Wednesday, December 4

- Read before class:
    - No readings. I did not get these scanned in time. Please come to class prepared to complete a closing survey, and reflect broadly on the course topics. We will have one final in-class activity to replace the reading/writing grade for this day.
    - ~~Le Guin, Ursula K. 1975. “The Ones Who Walk Away from Omelas.” In _The Wind’s Twelve Quarters_. Harper & Row. [Note: I will post a PDF.] <!-- TODO:  -->~~
    - ~~Jemisin, N.K. 2018. “The Ones Who Stay and Fight.” In _How Long ’Til Black Future Month_. Orbit. [Note: I will post a PDF.] <!-- TODO:  -->~~
- Homework:
    - Finish [end of term portfolio](../portfolios/#week-10-final-portfolio), due December 6
    - Continue working to incorporate feedback, revise draft. [Final Draft](../report/#finals-final-draft) due December 12.
    - Finish up class [presentation](../report/#finals-presentation), to deliver on December 12.
- Wednesday mentor
    - Group work time

#### Deadline Friday, December 6

**Due: [End of Term Portfolio](../portfolios/#week-10-final-portfolio), 11:59p**

### Finals

#### Thursday, December 12, 12:30p

**Due: [Final Essay/Report](../report/#finals-final-draft), start of session**

- Read before class: None
- Homework: None
- In Class:
    - Presentations
    - Closing
