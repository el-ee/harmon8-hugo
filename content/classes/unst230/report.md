---
title: Final Essay/Report
credits: 
cc: false
type: "class"
layout: "subpage"
weight: 400
icon: "fa-file-alt"

menu: 
  main:
    parent: "Teaching"
    identifier: "unst230-201904-report"

quarter: 201904

---


In this course you will write a brief 2,000-4,000 word essay/report that examines a specific scientific or technological innovation. You will complete this assignment in teams. You have a choice of two formats:

1. **Argumentative Essay**: In this essay, you will identify a specific scientific or technological innovation and make a case for whether or not it is appropriate and effective in a specific context. In your essay you should:
    1. Briefly describe the innovation and a context in which it could be used/applied.
    2. Identify and describe at least 3 examples of specific uses/applications that are or are not appropriate and effective, and explain why.
    3. Identify and provide the rationale for a set of limitations that you believe should be placed on the use/application of this innovation. If you do not believe that any limitations are necessary, then state this and explain why that is the case.

2. **Historical Report**: In this report, you will identify a specific scientific or technological innovation, and examine how the conditions of its production relate to its potential uses and impacts. In your report, you should:
    1. Briefly describe the innovation and the context in which it is/was used/applied.
    2. Describe its creation and production, answering questions such as: Who developed it? What people, corporations, governments, or other organizations were involved in its development? Who did or did not receive credit for their involvement? Why? What were the motivations for its initial creation? Where and when was it developed? After initial discovery/creation, what happened — is it still in use? Why or why not? If still in use, who and what organizations are involved in producing it?
    3. Describe the uses of the innovation including an analysis of its potential or real benefits and harms, and an explanation of how and why its ultimate uses and effects are linked to the conditions of its production.

Regardless of which topic you choose, your essay/report should incorporate references and citations to back up your claims. This project is divided up into 5 milestones:

1. [Week 3: Brain Dump](#week-3-brain-dump) - 4 points
2. [Week 5: Annotated Bibliography](#week-5-annotated-bibliography) - 10 points
3. [Week 7: First Draft](#week-7-first-draft) - 10 points
4. [Finals: Final Draft](#finals-final-draft) - 10 points
5. [Finals: Presentation](#finals-presentation) - 5 points

### General Formatting Guidelines

- All documents should be turned in in `.pdf` format.
- Put your team name and all individuals' names on the first page.
- Use headings as appropriate.
- Be consistent! Use the style feature of modern word processors!
- Use standard and professional fonts, margins, etc, such as:
    - One inch margins on all sides.
    - Size 11-12 font
    - Line height of 1.3-1.6
    - The height of one full line between paragraphs
    - An easy to read font (Like, no Comic Sans or script fonts, yes?)
    - Page numbers! They are great.
- If your document is very long with many sections, consider a table of contents.
- Here is a [sample google doc](https://docs.google.com/document/d/1SrHqs81xuBJcrlgxYMXYFBBmDVE29Nd3XQujNlKxM1s/edit?usp=sharing) that is very boring and generic but easy to read.


### Week 3: Brain Dump

This milestone has two parts (a) an overview memo and (b) a brain dump where you outline what your team already knows, collectively, about your chosen topic.

You should combine all of this information into a single, well-organized PDF. The entire document should be 2-4 pages in length.

#### Turn-in Logistics

- **Due:** Monday, October 14, start of class
- **What:** A single, well-organized PDF or Google Doc file
- **How:** On Google Classroom
- **One per group**

#### Specification

1. **Memo** (1-2 pages)

    - **A team name**.
    - **All team member names** and PDX email addresses.
    - **Project choice** Will you write an argumentative essay or an historical report?
    - **Topic** What technology/scientific innovation are you going to research?

    - **A Team Contract**:
        - **A communication plan**: how will you communicate with each-other in between classes and course meetings? Will you make a multi-person private chat on Facebook? Will you use email? Will you create a group SMS? Will you set up a Discord server or Slack channel somewhere? I don't care what it is, but I want to know all team members are on the _same_ page.
        - **A collaboration plan**:
            - Have you discussed each other's work, school, and family schedules? If not, do that now and then answer this with a Yes.
            - Does anyone on your team have a major time commitment this quarter that may make it hard for them to contribute to the project during some period of time (e.g., a week long work trip, a best friend's wedding, a really important midterm in a really hard class)? How will you work around these commitments?
            - When/how will you work outside of class? Will you have a regular meeting or schedule meetings as needed? Will you meet in person or remotely (Note! PSU offers free use of Zoom meetings now!)
            - What tools will you use to collaborate on writing: e.g., will you use Google Docs to draft text, and provide feedback to each other there? If not, what is your alternative plan?
            - Who will be your designated turn-in person to upload the file to Google Classroom? How will they let the rest of you know it is complete?
        - **Project Timeline** including individual responsibilities.  What are all the things that need to be done in each of the next 2 weeks so that you will be ready to turn in your bibliography on time? Who will take the lead on each of these tasks? Who will take the lead on motivating and coordinating the group, making sure everyone else has done their part?
        - **Individual contributions**: What has each person done to contribute to this first milestone? (_You will write a brief summary of individual contributions with each milestone. Please know that I read these, and will adjust grades accordingly._)
    - **Reflection**:
        - **Request for Feedback** What questions do you have? How can I help you move forward with your project? Where are you stuck?
        - **Self-Assessment:** What grade do you believe your team has earned on this milestone? Why? (_1-3 sentences._)

2. **Brain Dump** (1-2 pages)
    - **What** technology/scientific innovation did you choose?
    - **Why** did you choose it, what is interesting about it to your team?
    - **Existing Knowledge** What do you already know about your technology/scientific innovation?
    - **Gaps** in your knowledge: What will you need to research in order to answer the questions in the prompt and complete your report?

#### Grading

This is worth 4 points towards your final grade. All group members will receive the same grade.

- **0: Not turned in or contains plagiarized content.**
- **2.5: Turned in something.**
- **3.5: Memo addresses all items in the list above, at least superficially.**
- **4: Memo is not only complete but also thoughtful.** It could be used as an example of excellent work in a future class.


### Week 5: Annotated Bibliography

#### Turn-in Logistics

- **Due:** Monday, October 28, start of class.
- **What:** PDF or Google Doc
- **How:** On Google Classroom
- **One per group**

For this milestone, you will compile an annotated bibliography with at least **eight** references in it that you will use in your final paper. (Your final paper will need to include at least 12 references (see [Finals: Final Draft](#finals-final-draft)).

Preparing an annotated bibliography should be a useful step on the way to writing your final paper. If you do it well, you should be able to save yourself the time later of going back to re-read any of your sources when it's time to actually write. Think of your future self and your team-mates as the audience for your entries. What will you want to reference from this source when you're writing in the future? What should your team-mates know about it?


#### Specification

Prepare a single `.pdf` or Google Doc that includes:

1. **Memo**: _Try to keep this to a page or so._
    1. **Team name**
    2. **Team member names and pdx emails**
    3. **Topic statement:** What is your technology or scientific innovation? (_A few words_)
    4. **Contribution summary:** What did each person do for this assignment?
    5. **Self-Assessment:** What grade do you believe you have earned on this   assignment? Why? (_1-4 sentences._)
    6. **Request for feedback:** What do you need help on? What questions do you have for me? (_1-4 sentences. Or, a short bulleted list._)
    7. **What's next?** What's your team's timeline and plan for moving forward with the project? What do you need to do to have your draft done by Week 7? (_Maybe in list or table format?_)
2. **The Annotated Bibliography** with at least 9 (_nine_) entries.
    - The reference set must include:
        - At least 4 (_four_) peer-reviewed scholarly works (see, e.g., <https://dl.acm.org/> )
        - At least 4 (_four_) references that are not from the course schedule.
    - _Each entry_ in the annotated bibliography should include:
        1. _A full bibliographic entry_ for the reference in a standard reference format (I recommend Chicago, but I am ok with other standard formats (e.g., APA, MLA, IEEE, ACM, etc.) **as long as you are consistent.**). All citations must include, at minimum: author name(s), date of publication, title of publication, publication venue (if article, journal name; if book, the press), link to official online version (no random homepages, no SSRN links). I strongly recommend using a citation manager, such as [Zotero](https://www.zotero.org/) or [Paperpile](https://paperpile.com/). We will discuss citation management in Week 3.
        2. A 3-5 sentence _summary_ of the source. What is the author's question and answer? Why do they argue that their question/answer is better than previous question/answers? Any details about their argument/evidence that are worth mentioning? None of your own evaluation here. Just. Summarize.
        3. A 3-5 sentence _reflection_. How is this source useful for your project? How does it relate to other sources in your bibliography? Does it agree with or contradict them?
        4. A selection of _direct quotes_ (1-5) that you think might be useful when you are writing your paper. Make sure to note the page number if applicable so that you will have this information handy when you are writing later.


#### Resources

You can find more information about annotated bibliographies here:

- Overview: <https://owl.english.purdue.edu/owl/resource/614/01/>
- Examples: <https://owl.english.purdue.edu/owl/resource/614/03/>

_Note:_ You only have to write "summary" and "reflection" paragraphs for this assignment. You do not have to write the "assessment" paragraph that is shown in the OWL directions and examples. You _DO_, however, need to include an additional section that includes a selection of direct quotes that you may want to use later.

#### Grading

This assignment is worth 10 points. You can earn these points as follows:  

- Base Grade:
    - **0**: not completed, or contains plagiarized content.
    - **5.75**: A very low quality bibliography that fails to meet multiple requirements (e.g., 4 or fewer entries; only reflection paragraphs, and no summary paragraphs for all entries) and shows a serious lack of effort.
    - **6.75**: An okay bibliography, that meets most of the requirements, but may be missing one or two significant items. For example, everything is complete, but you included only summary paragraphs and no reflections for some of your bibliography entries; or, everything is complete with the exception that you only included 6 bibliography entries instead of 8.
    - **7.75**: A good response that includes each required element. It has 8 bibliography entries, but there are some minor deviations from the requirements. For example, some references have no direct quotations, or some of the reflection paragraphs are too short, or you have only 3 sources that are not one of the course readings.
    - **8.75**: A good response that fully meets all requirements (unless listed below as a separate grade). The document is consistent throughout in formatting, style, and language. There is some evidence of all team members' having given input into the document as a whole. It does not just appear to be a set of four individual documents copied together at the last minute.
    <!--  Next time: quality of summaries. -->
- Additional points, earned regardless of your base grade:
    - **+0.25**: Bibliography includes at least _FOUR_ peer-reviewed sources.
    - **+0.25**: All citations are _complete_, correct, and in a consistent, standard format.
    - **+0.25**: Document is reasonably & consistently formatted. See [General Formatting Guidelines](#general-formatting-guidelines) above.
    - **+0.5**: Exceptionally high quality work; could be used as an example. You can earn these points by, for example, writing particularly insightful reflections, or writing very high quality succinct but complete summaries.



### Week 7: First Draft

#### Turn-in Logistics

- **Due:** Wednesday, November 13, start of class
- **What:** A single, well-organized PDF or Google Doc
- **How:** On Google Classroom
- **One per group**

For this milestone, you will write a first draft of your final report/paper. Your paper should be between 2,000-4,000 words, be well-organized, and contain responses to all three of the requirements for the chosen topic.

#### Specification


Prepare a single `.pdf` or Google Doc that includes:

1. **Memo**: _Try to keep this to a page or so._
    1. **Team name**
    2. **Team member names and pdx emails**
    3. **Topic statement:** What is your technology or scientific innovation? (_A few words_)
    4. **Contribution summary:** What did each person do for this assignment?
    5. **Self-Assessment:** What grade do you believe you have earned on this   assignment? Why? (_1-4 sentences._)
    6. **Request for feedback:** What do you need help on? What questions do you have for me? (_1-4 sentences. Or, a short bulleted list._)
    7. **What's next?** What's your team's timeline and plan for moving forward with the project? What do you need to do to have your final report/paper done by Week 10? (_Maybe in list or table format?_)
2. **The paper itself**
    1. Is well-structured including a clear introduction, body, and conclusion.
    2. Total length, not including the bibliography is 2000-4000 words
    3. Fully responds to the prompt at the top of this page, including addressing all three sub-points.
    4. Is well-organized and professionally formatted.
3. **Citations:**
    4. Meaningfully cites at least 8 sources, with proper inline citations.
        - At least four citations are to peer-reviewed sources.
    5. Includes a bibliography at the end of the document with full reference information for all cited sources. (NOT an annotated bibliography, just a list of references).

#### Grading

- [Argumentative Essay Rubric - Draft (PDF)](https://drive.google.com/file/d/16M2GfbVByz9G5kKiCb6FJzCH9egyeErm/view?usp=sharing)
- [Historical Report Rubric - Draft (PDF)](https://drive.google.com/file/d/16QZRrUwo6nCVXesn4WsmONK1j-IxnL2T/view?usp=sharing)



### Finals: Final Draft

#### Turn-in Logistics

- **Due:** Thursday, December 12, start of class
- **What:** A single, well-organized PDF or Google Doc
- **How:** On Google Classroom
- **One per group**

For this milestone, you will revise your draft into a complete and finished product. Your paper should be between 2,000-4,000 words, be well-organized, and contain responses to all three of the requirements for the chosen topic.

#### Specification


Prepare a single `.pdf` or Google Doc that includes:

1. **Memo**: _Try to keep this to a page or two._
    1. **Team name**
    2. **Team member names and pdx emails**
    3. **Topic statement:** What is your technology or scientific innovation? (_A few words_)
    4. **Contribution summary:** What did each person do for this assignment?
    5. **Self-Assessment:** What grade do you believe you have earned on this   assignment? Why? (_1-4 sentences._)
    6. **Summary of Changes:** What have you changed in response to peer feedback and/or my feedback? What have you changed based on your own review of the work? (_1-2 paragraphs, bulleted list format also fine._)
    7. **Request for feedback:** What feedback would you like on this final version of the paper? Any remaining questions? (_1-4 sentences. Or, a short bulleted list._)
2. **The paper itself**
    1. Is well-structured including a clear introduction, body, and conclusion.
    2. Total length, not including the bibliography is 2000-4000 words
    3. Fully responds to the prompt at the top of this page, including addressing all three sub-points.
    4. Is well-organized and professionally formatted.
3. **Citations:**
    1. Meaningfully cites at least **12 sources**, with proper inline citations.
        - At least 4 citations are to peer-reviewed sources.
    2. Includes a bibliography at the end of the document with full reference information for all cited sources. (NOT an annotated bibliography, just a list of references).

#### Grading

- [Argumentative Essay Rubric - Final (PDF)](https://drive.google.com/file/d/16Wjl1_JVK0tv2VpSsp38IeumcPfiK8w9/view?usp=sharing)
- [Historical Report Rubric - Final (PDF)](https://drive.google.com/file/d/16Vow1zXM2Y7vQewWbACQyeGJOuPTPx1F/view?usp=sharing)


### Finals: Presentation

#### Turn-in Logistics

- **Due:** Thursday, December 12, 11:30am (2 hours before meeting)
- **What:** A slide deck in PDF, PPT, or Google Slides format
- **How:** On Google Classroom
- **One per group**

Each team will be alloted 10 minutes including questions & feedback. I encourage you to plan to talk for about 5 minutes.

#### Specification

- Aim for 5 minutes. Anything in the 4-6 minute range is okay.
- **Everyone** in your team must meaningfully participate.
- Your presentation should include each of the following pieces of information:
    1. Who are you? Introduce yourselves.
    2. What is your topic area? Why is it interesting or important?
    3. What background is important for people to know in order to understand your topic?
    4. What is the main take away from your research project this quarter:
        1. If you chose the **argumentative** project, you should make a case for whether or not the technology you chose is appropriate and effective in a specific context.
        2. If you chose the **historical** project, you should make a case for how the conditions of production for the technology you chose relate to the technology's potential uses and impact.

_Your presentation should not include an argument for taking any particular action._


#### Grading

This assignment is worth 5 points total:

|.95|.85|.75|.65|
|:--|:--|:--|:--|
|**Mechanics** <br/> Presentation lasted 4-6 minutes. Slides were turned in by 11:30. All team members participate. | Slides not turned in by noon. | Presentation lasted 7 or more minutes. | Not even 4 minutes -or- all team members do not participate in a meaningful way. |
|**Preparation** <br/> You manage your own time. You seem prepared. Perhaps you have notes. You do not ramble. | . | Presentation ends up fine, but you do not seem well-prepared or rehearsed. | . |
|**Delivery & Organization** <br/> Strong focus & structure. Clear speaking. Engaging delivery. Increases audience _interest_.| Well-organized, clear delivery, somewhat interesting. | Audience can follow the big picture story but some details are confusing. |  Uninteresting or unclear delivery. Audience struggled to follow your point.|
|**Content** <br/> Increases audience _knowledge_. Hits all four requirements (who are you, topic, background, main take away)| Overall a fine presentation, but misses one of the key requirements. | Content doesn't match the audience. We are unable to follow your main point because you talk above our heads. | Fails to fulfill multiple requirements. |
|**Quality** <br/> Graphics are used liberally, and integral to the presentation. All text is presented in easily digestible chunks. | There are at least three meaningful graphics, and no more than one or two lengthy bulleted lists. | There are many lengthy bulleted lists. | There are whole paragraphs of text. Graphics are distracting / extraneous / non-existent. |

- **+0.25** point is reserved for exceptionally high quality work, such as especially well-designed slides, a particularly engaging presentation, or especially thought-provoking content.
- If you do not give your presentation, but you do turn in a complete slide deck, you will receive 1 point.
