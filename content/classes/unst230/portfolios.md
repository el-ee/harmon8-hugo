---
title: Portfolio
credits: 
cc: false
type: "class"
layout: "subpage"
weight: 500
icon: "fa-images" 

menu: 
  main:
    parent: "Teaching"
    identifier: "unst230-201904-portfolio"

quarter: 201904

---

We will conduct numerous activities in class and in mentor session throughout the term. I will not collect them in class each day. Instead, you should document and keep track of these assignments throughout the term. At both the mid-term, and end of term, you will prepare a portfolio document that highlights your 2 best products from class-related activities, your 1 worst product, and includes a brief self-evaluation statement reflecting on your progress to date in the course.

Any class related activity is acceptable for inclusion other than the group project work. Please pick a diversity of activities to highlight. Don’t just select three sets of reading notes.

Each portfolio should be designed around 2 UNST goals that you want to make progress on this quarter, and include reflections on your progress to date in the course. From reading your portfolio, I should be able to learn the following things:

- What 2 goals did you pick?
- Why did you pick those goals to focus on?
- Where were you at with these goals at the beginning of the quarter? What have you learned / how have you made progress?
- What is standing in your way / or would help you make further progress?
- What activities did you pick, and what work products were created (i.e., include photos, etc as relevant)?
- Why did you pick these activities -- what are you proud of or not so proud of?
- What did you learn from these activities? How did your participation in them relate to the UNST goals you have chosen to focus on?
- What would have made these activities better?

The specific formatting of this assignment is up to you, and I would encourage you to think about presenting your portfolio in a way that is engaging for you to create and for me to read/review. It might be a website, on paper, in a document, in a sketchbook, in a video, etc.

### Week 6: Midterm Portfolio

#### Turn-in Logistics

- **Due:** Monday, November 4
    - Digital: at 11:59p
    - Paper: at the _start of class_
- **What:**
    - Digital: A single, well-organized PDF, Google Doc, link to video, etc.
    - On paper: up to you, but I need to be able to carry it home with me to grade.
- **How:**
    - Digital: On Google Classroom
    - On Paper: At the _start of class_


#### Grading

Some ideas for the first portfolio: a set of reading notes you took in preparation for class, a class discussion that you documented in personal notes, your contributions to the in-class annotated bibliography, the surveillance posters you’ll make this week in mentor session, a writing response, one of the group technology posters from the beginning of the term. I have a few remaining in-class activities planned before this is due: PSA posters on Wednesday Oct 23, a data analysis/presentation activity on Oct 28, and a sketching activity coming up on Oct 30.

- **0**: not completed, or contains plagiarized content.
- **6**: A very low quality portfolio that fails to meet multiple requirements and shows a serious lack of effort.
- **7.5**: An okay portfolio, that meets most of the requirements, but may be missing one or two significant items. For example, you forgot to include the one bad assignment.
- **8.5**: A good response that includes all required elements:
    - Clearly identifies 2 UNST goals that you care about. Explains why you chose them, what progress you feel you have made so far, and what would help you make continued progress.
    - All three required assignments (2 good, 1 bad) are included. It is clear why you chose them, what you learned from them, how they relate to the goals you chose to focus on, and what would have made them better.
- **9.5**: A great response that includes all required elements, uses a diversity of class assignments to illustrate progress, demonstrates professional organization and formatting, and is clearly thoughtful.
- **+0.5**: _Exceptionally high quality work_. You can earn this final half point regardless of the rest of your grade. It is meant to recognize high quality or creative work that shows evidence of significant effort and goes above and beyond expectations.


### Week 10: Final Portfolio

Everything about this is the same as the midterm portfolio, except it is at the end of the term. **Please review the detailed directions at the top of this page.** Please find current definitions of the UNST goals in the course [syllabus](../syllabus).

#### Turn-in Logistics

- **Due:** Friday, December 6, 11:59p
- **What:** A single well-organized PDF or Google Doc
- **How:** On Google Classroom

#### Grading Details

- **0**: not completed, or contains plagiarized content.
- **6**: A very low quality portfolio that fails to meet more than two requirements and shows a serious lack of effort.
- **7.5**: An okay portfolio, that meets most of the requirements, but may be missing one or two significant items.
- **8.5**: A good response that includes all required elements, at least superficially:
    - Clearly identifies _2 UNST goals_ that you care about and explains:
        - _Why_ you chose each one
        - _What progress_ you have made in the course this year on each one (where did you start and where are you now?)
        - What would allow you to make _further progress_?
    - All three required assignments are included, all are from this class and NOT related the main group research project and it is clear:
        - _Why_ you chose each one
        - _What you learned_ from each one
        - How each one _relates to one or both of the goals_ you chose to focus on
        - What would have made each one _better_
- **9.5**: A great response that includes all required elements, uses a diversity of class assignments to illustrate progress, demonstrates professional organization and formatting, and includes a self-reflection that addresses all points in a serious and thoughtful manner.
- **+0.5**: _Style & Quality_. You can earn this final half point regardless of the rest of your grade. It is meant to recognize high quality or creative work that shows evidence of significant effort and goes above and beyond expectations.
