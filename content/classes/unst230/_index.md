---
title: "UNST 230 (Fall 2019)"
date: 2019-08-01

courseNumber: "UNST 230"
courseTitle: "Freedom, Privacy, and Technology (SINQ)"
        
sidebarItems:

meetings: "MW 12:30-1:45"
location: "Cramer 103"

shortDescription: "This course is the Sophomore Inquiry (SINQ) course for the University Studies 'Freedom, Privacy, and Technology' cluster. The aim of this cluster is to provide the knowledge that will enable those who complete the cluster to face thoughtfully the question of the appropriate use of and limitations upon modern technology. One important feature of the cluster is that it brings together actual sciences with humanistic and social science disciplines."

type: "class"
layout: "class-home"

menu: 
    main:
        parent: "Teaching"
        
quarter: 201904    

---

This past June, Tristan Harris, Stanford graduate, former Google Design Ethicist, and current Executive Director of the Center for Humane Technologies, called for a new field of study, "Society & Technology Interaction"[^tristan]. As noted by others[^irani-chowdhury], such an area of study does, in fact, already exist! 

In this course, we will read some key works in this area and engage in a series of practical activities to increase your ability to be what Lilly Irani and Rumman Chowdhury call "active technological citizens":

> We should educate all--not just engineers--in how to become active technological citizens. This does not mean learning how to code. It means knowing enough to ask questions about how the code affects work, life, and community. It means understanding how people can hold institutions and firms accountable through literacy, advocacy, and social movements[^irani-chowdhury].


### Course goals
**This course is the Sophomore Inquiry (SINQ) course for the University
Studies "Freedom, Privacy, and Technology" cluster:** 

> The aim of this cluster is to provide the knowledge
that will enable those who complete the cluster to face thoughtfully the
question of the appropriate use of and limitations upon modern
technology. One important feature of the cluster is that it brings
together actual sciences with humanistic and social science
disciplines.[^1]

[^1]: See <https://sinq-clusters.unst.pdx.edu/cluster/freedom-privacy-and-technology>

In addition to the specific goals related to Freedom, Privacy, and Technology, this course is also designed to meet the four core University Studies goals[^2]: 

[^2]: See <https://www.pdx.edu/unst/university-studies-goals>

1. **Diversity, Equity, and Social Justice**: Students will explore and analyze identity, power relationships, and social justice in historical contexts and contemporary settings from multiple perspectives.
2. **Communication**: Students will enhance their capacity to communicate in various ways—writing, graphics, numeracy, and other visual and oral means—to collaborate effectively with others in group work, and to be competent in appropriate communication technologies.
3. **Inquiry and Critical Thinking**: Students will learn various modes of inquiry through interdisciplinary curricula—problem-posing, investigating, conceptualizing—in order to become active, self-motivated, and empowered learners.
4. **Ethics, Agency, and Community**: Students will examine values, theories and practices that inform their actions, and reflect on how personal choices and group decisions impact local and global communities.


[^tristan]: Tristan Harris, 2019. <https://twitter.com/tristanharris/status/1138126884330278912?ref_src=twsrc%5Etfw>

[^irani-chowdhury]: See replies to tweet. Also: Lilly Irani and Rumman Chowdhury. 2019. "To Really Disrupt, Tech Needs To Listen To Actual Researchers" _Wired_. <https://www.wired.com/story/tech-needs-to-listen-to-actual-researchers/>

