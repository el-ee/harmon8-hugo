---
title: Paper & Presentation
credits: <p>This course -- including the schedule, homework assignments, and lecture slides -- has been only slightly modified from a previous version developed by <a href="http://web.cecs.pdx.edu/~wuchi/#teaching">Wu-chi Feng</a>.</p>
cc: false
type: "class"
layout: "subpage"

menu: 
    main:
        parent: "Teaching"
---

In this course, you will write a final term paper on a topic of your choice related to the social, ethical, and legal dimensions of computing. In preparation for this paper, you will first give an 8-10 minute presentation in class on your topic.

For all of the written parts of the assignment (P1, P2, P4), you must submit your work in a format that I can easily open and check the word count: 
   - `.docx`, `.doc`, `.txt`, `.pages`, `.rtf`, `.odf` are all acceptable.
   - If you prepare your paper in a markup language such as LaTeX or markdown, then you should submit both the generated PDF (for me to read) **and** the original `.tex` or `.md` file (so that I can easily check the word count).
   
* TOC
{:toc}

## Overview

There are five concrete deliverables for the term paper that are designed to help you develop this project in stages over the course of the term.
  - P0: Propose a topic (Week 4)
  - P1: Annotated Bibliography (Week 6)
  - P2: Abstract (Week 7)
  - P3: Presentation (Weeks 8-11) 
    - You will give an 8-10 minute in-class presentation on your topic in class on an assigned date.
    - NOTE: **Your presentation slides are due via the D2L dropbox by 5:00pm on the DAY BEFORE you are scheduled to present.** All slides will be loaded on the classroom computer for presentation in class. You may not present off of your own computer.
  - P4: Final Paper (Finals Week)
    - Your final term paper is due via the D2L dropbox by 5pm on the Friday of finals week (December 8)

You will also write short reflections / responses to the presentations given by your classmates.


## P0: Propose a topic (Week 4)

**Due on d2l at 10am on Friday October 20** (as part of HW4).

As part of HW4, you will submit a topic for your final paper. This topic can be any issue related to the social, ethical, and legal dimensions of computing.  

Here is a list of [news articles, academic articles, and twitter commentary](/~harmon8/cs305/topicinspiration.html) if you need inspiration. Please choose a topic that interests you!

I will notify you by Tuesday October 24 if I think your topic might not be suitable.

## P1: Annotated Bibliography (Week 6)

**Due to d2l by 5pm on Friday November 3**

By 5pm Friday of week 6, you should submit an annotated bibliography with at least **three** of the references you will use in your final paper. (Your final paper will need to include at least 5 references (see [P4: Final Paper](#p4-final-paper-finals-week)).

This reference set must include:
- 2 scholarly works (see, e.g., <https://dl.acm.org/> )
- a third reference that you have located on your own, and that we have not read in class. 

Each entry in the annotated bibliography should include: 

1. A citation for the reference in a standard reference format (e.g., ACM, MLA, Chicago, etc.)
2. A short summary of the source (3-5 sentences)
3. A short reflection on how this source is useful for your project (3-5 sentences)

You can find more information about annotated bibliographies here: 

- Overview: <https://owl.english.purdue.edu/owl/resource/614/01/>
- Examples: <https://owl.english.purdue.edu/owl/resource/614/03/>

You only have to write "summary" and "reflection" paragraphs for this assignment. You do not have to write the "assessment" paragraph that is shown in the directions and examples linked above.

## P2: Abstract (Week 7)

**Due to d2l by 5pm on Friday November 11**

Your abstract should be 300-600 words long. 

Your abstract should cover the same elements as the eventual introduction to your paper:

  - Clearly and succinctly identify the issue you have investigated: _What_ is the problem?
  - Situate this issue in a social and technical context to motivate the reader's attention:  _Why_ is this particular issue important for society?  _How_ does this issue relate to computer science? 
  - Include a thesis statement that clearly conveys an opinion on the issue, based upon a reasonable analysis of the data that you gathered for the paper. 
  - Briefly summarize the argument you will make in support of this thesis, and explain how you will use each of the references identified in your annotated bibliography.


## P3: Presentation (Weeks 8-11) 

You will give an 8-10 minute in-class presentation on your topic in class on an assigned date (see below).

- Your slides must be uploaded to d2l by **5pm on the day before** your presentation. 
- Your slides must be in either PowerPoint or PDF format. 
  - We will be presenting slides off of the computer in the room. It is a Windows computer. If you plan to submit a PowerPoint file, you should only use standard fonts that will be installed by default on a typical Windows / MS Office install. The projector is a widescreen format projector.

Your speech will be on the topic you have selected.  Your speech must meet the following specifications:

- It will be 8-10 minutes in length*
- The slides must be in “presentation form”. That is, not a cut and paste of your abstract, annotated bibliography or other paper materials. Text and images should be useful and supportive, but not distracting to the audience. 
- The slides must at a minimum include:
  - **First slide**: title, your name, and date
  - **Second slide**: your central thesis
  - **Third slide**: an outline for the rest of the talk, including the type of argument you are making, and the main points you are going to cover
  - **Background on the topic** (1-2 slides) - why it came about, why is it important, why the audience should care [use references]
  - **Analysis** (2-3 slides) - explore the idea and its ethical implications, data to support your position [use references]
  - **Summary & Conclusion** (1 slide)
  - **Last slide** references list (you do not need to talk through this in class, just needs to be included)

This website has some very good resources for peparing for a presentation:
 
- 2 short TED talks about how to speak with confidence! 
- And one slide deck about making slides that aren't just long bulleted lists

<http://capitalcomtech.info/2016/05/31/how-to-present-with-power-and-poise/>

  
\* **Exception to the above length requirement:** IF you want to give a PechaKucha style talk, and IF you follow the format exactly, I'll allow it even though it is slightly less than 8 minutes. You will instead accept questions for 2 minutes at the end. If you think you might ever want to interact in more design-erly spaces, it's a good format to learn. But it's different than the kind of talk you'd give at an engineering meeting or executive meeting, etc. It's also harder to do well. Details: <http://www.pechakucha.org/faq> Check in with me **in advance** if you want to do this.


### Presentation Schedule

**November 17, 2017 (10:00am)**
- Matthew Gieger
- Jordan Le
- Michael Long
- James Camarillo Ricks
- Christopher Dietrich
- Ashton Hunger
- Andy Mayer
- Ryan Moore
- Evghenii Siretanu

**December 1, 2017 (10:00am)**
- Michael Blauvelt
- Seyed Amirreza Goharjo
- Benjamin Matteson
- Cole Nixon
- Annelise Peake
- Miles Sanguinetti
- Karis Sponsler
- Anders Swanson
- Aleena Watson

**December 6, 2017 (10:15am)**
- Halala Khoshnaw
- Gavin Balakrishnan
- Bin Chen
- Jake Demming
- Rupika Dikkala
- Princess Kim
- Adam Kowalski
- Joseph Sands

## P4: Final Paper (Finals Week)
Your final term paper is due via the D2L dropbox by 5pm on the Friday of finals week (December 8).

Your paper should meet the following criteria:

- **Formatting**: Letter sized paper, 1" margins, standard writing font at size 10-12pt, 1.5 line spacing

- **Length**: 1400-1800 words

- **Introduction** (~400 words): 
  - Clearly and succinctly identify the issue you have investigated: _What_ is the problem?
  - Situate this issue in a social and technical context to motivate the reader's attention:  _Why_ is this particular issue important for society?  _How_ does this issue relate to computer science? 
  - Include a thesis statement that clearly conveys an opinion on the issue, based upon a reasonable analysis of the data that you gathered for the paper. 
  - Briefly summarize the argument you will make in support of this thesis.

- **Body** (~800 words):
  - Give the reader a more detailed background on the issue and its social, historical, and/or technical dimensions.
  - Analyze the issue in terms of your thesis statement: What are the evidences and arguments you have found in your research that either support or oppose your thesis? What is the rationale for dismissing evidence or arguments that do not align with your thesis? 
  - As a whole, this main section of the paper should make a compelling argument in favor of your thesis. This argument should be grounded in reasoning and evidence, and you should identify what type of argument you are making. For example, a compelling argument could take any of these forms: 
    - Ethical – base the opinion upon an ethical theory analysis, or ethical best practices
    - Extrapolation – base the opinion on a prediction of future that has some scientifically sound basis
    - Legal – base the opinion upon some laws or legal theories
    - Social - base the opinion upon social science, psychology, or other scholarship that provides evidence for your claim/position.
    
- **Conclusion** (~400 words):
   - Briefly recap the issue. 
   - Clearly articulate *your* opinion on the topic / your thesis statement.
   - Summarize the arguments that you presented in the body of the paper in support of your thesis.

- **References**: 
  - The body of the paper should meaningfully incporporate at least 5 credible references.
  - **At least 2 references must be peer-reviewed scholarly works**
  - **At least 3 references should be things you located on your own**, and which were not assigned readings for the course
  - References should be cited in a standard format (e.g., ACM, IEEE, MLA, Chicago).  
  - References should be listed at the end of the paper in a 'Works Cited,' 'Bibliography,' or 'References' section. If you use a footnote-based style, you should still include a separate bibliography list at the end.
  
  
**Please note:** the upper bound for the word count is as important as the lower bound! It is important for computer scientists to be clear and concise.

The campus Writing Center offers many free and useful services to students: <https://www.pdx.edu/writing-center/>. 

This overview may be helpful if you have not written many term papers: <https://www.pdx.edu/writing-center/guided-tour-step-one-understanding-an-assignment>
