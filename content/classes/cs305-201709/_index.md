---
title: "CS 305 (Fall 2017)" 
date: 2017-09-01 
quarter: 201704 
courseNumber: "CS 305" 
courseTitle: "Social, Ethical, and Legal Implications of Computing" 

shortDescription: "This course includes a survey of topics related to the social, ethical, and legal implications of computing, including: the history of computing, the social context of computing, professional and ethical responsibilities, the risks and liabilities of safety-critical systems, intellectual property, privacy and civil liberties, the social implications of the Internet, computer crime, and economic issues in computing."

type: "class"
layout: "class-home"

menu: 
    main:
        parent: "Teaching"
---

This course -- including the schedule, homework assignments, and lecture slides -- has been only slightly modified from a previous version developed by <a href="http://web.cecs.pdx.edu/~wuchi/#teaching">Wu-chi Feng</a>.

This course provides an insight into the social, ethical, and legal implications of computing. The introduction of advanced computing hardware and software has had significant impact on society, both good and bad. This course is intended to make students think critically about the impact of such technologies and the legal and ethical implications of the profession. Students will be required to write a term-paper on a chosen area of computing and ethics or its impact on society. Furthermore, students are required to present their topic to the class in a 8-10 minute presentation in the last half of the quarter.
