---
title: Syllabus
credits: <p>This course -- including the schedule, homework assignments, and lecture slides -- has been only slightly modified from a previous version developed by <a href="http://web.cecs.pdx.edu/~wuchi/#teaching">Wu-chi Feng</a>.</p>
cc: false
type: "class"
layout: "subpage"

menu: 
    main:
        parent: "Teaching"
---

* TOC
{:toc}

## Instructor Information
- Ellie Harmon
  - <ellie.harmon@pdx.edu>
  - FAB 120-15
- Office Hours:
	- Tuesdays 9am-11am
	- Fridays 1pm-3pm

## Class Information
- Fridays 10:10am-12:05pm
  - FAB 170
  - <http://www.cs.pdx.edu/~harmon8/cs305.html>

### Final Exam Time
- _There will be no written final exam, but this time slot will be used for final presentations. Attendance is mandatory._
- Wednesday December 6
  - 10:15am-12:05pm
  - FAB 170

## Text (Optional)
We will be loosely following this textbook in the course lectures. The book may be a useful resource for you in completing homeworks and writing your final paper; you may also find it a useful reference book in the future. However, it is not required and you will not be responsible for reading the textbook chapters before class meetings. There will be no quizzes or exams based on any textbook material that is not covered in class.

- Michael Quinn, _Ethics for the Information Age_

### Additional Readings
Additional short readings posted to the course schedule are required, and will all be available online. I am still locating readings for weeks 3-6. You can expect one short reading (or one ~30 minute podcast episode to listen to) for each of weeks 2-6. After that, we will just be focusing on your own presentations.

## Access and Inclusion for Students with Disabilities

PSU values diversity and inclusion; we are committed to fostering mutual respect and full participation for all students. My goal is to create a learning environment that is equitable, useable, inclusive, and welcoming. If any aspects of instruction or course design result in barriers to your inclusion or learning, please notify me. The Disability  Resource Center (DRC) provides reasonable accommodations for students who encounter barriers in the learning environment.

If you have, or think you may have, a disability that may affect your work in this class and feel you need accommodations, contact the Disability Resource Center to schedule an appointment and initiate a conversation about reasonable accommodations. The DRC is located in 116 Smith Memorial Student Union, 503-725-4150, drc@pdx.edu, https://www.pdx.edu/drc.

* If you already have accommodations, please contact me to make sure that I have received a faculty notification letter and discuss your accommodations.
* Students who need accommodations for tests and quizzes are expected to schedule their tests to overlap with the time the class is taking the test.
* Please be aware that the accessible tables or chairs in the room should remain available for students who find that standard classroom seating is not useable.
* For information about emergency preparedness, please go to the [Fire and Life Safety webpage](https://www.pdx.edu/environmental-health-safety/fire-and-life-safety) for information.

**Note: there will be no written exams in this course.**

## Communication Policy

There is a Slack team set up for questions about the course. I recommend asking questions in the Slack team so that everyone can benefit from the answer.

https://pdx-cs305.slack.com/

You may also email me with any questions. If you do need to email, please make sure to include CS305 in the subject line.

Emails and Slack messages will be responded to within 48 hours (or by 5pm on Monday for all messages sent on a Friday). Do not expect an immediate response.

## Late Work
- Assignments must be completed by the specified time on their due date or will be counted late (10% deduction each day late). No assignments will be accepted more than 1 week (7 days) late.
- **EXCEPTION: The final paper and final presentation reflection will not be accepted after 5pm on Sunday December 10.**

## Academic Honesty

Do not cheat. Do not plagiarise. Properly cite anything you reference in your writing assignments. Definitely do not copy and paste blog posts from the internet into Word Documents and turn them in as if they are your own personal essays!

Please see this library website for more information about how to avoid plagiarism: <https://library.pdx.edu/diy/avoid-plagiarism>

**Any assignment determined to be plaigarised will receive an automatic zero. A letter will be sent to the head of the CS Department.**

## Course Objectives
1. Identify the ethical issues that relate to computer science in real situations they may encounter.
2. Decide whether a given action is ethical as regards computer science professional ethics, and justify that decision.
3. Look up relevant ethical standards as developed by the ACM.
4. Prepare and deliver a short (8-10 minute) professional-quality talk on a topic relating to ethical, legal, and social implications of computer science.
5. Research and write a professional-quality paper about a topic relating to social, legal, and ethical implications of computer science.
6. Recognize situations in which there may be legal issues as regards computer science and related topics such as intellectual property, and know some legal principles to apply.
7. State several important impacts of computer science and related fields on contemporary society.
8. State several examples of important ethical principles as they apply to computer science related situations.

(See: <https://www.pdx.edu/computer-science/cs305>)


## Course Schedule Overview

Week | Topic
-|-
1| History of Computing, Intro to Ethics
2| Networks & Freedom
3| Intellectual Property
4| Labor & Inequality
5| Privacy
6| Security & Reliability
7| NO CLASS - Veteran's Day.
8| Class Presentations
9| NO CLASS - Thanksgiving.
10| Class Presentations
F| Class Presentations
 | _This meeting is during the finals time slot_
 | _WEDNESDAY from 10:15am-12:05pm_


## Grading

Points|Assignment
-|-
18|Attendance & Participation
20|Homeworks (4 x 5)
1|P0: Topic
9|P1: Annotated Bibliography
7|P2: Abstract
15|P3: Presentation
15|P4: Final Paper
15|Reflections on Others' Presentations (3 x 5)
-|-
100|Total

**Extra Credit:** If at least 90% of the class fills out the teaching evaluation at the end of the term, everyone gets 1 extra point added to their final grade.

### Conversion to Letter Grades:

|Total Points|Grade|
|:----|:----|
|----
|98 - 100|A+|
|93 - 97|A|
|90 - 92|A-|
|----
|88 - 89|B+|
|83 - 87|B|
|80 - 82|B-|
|----
|78 - 79|C+|
|73 - 77|C|
|70 - 72|C-|
|----
|68 - 69|D+|
|63 - 67|D|
|60 - 62|D-|
|----
|Below 60|F|

## Attendance and Participation

Attendance and participation is a major part of this course. Therefore, attendance is required. All students are expected to come to class prepared to participate fully in-group discussions, and come to class with questions about the reading material and viewpoints from their own experiences and other relevant literature and coursework.

### Attendance

You will get *one no-questions-asked absence*. Each additional absence will automatically lower your attendance and participation score by 2 points (total possible score is 18 points). Exceptions to this policy will only be made in the case of _documented_ medical or family emergencies (i.e., you have a doctor's note).

### Participation

Here are some examples of how you can participate:

* Treat all with respect â be constructive in all discussions
* Come to class prepared â read carefully prior to class meetings, and
* Practice active listening â be attentive, be engaged, and use in-class technology with discretion
* Ask challenging questions in class
* Comment, build on, or clarify othersâ contributions
* Post useful or interesting information to the class slack channel
* Visit the instructor or teaching assistant to chat, ask questions, or give feedback
* For the week that you present, you automatically earn your participation point for that day.

* **Collaborative Course Notes**: We will use a shared Google Doc to create an archive of notes on class material. This document will remain acessible to you after you finish the class, serving as a resource of information discussed. If you are someone who does not like talking in class, you can  participate in the course by contributing to this shared archive. _**NOTE**: In order for me to see your contributions and count them towards your participation grade, you will need to make sure you are logged into the Google Doc with your pdx.edu user account. We will cover this on the first day of class. Please see me during my office hours if you have questions._

## File Formats
All assignments in this course, unless otherwise noted in the specific assignment description, will be turned in in `.docx` format. You must contact me by the end of the first week if you think this presents a significant problem for you, and I will help you figure out how to meet this requirement. Here are your choices for composing documents:
    - Obviously you can use MS Word.
    - LibreOffice & Google Docs can directly export to `.docx`
    - If you are a LaTeX user, that's great. You can very easily convert your `.tex` file to MS Word format using the excellent [Pandoc](http://pandoc.org/) library. I wrote my dissertation in markdown, with final PDFs generated through LaTeX, and with preliminary drafts circulated to committee members in MS Word format because that is the standard professional format used for sharing and commenting on documents. Please feel free to stop by my office hours if you need help setting this up.

I will not accept `.pdf`, `.txt`, `.md`, `.tex`, `.pages`, or any other types of files for any assignments. It becomes too much of a problem for grading and commenting on my end, partially thanks to the features of D2L, partially because of my need to easily check word count requirements, and partially because of a desire to make comments in an easy and consistent manner across assignments.

I do not want to hear about how much you hate Microsoft Word. I have written many more (and many longer) documents than you and I promise you I have more horror stories and frustration with the software. Nonetheless, `.docx` is the standard professional file format today, and that's what we will use in this class.

You are always welcome to turn in a `.pdf` file in addition to a `.docx` file, but I will always grade the `.docx` file.

## Homework
- There are four designated homework assignments that ask you to reflect on course topics and research related information.
  - HW1 - Professional Codes
  - HW2 - Networks & Freedom
  - HW3 - Intellectual Property
  - HW4 - Labor & Inequality

- HW1 is due the Tuesday after the first class. All other homeworks are due at the **start of class** on the day we discuss that topic.


## Presentation and Term Paper

Unlike many other courses in computer science, this course will require you to write a final term paper on a topic of your choice related to the social, ethical, and legal dimensions of computing.. In preparation for this paper, you will first give an 8-10 minute presentation in class on your topic. The campus Writing Center offers many free services to students, and I encourage you to take advantage of this resource: <https://www.pdx.edu/writing-center/>.

- There are five concrete deliverables for the term paper that are designed to help you develop this project in stages over the course of the term.
  - P0: Propose a topic (Week 4)
  - P1: Annotated Bibliography (Week 6)
  - P2: Abstract (Week 7)
  - P3: Presentation (Weeks 8-11)
    - You will give an 8-10 minute in-class presentation on your topic in class on an assigned date.
    - NOTE: **Your presentation slides are due via the D2L dropbox by 5:00pm on the DAY BEFORE you are scheduled to present.** All slides will be loaded on the classroom computer for presentation in class. You may not present off of your own computer.
  - P4: Your final term paper is due via the D2L dropbox by 5pm on the Friday of finals week (December 8)

Detailed rubrics for the Paper and Presentation requirements will be posted to the course website no later than Monday October 16. A Presentation schedule will also be posted no later than Monday October 16.

## Presentation Reflections
You are expected to pay attention to others' presentations in the last three weeks of class. You will write 3 short reflections about what you learned on each day that there are presentations. These assignments will be due by 5:00pm 2 business days after the presentation (for Friday presentations, reflections are due the following Tuesday night; for Wednesday presentations, reflections are due the following Friday night). Due dates are all on the course schedule.

Detailed requirements and rubrics for these reflections will be posted to the course website no later than Monday, November 13.
