---
title: Schedule
credits: <p>This course -- including the schedule, homework assignments, and lecture slides -- has been only slightly modified from a previous version developed by <a href="http://web.cecs.pdx.edu/~wuchi/#teaching">Wu-chi Feng</a>.</p><p>Some reading assignments from Dr. Casey Fiesler’s Information Ethics &amp; Policy class, offered at CU Boulder, Fall of 2016. <a href="https://informationethicspolicy.wordpress.com/">https://informationethicspolicy.wordpress.com/</a></p>
cc: false
type: "class"
layout: "subpage"

menu: 
    main:
        parent: "Teaching"
        identifier: "cs305-201709-schedule"
---

Course meetings are highlighted in bold. Italicized rows indicate holidays and assignment deadlines on days when there is no course meeting.

Note that while there is no written final exam, the final exam time period will be used for final presentations. Attendance is mandatory.

_Note: This schedule is subject to change. All changes will be announced and discussed in class no less than one week before any assignments are due._

| Week | Date | Topic & Readings | Assignment|
|:-----|:-----|:------|:----------|
1| **F 29-Sep** | **A Brief History of Computing, Intro to Ethics** [slides](/~harmon8/assets/cs305/CS305-Fall2017-Day1.pdf)||
|||No required readings||
||| _Optional:_ Textbook Chapters 1, 2, 9 ||
|--
|2| _T 3-Oct_ | -- | *HW1 (5pm)*
|--
|| **F 6-Oct** | **Networks & Freedom** [slides](/~harmon8/assets/cs305/CS305-day2.pdf) | [HW2](/~harmon8/cs305/homework.html)
|||**Required:** "Free Speech, Limited?" Mozilla IRL Podcast. Available as a podcast to listen to or transcript to read at: <https://irlpodcast.org/episode7/>||
||| _Optional:_ Textbook Chapter 3 ||
|--
|3| **F 13-Oct** | **Intellectual Property** [slides](/~harmon8/assets/cs305/CS305-day3.pdf)| [HW3](/~harmon8/cs305/homework.html)
| |  | **Required:** Will Frank. “IP-rimer: A Basic Explanation of Intellectual Property” blog post on Medium. <https://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711> | |
| |  | **Required:** Aaron Swartz. Code and Other Laws of Wikipedia. <http://www.aaronsw.com/weblog/wikicodeislaw> | |
||| _Optional:_ Textbook Chapter 4 ||
|--
4| **F 20-Oct** | **Labor and Inequality** [slides](/~harmon8/assets/cs305/CS305-day4.pdf) | [HW4](/~harmon8/cs305/homework.html) + P0
|||**Required:** CHOOSE ONE of the following two articles:
|||**Choice 1 (digital inequality):** Jonas Lerman. Big Data and Its Exclusions. *Stanford Law Review*, September 2013. <https://www.stanfordlawreview.org/online/privacy-and-big-data-big-data-and-its-exclusions>||
|||**Choice 2 (automation and digital labor):** Lilly C. Irani and M. Six Silberman. 2013. Turkopticon: interrupting worker invisibility in amazon mechanical turk. In Proceedings of the SIGCHI Conference on Human Factors in Computing Systems (CHI '13). ACM, New York, NY, USA, 611-620. DOI: <https://doi.org/10.1145/2470654.2470742> Also available at: <http://wtf.tw/text/turkopticon.pdf>||
||| _Optional:_ Textbook Chapter 10 ||
|--
5| **F 27-Oct** | **Privacy** [slides](/~harmon8/assets/cs305/CS305-day5.pdf)|
| |  | **Recommended:** Leysia Palen and Paul Dourish. 2003. Unpacking "privacy" for a networked world. In Proc. CHI '03. <http://dx.doi.org/10.1145/642611.642635> or <http://dourish.com/publications/2003/chi2003-privacy.pdf>  | |
|||**Supplements to class discussion:**||
|||Agre, Philip E. “Beyond the Mirror World: Privacy and the Representational Practices of Computing.” In Technology and Privacy : The New Landscape, edited by Philip E. Agre and Marc Rotenberg. 1997. Reprint, Cambridge, United States: MIT Press, 2015. <http://ebookcentral.proquest.com/lib/psu/detail.action?docID=3338469>||
|||Shapiro, Stuart. “Places and Spaces: The Historical Interaction of Technology, Home, and Privacy.” The Information Society 14, no. 4 (November 1, 1998): 275–84. <https://doi.org/10.1080/019722498128728>||
||| _Optional:_ Textbook Chapters 5 & 6 ||
|--
6| **F 3-Nov** | **Security & Reliability** [slides](/~harmon8/assets/cs305/CS305-day6.pdf)| P1 (5pm)
| |  | **Recommended:**  Big Data's Disparate Impact <http://www.californialawreview.org/2-big-data/> | |
||| _Optional:_ Textbook Chapters 7 * 8 ||
|--
7| *F 10-Nov* | *No class. University Closed. Veteran's Day* | *P2 (5pm)*
|--
8| *R 16-Nov* |  -- | _Group 1: Slides due 5pm_
|--
|| **F 17-Nov** | **Class Presentations, Group 1** |
|--
9| *T 21-Nov* | -- | *R1 (5pm)*
|--
|| *F 24-Nov* | *No class. University Closed. Thanksgiving* |
|--
|10| _R 30-Nov_ |  -- | _Group 2: Slides due 5pm_
|--
|| **F 1-Dec** | **Class Presentations, Group 2** |
|--
|F| *T 5-Dec* | -- | *R2 (5pm)*
||||_Group 3: Slides due 5pm_
|--
|| **W 6-Dec** | **Class Presentations, Group 3** |
||| _During the final time slot 10:15am-12:05pm_ |
|--
|| *F 8-Dec* |  -- | *R3 (5pm)*
|||  -- | *P4 (5pm)*
{:.full}

\* P3 is the presentation assignment. Presentations will be delivered in class. Slides are due to D2L by 5pm on the _day before_ you are scheduled to present. Slides will only be presented off the classroom computer. **You may not connect your laptop to the display for your presentation.**

- Course meetings are on Fridays.
- All assignments should be turned in on D2L.
- All assignments are due at 10:00am unless otherwise noted on the schedule.
  - Assignments must be completed by the specified time on their due date or will be counted late (10% deduction each day late). No assignments will be accepted more than 1 week (7 days) late.
  - **EXCEPTION: The final paper and final presentation reflection will not be accepted after 5pm on Sunday December 10.**
