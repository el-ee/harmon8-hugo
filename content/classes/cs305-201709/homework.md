---
title: Homework
credits: <p>This course -- including the schedule, homework assignments, and lecture slides -- has been only slightly modified from a previous version developed by <a href="http://web.cecs.pdx.edu/~wuchi/#teaching">Wu-chi Feng</a>.</p>
cc: false
type: "class"
layout: "subpage"

menu: 
    main:
        parent: "Teaching"
---

There are four homework assignments in this course that will be due during the first four weeks of the term. All homework assignments should be turned in on D2L. 

* TOC
{:toc}

## Homework 1:  Professional Codes

**Due: Tuesday October 3, 2017 at 5pm**

_All assignments should be turned in on D2L. Scores will be reduced by 10% for each day a homework is late. Assignments more than 7 days late will not be accepted._

**You will need to upload two files to D2L for this homework:**

- A square image (selfie) of you. The file should be named lastname.firstname.jpg. This image will be used solely for creating a picture roster so I can learn your names
- A text-based document with responses to the homework questions. Any standard file format is acceptable, e.g. .docx, .txt, .rtf, .pdf. Please include your name, the date, and the assignment name (HW1) at the top of the file.

**Homework Questions:**

1. Codes of ethics and conduct. Locate websites that match each of the following criteria. List the URL for the website and explain in 1 or 2 sentences how/why the site meets the criteria. If it is completely obvious, then just say so:
    1. Find **two** sites that contain a code of ethics *for computer scientists or software engineers*. They should be well-known professional organizations in computing (e.g. ACM)
    1. Find **one** site that contains a code of ethics for a discipline *other than computing* (e.g. mechanical engineering, anthropology).
    1. Find one site that contains a *code of conduct* for a well-known & reputable computing or software industry conference or professional *event*.
    1. Find one site that contains information that could be used as a guide when developing a Code of Ethics for yourself or a company. The site should be from a reputable organization or individual.
    1. Find one site that provides guidance to an individual who finds a significant legal or ethical problem within an organization but is afraid to report it. The site should be from a reputable organization or individual.

2. Explain the similarities and the differences between the codes of ethics identified in questions 1.1 and 1.2. This reflection should be at a high level and about general themes, not point-by-point differences in each line item. A few sentences is plenty.

*NOTES: I will share useful answers with the class at the start of our next meeting. Please indicate if you would prefer that I do or do not credit you when sharing your URLs / comments.*

## Homework 2: Networks and Freedom

**Due: Friday October 6, 2017 at 10am**

_All assignments should be turned in on D2L. Scores will be reduced by 10% for each day a homework is late. Assignments more than 7 days late will not be accepted._

You will need to upload one file to D2L for this homework: 

- A text-based document with responses to the homework questions. Any standard file format is acceptable, e.g. .docx, .txt, .rtf, .pdf. Please include your name, the date, and the assignment name (HW2) at the top of the file.

**Homework Questions:**

1. What is the difference between ethics and morality?
3. Listen to the Mozilla podcast / or read the transcript linked on the schedule for Friday Oct 6, titled “Free Speech, Limited?”. In the podcast, Veronica Belmont leads a discussion with Jillian York with the EFF, Brandi Collins with Color of Change, and Anil Dash, a tech entrepreneur. 
    1. In 1-3 sentences, summarize the position of **two** of these three people with regards to free speech online.
    2. If you had been on this podcast, what position would you have taken? Why? (A few sentences is fine.)
4. Look up the court case: Hazelwood School District v. Kuhlmeier:
    1. Summarize what the case was about in a couple of sentences
    2. What was the outcome?
    3. What was the basis for the ruling? One or two sentences are fine.
    4. What was the basis for the dissenting votes? One or two sentences are fine.


## Homework 3: Intellectual Property

Due: Friday October 13, 2017 at 10am

You will need to upload one file to D2L for this homework. PDF is preferred.

Please include your name, the date, and the assignment name (HW2) at the top of the file.

_You should only need about 1 sentence per answer for each of these questions, except for #4 & #11. These should probably take 3-5 sentences each._

1. Suppose Jim is in a middle school band that is selling cookie dough. His mom sends and e-mail to all the employees of her company (100 people),inviting them to stop by after work to place orders for the cookie dough. There are no company rules prohibiting the use of the email system for personal emails. 15 people do so, but the other 85 were annoyed with the email.
    1. Did Jim’s mom do anything wrong according to Kantianism? (yes/no + explain in 1-2 sentences)
    2. Did Jim’s mom do anything wrong according to Act Utilitarianism? (yes/no + explain in 1-2 sentences)
    3. What’s your personal opinion? Do you think it was okay for Jim’s mom to do what she did? (Yes/No/Not sure + Explain in 1-2 sentences. If your answer is not sure, what other information would you need to make a decision?)
2. What is Intellectual Property?
3. What are two issues with regard to dealing with intellectual property?
4. What are three ways to protect intellectual property? Give a one sentence definition of each (e.g. what can it be used for / what does it protect)
5. What are two factors that impact a trademark's validity?
6. What is the idea/expression distinction and which kind of IP protection does it impact?
7. What is 'fair use'?
8. What was important about the Lenz v. Universal Music Corp. case?
9. What is the purpose of IP rights as laid out in the US Constitution? Beyond the direct quote, what does this mean in more simple terms?
10. What is the purpose of Creative Commons?  (Beyond the reading, you may also find the CC website useful: <https://creativecommons.org/> )
11. Personal opinion. Please write 3-5 sentences about your opinion and experiences with IP and software. For example, you could write about things like: Have you ever licensed software? Have you thought about what license you should/would use for software you write? What do you know about the differences between the GPL, MIT, BSD, and Apache licenses for open source projects? Have you ever filed for a patent? Do you want to file for a patent? Do you think software patents are appropriate?

## Homework 4: Labor and Inequality (includes P0)

Due: Friday October 20, 2017 at 10am

You will need to upload **one** file to D2L for this homework. PDF is preferred.

Please include your name, the date, and the assignment name (HW2) at the top of the file.

**For P0:**  
[worth 1 point towards your paper grade]

1. What topic do you want to research for your final presentation and paper?
  - _Just a few words here is plenty. You will submit a longer abstract in a couple of weeks. For now, I just want to know your planned topic. Err on the side of more specificity rather than less (e.g., 'Facebook's ad tracking technology' is well-scoped for a paper that can be no more than 1800 words. Something big like 'privacy and the internet' is probably too broad.)_

**Regarding the reading**:  
[worth 5 points towards your homework grade]  
(_each answer should be 2-5 sentences in length_)

1. Which article did you choose to read, and why?
2. What is the problem or question to which the author of this article is responding?
3. What is the author's response or answer to this problem or question? Or, put differently, what is their argument?
4. What evidence do they present in support of this answer/argument?
5. Is the author's argument compelling? Why or why not? 
6. What is your opinion on the subject? Does reading this piece raise any new questions for you?