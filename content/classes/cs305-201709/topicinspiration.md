---
title: Final Paper Inspiration
credits: <p>This course -- including the schedule, homework assignments, and lecture slides -- has been only slightly modified from a previous version developed by <a href="http://web.cecs.pdx.edu/~wuchi/#teaching">Wu-chi Feng</a>.</p>
cc: false
hidemenu: true
type: "class"
layout: "subpage"

menu: 
    main:
        parent: "Teaching"
---

If you're not sure what you want to write about, here are some things to look at for inspiration:

* TOC
{:toc}

## Popular Media

- The Oversharing archive: <https://tinyletter.com/oversharing/archive>

- QZ Machines with Brains collection: <https://qz.com/se/machines-with-brains>
- QZ Future of Finance: <https://qz.com/on/future-of-finance/>
- QZ Future of Work: <https://qz.com/on/future-of-work/>

- Sarah Jeong at [The Atlantic](https://www.theatlantic.com/author/sarah-jeong/) + [Motherboard](https://motherboard.vice.com/en_us/contributor/sarah-jeong) + [Forbes](https://www.forbes.com/sites/sarahjeong/#7740445e3ffe) + [The Guardian](https://www.theguardian.com/profile/sarah-jeong) + [The Verge](https://www.theverge.com/users/Sarah%20Jeong/posts)

- "New research from AAA and the University of Utah found that car, and 29 other 2017 models from various manufacturers, each had systems that put a significant burden on drivers to use — mentally or visually pulling attention from the road." <https://wtop.com/dc-transit/2017/10/car-can-now-cause-distracted-driving/>

- "Boring, complex and important: the deadly mix that blew up the open web" Cory Doctorow on _BoingBoing_ <https://boingboing.net/2017/09/21/democracy-dies-in-dullness.html>

- "Researchers: Uber’s iOS App Had Secret Permissions That Allowed It to Copy Your Phone Screen" _Gizmodo_ <https://gizmodo.com/researchers-uber-s-ios-app-had-secret-permissions-that-1819177235>
- "Equifax Was Warned About Vulnerability But Failed To Patch It" _Gizmodo_ <https://gizmodo.com/equifax-was-warned-about-vulnerability-but-failed-to-pa-1819065186>

- "Sherry Turkle: ‘I am not anti-technology, I am pro-conversation’ " in _The Guardian_ <https://www.theguardian.com/science/2015/oct/18/sherry-turkle-not-anti-technology-pro-conversation>
- "'Our minds can be hijacked': the tech insiders who fear a smartphone dystopia " in _The Guardian_ <https://www.theguardian.com/technology/2017/oct/05/smartphone-addiction-silicon-valley-dystopia>
- "'After, I feel ecstatic and emotional': could virtual reality replace therapy? " in _The Guardian_ <https://www.theguardian.com/technology/2017/oct/07/virtual-reality-acrophobia-paranoia-fear-of-flying-ptsd-depression-mental-health>
- "We can't ban killer robots -- it's already too late." in _The Guardian_ <https://www.theguardian.com/commentisfree/2017/aug/22/killer-robots-international-arms-traders>
- "Are patent trolls strangling sustainable innovation?" <https://www.theguardian.com/sustainable-business/patent-trolls-sustainable-innovation>

- Next:Economy newsletter: "What's in store for Amazon, Piketty strikes again + the posthuman world" <http://post.oreilly.com/form/oreilly/viewhtml/9z1zhb13rub598qb6ip84q8gggbpp5k5uf3kdohd860?imm_mid=0eb6e9&cmp=em-business-na-na-newsltr_econ_20161209>
- Next:Economy newsletter: "The impact of automation, the dark side of competition, and rights for robots" <http://post.oreilly.com/form/oreilly/viewhtml/9z1zkr98i50vdsbfldvpheg8rket3b3td9rs5pkj940?imm_mid=0ec5fe&cmp=em-business-na-na-newsltr_econ_20170120>
- Next:Economy newsletter: "Flying cars, automation at Amazon + Agile in the enterprise" <http://post.oreilly.com/form/oreilly/viewhtml/9z1z6v1oe4v0ho1513oouljogdk5pd1ufujl45lfasg?imm_mid=0ed7ca&cmp=em-business-na-na-newsltr_econ_20170217>
- Next:Economy newsletter: "Peak gig, superhuman AI + the conspicuous consumption of time" <http://post.oreilly.com/form/oreilly/viewhtml/9z1z1b6i5n8184ouj2mcuf0st83loodf0gpe0p8rnuo?imm_mid=0f1898&cmp=em-business-na-na-newsltr_econ_20170512>


## ACM Publications

- Christoph Lutz and Aurelia Tamò. 2015. RoboCode-Ethicists: Privacy-friendly robots, an ethical responsibility of engineers?. In Proceedings of the 2015 ACM SIGCOMM Workshop on Ethics in Networked Systems Research (NS Ethics '15). ACM, New York, NY, USA, 27-28. DOI= <http://dx.doi.org/10.1145/2793013.2793022> 
- Yang Sun, Isaac G. Councill, and C. Lee Giles. 2010. The Ethicality of Web Crawlers. In Proceedings of the 2010 IEEE/WIC/ACM International Conference on Web Intelligence and Intelligent Agent Technology - Volume 01 (WI-IAT '10), Vol. 1. IEEE Computer Society, Washington, DC, USA, 668-675. DOI= <http://dx.doi.org/10.1109/WI-IAT.2010.316>
- M. J. Wolf, K. Miller, and F. S. Grodzinsky. 2017. Why we should have seen that coming: comments on Microsoft's tay "experiment," and wider implications. SIGCAS Comput. Soc. 47, 3 (September 2017), 54-64. DOI: <https://doi.org/10.1145/3144592.3144598>
- Paul B. de Laat. 2017. Big data and algorithmic decision-making: can transparency restore accountability?. SIGCAS Comput. Soc. 47, 3 (September 2017), 39-53. DOI: <https://doi.org/10.1145/3144592.3144597>
- Simon Rogerson. 2017. Is professional practice at risk following the Volkswagen and Tesla revelations?: software engineering under scrutiny. SIGCAS Comput. Soc. 47, 3 (September 2017), 25-38. DOI: <https://doi.org/10.1145/3144592.3144596> 
- Bo Brinkman, Catherine Flick, Don Gotterbarn, Keith Miller, Kate Vazansky, and Marty J. Wolf. 2017. Dynamic technology challenges static codes of ethics: a case study. SIGCAS Comput. Soc. 47, 3 (September 2017), 7-24. DOI: <https://doi.org/10.1145/3144592.3144595>


## Twitter Commentary

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Let&#39;s pause to applaud someone making the right decision about a tech product... <a href="https://t.co/R7VYSer6pq">https://t.co/R7VYSer6pq</a></p>&mdash; Mel (@melgregg) <a href="https://twitter.com/melgregg/status/916385515061911552?ref_src=twsrc%5Etfw">October 6, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">under-educated computer scientists are ensuring that machine learning is phrenological determinism <a href="https://t.co/tTH1ZpQfaW">https://t.co/tTH1ZpQfaW</a></p>&mdash; cameron tonkinwise (@camerontw) <a href="https://twitter.com/camerontw/status/905768242215510016?ref_src=twsrc%5Etfw">September 7, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">*pinching the bridge of my nose* okay. all right. next time...next time lets consider the implications...BEFORE writing the algorithm <a href="https://t.co/IZ5KFUkVcA">pic.twitter.com/IZ5KFUkVcA</a></p>&mdash; judas priest friday (@markpopham) <a href="https://twitter.com/markpopham/status/905500529584992258?ref_src=twsrc%5Etfw">September 6, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">how data can exacerbate biases in policing, awesome coverage on new work from <a href="https://twitter.com/Sarah_Brayne?ref_src=twsrc%5Etfw">@Sarah_Brayne</a> <a href="https://t.co/tbEMry2xrH">https://t.co/tbEMry2xrH</a></p>&mdash; Amelia 😈🤳👻📝🎃📊 (@amelia_acker) <a href="https://twitter.com/amelia_acker/status/907321308081467393?ref_src=twsrc%5Etfw">September 11, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Facebook let advertisers target Nazis, &quot;Jew haters,&quot; and other anti-Semitic categories. <br><br>We tested it: <a href="https://t.co/bOSxRzFmBE">https://t.co/bOSxRzFmBE</a></p>&mdash; ProPublica (@ProPublica) <a href="https://twitter.com/ProPublica/status/908422250268946432?ref_src=twsrc%5Etfw">September 14, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Facebook allows ads targeting people by school/employer. Some jerks put &quot;Jew hater&quot; as employer. Media freaks out - <a href="https://t.co/r6HaFWWJdi">https://t.co/r6HaFWWJdi</a></p>&mdash; Dare Obasanjo (@Carnage4Life) <a href="https://twitter.com/Carnage4Life/status/908553075056451584?ref_src=twsrc%5Etfw">September 15, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">For my <a href="https://twitter.com/hashtag/digsoc?src=hash&amp;ref_src=twsrc%5Etfw">#digsoc</a> students: a tech company that routinizes and scales race-based access to its resource is a racial project. <a href="https://t.co/Q2qttud5k3">https://t.co/Q2qttud5k3</a></p>&mdash; Tressie Mc (@tressiemcphd) <a href="https://twitter.com/tressiemcphd/status/908439848511516680?ref_src=twsrc%5Etfw">September 14, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Finished a blog post: ~6000 words on lessons learned from creating an accessibility engineering practice: <a href="https://t.co/NEVu1xEzbq">https://t.co/NEVu1xEzbq</a> <a href="https://twitter.com/hashtag/a11y?src=hash&amp;ref_src=twsrc%5Etfw">#a11y</a></p>&mdash; dan 🙌 (@dxna) <a href="https://twitter.com/dxna/status/908400320123994112?ref_src=twsrc%5Etfw">September 14, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">It should not surprise you that no author on this &quot;objective&quot; pain detection algorithm paper is in a medical dept: <a href="https://t.co/W3u26yh4MN">https://t.co/W3u26yh4MN</a></p>&mdash; Ghastly Nick Seaver (@npseaver) <a href="https://twitter.com/npseaver/status/910454598510538752?ref_src=twsrc%5Etfw">September 20, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">If $1/person is the risk you face for screwing half the US adult population, why spend a dime for data security? <a href="https://t.co/8ELr09qaEn">https://t.co/8ELr09qaEn</a></p>&mdash; Kontra (@counternotions) <a href="https://twitter.com/counternotions/status/910483686046519296?ref_src=twsrc%5Etfw">September 20, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">What we mean when we talk about Broadband <a href="https://t.co/SU7oGXAVsz">https://t.co/SU7oGXAVsz</a></p>&mdash; Amelia 😈🤳👻📝🎃📊 (@amelia_acker) <a href="https://twitter.com/amelia_acker/status/910691343688781824?ref_src=twsrc%5Etfw">September 21, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

You need to click through for the thread on the next one:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">On Monday I&#39;m speaking at Harvard/<a href="https://twitter.com/daeaves?ref_src=twsrc%5Etfw">@daeaves</a>&#39;s course on Digital Government (&amp; then seminar, which is open!) at the public policy school...</p>&mdash; Dave Guarino (@allafarce) <a href="https://twitter.com/allafarce/status/911287613570584576?ref_src=twsrc%5Etfw">September 22, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">“People must label massive collections of unsorted data so computers can” generate profits  <a href="https://t.co/kxSWImbAwH">https://t.co/kxSWImbAwH</a></p>&mdash; Frank Pasquale (@FrankPasquale) <a href="https://twitter.com/FrankPasquale/status/911567186526863360?ref_src=twsrc%5Etfw">September 23, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="und" dir="ltr">The Digital Divide in Rural America <a href="https://t.co/2a6Zv2YOPS">https://t.co/2a6Zv2YOPS</a></p>&mdash; Yashar Ali 🐘 (@yashar) <a href="https://twitter.com/yashar/status/912003076105621504?ref_src=twsrc%5Etfw">September 24, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">#1984  👉 <a href="https://twitter.com/neofacesoftware?ref_src=twsrc%5Etfw">@neofacesoftware</a> <br><br>NEC face recognition technology, and how it&#39;s WATCHING YOU... to build safer cities??<a href="https://t.co/qOJBXhUaf2">https://t.co/qOJBXhUaf2</a> <a href="https://t.co/CCWPhiAKHo">pic.twitter.com/CCWPhiAKHo</a></p>&mdash; Bì乙ｴ (@Icon99558924) <a href="https://twitter.com/Icon99558924/status/912369803398193153?ref_src=twsrc%5Etfw">September 25, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">1. This thread shows the problems w defining racism and white supremacy by &quot;intent of actor&quot; rather than &quot;impact on target.&quot; <a href="https://t.co/XCONmhzFoA">https://t.co/XCONmhzFoA</a></p>&mdash; John Pfaff (@JohnFPfaff) <a href="https://twitter.com/JohnFPfaff/status/912666770112204802?ref_src=twsrc%5Etfw">September 26, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">interesting that apple specifically uses retargeting as the irritating example of tracking to be blocked: <a href="https://t.co/m63BTyoSKv">pic.twitter.com/m63BTyoSKv</a></p>&mdash; Ghastly Nick Seaver (@npseaver) <a href="https://twitter.com/npseaver/status/912679643068628992?ref_src=twsrc%5Etfw">September 26, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Publishers seek removal of millions of papers from ResearchGate: <a href="https://t.co/NpUsz5YKpt">https://t.co/NpUsz5YKpt</a></p>&mdash; Janneke Adema (@Openreflections) <a href="https://twitter.com/Openreflections/status/916217710220070913?ref_src=twsrc%5Etfw">October 6, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>