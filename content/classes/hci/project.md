---
title: Design Project
layout: "subpage"
icon: "fa-shapes"
weight: 200

menu:
    main:
        parent: "Teaching"
        identifier: "hci-202001-design"

---

_**Note: Directions for each milestone are subject to change until the date that the milestone is assigned and discussed in class.**_

The design project is meant to give you hands-on experience with a variety of techniques for HCI design. By the end of the quarter you will have engaged in substantive collaborative work, and should have material of appropriate quality for including in a portfolio.

In **weeks 2-6**, you will work with a team to conduct  research on a shared topic, and identify a problem that you will address through a design project.

In **weeks 7-10**, you will develop a response to your problem through storyboarding, prototyping, and preliminary user evaluation.

During the scheduled **finals** time slot, you will present your work to the rest of the class.

_A Note About Learning Goals:_ Because this will be the first time that most of you have ever engaged in a major HCI research and design project, I do not expect you to complete it all _perfectly._ I do, however, expect you to put in a good faith effort and learn from both your successes and failures. Learning through project-based work is an incremental process that requires your continued reflection. Similarly, good design does not appear out of thin air, but is, instead, the result of a careful process. As such, each milestone includes a written reflection requirement in which you will consider your experience with that stage of the design process, challenges you faced, and things you learned.  Because I am much more interested in your _learning_ (and not your ability to create perfect things on the first try), these reflections are a significant portion of your grade.

_This project is a group project._ All team members will receive the same grade on each milestone unless there is a major work discrepancy, in which case _I reserve the right to lower the grades of those who do not fully contribute to the workload_. I know group work can be more challenging than individual work, however, learning to work on teams -- navigating the good and the bad -- is a vital skill for your future. You will _never_ work alone in professional settings, and you need team projects to talk about in interviews. While many activities will be easier to complete in face-to-face meetings, you might also use this project as an opportunity to develop your remote working skills. Again, it will be very common in professional settings to have remote team members. As of this year, PSU finally has a Zoom license. You might use this project as an opportunity to familiarize yourself with this professional video-conferencing and screen sharing tool.


| Milestone | Due Date | Points |
|:---|:---|:---|:--|
| [D0: Team Contract](#d0-team-contract) | Sun, Jan 19, 11:59p | 5 |
| [D1P: Research Presentation](#d1p-research-presentation) | In class, Week 6, Meeting 1 (Feb 10 or 11)  | 5 |
| [D1: Research Report + Problem Statement](#d1-research-report--problem-statement) | Sun, Feb 16, 11:59p| 10 |
| [D2: Design Proposal](#d2-design-proposal) | Sun, Mar 1, 11:59p | 10 |
| [D3: Design Case Study](#d3-design-case-study) | Sun, Mar 15, 11:59p | 10 |
| [D3P: Final Presentation](#d3p-final-presentation) | In Class, Final Exam Period | 5 |

## D0: Team Contract

This is your first group milestone, and should be a very short turn in. It is mostly about team logistics, and making sure you are all set up for a successful quarter. Should be 5 very easy points.

### D0: Logistics

**Assigned & Discussed in Class:** Week 2, Meeting 2

**Due:** End of Week 2 / Sunday, January 19, 11:59p

**Turn In:** A _**single**_, _**well-organized**_ `.docx` or `.pdf` file to the appropriate folder on Canvas.

The memo should include:

- A team name.
- All team member names.

- A _brief_ (2-5 sentence) summary of your design challenge. You may use the [IDEO project framing template](https://drive.google.com/open?id=19mIcIXQWi2po8vULP6KRCijHGD2PLZvl), but it is not required. See <http://www.designkit.org/resources/1> for a full copy of the book this is from.

- A Team Contract:
    - A list of all team member names and email addresses.
    - A communication plan: how will you communicate with each-other? Will you make a multi-person private chat on Twist? Will you use email? Will you create a group SMS or Facebook message? I don't care what it is, but I want to know all team members are on the _same_ page.
    - A work plan:
        - Have you discussed each other's work and school schedules? If not, do that now and then answer this question with a Yes.
            - Does anyone on your team have a major time commitment this quarter that may make it hard for them to contribute to the project during some period of time (e.g., a week long work trip, a best friend's wedding, a really important midterm in a really hard class)? How will you work around these commitments?
        - When/how will you work outside of class? Will you have a regular meeting or schedule meetings as needed?
        - What tools will you use to collaborate: e.g., will you use Google Docs to draft text, and provide feedback to each other there? If not, what is your alternative plan?
    - An outline of individual responsibilities and project timeline: There are a lot of moving parts between now and your first major milestone, due in week 6. Make a specific timeline with clear responsibilities and internal deadlines ahead of the class submission deadline. Who will take the lead on each of these tasks? Who will take the lead on motivating and coordinating the group, making sure everyone else has done their part?
    - An outline of individual contributions so far. What has each person done to contribute to this first milestone? (_You will write a brief summary of individual contributions with each milestone. Please know that I read these, and will adjust grades accordingly._)
- Request for Feedback:
    - What kind of feedback do you want from me right now? What questions do you have?

### D0: Grading

This is worth 5 points towards your final grade. All group members will receive the same grade.

- **0:** Not turned in or contains plagiarized content.
- **3.5:** Turned in something that partially fulfills requirements.
- **4.5:** Memo addresses all items in the list above, at least superficially.
- **5:** Memo is not only complete but also thoughtful. It could be used as an example of excellent work in a future class.


## D1P: Research Presentation

This is an informal 4-6 minute presentation in which you will share your research-in-progress with the class and get feedback on your problem statement.

### D1P: Logistics

**Assigned & Discussed in Class:** Week 4

**Due:** Noon, day of presentation: Week 6, Meeting 1

**Turn In:** (1) Upload a `.ppt` or `.pptx` or `.pdf` file to Canvas by noon. (2) Give your presentation in class.

Your slides MUST be in PDF or PowerPoint format. No, you absolutely may NOT present from Google Slides or Prezi or any other online service.

Everyone will present off of the computer in the room. It does not support dual displays.

### D1P: Specification

- Aim for 5 minutes. Anything in the 4-6 minute range is okay.
- **Everyone** in your team must meaningfully participate.
- Your presentation should include, at minimum:
    1. Who are you? Introduce yourselves.
    2. What is your general topic area? Why is it interesting or important?
    3. What have you learned from your research so far?
    4. What is the problem that you have identified?

_Your presentation should not include a proposal for a design or technology solution of any kind._

### D1P: Grading

This assignment is worth 5 points. You must attend class and participate in your team's presentation to receive any points for this assignment.


|.95|.85|.75|.65|
|:--|:--|:--|:--|
|**Mechanics** <br/> Presentation lasted 4-6 minutes. Slides were turned in by noon. All team members participate. | Slides not turned in by noon. | Over time by > 2 minutes. | Not even 4 minutes -or- all team members do not participate in a meaningful way. |
|**Preparation** <br/> You manage your own time. You seem prepared. Perhaps you have notes. You do not ramble. | . | Presentation ends up fine, but you do not seem well-prepared or rehearsed. | . |
|**Delivery & Organization** <br/> Strong focus & structure. Clear speaking. Engaging delivery. You face the audience and make good eye contact. Increases audience _interest_.| Well-organized, clear delivery, somewhat interesting. | Audience can follow the big picture story but some details are confusing.  | Uninteresting or unclear delivery. Audience struggled to follow your point. You do not face the audience and/or do not make good eye contact.|
|**Content** <br/> Increases audience _knowledge_. Hits all five requirements (who are you, topic, research, problem, _no solution_)| Overall a fine presentation, but misses one of the key requirements. | Content doesn't match the audience. We are unable to follow your main point because you talk above our heads or don't provide sufficient context. | Fails to fulfill multiple requirements. |
|**Quality** <br/> Graphics and/or charts are used liberally and to good effect. Text is presented in small easily digestible chunks. Slides are useful as visual aids for the audience and do not distract from your speaking. | There are at least three _meaningful_ graphics, and no more than one or two lengthy bulleted lists. | There are many lengthy bulleted lists. | There are whole paragraphs of text. Graphics are distracting / extraneous / non-existent. (NB: If you never, in the course of speaking, refer to your graphics, they are probably extraneous.) |

- **+0.25** point is reserved for exceptionally high quality work, such as especially well-designed slides, a particularly engaging presentation, or especially thought-provoking content.
- If you do not give your presentation, but you do turn in a complete slide deck, you will receive 0 points.


## D1: Research Report + Problem Statement

In this milestone you will identify something (a practice or a technology) that is broken or could be improved. You will talk to other people about how it is broken/challenging/frustrating/difficult. It will be important to focus your research on what people are doing / want to do. Don’t ask your research question directly!

You will then use your research findings to define and scope an actionable problem statement. You will create analysis & communication artifacts to identify WHO your target users are and WHAT they want/need to do. You will define success CRITERIA, but you will _not_ develop a solution yet.

### D1: Logistics

**Assigned & Discussed in Class:** Week 2, Meeting 2

**Due:** End of week 6 / Sun, Feb 16, 11:59p

**Turn in Directions:** You should upload to Canvas:

- A single `.zip` file containing:
    - 4 audio files or other proof of completing the interviews
    - A _**single**_, _**well-organized**_ `.docx` or `.pdf` file containing _**all**_ other required contents.
    - _If your interview files are too large to upload to Canvas, you may alternately share them with me via Google Drive or another service of your choice. However, you must be careful to respect participant privacy by limiting access to the link only to me (e.g., share it with my email address via Google Drive, but do not open the link up to the public)._

### D1: Directions

1. **Background:** Pick one activity (a) conduct at least 2 20-minute observations or (b) conduct some background research that includes reviewing at least 2 research publications related to your project
2. **Interviewing:** Conduct at least 4 interviews. You must have completed all of these interviews no later than **the start of class in Week 5 Meeting 2**
3. **Diagramming:** Create three diagrams or other data representations to summarize your major research findings. _We will start making diagrams/maps/representations in class in Week 5, Meeting 2._
4. **Problem Statement:** Refine your initial design challenge into a short 1-2 paragraph problem statement. _We will discuss problem statements in class in Week 4, but you should finalize this at the end of week 6._

#### Part 1: Choice A: Observations

If you choose this option for part 1, you should conduct two periods of observation in a public location. Each period should be at least 20 minutes long. You should vary either the time or location of your 2 observation periods meaningfully.

Because this is a very short class assignment, and you are a novice, you should not conduct any observations in a location where people would have a reasonable expectation of privacy. **Exception:** _**With explicit permission**_, you may observe someone else in this class or a friend if you want to study something that does not occur in a public location -- e.g., if you want to observe how someone follows a recipe to cook something in their kitchen. You must get my okay in advance for what you plan to observe.

- For example, you might observe for 20 minutes at the food trucks at lunch time, and come back and conduct observations again at the same location for 20 minutes at 5pm.
- Or, you might observe people at the bus stop near city hall from 4:45-5:15 on one day, and observe people at Urban Plaza on PSU campus from 12:00-12:20 on another day.

You may take field notes in a medium of your choice (e.g., on your phone, laptop, or in a small notebook).

- Field notes should total **at least 4 pages** in length. I should see things like time stamps, written notes, small sketches or maps of the location. You can see examples of my own field notes in the lecture slides from the first course meeting.

#### Part 1: Choice B: Research

If you choose this option for pat 1, you should conduct a mini lit review to help you better understand your problem space. This review should include at least 2 academic research publications in the area of your project. It may additionally include some market research in which you look at existing products in your area of interest.

The goal of this part of the project is to help you better understand the current situation with regards to your topic area. For some of your pojects, this might be far more valuable than conducting raw observations.

The research publications you look at do **not** need to be specifically related to technology. They _might_ be, but you might also think about who might be a domain expert relevant to your project. For example, in the kids toothbrush design video, we could go look ourselves at how children use toothbrushes to realize they need bigger handles than adults ... or we could talk to any Kindergarten teacher or parent of a young child to better understand how other kinds of things are designed for kids (e.g., jumbo crayons!).

#### Part 2: Interviews

- You must conduct 4 interviews.
- At least 3 interviews must be at least 15 minutes long. (You can have one failure! But after that, you need to redo them if they aren't lasting at least 15 minutes.)
- Prepare an interview guide with at least 10 questions on it before the first interviews (We will work on this in class in Week 3).
    - Revise the interview guide between interviews
- Record the interview. You MUST SECURE EXPLICIT PERMISSION from the interviewee before starting the recording!
- No more than 1 interviewee may be in this class (across both sections).
- At least 1 interviewee must be someone who is NOT a computer science major.


#### Part 3: Diagramming

You should create three diagrams for this project. At least one diagram must be focused on the _who_ of your project and one must focus on the _what_ of your project. We will discuss options in class in Week 5, and you will have time in class to complete at least one and maybe two diagrams. Here is a list of resources for reference:

- Experience Mapping from Adaptive Path ~~[[Website]](http://mappingexperiences.com/)~~ and [[Guide]](http://ellieharmon.com/wp-content/uploads/Adaptive_Paths_Guide_to_Experience_Mapping.pdf)
- IDEO HCD Toolkit: [Journey Maps, Relational Maps, 2x2s](https://drive.google.com/open?id=11qd21HlISl2m57mkTW1YitRtT_NmIdEF)
- Universal Methods of Design: [Cognitive Mapping](http://ellieharmon.com/wp-content/uploads/UMD-Cognitive-Mapping.pdf)
- Universal Methods of Design: [Concept Mapping](http://ellieharmon.com/wp-content/uploads/UMD-Concept-Mapping.pdf)
- Universal Methods of Design: [Thematic Networks](http://ellieharmon.com/wp-content/uploads/UMD-Thematic-Networks.pdf)
- Universal Methods of Design: [Stakeholder Maps](http://ellieharmon.com/wp-content/uploads/UMD-Stakeholder-Maps.pdf)
- EMPATHY MAPS in the d.school [Understand Mixtape](https://drive.google.com/open?id=1lMoG9lVgkGAyJrSyu2N4cs3bHeWwrb98)
- Personas [LUMA INSTITUTE](https://drive.google.com/file/d/1IgjGxy3iFfysnCKJMfpTj2h0aVcpXB6V/view?usp=sharing) + [bolt|peters slide deck](https://speakerdeck.com/boltpeters/avoiding-bullshit-personas) + [bolt|peters video](https://vimeo.com/37941863) + [UX Mag](https://uxmag.com/topics/personas)

#### Part 4: Problem Statement

After exploring your data, refine your topic area into a clear problem statement that explains the problem that you will tackle in your project. We will discuss problem statements in week 4 and 6. Please refer to the readings in week 4, in particular, for guidance on _how_ to create good problem statements.

### D1: Document Specification:

You should turn in a single document which includes, in an organized fashion:

1. **Cover Page:**
    - Project Title
    - All team member names & PSU Emails
    - **Contribution Summary:** What has each person done to contribute to this first major team milestone?
    - **Timeline & Next Steps:** What is your plan for completing the next milestone, due in two weeks?
    - **Feedback Request:** What specific feedback would you like on this milestone? What questions do you have about moving forward with your project?

2. **Research Summary (3-5 paragraphs):**
    - 3-5 key findings from across your observation, background research, and interview data.
        - Back up each finding with specific interview quotes and/or narrative from your observations.
        - Integrate photos or digital versions of the three diagrams/maps/representations you made to analyze your data and/or communicate your findings. Caption them appropriately.

3. **Problem Statement (1-2 paragraphs)**
    - This should be 1-2 paragraphs long, and meet the criteria discussed in weeks 4 & 6:
        - Grounded in data, clearly connected to research summary
        - Framed in a way that _invites creative solutions_
        - Specific about where/when/why/how a problem is occurring
        - Includes criteria for judging the validity of a proposed response/solution
        - Specifies whether you are embarking on a traditional user-centered design project or a critical design project.

4. **Reflection (3-5 paragraphs)**: As a team, reflect on your experience conducting and analyzing user research. This reflection should include answers to questions such as:
    - Research
        - _If observations:_ What did you do differently in this round of observations (compared to your week one sketching assignment)? Do you feel your observation skills have improved? Why did you choose to do observations instead of background research? What kinds of things might you have learned from background research that you may have missed out on by choosing to conduct your own observations?
        - _If background research:_ Why did you choose to do this instead of observations? What did you learn from this that you could not have learned from your own observations? What kinds of things might you have learned from observations that you may have missed out on by choosing to reveiw prior work?
        - How and why were each of your interviews different?
        - What kinds of things did you learn from your different forms of research (interviews, observations, and/or background research)?
    - Analysis
        - What diagrams have you included?
        - Why did you chose each diagram?
        - Who participated in each diagramming activity?
        - Were these activities useful? Why or why not?
    - Overall Reflection
        - What was the easiest part of this milestone? Why?
        - What was the most challenging part of completing this milestone? Why?
        - Do you feel that you were successful in conducting research about your topic area? Why or why not? What would have made you more successful?
        - What would you do differently next time?
        - **For full credit, this reflection _must_ include references to course readings on relevant topics such as interviewing, observing, research ethics, values & design, problem formulation, etc.**

5. **Appendix**: containing well-organized documentation of all research activities:
    - **Observation Summaries** _If you conducted observations_, for each of 2 observation periods:
        - Day, time, location
        - How and why did you choose this location/day/time?
        - Copies of all fieldnotes
    - **Background Research Summaries** _If you chose to do background research_
        - Full citation for at least 2 academic research publications
        - 1-2 paragraph summary of key points of each article.
        - High-level summary of any additional market research with citations.
    - **Interview Summaries:** For **each** interview note:
        - The date, time, and setting of the interview
        - The length of the interview
        - A general background / brief bio of the interviewee.
            - Is the interviewee in this class?
            - What is your relationship to this person (e.g., friend, family, roommate, classmate, stranger?)
            - If a student, what is their major?
            - Why did you choose to interview this person? What about them makes them a good candidate for an interview about your project?
            - **Do NOT include any personally identifiable information (e.g. no names)**.
        - In lieu of a full transcript, please provide a 1-2 paragraph summary (or 1/2 - 1 page bulleted list) of the contents of the interview.
        - Link to an audio file of the interview, or upload the audio file as part of a .zip file containing the report as well. Please be careful if you submit a link to your file that you are respecting participant privacy by limiting access to the link only to me (e.g., share it with my email address via Google Drive, but do not open the link up to the public).
        - A photo or scan of the interview guide you used.
            - The TA must be able to tell from this photo/scan that you took notes as you went through the interview.
            - The TA should also be able to tell from this document that you revised your questions at least once.

### D1: Grading

- **+2 points: Mechanics** Evidence of completing the basic requirements
    - Compiled into a single document, with required cover page.
    - 4 15-minute interviews.
    - 2 20-minute observations // or background research including at least 2 academic articles
    - 3 diagrams.
- **+2 points: Research presentation**
    - Clear identification of 3-5 key findings.
    - Findings are backed up with concrete evidence (e.g., quotes, anecdotes, citations) from your research.
    - Diagrams are well-integrated in the findings narrative.
    - Diagrams are sufficiently detailed and help to communicate some aspect of your research process and/or findings.
- **+3 points: Problem Statement** - Problem statement is complete, and _meets criteria discussed in Week 4_:
    - Grounded in data, clearly connected to research summary
    - Framed in a way that _invites creative solutions_
    - Specific about where/when/why/how a problem is occurring
    - Includes criteria for judging the validity of a proposed response/solution
    - Specifies whether you are embarking on a traditional user-centered design project or a critical design project.
    - **Does not propose a solution**
- **+1.5 points: Reflection**
    - Meets length requirement (at least 3 paragraphs)
    - Thoughtful reflection shows clear evidence of engagement with course readings and/or other materials.
    - Reflection documents your own learning and progress in the course, including honest reflection on challenges & shortcomings.
- **+1.5 points: Quality**
    1. Taken as a whole, your report leverages your research data and tells a _compelling story_ about your problem statement.
    2. Taken as a whole, your report shows evidence of good design, presentation, and communication skills. For example, the writing appear to have been proof-read. The diagrams look professional and show evidence of creativity. The visual layout is professional in appearance.  crop and rotate images appropriately? Did you caption images and diagrams? Did you integrate images and text together in compelling ways? If I received this brief as a client, would I feel confident that I should continue to pay you money to do design work for me?
    3. Wow-factor: Goes above and beyond in some way. Exemplary work.




## D2: Design Proposal

For this milestone, you will (1) brainstorm possible design responses to your problem statement, (2) explore three possible responses in more detail through storyboarding, and (3) choose _one_ design to move forward with.


### D2: Logistics

**Assigned and Discussed in Class:** Week 7, Meeting 1

**Due:** End of Week 8 / Sun, March 1, 11:59p

**Turn In Directions:** You should upload a _**single**_, **_well-organized_** `.docx` or `.pdf` file to Canvas, containing all required components. **Just one file per group**



### D2: Directions

#### Part 1: Brainstorming

We will have a brainstorming session in class in Week 7 to get you started on this milestone.

You should aim to complete all of your brainstorming work **before** the second class meeting in Week 7.

1. **Identify Key User Needs**: You may already have these from your problem statement, but if not, spend 10 minutes brainstorming the key criteria for a solution. What are the important requirements that any design must meet in order to successfully respond to your problem statement? What is it that your users need/desire to be different? You might think of this in terms of the 'how might we' questions from last time. From the d.school mixtape:
    > What do you (and your team) know about this challenge that no one else has considered? Not in terms of your long-held expertise, but instead unique user-centered insights. What’s your specific vision (not solution) about how you want change people’s lives? Another way to think about this is: what assumption are you going to disrupt on your way to a successful solution?

    - Once you have a good sized list, work with your team to narrow it down to 3-5 critical needs/requirements. You will need to use these later to compare your various design alternatives.

2. **Brainstorming session 1**: Design Alternatives I
    - Start in class, dedicate at least 20 minutes to this, with your **entire** team.
    - Thinking about your identified problem area, and _using your how-might-we questions as prompts,_ **identify at least 25 potential design ideas, innovations, or interventions** with your team mates. These should be concrete _things_. These can be far-fetched ideas -- in fact, wild ideas are encouraged at this stage of the process! Multiple iterations on the same general idea are acceptable.

3. **Brainstorming session 2**: Design Alternatives II
    - With _at least 2 people not on your team_
    - Facilitate a brainstorming session with people who are not your group members. (OK to partner up with another group!) Identify at least **10 additional design alternatives**.

_**After the two brainstorming sessions**: Decide on 3 design ideas that you will storyboard as a way of exploring them in more depth. You should choose one radical design that departs significantly from the presesnt/status quo, one design that is more of an incremental or small change from the present, and one other design idea of your choice.

#### Part 2: Storyboarding

_We will discuss storyboards in class on the second meeting of Week 7. I recommend waiting until then to start this portion of the milestone._

I am personally a big fan of simple paper sketches. However, you may prefer to use some online tool like, [StoryboardThat](http://www.storyboardthat.com)

For **each** of your three proposed design alternatives, you should either:

1. Create **two** storyboards of 3-5 frames each. Each storyboard should depict a key use case for that design intervention. Your 2 storyboards may either:
    - Depict two different usage contexts or two distinct features of your proposed design.
    - Or illustrate a contrast by depicting the situation _without_ your new technology in one storyboard, and the the situation _after_ your designed intervention in the second storyboard.
2. Or create **one** storyboard **and one** concept poster. See [LUMA Institute Storyboards + Concept Poster](https://drive.google.com/file/d/1NrGoQnNx9hHn8hMZx9k8cd3xIHqzOxJK/view?usp=sharing) and [Danish Design Centre: Concept Poster](https://danskdesigncenter.dk/en/concept-poster)

After creating the storyboards (and concept posters as applicable), you should decide, as a group, what design you are going to move forward with. In making this decision, you should consider how well each intervention is likely to intervene in the situation you identified in D1, and how well it responds to the user needs/requirements you selected at the start of your brainstorming sessions.

#### Part 3: Choose one!

Choose one design alternative to move forward with.

### D2: Document Specification

Turn in a single, well-organized file, with all of the following sections:

1. **Cover Page:**
    - Project Title
    - All team member names & PSU Emails
    - **Contribution Summary:** What has each person done to contribute to this team milestone?
    - **Feedback Request:** What specific feedback would you like on this project? What questions do you have about moving forward with your project?
    - **Timeline & Planning Ahead:** What is your plan for the next and final milestone?

2. **Problem Statement:** If this is the same as last time, just copy and paste it in. If you want to revise it from your D1 turn in, you may!
    - Same criteria as last time.
    - Make sure your problem statement includes a clearly specified list of key user needs / criteria for evaluating your design ideas.

3. **Brainstorming Documentation:** A photo of the results of your two brainstorming sessions // or // a copied in list of your alternatives if you did this digitally. Include information about who on your team facilitated each session and who participated in brainstorming ideas.

4. **Design Alternatives:**
    - For _each_ of your three design alternatives:
        - Give a brief description or overview of the intervention (2-3 sentences).
        - Include an image of your 2 storyboards (or 1 storyboard + 1 concept poster) for this intervention. You should include a short textual caption with each storyboard that describes the use case depicted in that storyboard (1-3 sentences).
        - Explain how well this intervention addresses the problem or situation you identified in D1 and how well it meets the user needs/requirements (2-5 sentences).

5. **Design Choice:** Write a 1-3 paragraph explanation about which design solution you will move forward with (or how you will combine your different interventions together). This paragraph should clearly explain:
    - what the design intervention is
    - how it addresses your design problem/situation
    - and *why* you chose this particular intervention over the others. Reference your user research, problem statement, and key user needs.

6. **Reflection:** A 2-5 paragraph reflection on this milestone. You must, _at minimum_, respond to these questions:
    - How did the the brainstorming session with your team differ from the one with outsiders?
        - Which session produced the ideas that you chose for your project?
        - Which method was more valuable, or did they each produce different results?
        - What would you do differently the next time you need to brainstorm design ideas?
    - How did you decide what three design ideas to storyboard?
    - What did you learn about your project from creating the storyboards? Do you have different ideas now about your users? Did you use your personas to think about how to create the storyboards?
    - How did you decide which one solution to move forward with after your storyboarding? Was it challenging to choose which design alternative you wanted to move forward with?
    - What did you learn from this milestone? What would you do differently in the future?
    - **For full credit, this reflection _must_ include references to course readings on relevant topics such as brainstorming, storyboarding, values & design, critical design, human factors, etc.**


### D2: Grading

- **+1.5 points: Mechanics** Evidence of completing the basic requirements
    - Compiled into a single document, with required cover page.
    - Problem Statement
    - Brainstorming Documentation
- **+3 points: Design Alternatives**
    - For each of 3 alternatives:
        - Brief description of the design
        - 2 x storyboards // or // 1 storyboard + 1 concept poster
        - Evaluation related to problem statement + user needs
- **+2.4 points: Design Choice**
    - Clear articulation of which design intervention you will proceed with
    - Explanation of how (or to what extent) this design intervention meets the criteria set out in the problem statement.
    - Compelling explanation of _why_ you chose this intervention to move forward with
- **+1.5 points: Reflection**
    - Meets length requirement (at least 2 paragraphs)
    - Thoughtful reflection shows clear evidence of engagement with course readings and/or other materials.
    - Reflection documents your own learning and progress in the course, including honest reflection on challenges & shortcomings.
- **+1.6 points: Quality**: Much of design work is about communication. Presentation quality and style matters!
    1. Taken as a whole, your report tells a _compelling story_ about your planned design intervention.
    2. Taken as a whole, your report shows evidence of good design, presentation, and communication skills. Your text sections are proof-read. Your writing is clear and concise. You use things like headers and page breaks to separate content. You caption any images and diagrams.
    3. Your storyboards look neat and professional. They do a good job of communicating your intervention-in-context.
    4. Wow-factor: Goes above and beyond in some way. Exemplary work.



## D3: Design Case Study

As part of this milestone, you will create a paper or other low-fidelity prototype of your chosen design solution, conduct some simple preliminary evaluations, and write a retrospective case study that reviews your project to date, explains the rationale behind design decisions, and looks ahead to what you might do next if you had more time.

### D3: Logistics

**Assigned & Discussed in Class:** Week 9, Meeting 1.

**Due:** Sun, Mar 15, 11:59p

**Turn In Directions:** You should upload a _**single**_, **_well-organized_** `.docx` or `.pdf` to Canvas, containing all required components.

- _Please reduce image sizes when you generate your final file._

**Grading:** This project is worth up to 10 points. All team members will receive the same grade unless there is a major work discrepancy, in which case I may lower the grades of those who do not contribute to the assignment.



#### D3: Prototyping Tools

I recommend paper. Here is an example of using [paper prototyping for website development](http://alistapart.com/article/paperprototyping). Here is a [paper prototyping video by Google UX engineers](https://youtu.be/JMjozqJS44M?t=74).

For mobile apps, you might consider using a tool like [POP App](https://marvelapp.com/pop/) to make your paper prototypes come alive.

You can do something similar with presentation software like [PowerPoint](http://boxesandarrows.com/interactive-prototypes-with-powerpoint/), also see this article on [PowerPoint](http://keynotopia.com/guides-ppt/).

You may also use specialized tools like [InVision](https://invisionapp.com) or [Sketch](https://www.sketchapp.com/), but this is not required. Some people like to use diagramming tools like Visio or OmniGraffle. All of these options will take you longer. You have been warned.



### D3: Directions

1. Create a prototype! See prototyping tools section above.
    - Your prototype must be detailed enough to allow you to examine at least one of the use cases described in your storyboards turned in for the last milestone. If you have a concern about this, please meet with me about an appropriate scope for your prototype.
2. Evaluate your prototype:
    1. Conduct one self-reflective evaluation, based on a list of 8-12 relevant guidelines selected from heuristics lists discussed in class and/or your list of relevant human factors criteria created in week 8.
        - Develop a standard template for recording your evaluation that includes:
            - A clear list of the guidelines, and citations for where each comes from at the top.
            - A description of the use case you are evaluating at the top.
            - For each guideline, a definition of the guideline in 2 sentences or less, and a clear assessment of how your design upholds or violates that guideline.
            - Use the form on page 3 of [Sambasivan et al.'s article](https://doi-org.proxy.lib.pdx.edu/10.1145/3058496) as an example for how to build out your own template.
    2. **And** conduct one **Think-Aloud** evaluation with a prospective user of your design:
        - Review the relevant sections in [this chapter](https://drive.google.com/open?id=1e23Pt4SyuuE2tNzrUa1iHg01P7-xuDzi) from Bill Buxton's sketching book for a detailed explanation. You will give them a task or two to do and then you will observe how they carry it out.
        - Develop a simple template for recording your Think-Aloud process and results that includes:
            - A brief description of the task you asked the prospective user to complete at the top
            - A recording (simple list form) of what they person did or tried to do
            - A list of outcomes: both positive and negative aspects of your design identified through this process.
3. Develop a list of next steps: _discuss_ with your team what you would do next _if you had more time_. You do not actually need to _do_ anything after the evaluation.

### D3: Document Specification

Turn in A SINGLE well-organized file containing:

1. **Intro Memo**:
    - Team names
    - Summary of individual contributions
    - Requests for feedback
2. **Breif recap of problem and proposed solution**:
    - Summarize your problem & proposed solution clearly, but concisely.
    - Include images of prototype and/or storyboards as useful to convey your design.
3. **Evaluation Summary**:
    - Identify the features or use cases you tested.
    - Synthesize across your various evaluations to create a list and brief description of the key results of your evaluation: What areas of your design worked well? What problems did you identify?
4. **Next Steps:** Propose a set of _next steps_ as if you had more time on this project.
    - What would you change in response do feedback?
    - What would you not change?
    - Would you want to conduct more evaluations before iterating the design or are you ready to make a revision first?
5. **Reflection** (3-6 paragraphs): Focus on what you learned, what challenges you faced, and how the design process worked for you in this milestone. You should answer questions like:
    - What went well in this milestone? What challenges did you face? What would have made you more successful?
    - What prototyping methods/tools did you choose, and _why_ you made that decision. In retrospect, was it a good decision? Why?
    - How did the different evaluation methods compare to eachother in terms of what you learned from them / what _kind_ of problems they surfaced? Which evaluation was the most useful for you (or were they all equally helpful)? Why? This should be a reflection on the experience of conducting the evaluation, not a reflection on the contents of what you found in your evaluation.
    - What would you do differently next time regarding **both** prototyping and evaluation?
    - Did you learn anything from this exercise? If so, what? If not, why not?
    - **For full credit, this reflection _must_ include references to course readings/materials on relevant topics such as brainstorming, storyboarding, values & design, critical design, human factors, etc.**
6. **Appendix**:
    - One evaluation report for each evaluation in the templates you developed.
    - Prototype Description with Pictures/Images/Scans to fully document your prototype.

### D3: Grading

The grade for each listed criteria will have a _completeness_ component and a _quality_ component.

- 2 -- **Format and Presentation:** Well-organized document with a high quality of presentation style, good grammar, clear writing. Document includes required memo up front. All materials are compiled into a single document.
- 1 -- **Problem + Solution Recap** Clear and concise recap of problem space that motivates your proposed solution.
- 2 -- **Evaluation Summary:** Clear and concise summary of evaluation results highlighting key takeaways for the reader.
- 1 -- **Next Steps:** Degree to which the report is compelling in motivating a set of proposed next steps.
- 2 -- **Reflection:** Includes references to course materials, compelling explanation of _why_ prototyping decisions were made, is thoughtful and shows evidence of _reflection on personal learning_.
- 2 -- **Appendix:**
    - Complete templates that capture the results of each evaluation.
    - Complete description and documentation of prototype (with images/scans/etc as appropriate).



## D3P: Final Presentation

**Since we are no longer meeting in person, please review updated instructions posted as a Canvas announcement for details on how to create and upload a video recording of your presentation.**

~~During the Finals time slot, each team will give a short (5-10 minute) presentation on their design project.~~

This assignment is worth 5 points.

### D3P: Logistics

**Assigned & Discussed in Class:** Week 9

**Due:**

- **MW Section**: Presentation videos should be uploaded to Canvas by March 18, 1:30pm.
    - ~~Presentations will be on Wednesday, March 18. Slides due by 10:15am.~~
- **TR Section**: Presentation videos should be uploaded to Canvas by March 16, 11:15am.
    - ~~Presentations will be on Monday, March 16. Slides due by 8:15am.~~

**Turn In Directions:**

Post a video presentation to Canvas in the designated discussion forum. See canvas announcement for details.

_Note 3/15: You do not need to turn in your slides in advance since I do not need to load them on the class computer. Also, you may use any slide software you want (including Google Slides, Prezi, etc.) since, again, I do not need to be able to load them on the classroom computer :)_

~~_Upload a `.ppt`, `.pptx`, or `.pdf` file with your presentation in it to Canvas._~~

~~Your slides MUST be in PDF or PowerPoint format. No, you absolutely may NOT present from Google Slides or Prezi or any other online service.~~

~~Everyone will present off of the computer in the room. It does not support dual displays.~~


### D3P: Specification / Requirements

You should prepare your presentation as a human-centered design case study.

Do not feel compelled to focus on every single thing you did, instead _tell us a story_ about your _process_ that helps us understand how you arrived at your final _result_, and has a clear takeaway for the audience (e.g., our design is good, our design needs work, we realized we solved the wrong problem, etc.). Your takeaway does not have to be "positive" about your work to be high quality or deserving of a good grade. You do not need to cover _everything_ you did or all aspects of your design. Pick one important point you can realistically make in 5 minutes.

You might review [Designing Case Studies: Showcasing a Human-Centered Design Process](https://www.smashingmagazine.com/2015/02/designing-case-studies-human-centered-design-process/) for guidance & ideas.

**All team members should participate in the presentation.**

### D3P: Grading

[Presentation Rubric - D3P](https://drive.google.com/file/d/1HSF9Wza0G7hAEumCyS7SSGBReXgMLFzN/view?usp=sharing)
