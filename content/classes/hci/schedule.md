---
title: Schedule
page-class: schedule
layout: "subpage"
icon: "fa-calendar-alt"
weight: 20

menu:
    main:
        parent: "Teaching"
        identifier: "hci-202001-schedule"
---

| Week | Meeting 1 | Meeting 2 | Sunday, 11:59 |
|:--|:--|:--|:--|
| [1](#week-1) | Observing | Project Intro [RR1](../readings/)  | [Intro Survey](https://portlandstate.qualtrics.com/jfe/form/SV_3kH2FR8nxeay7WJ)|
| [2](#week-2) | What is UX?  | [[S1](../sketching/)] Ethics [RR2](../readings/)  | [D0](../project/#d0-team-contract) |
| [3](#week-3) | | [[S2](../sketching/)] Interviewing <br/> **_Meeting on T+W only_**  |  |
| [4](#week-4) |  Values [RR3](../readings/)  | [[S3](../sketching/)] Problems [RR4](../readings/)| |
| [5](#week-5) |  Critical Design [RR5](../readings/) | [[S4](../sketching/)] Data |  |
| [6](#week-6) | [Presentation I](../project/#d1p-research-presentation)  | [[S5](../sketching/)] Inclusion [RR6](../readings/) | [D1](../project/#d1-research-report--problem-statement) |
| [7](#week-7) | Brainstorming | [[S6](../sketching/)] Storyboarding |  |
| [8](#week-8) |  Human Factors [RR7](../readings/) |  [[S7](../sketching/)] Prototyping & Evaluation| [D2](../project/#d2-design-proposal) |
| [9](#week-9) |  HCI History [RR8](../readings/) | [[S8](../sketching/)] HCI Present [RR9](../readings/) | |
| [10](#week-10) |  Choose your own adventure I [RR10](../readings/)  | Choose your own adventure II [RR10](../readings/) | [D3](../project/#d3-design-case-study) |
| [F](#finals-week) | Final: [Presentation II](../project/#d3p-final-presentation) | Friday: [Final Reflection](../individual-reflections/#ir-individual-reflection) | |

**Finals Week:** The final meeting is [scheduled by the registrar and is considered an integral part of the course](https://www.pdx.edu/registration/final-exams). Make plans now to attend at the proper time for your section.

### Can I attend the other class meeting sometimes?

No! Not only are we limited by the seats in the room, we will often be doing project work in class, and so you need to be there at the same time as your group :)

### Deadlines

Reading responses & sketchbook entries are due promptly at the start of class. Most other deadlines will be on **Sundays at 11:59p**.


### Week 1

#### Meeting 1: Observing

... and Sketching!

- In Class:
    - [Course syllabus](../syllabus/) + [[One-page reference]](https://drive.google.com/file/d/1J53BLNWlswolcjVpI_YKZuYJdUCfK_1L/view?usp=sharing)
    - Video: [IDEO Toothbrush Design](https://www.youtube.com/watch?v=tvkivmyKgEA)
    - Observations & Sketching [[In-class handout]](https://drive.google.com/file/d/1Iz_-b9fVyhtG-1Lr7x21LC1GMsArg2jz/view?usp=sharing)
    - [Slides](https://drive.google.com/file/d/1IqlPHXxTEPF6BvndJr3Yvtbra6_nlXqN/view?usp=sharing)
- Resources for Sketching Journal:
    - Rohde, Mike. 2011. "Sketching: The Visual Thinking Power Tool." *A List Apart* (blog). January 25, 2011. <http://alistapart.com/article/sketching-the-visual-thinking-power-tool/>.
    - Buxton, Bill. 2007. "Anatomy of Sketching." In *Sketching User Experiences: Getting the Design Right and the Right Design*. Morgan Kaufmann. [PDF (Google Drive)](https://drive.google.com/open?id=1h33bo6bqfEpBWmezVYynKDk2IC8ByQ7e)
- Resources for Observations:
    - Jon Kolko. "Contextual Inquiry" in _Wicked Problems_. Austin Center for Design. Available online: <https://www.wickedproblems.com/4_methods_for_research.php>
    - Helsinki Design Lab / Sitra. n.d. *Ethnography Fieldguide*. <http://www.helsinkidesignlab.org/files/731/Field%20Guide%20English.pdf>.


#### Meeting 2: Project Intro

... sketching details, and how to read academic papers!

- Read Before Class [[RR1]](https://portlandstate.qualtrics.com/jfe/form/SV_b2cTIT7tV7k7i73):
    - Dombrowski, Lynn, Adriana Alvarado Garcia, and Jessica Despard. 2017. "Low-Wage Precarious Workers' Sociotechnical Practices Working Towards Addressing Wage Theft." In *Proceedings of the 2017 CHI Conference on Human Factors in Computing Systems*, 4585--4598. CHI '17. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/3025453.3025633>.
- In Class:
    - Video: [Suchman Copier](https://www.youtube.com/watch?v=DUwXN01ARYg)
    - Q&A, Course Goals & Outcomes
        - What makes a good sketch?
    - Reading Strategies for Academic Papers
    - Discussion: Wage Theft + HCI
    - What makes a good project topic?
    - [Slides](https://drive.google.com/file/d/1J7L6FskxqZ-2Y7o7ylbc9L6wKP_Jb6-m/view?usp=sharing)
  
#### Survey: Sun, Jan 12

- Due:
    - [Intro Survey](https://portlandstate.qualtrics.com/jfe/form/SV_3kH2FR8nxeay7WJ)

### Week 2

#### Meeting 1: What is UX?

What are we doing in this class anyway? What is UX Design & Research?

- Read Before Class (Choose any _two_):
    - UX Booth Editorial Team. 2018. "Complete Beginner's Guide to Interaction Design." *UX Booth* (blog). August 15, 2018. <https://www.uxbooth.com/articles/complete-beginners-guide-to-interaction-design/>.
    - Babich, Nick. 2017. “UX Process: What It Is, What It Looks Like and Why It’s Important.” _Adobe Blog_ (blog). August 4, 2017. <https://theblog.adobe.com/ux-process-what-it-is-what-it-looks-like-and-why-its-important/>
    - Dam, Rikke Friis, and Yu Siang Teo. 2019. “5 Stages in the Design Thinking Process.” _The Interaction Design Foundation_ (blog). Accessed December 31, 2019. <https://www.interaction-design.org/literature/article/5-stages-in-the-design-thinking-process>
    - Minhas, Saadia. 2018. “User Experience Design Process.” _UX Planet (Medium)_ (blog). April 3, 2018. <https://uxplanet.org/user-experience-design-process-d91df1a45916>
    - Capital One Design. "Service Design Tools." _ONE Design (Medium)_. January 25, 2018. <https://medium.com/capitalonedesign/service-design-tools-methods-6e7f62fcf881>
- In Class ([Agenda/Slides](https://drive.google.com/file/d/1JK1eMqO-PSNkCjEpztHxeXx3btHmMgqb/view?usp=sharing)):
    - ~~Video: [What is UX](https://www.youtube.com/watch?v=Um3BhY0oS2c)~~
    - Intros / Topic pitches
    - The UX / IXD Process: [Group Activity](https://drive.google.com/file/d/1JPnEzGrPLjnrMOl-F7eo43H5j33miKrf/view?usp=sharing)
    - Form project teams


#### Meeting 2: UX Research Ethics

- Read Before Class [[RR2]](https://portlandstate.qualtrics.com/jfe/form/SV_3QQPdycjRbFMa1f):
    - Suchman, Lucy. 1995. "Making Work Visible." *Communications of the ACM* 38 (9): 56--64. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/223248.223263>.
    - Mortensen, Ditte. 2018. "Conducting Ethical User Research." *The Interaction Design Foundation* (blog). July 24, 2018. <https://www.interaction-design.org/literature/article/conducting-ethical-user-research>.
- In Class ([Slides](https://drive.google.com/file/d/1JS3n9ODgWhGIBK5R0Bf3a16HqZ8gRbM7/view?usp=sharing)):
    - [Sketch 1](../sketching/)
    - Discuss Readings
    - Project Teams: What are the ethical considerations for your project work?


#### D0: Sun, Jan 19

- **Due:**
    - [D0: Team Contract](../project/#d0-team-contract)


### Week 3

Class will meet only once this week, on either Tuesday or Wednesday depending on your section. The university is closed for MLK Day on Monday.

#### Meeting 1: Interviewing

- Read Before Class:
    - Bernard, H. Russel. 2006. Excerpts from "Interviewing: Unstructured and Semistructured" in _Research Methods in Anthropology_ (pp. 210-224, _required_).  [PDF (Google Drive)](https://drive.google.com/file/d/1IRZS9jM542XsYy14hE29LwnYmGVqJwWY/view?usp=sharing)
    - Spradley, James P. 1979. Selected excerpts from *The Ethnographic Interview*: 'Asking Descriptive Questions' (pp. 78-91, _required_); 'Interviewing an Informant'  (pp. 55-67, _recommended_).  Holt, Rinehart and Winston. [PDF (Google Drive)](https://drive.google.com/file/d/1INwKT1h4YDlpK92NiFVJ5i9MMehnTSEa/view?usp=sharing)
    - Vido: Tate, Emily. “Cognitive Biases & The Questions You Shouldn’t Be Asking by Cindy Alvarez.” _Mind the Product (blog)_, August 3, 2018. <https://www.mindtheproduct.com/cognitive-biases-the-questions-you-shouldnt-be-asking-by-cindy-alvarez/>
- In Class:
    - [Slides](https://drive.google.com/file/d/1Jmbtni7c0ohZJfjfGV82n3goIrtJhKvF/view?usp=sharing)
    - [Sketch 2](../sketching/)
    - Develop Interview Questions


### Week 4

#### Meeting 1: Values

- Read Before Class [[RR3](<https://portlandstate.qualtrics.com/jfe/form/SV_af4qLWgp4Pvdlg9>)]:
    - Sengers, Phoebe. 2011. "What I Learned on Change Islands." *Interactions* 18 (2). <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1925820.1925830>
    - Cowan, Ruth Schwartz. Selections from “The Postwar Years.” In _More Work for Mother: The Ironies of Household Technology from the Open Hearth to the Microwave_, pp. 192-193, 195-201, 208–216. New York: Basic Books, 1983. [PDF (Google Drive)](https://drive.google.com/file/d/1IPnCrsJHktalSoLtpq9cV6jBMUW4RCkl/view?usp=sharing)
- In Class:
    - The power of design?
    - Value Sensitive Design
    - What values are important for your project?
    - [Values + Design Handout](https://drive.google.com/file/d/1KDKxT71sXiTaNns6Q7fX_k65HMU0OW_d/view?usp=sharing)

#### Meeting 2: Problems

- Read Before Class [[RR4](https://portlandstate.qualtrics.com/jfe/form/SV_2fRj80levpPZGT3)]:
    - Baumer, Eric P S, and M Six Silberman. 2011. "When the Implication Is Not to Design (Technology)." In *Proc. CHI 2011*, 2271--2274. CHI '11. Vancouver, BC, Canada: ACM. <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1978942.1979275>.
    - Sullivan, Paul. 2014. “Stop Jumping to Solutions and Think About the Problem.” ThoughtWorks (blog). October 27, 2014. <https://www.thoughtworks.com/insights/blog/you-need-understand-problem>.
- In Class:
    - [Sketch 3](../sketching/)
    - [Problems + Solutions Handout](https://drive.google.com/file/d/1KDKxT71sXiTaNns6Q7fX_k65HMU0OW_d/view?usp=sharing)
- Project References:
    - Optional readings to _help you_ with your project.
    - Lee, Geunbae “GB.” 2017. “Designer’s Indispensable Skill: The Ability to Write and Present a Solid Problem Statement.” UX Planet (Medium) (blog). June 27, 2017. <https://uxplanet.org/designers-indispensable-skill-the-ability-to-write-and-present-a-solid-problem-statement-56a8b4b8060>
    - Gygi, Craig, Bruce Williams, Neil DeCarlo, and Stephen Covey. n.d. "How to Write a Problem Statement for Six Sigma." *Dummies (blog)*. Accessed January 5, 2019. <https://www.dummies.com/careers/project-management/six-sigma/how-to-write-a-problem-statement-for-six-sigma/>.


### Week 5



#### Meeting 1: Critical Design

When design isn't about fixing things

- Read Before Class (choose 1) [[RR5](../readings/)]:
    - Gaver, Bill, and Heather Martin. “Alternatives: Exploring Information Appliances Through Conceptual Design Proposals.” In _Proceedings of the SIGCHI Conference on Human Factors in Computing Systems_, 209–216. CHI ’00. New York, NY, USA: ACM, 2000. <https://doi.org/10.1145/332040.332433>
    - Steup, Rosemary, Lynn Dombrowski, and Norman Makoto Su. 2019. “Feeding the World with Data: Visions of Data-Driven Farming.” In _Proceedings of the 2019 on Designing Interactive Systems Conference_, 1503–15. DIS ’19. New York, NY, USA: ACM. <https://doi.org/10.1145/3322276.3322382>.
- In Class:
    - Video - [MIT: AttentivU](https://youtu.be/znFWCvTPuuk)
    - Video - [Harvard/BrainCo: Focus EDU](https://youtube.com/watch?v=fQ3eW3qQ2pk)
    - Video - [Corning: A Day Made of Glass](https://youtube.com/watch?v=6Cf7IL_eZ38)
    - Video - [Near Future Laboratory: Curious Rituals](https://vimeo.com/92328805)
    - [Stories, Values, & Alternative Futures Handout](https://drive.google.com/file/d/1KM1XFtp-URDgBoAoDB1uMGH9359Y1uxo/view?usp=sharing)


#### Meeting 2: Making Sense of Data

- Read Before Class:
    - Edeker, Kyra, and Jan Moorman. 2013. "Love, Hate, and Empathy: Why We Still Need Personas." *UX Magazine*, February. <https://uxmag.com/articles/love-hate-and-empathy-why-we-still-need-personas>.
    - IDEO HCD Toolkit Excerpts: [Download Ideas, etc.](https://drive.google.com/open?id=1DLIMEBGehOOZWpH0CeQouUAkAhpD_AdN)
- In Class:
    - [Sketch 4](../sketching/)
    - [Handout - Data Analysis](https://drive.google.com/file/d/1KUaxgLlE2klNMkLpBUzrQVbDbAgT9tZT/view?usp=sharing)
    - Your data: who and what?
    - Video: [Affinity Diagrams](https://www.youtube.com/watch?v=7CXkqNa6X0c)
- Resources:
    - Video: [bolt|peters, Personas](https://vimeo.com/37941863)
    - Project page has a list of [Diagrams](https://web.cecs.pdx.edu/~harmon8/classes/hci/project/#part-3-diagramming)


### Week 6

#### Meeting 1: Presentations

[D1P: Research Presentation](../project/#d1p-research-presentation)

#### Meeting 2: Inclusion

- Read Before Class [[RR6](../readings/)]
    - Choice 1:
        - Schulman, Sarah. 2016. "Redefining the Politics of Inclusion with George and Dustin." *The Journal of Design Strategies* 8 (1): 50--55. <https://issuu.com/journalofdesignstrategies/docs/the_journal_of_design_strategies_vo>. [Alternate PDF (GDrive)](https://drive.google.com/open?id=1I_wIhqt-6o36zVHpIaeD8PF8MbsbHmEC)
        - Martin, Courtney. 2016. "The Reductive Seduction of Other People's Problems." *BRIGHT Magazine (Medium)*, January. <https://brightthemag.com/the-reductive-seduction-of-other-people-s-problems-3c07b307732d>.
        - Abreu, Amelia. 2018. "Why I Won't 'Try on' Disability to Build Empathy in the Design Process (and You Should Think Twice)" *Prototypr (Medium)*, May 1, 2018. <https://blog.prototypr.io/why-i-wont-try-on-disability-to-build-empathy-in-the-design-process-and-you-should-think-twice-7086ed6202aa>.
    - Choice 2:
        - Hamraie, Aimi. 2018. “Mapping Access: Digital Humanities, Disability Justice, and Sociospatial Practice.” _American Quarterly_ 70 (3): 455–82. <https://doi.org/10.1353/aq.2018.0031>.
- In Class:
    - [Sketch 5](../sketching)



#### D1: Sun, Feb 16

- Due:
    - [D1: Research Report + Problem Statement](../project/#d1-research-report--problem-statement)


### Week 7

#### Meeting 1: Brainstorming

- Read Before Class:
    - Shedd, Catriona. 2013. "Tips for Structuring Better Brainstorming Sessions." *InspireUX (Online Blog)*, July. <http://www.inspireux.com/2013/07/18/tips-for-structuring-better-brainstorming-sessions/>.
    - [IDEO Brainstorming Tips](https://drive.google.com/open?id=1BCYlxkv-MJUxWc2hqrsJUk5G5LePTa1S)
- In Class:
    - Inclusion Discussion Recap [MW Slides](https://docs.google.com/presentation/d/1-mYzf8OrMVFEH9f7BCYxxq_OF2y0t-SHZgH-3bwdUWI/edit?usp=sharing) | [TR Slides](https://docs.google.com/presentation/d/1EjqzO3BYugaD24_EqirtaJ0bBAbxNRkPeTXPzVRLDOc/edit?usp=sharing)
    - Brainstorming!
    - Assign groups for Human Factors readings (W8 M1)


#### Meeting 2: Storyboarding

- Read Before Class:
    - Little, Ambrose. 2013. "Storyboarding in the Software Design Process." UX Magazine (blog). February 8, 2013. <https://uxmag.com/articles/storyboarding-in-the-software-design-process>.
    - Schauer, Brandon. 2007. "Sketchboards: Discover Better + Faster UX Solutions." Adaptive Path (blog). December 14, 2007. <https://web.archive.org/web/20181213044552/http://adaptivepath.org/ideas/sketchboards-discover-better-faster-ux-solutions/>.
- In Class:
    - [Sketch 6](../sketching/)
    - Project team work: Storyboarding
    - Handout [LUMA Institute Storyboards + Concept Poster](https://drive.google.com/file/d/1NrGoQnNx9hHn8hMZx9k8cd3xIHqzOxJK/view?usp=sharing)
    - [Slides](https://drive.google.com/file/d/1O0eEK5vm-K7tFoNoUKyI0cxxe1t7N7U2/view?usp=sharing)

### Week 8

#### Meeting 1: Human Factors & Foundations

In this class we will look at some things we already know about people! And discuss how this knowledge can be applied to design. It is not always necessary to reinvent the wheel with new research ;)

**We signed up for readings in Week 7, meeting 1. The list is on Canvas. You do NOT need to read ALL of these things, only the one topic area that you signed up for (or I assigned you to if you were not present).**

- Read Before Class [[RR7](../readings/)]:
    1. **Fitt's Law**:
        - Karafillis, Anastasios. 2012. "When You Shouldn't Use Fitts Law To Measure User Experience." *Smashing Magazine (Online Magazine)*, December. <https://www.smashingmagazine.com/2012/12/fittss-law-and-user-experience/>.
    2. **Affordances**:
        - Gaver, William W. 1991. "Technology Affordances." In *Proceedings of the SIGCHI Conference on Human Factors in Computing Systems*, 79--84. CHI '91. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/108844.108856>.
    3. **Mental Models** (Read both):
        - Jakob Nielsen. 2010. "Mental Models" _NN/g Nielsen Norman Group_, <https://www.nngroup.com/articles/mental-models/>
        - Willet Kempton & Jean Lave. 1983. Book Review of _Mental Models_ by Dedre Gentner & Albert L Stevens. _American Anthropologist_, vol. 85, no. 4, pp. 85-88. <https://www.jstor.org/stable/679637>
    4. **Distributed Cognition**:
        - Edwin Hutchins. 1995. "How a Cockpit Remembers its Speeds." _Cognitive Science_, 19, pp. 265-288. [Author PDF](http://pages.ucsd.edu/~ehutchins/documents/CockpitSpeeds.pdf)
    5. **Direct Manipulation**:
        - Edwin L. Hutchins, James D. Hollan & Donald A. Norman (1985) Direct Manipulation Interfaces, Human–Computer Interaction, 1:4, 311-338, [PSU Library Link](https://search.library.pdx.edu/primo-explore/openurl?sid=google&auinit=EL&aulast=Hutchins&atitle=Direct%20manipulation%20interfaces&id=doi:10.1207%2Fs15327051hci0104_2&title=Human%20computer%20interaction&volume=1&issue=4&date=1985&spage=311&issn=0737-0024&vid=PSU&institution=PSU&url_ctx_val=&url_ctx_fmt=null&isSerivcesPage=true) (If that doesn't work, just look it up at PSU library, you can access it for free through EBSCO host).
    6. **Intermediation**:
        - Nithya Sambasivan, Ed Cutrell, Kentaro Toyama, and Bonnie Nardi. 2010. Intermediated technology use in developing communities. In Proceedings of the SIGCHI Conference on Human Factors in Computing Systems (CHI ’10). Association for Computing Machinery, New York, NY, USA, 2583–2592. <https://doi.org/10.1145/1753326.1753718>
    7. **Accessibility** (Read Both):
        - Microsoft. n.d. "Inclusive: A Microsoft Design Toolkit." Accessed December 28, 2017. <https://www.microsoft.com/design/inclusive/>. Read the **Manual**, and review the **Activities** booklet.
        - UK Home Office Posters: <https://github.com/UKHomeOffice/posters/blob/master/accessibility/dos-donts/posters_en-UK/accessibility-posters-set.pdf>
- In Class:
    - Human Factors Jigsaw Groups
    - Project team work: develop a list of 8-12 relevant criteria for your project

#### Meeting 2: Prototyping & Evaluation

- Project References
    - Optional readings to _help you_ with your project.
    - Prototyping References:
        - Cao, Jerry. 2016. "Paper Prototyping: The 10-Minute Practical Guide." *Studio by UXPin* (blog). January 27, 2016. <https://www.uxpin.com/studio/blog/paper-prototyping-the-practical-beginners-guide/>.
        - Moggridge, Bill. 2007. "Prototypes." In *Designing Interactions*, 1st ed. MIT Press. [\[PDF\]](https://drive.google.com/file/d/1BRVfqvUsJKakP0IFnZ9iZ9_OSgjDwBgF/view?usp=sharing)
    - Think Aloud:
        - Buxton, Bill. 2007. "Interacting with Paper." In *Sketching User Experiences: Getting the Design Right and the Right Design*. Morgan Kaufmann. [\[PDF\]](https://drive.google.com/file/d/1BPNKwTtvQUX21I_NNB5PRJHNE-YiZ14-/view?usp=sharing)
        - <https://www.nngroup.com/articles/thinking-aloud-the-1-usability-tool/>
    - Heuristics:
        - [Jakob Neilsen's famous list](http://www.nngroup.com/articles/ten-usability-heuristics/)
        - Nithya Sambasivan, Nibha Jain, Garen Checkley, Asif Baki, and Taylor Herr. A framework for technology design for emerging markets. _Interactions 24_, 3 (April 2017), 70-73. <https://doi-org.proxy.lib.pdx.edu/10.1145/3058496>
        - Apple iOS "Design Themes" & "Design Principles" <https://developer.apple.com/design/human-interface-guidelines/ios/overview/themes/>
        - Dieter Ram's 10 Commandments for Good Design: <https://www.interaction-design.org/literature/article/dieter-rams-10-timeless-commandments-for-good-design>
    - ~~Cognitive Walkthrough:~~
        - ~~Wharton, Cathleen, et al. 1994. "The Cognitive Walkthrough Method: A Practitioners' Guide." Chapter 5 in J. Nielsen and R. Mack (eds.), _Usability Inspection Methods_. Wiley. [\[PDF\]](https://drive.google.com/open?id=1Mk9txWxZB92AEghQNqZjX0v4JHFbyCba)~~
        - ~~Spencer, Rick. 2000. "The Streamlined Cognitive Walkthrough Method, Working Around Social Constraints Encountered in a Software Development Company" _CHI 2000_. [\[PDF\]](https://drive.google.com/file/d/1L8Aio1Ijve1tAccVVbWK_A8bPqM_x95L/view?usp=sharing)~~
- In Class:
    - [Sketch 7](../sketching/)
    - Video: [Prototyping - Planes](http://www.youtube.com/watch?v=sp7yv67B5Sc)
    - Video: [The Mother of All Demos](https://www.youtube.com/watch?v=B6rKUf9DWRI)
    - Video: [Paper prototyping](https://youtu.be/JMjozqJS44M?t=74)
    - Project team work: Prototyping plan
    - Sign up for topic/day for Week 10


#### D2: Sun, March 1

- Due:
    - [D2: Design Proposal](../project/#d2-design-proposal)

### Week 9

#### Meeting 1: HCI History

- Read Before Class [[RR8](../readings/)]:
    - Moggridge, Bill. 2007. "Interviews with Stu Card and Tim Mott." In *Designing Interactions*, 1st ed. MIT Press. [PDF (GDrive)](https://drive.google.com/open?id=1xmvq9cxq3kwwTA8Iv2v-udZ3_CLmGTK1)
- In Class:
    - _Possible_ presentation from folks who attended Safiya Noble talk
    - HCI History Discussion [Slides](https://drive.google.com/file/d/1OZGaMCGrgpuUCDNOwfz9KFoSQi8MWJsY/view?usp=sharing)
    - Project team work time


#### Meeting 2: HCI Present

- Read Before Class [[RR9](../readings/)]:
    - Taylor, Alex. “After Interaction.” _Interactions_ 22, no. 5 (August 2015): 48–53. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2809888>.
    - Khovanskaya, Vera, Lynn Dombrowski, Ellie Harmon, Matthias Korn, Ann Light, Michael Stewart, and Amy Voida. “Designing Against the Status Quo.” _Interactions_ 25, no. 2 (February 2018): 64–67. <https://doi.org/10.1145/3178560>.
    - Dourish, Paul. “User Experience As Legitimacy Trap.” _Interactions_ 26, no. 6 (October 2019): 46–49. <https://doi.org/10.1145/3358908>.
- In Class:
    - [Sketch 8](../sketching/) (final sketch)
    - HCI Present Discussion
    - Project team work time

### Week 10

#### Meeting 1: Choose Your Own Adventure 1

- In week 9 you will sign up to read and share somethign with the rest of the class on just one day in week 10. This activity will count for _2_ reading response grades. [[RR10](../readings/)]
    - Ideas if you are stuck:
        - Seaver, Nick. 2018. "Captivating Algorithms: Recommender Systems as Traps." *Journal of Material Culture*, December. <https://doi-org.proxy.lib.pdx.edu/10.1177/1359183518820366>.
        - Irani, Lilly C., and M. Six Silberman. 2016. "Stories We Tell About Labor: Turkopticon and the Trouble with 'Design.'" In *Proceedings of the 2016 CHI Conference on Human Factors in Computing Systems*, 4573--4586. CHI '16. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2858036.2858592>.
        - Srinivasan, Janaki, Megan Finn, and Morgan Ames. 2017. “Information Determinism: The Consequences of the Faith in Information.” _The Information Society_ 33 (1): 13–22. <https://www-tandfonline-com.proxy.lib.pdx.edu/doi/full/10.1080/01972243.2016.1248613>.
        - Harrington, Christina, Sheena Erete, and Anne Marie Piper. 2019. “Deconstructing Community-Based Collaborative Design: Towards More Equitable Participatory Design Engagements.” _Proc. ACM Hum.-Comput. Interact. 3 (CSCW)_: 216:1–216:25. <https://doi-org.proxy.lib.pdx.edu/10.1145/3359318>.
        - Keyes, Os, Josephine Hoy, and Margaret Drouhard. 2019. “Human-Computer Insurrection: Notes on an Anarchist HCI.” In _Proceedings of the 2019 CHI Conference on Human Factors in Computing Systems_, 339:1–339:13. CHI ’19. New York, NY, USA: ACM. <https://doi-org.proxy.lib.pdx.edu/10.1145/3290605.3300569>.
        - Noopur Raval and Paul Dourish. 2016. Standing Out from the Crowd: Emotional Labor, Body Labor, and Temporal Labor in Ridesharing. In Proceedings of the 19th ACM Conference on Computer-Supported Cooperative Work & Social Computing (CSCW ’16). Association for Computing Machinery, New York, NY, USA, 97–107. DOI: <https://doi.org/10.1145/2818048.2820026>
        - Mariam Asad. 2019. Prefigurative Design as a Method for Research Justice. Proc. ACM Hum.-Comput. Interact. 3, CSCW, Article 200 (November 2019), 18 pages. <https://doi.org/10.1145/3359302>
        - Mariam Asad and Christopher A. Le Dantec. 2015. Illegitimate Civic Participation: Supporting Community Activists on the Ground. In Proceedings of the 18th ACM Conference on Computer Supported Cooperative Work & Social Computing (CSCW ’15). Association for Computing Machinery, New York, NY, USA, 1694–1703. DOI: <https://doi.org/10.1145/2675133.2675156>
        - Vasiliki Tsaknaki, Marisa Cohn, Laurens Boer, Ylva Fernaeus, and Anna Vallgårda. 2016. Things Fall Apart: Unpacking the Temporalities of Impermanence for HCI. In Proceedings of the 9th Nordic Conference on Human-Computer Interaction (NordiCHI ’16). Association for Computing Machinery, New York, NY, USA, Article 141, 1–3. DOI: <https://doi.org/10.1145/2971485.2987680>
        - Lucian Leahu, Marisa Cohn, and Wendy March. 2013. How categories come to matter. In Proceedings of the SIGCHI Conference on Human Factors in Computing Systems (CHI ’13). Association for Computing Machinery, New York, NY, USA, 3331–3334. DOI: <https://doi.org/10.1145/2470654.2466455>
        - Sarah Fox. 2018. Design, Maintenance, and the Menstruating Body. In Proceedings of the 2018 ACM Conference Companion Publication on Designing Interactive Systems (DIS ’18 Companion). Association for Computing Machinery, New York, NY, USA, 375–378. <https://doi.org/10.1145/3197391.3205386>
        - Stephanie B. Steinhardt and Steven J. Jackson. 2015. Anticipation Work: Cultivating Vision in Collective Practice. In Proceedings of the 18th ACM Conference on Computer Supported Cooperative Work & Social Computing (CSCW ’15). Association for Computing Machinery, New York, NY, USA, 443–453. <https://doi.org/10.1145/2675133.2675298>
        - Vera Khovanskaya and Phoebe Sengers. 2019. Data Rhetoric and Uneasy Alliances: Data Advocacy in US Labor History. In Proceedings of the 2019 on Designing Interactive Systems Conference (DIS ’19). Association for Computing Machinery, New York, NY, USA, 1391–1403. <https://doi.org/10.1145/3322276.3323691>
        - Richmond Y Wong, Vera Khovanskaya, Sarah E Fox, Nick Merrill, and Phoebe Sengers. 2020. Infrastructural Speculations: Tactics for Designing and Interrogating Lifeworlds. UC Berkeley. In _Proceedings of the 2020 CHI Conference on Human Factors in Computer Systems_ <http://dx.doi.org/10.1145/3313831.3376515> Author pre-print available at: <https://escholarship.org/uc/item/4bm4g0fn>

- In Class:
    - Jigsaw groups to share readings.
    - Project team work time



  
#### Meeting 2: Choose Your Own Adventure 2

- See notes on meeting 1. You will select a topic in week 8.

- In Class:
    - Jigsaw groups to share readings.
    - Closing Discussion, Course Evaluation

#### D3: Sun, Mar 15

- Due:
    - [D3: Design Case Study](../project/#d3-design-case-study)


### Finals Week

#### Final Exam Period

There is no final exam in this course. Instead, the final meeting will be used for final presentations. The time of this meeting is [scheduled by the registrar and is considered an integral part of the course](https://www.pdx.edu/registration/final-exams). You must plan to attend at the scheduled time.

- MW Class:
    - Weds, March 18, 12:30 - 2:20
- TR Class:
    - Mon, March 16, 10:15 - 12:05
- In Class:
    - [D3P: Final Presentation](../project/#d3p-final-presentation)

#### IR: Fri, March 20

- Due:
    - [Individual Reflection](../individual-reflections/#ir-individual-reflection)
