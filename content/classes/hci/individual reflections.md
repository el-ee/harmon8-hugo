---
title: Individual Reflections
layout: "subpage"
icon: "fa-comment-dots"
weight: 200

menu:
    main:
        parent: "Teaching"
        identifier: "hci-202001-individual"

---

As you all look ahead to future careers outside of the classroom, your ability to direct your own learning will become increasingly important. The working world -- especially related to computing -- is always changing, and your skills at self-directed learning -- and self-assessment -- will help you to be more successful over time. Moreover, research shows that students who set goals and reflect on their progress are much more successful learners.[^ambrose]

As part of our class meetings, I will collect brief daily reflections on your goals and progress towards them thoughout the quarter, both as a way of helping you to develop your metacognitive skills and helping me to know where you're at.

You will also complete two graded assignments at the start and end of the term in which you reflect in more depth on (1) what goals you have for this quarter and (2) how well you were able to achieve those goals.

[^ambrose]: Ambrose, Susan A. 2010. _How Learning Works Seven Research-Based Principles for Smart Teaching_. 1st ed. Jossey-Bass Higher and Adult Education Series. San Francisco: Jossey-Bass.

### Start of Term Survey/Intro

In the first week of the term, you should complete a brief survey to help me understand your background and goals for the class.

#### Logistics (STS)

**Assigned & Discussed in Class:** Week 1

**Due:**  Sunday, January 12, 11:59pm

**Turn In Directions:** Fill it out at this link: <https://portlandstate.qualtrics.com/jfe/form/SV_3kH2FR8nxeay7WJ>


### IR: Individual Reflection

**This milestone should be completed individually. It is worth 15 points.**

In this assignment, you will each reflect, individually, on your learning in the course, the experience of keeping a weekly sketching journal, and going through one cycle of an HCI design process.

If you are a graduate student, you must also reflect on the connection between HCI and your own specialization area. Please make sure you read the separate section explaining these additional requirements.

#### Logistics (IR)

**Assigned & Discussed in Class:** Week 10

**Due:**  Friday, March 20, 11:59pm

**Turn In Directions:** Upload a `.docx` or `.pdf` file to Canvas. **No other formats are acceptable.**

- **If you upload a PDF:** you must note the word count at the top of the first page. Do NOT include the title, your name, the word count line, etc. in this count, ONLY include the main body of your reflection. **Do not lie about how long your reflection is. Lying about word count is a form of academic dishonesty and is grounds for a 0 on the assignment.**


#### Specification (IR)

**Length**: 1500-3000 words

The objective of this culminating essay is to thoughtfully reflect on your experience in the course. You should address all of the following questions, and in so doing, meaningfully reference **at least 3 readings** from the course.

1. **Personal Learning**
    - When you first signed up for this class, what goals did you have for this class? What did you hope to learn or get out of it?
    - Did your goals change at all over the quarter, or did new goals emerge? If so, how and why did these goals change?
    - How successful were you at achieving your goals? Why were you successful to this degree? What would have made you more successful?
    - What are the **three** most important things you’ve learned this quarter? Why are these things important to you?
    - What grade would you give yourself for this class, and how would you justify that grade? Are you satisfied with your performance and work in the class? Why or why not?
2. **Sketching**
    - Review your sketches, and spend some time thinking about how your relationship with sketching has changed (or not) over the duration of the quarter. At minimum, this portion of the reflection must include:
        - Integrated and captioned images of your **favorite** and **least favorite** sketches:
            - An explanation of why you chose each one for inclusion
            - A summary of the feedback you received on each one
        - A response that includes all of the following question areas:
            - _General Experience:_ Did you ever do any sketching before taking this class? How did you initially feel about sketching, and, how has that feeling changed? Were there any surprising moments? Will you ever using sketching again in the future? Why or why not?
            - _Learning:_ What have you learned through or about sketching? When is sketching most useful (a) for you, personally and (b) within a design process?
            - _Critique:_ What kinds of feedback did you get in class? What kind of feedback was helpful and what was not helpful? What have you learned about feedback and critique from the in-class sessions?
            - _Improvement:_ Are there things that could improve the sketching component of this class?
3. **HCI Design**
    - The next time you are working on a software project, will you use a human-centered process like we used in the class project? Why, how, to what extent, and/or why not?
    - Think about the different images of an HCI/UX/IXD process that we’ve seen this quarter. What would your ideal HCI design process look like?
        - **Draw a picture or diagram**.
        - Explain the picture/diagram and give examples of the kinds of methods or activities that should happen at each stage.
        - Explain _why_ this is your ideal process.

[^1]: The sketching portion of the reflection is modeled very closely on Jed Brubaker's <a href="http://www.jedbrubaker.com/teaching/inf132-sp2013/sketching-project/">sketching project</a> for Informatics 132, as taught in the Spring of 2013 at the University of California, Irvine.

##### Graduate Students Only

**In addition** to the requirements above, you should also include a fourth section in your document that contains a 750-word reflection on **how Human-Computer Interaction matters for your own area of interest**. Because this course is taught in a department in which HCI is not a specialization area, your long term relationship to the material will depend on your ability to see the connections between HCI and other areas of computer science.

**MS Students**: You should write this reflection with regards to your [track](https://www.pdx.edu/computer-science/track-courses). If you have not yet chosen your track, you should simply pick one that you are considering, and write this reflection in terms of that area of computer science.

**PhD Students**: You should write this reflection with regards to your own current or planned area of dissertation research. If you are not yet sure about what area of research you will pursue,
you should choose an area from the [MS Track Listing](https://www.pdx.edu/computer-science/track-courses).

This **additional** reflection must:

- Be at least 750 words long. (So, **_your total reflection, should be 2250-3750 words in length_**).
- Include a clear statement of your own area of interest or work in the first sentence.
- Reference at least _1_ peer-reviewed article/book from your own area of specialization.

#### Grading (IR)


- [Graduate Rubric](https://drive.google.com/file/d/1IWep9fWVjB6Ybwup6VEtNsipmlKwYic2/view?usp=sharing)
- [Undergraduate Rubric](https://drive.google.com/file/d/1Ifj_Y76_peyhCQmlEy0xm4r419kKx6G7/view?usp=sharing)
