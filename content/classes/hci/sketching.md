---
title: Sketchbook
layout: "subpage"
icon: "fa-pencil-paintbrush"
weight: 200

menu:
    main:
        parent: "Teaching"
        identifier: "hci-202001-sketchbook"
---


Sketching is a common practice used by designers that fosters the ability to think critically about existing objects and interactions, and generate ways of improving them.

As part of this class, you will keep a sketchbook, in which you will think about objects or interactions in your daily life and sketch situations that are problematic or frustrating and ideas for how they could be improved.[^1]

### Assignment Overview

Each week, for weeks 2-9, you will need to make at least **three** entries in your sketching journal. At least one entry should be related to the week's theme. The other two can be on the theme, or on any other topic related to HCI.

"Related to HCI" is meant to be understood broadly. You might sketch ideas for new interactions, processes, things, objects, or interfaces. You might sketch observations of people using a technology. Your sketches can focus on an entire system (e.g., how to follow a recipe) or one specific input/interaction (e.g., redesigning an oven knob).

Each sketch should tell a story or help you communicate something. Sketches may focus on either the identification of problems and design opportunities _or_ the generation of design ideas. It is ok to add some annotations, but you should not write lengthy paragraphs alongside your sketches for this activity.

**The focus is on the quantity of sketches and communication of your ideas, not the quality of your drawing skills.** The best way to have a good idea is to have lots of ideas. Futuristic, off-the-wall, and original ideas are welcome and encouraged!

#### Logistics

All sketches should be entered in a single notebook.

- Each sketch should be on it's own page in your sketchbook, with the facing page BLANK.
- On the facing page, please write the date, but leave everything else blank. You will need this space to record feedback and critique in class.

You must bring your sketchbook to class every Wednesday/Thursday (whichever is the second meeting of the week for you). During this period, you will break into groups of 4-5 and exchange feedback and critique.

Within your critique group you will follow a regular pattern each week:

1. Exchange sketchbooks so that everyone has someone else's sketches.
2. Without discussing the sketches, add the following notes to the facing page of the each of the 3 weekly sketches in the book you are looking at:
    - Your name & the date
    - What does this sketch communciate to you? Can you tell if it is focused on identifying a problem/design opportunity or generating design ideas?
    - What is the _best_ thing about this sketch?
    - What is one thing that could be _improved_ about this sketch?
3. When everyone is done writing some brief feedback, go around the circle, and share _one_ of the sketches you reviewed that week that you think is particularly good and/or useful to discuss.
4. As a group, decide on one sketch your group would like to share & discuss with the rest of the class.

#### Resources

- Buxton, Bill. 2007. "Anatomy of Sketching." In *Sketching User
Experiences: Getting the Design Right and the Right Design*. Morgan
Kaufmann. [PDF (GDrive)](https://drive.google.com/open?id=1h33bo6bqfEpBWmezVYynKDk2IC8ByQ7e)
- Rohde, Mike. 2011. "Sketching: The Visual Thinking Power Tool." *A List
Apart* (blog). January 25, 2011.
<http://alistapart.com/article/sketching-the-visual-thinking-power-tool>.

#### Grading

There are two main parts to this assignment:

1. 8 weekly sketching assignments: 1.5 points each, for a total of 12 points towards your final grade.
2. Your [final reflection](../individual-reflections/#ir-individual-reflection) in the course will include a section on your sketching experience.

I will circulate during the sketching activity and periodically collect sketchbooks in batches on Wednesdays/Thursdays to check off your progress to date.

| Points | Criteria |
|---:|:---|
| +1.5 | All three sketches are complete for a given week and there is evidence of your participation in the critique activity. |
| +1 | Sketches are complete, but evidence of full critique participation is lacking. |
| 0 | All three sketches are not complete / you are absent for the check and critique. |

NOTE: If you do not have three sketches complete at the start of class, or you do not have your sketching notebook with you, or you show up too late to participate in the activity, you will receive **zero points for the week**. There will be no partial or late credit for sketches. Each one is only worth 1.5 points.


### Sketching topics

#### S1: Objects and interactions in daily life

**Due in class, 2nd meeting, Week 2**

Find a public location where you can observe people. Make some sketches to document what you observe. Some prompts to get you started:

- What do you notice people around you doing? How are they accomplishing these activities?
- What kind of technologies are people using?
- Who/what are people interacting with? How are they interacting?
- How could you make it easier? How could you make it more fun?
- How might this scene be different in ten years?
- What kind of new technologies would change this situation? How?

You may also take notes about what you are seeing, but it is not required. Make sure if you do take notes that you leave some room for your sketches to stand on their own, with a blank page by each one.

#### S2: Objects and Interactions in Daily Life II

**Due in class, 2nd meeting, Week 3**

What kind of new interactions or new things would change your daily life as a student at Portland State?

Try to capture moments when you or someone you know is frustrated and find yourself imagining a way to improve something you deal with as part of your regular day.

To help you get started, you might want to think about questions like:

- What kinds of problems do you have during the day? What frustrates you? What would make these frustrating situations better?
- Complete this sentence "wouldn't it be great if ..."
- What do you imagine campus will look like in 20 years? How might classes be different? How might people communicate differently? How might people pay for things differently? How might people travel differently?

You might create these sketches as part of a new set of observations of people trying to do something, or you can just create these based on your own imagination.


#### S3: Experiencing Your Surroundings

**Due in class, 2nd meeting, Week 4**

For this sketching topic, pay attention to how people around you are experiencing (or not experiencing) their surroundings.

Some questions to get you thinking:

Are people texting while walking? Is it sometimes a problem? What would make it better? Do you wish you could more easily send a message to someone while you’re on the move? How could you do that? Do you notice event fliers posted around campus? What would make you notice them more? OR would help you ignore them if they’re bothersome? What do you know about the places where you are walking – do you know their history, who else has been there? What things would you want to know when you’re out and about? Do you get lost riding bikes? Do you know where you’re allowed (and not allowed) to skateboard?


#### S4: Speculative Sketching

**Due in class, 2nd meeting, Week 5**

Sketch something fantastical and futuristic, something that isn't real and might not even seem possible. How would your life (or someone else's life) be different with this new technology?

Try sketching something out of the norm. For example: what would a radically inefficient technology look like, something that slows you down and gets in the way? Or, what kind of experience would result from an unreliable technology?

Try sketching something that would have a positive impact, something fun and exciting; and try sketching something else that could be scary or harmful.

In addition to using sketching to demonstrate new ideas that you think are good; think this week about how you can also use sketching to communicate how/why a [current or new] technology might be a _bad_ idea.


#### S5: Revise a Sketch

**Due in class, 2nd meeting, Week 6**

Go back through your first four weeks of sketches, and choose at least 1 sketch to revise based on the feedback you got in class. Redraw it as your on-topic sketch for this week.

For your other two sketches you can either (a) revise 2 more sketches, or (b) sketch anything else you like related to HCI.


#### S6: Your Projet

**Due in class, 2nd meeting, Week 7**

You should be starting to develop ideas for your final design project this week. Use your sketching journal to brainstorm ideas and/or think through ideas that your team has already come up with.

#### S7: Human Factors

**Due in class, 2nd meeting, Week 8**

Think of a technology you use every day, and make some sketches that can help you reflect on the human factors principles you're reading about this week. Any kind of relation to human facotrs is fine, but here are some more  prompts if you're feeling stuck:

- What kinds of human factors pain points have you experienced (tiny buttons? confusing door handles? impossible to control-while-driving car radio?)? Sketch the problem.
- What would it look like to revise some technology to follow one of the human factors principles better? Sketch the solution.
- Can you follow a human factors principle to the point of absurdity? What would it look like to take one of these principles too far?


#### S8: Technology Non-Use

**Due in class, 2nd meeting, Week 9**

For this weeks' sketching assignment you should try to focus on times or settings in which people are _not_ using digital computing technology.

- Where are these places? Why is digital technology not used?
- Are there other non-digital / non-computation technologies that people are using? What are they?
- What are those interactions like?
- Are there digital technologies that you think could or should be added to these spaces/interactions/times? What are thoes technologies? What would be the result?

-- AND/OR --

- Are there places in which you see a lot of digital technology use, but think that the experience of the place/time/interaction would be improved if technology was taken away?
- What would interactions look like with a _lessened_ technological presence?
