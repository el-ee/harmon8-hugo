---
title: "HCI"
courseNumber: "CS 410 / 510"
courseTitle: "Introduction to Human Computer Interaction (HCI)"
multisection:
    -
        name: "MW Section"
        meetings: "MW 2:00-3:50"
        location: "FAB 46"
    -
        name: "TR Section"
        meetings: "TR 2:00-3:50"
        location: "UTS 304"
layout: "class-home"
shortDescription: |

    This course will introduce you to the fundamental principles of human-computer interaction (HCI), more often referred to as “User Experience” in industry and professional contexts. You will try out a variety of research and design techniques through a team-based human-centered design project.

menu:
    main:
        parent: "Teaching"

cascade:
    tentative: false
    quarter: 202001
    type: "class"
    date: 2019-11-01
    cc: true


---

**Instructor: Dr. Ellie Harmon**

- MW Section: 2:00-3:50, FAB 46
- TR Section: 2:00-3:50, UTS 304
- **You may only attend the section for which you are registered.**

This course will introduce you to the fundamental principles of human-computer interaction (HCI), more often referred to as "User Experience" in industry and professional contexts. You will try out a variety of research and design techniques through a team-based human-centered design project. By the end of the quarter you should have material of appropriate quality for including in a portfolio or discussing in a job interview.

That said, you will not become a UX expert in a single 10-week course. You will leave this course with a better understanding of what skills you might wish to develop further on your own or through future coursework.

Given that most PSU students will not go on to be UX professionals, the larger goal of the course is to help you understand how, when, and why HCI/UX expertise can contribute to technology design and development.

**There are no pre-requisites for this class.** All students are expected to be prepared to read and write at the college level, and should be willing to learn new things!

There will be **no programming** in this class.


### This Website is a Living Document

This website is a starting point for the course. It is subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced on Canvas, so attend class and check your inbox/Canvas.


_Note: This course is still under development. The course description is slightly revised from: <https://www.pdx.edu/computer-science/cs-410510-top-introduction-to-hci>_
