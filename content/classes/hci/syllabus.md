---
title: Syllabus
layout: "subpage"
icon: "fa-info-circle"
weight: 10

menu:
    main:
        parent: "Teaching"
        identifier: "hci-202001-syllabus"

---

| | |
|--:|:-----|
| **Course** | CS 410/510 - Introduction to Human Computer Interaction |
| **Meetings**| MW Section: 2:00-3:50, FAB 46<br/>TR Section: 2:00-3:50, UTS 304<br/>**You may only attend the section for which you are registered.** |
| **Final Exam**| MW Section: Weds March 18, 12:30-2:20<br/>TR Section: Mon March 16, 10:15-12:05 |
| **Instructor** |Dr. Ellie Harmon <br/> <ellie.harmon@pdx.edu> <br/> she / her / hers |
| **Office Hours** | M+T 4:00 - 5:00<br/>[FAB 120-15](/~harmon8/faq#how-to-find-my-office)<br/>Or [by appointment](/~harmon8/faq#meetings-with-me)|
| **TA** | Belén Bustamante<br/><bbelen@pdx.edu> |
| **TA Office Hours** | M 12:00 - 2:00<br/>CS Fishbowl |
| **Prerequisites** | College level reading and writing. **There is no programming in this course.** |
| **Website** | <https://web.cecs.pdx.edu/~harmon8/classes/hci/> |


\* _Please make note of the unusual final exam time now and make plans to attend. The time is [scheduled by the registrar](https://www.pdx.edu/registration/final-exams) and I have no control over it. Although there is no final exam , we will meet during the scheduled time per [university policy](https://www.pdx.edu/registration/final-exams)._


### Hello! And Welcome.

I'm looking forward to our course this term, and I hope you are as well. All major course policies are outlined on this webpage. Please note that all materials on the course website -- including this policy overview as well as the course schedule -- are designed to be a starting point for the course. They are subject to change as the term unfolds, in response to your feedback and my assessment of how things are going. I’ll be seeking out your feedback regularly. Some adjustments are likely. These adjustments may involve altering assignments or adding, removing, or modifying readings. Any changes will be discussed in class and announced via email, so attend class and check your inbox.

Although no longer in a survey form, parts of this syllabus are adapted from the [interactive syllabus](https://interactivesyllabus.com) developed by Dr. Guy McHendry and Dr. Kathy Gonzales, both of Creighton University, and further adapted by Dr. Lindsey Passenger Wieck (St. Mary's University) and Dr. Angela C. Jenks (UC Irvine). It is licensed under Creative Commons Attribution - Non-Commercial 4.0. Further acknowledgements about the content of specific sections are noted throughout the document.

### About Dr. Harmon

I am a Senior Instructor in the [Department of Computer Science](http://cs.pdx.edu) at Portland State University. I also teach in the [Freedom, Privacy, and Technology](http://sinq-clusters.unst.pdx.edu/cluster/freedom-privacy-and-technology) cluster for [University Studies](https://www.pdx.edu/unst/unst-introduction).

I [teach](/~harmon8/classes/) courses in introductory computer science, human-computer interaction, and computing & society.

I conduct [research](/~harmon8/research/) about the promises, threats, and impacts of contemporary computing. I've studied: labor and the future of work, philanthropy and social change, microbial science, suburban family life, and possibilities for disconnection on the Pacific Crest Trail.

I work to make computer science more broadly accessible, relevant, and welcoming. I am developing [a new intro CS class](/~harmon8/classes/cs199/) at PSU; I am co-director of [CyberPDX](http://cyberpdx.org), a summer camp for broadening participation in cybersecurity; and I am the faculty advisor for the PSU [We in Computer Science (WiCS)](http://wics.cs.pdx.edu) student organization.

I use she/her pronouns and I thru-hiked the PCT in 2013.

### Course Description

This course will introduce you to the fundamental principles of human-computer interaction (HCI), more often referred to as "User Experience" in industry and professional contexts. You will try out a variety of research and design techniques through a team-based human-centered design project. By the end of the quarter you should have material of appropriate quality for including in a portfolio or discussing in a job interview.

That said, you will not become a UX expert in a single 10-week course. You will leave this course with a better understanding of what skills you might wish to develop further on your own or through future coursework.

Given that most PSU students will not go on to be UX professionals, the larger goal of the course is to help you understand how, when, and why HCI/UX expertise can contribute to technology design and development.

This is a project-based course, grounded in the belief that students learn best by _doing_. Your active participation is critical for your own success. There will be no memorization-based quizzes or exams.

#### Learning Goals

Upon the successful completion of this course students will be able to[^2]:

1. Explain why, when, and how to engage stakeholders in an iterative human-centered design process
2. Apply some basic HCI principles (e.g., affordances, constraints, Fitt's law) to improve the design of computer systems
3. Create sketches to capture, envision, and critique design ideas
4. Describe how human-centered research techniques such as interviewing and observations can be used to better understand stakeholder needs
5. Apply techniques such as storyboarding, user stories, and personas that are useful for synthesizing research and incorporating research findings into a design process
6. Create a low-fidelity prototype suitable for basic user evaluation, and perform some basic types of evaluation
7. Identify some of the current research areas and open questions in the field of HCI
8. Analyze how computer systems influence human experience, and explain the value of human-centered design practices for computer systems development

[^2]: This section contains lightly revised text from: <https://www.pdx.edu/computer-science/cs-410510-top-introduction-to-hci>

### Credits

This course is based on a course I developed as a Graduate Student Instructor at University of California, Irvine and which was itself based on the Intro to HCI course I took as part of my MS in HCI at Georgia Tech. Although it has evolved in the years since, the general structure of the course benefited greatly from the input of [Lynn Dombrowski](http://lynndombrowski.com), [Jed Brubaker](https://jedbrubaker.com), [Gillian Hayes](http://www.gillianhayes.com/), [Paul Dourish](http://www.dourish.com/), and [Lilly Irani](https://quote.ucsd.edu/lirani/).

### Course Materials

#### Textbooks

There is no textbook for this course.

#### Supplementary Readings / Videos

All readings are available on the [course schedule](../schedule/). Full bibliographic detail is provided, and you should attempt to locate them on your own in the event of a broken link. _**The failure of any schedule link is not an excuse to skip the assigned reading.**_

Please note that some links may require that you are on the campus network, or connected through a VPN in order to access the content for free. **You will _never_ have to pay for any readings in this class.** If you are having trouble accessing one of the readings, please visit my office hours, ask a question in class, or visit the library and ask them how to gain access to the material.

#### Sketchbook

You will need to aquire a book to use for the [sketching](../sketching) journal that you will keep as part of this class.

You will need a blank sketchbook for this assignment. This book should be:

- **Unlined** (I will allow dots, but not a heavy grid)
- At least 30 pages
- Dedicated only to sketching
- Sized appropriately to (a) carry around with you and (b) accomodate the notes you will need to add to sketches in class. I recommend about A5 or A6 size pages. Letter paper folded in half works great.

The PSU Bookstore sells many sizes and types of unlined books that would be appropriate. However, this does not need to be expensive. You could also
[make your own book](http://www.marcjohns.com/blog/2013/01/30/i-make-my-own-sketchbooks).

I have a long-arm stapler, and can staple paper together for books. Please drop in my office hours in week 1 if you would like to use it to create your own book.

#### Laptops & Phones & Tablets, Oh My!

This class requires your attention and participation in course discussions and activities. I understand that laptops, etc. can be useful for note taking and looking up information relevant to course discussions; and we will often conduct in-class projects that will be easier with a laptop.

However, I find that they can also be quite the distraction. We all have other people and things -- families, friends, work, classes, YouTube videos, games, etc. -- that we might feel compelled to check in on during class.

Therefore, laptops and phones will be allowed in class on a situational and tentative basis. If your digital technology becoms a problem for you or others around you, I may ask you to stop using it either for the rest of a class period, or if it becomes a recurring issue, for the rest of the term.


### Workload

One credit hour is defined by federal regulations as "One hour of classroom or direct faculty instruction and a minimum of two hours of out‐of‐class student work each week"[^credit-hour].

This is a four-hour course, which means that you should spend approximately eight (8) hours on out-of-class work each week. This includes both reading assignments in preparation for class, as well as research, writing, and/or programming assignments as applicable.

I expect reading assignments to take you roughly 1-3 hours, leaving you ~6 hours per week to work on your projects. Please reach out to me if the readings are taking you significantly longer than this. We should meet to discuss your reading strategies.

[^credit-hour]: See the Credit Hour Policy of the Northwest Commission on Colleges and Universities (this is the organization which accredits PSU) <https://www.nwccu.org/wp-content/uploads/2017/05/Credit-Hour-Policy.pdf>

### Major Assignments

This class involves both individual and group assignments. Below is a brief sketch of the assignments we will complete in this class. All due dates are noted in the [course schedule](../schedule/) and on Canvas along with links to detailed assignment directions.

| Assignment | Due | Quantity | Points Each | Total |
|:---|:---|---:|---:|---:|
| Intro Survey | Jan 12 | 1 | 4 | 4|
| Sketches | Weeks 2-9 | 8 | 1.5 | 12|
| Reading Responses | Weeks 2-10 | 8 | 3 | 24|
| D0: Team Contract | Jan 19 | 1 | 5 | 5 |
| D1P: Research Presentation | Feb 10 or 11 | 1 | 5 | 5|
| D1: Research + Problem | Feb 16 | 1 | 10 | 10|
| D2: Design Proposal | Mar 1 | 1 | 10 | 10|
| D3: Design Case Study | Mar 15 | 1 | 10 | 10|
| D3P: Final Presentation | Mar 16 or 18 | 1 | 5 | 5|
| Individual Reflection | Mar 20 | 1 | 15 | 15|
| **+ Extra Credit Opportunities** | | | |  |
| Reading responses | Per extra | 3 | 1 | 3 |
| Attendance | See below | 1 | 2 | 2 |
| **Maximum Possible**  |  |  |  | **105**|

Note that there are 105 points that you can earn in this class, though final grades will be calculated out of 100. You can use these extra points as you see fit, for example, to cover a missed sketch entry, missed reading response, etc.

Due to the fact that there are several extra credit points built in, and that attendance is extra credit, there will be no excused absences from class or makeup work, barring [exceptional circumstances](#exceptional-circumstances). Please note that any days missed at the start of the term due to late registration fall under this same policy.

#### Design Project

The major assignment in this course is a [quarter-long group project](../project/) in which you will gain experience with each stage of a HCI design process, from research through simple prototyping and evaluation.

#### Reading Reflections

In order to prepare for our class discussions you will complete brief [reading reflections](../readings/) before about half of our class meetings (as noted on schedule and on Canvas). You will complete these reflections through a survey linked on the [schedule](../schedule/) and on Canvas.

#### Sketching Journal

Sketching is a common practice used by designers that fosters the ability to think critically about existing objects and interactions, and generate ways of improving them. As part of this class, you will keep a sketching journal, in which you will think about objects or interactions in your daily life and sketch situations that are problematic or frustrating and ideas for how they could be improved.

You will need to make three entries in your sketchbook in each of weeks 2-9, which you will share with your peers in class.


#### Self-Evaluations

Self-evaluations are used routinely in corporate America as a way of assessing employee performance and making decisions about raises and promotions. As part of the professionalization aspects of this course, you will write a brief self-evaluation for each project milestone.

A week one introductory survey counts for 4 points towards your final grade. It will be graded very simply for effort and completeness.

At the end of the term, you will write a more detailed essay worth 15 points in which you reflect more broadly on your individual progress in the course.

#### Exams

There are no exams in this course.

#### Extra Credit

In addition to the [extra credit opportunity associated with reading responses](../readings#extra-credit-opportunity) (up to 3 points total), you may also receive extra credit for excellent class attendance and participation:

- 2 points: Attend & participate in all but one (1) class meetings
- 1.5 points: Attend & participate in all but two (2) class meetings
- 1 point: Attend & participate in all but three (3) class meetings
- 0 points: Missed four (4) or more class meetings

### Final Grades

Show up to class, participate in discussion, turn in all assignments, and you will do great in this class. Skip class and assignments and you will do poorly.

Letter grades will be assigned based on the following fairly standard conversions:

| Total Points Earned | Letter Grade |
|--------------|--------------|
| 93-100       | A            |
| 90-92        | A-           |
| 87-89        | B+           |
| 83-86        | B            |
| 80-82        | B-           |
| 77-79        | C+           |
| 70-76        | C            |
| 60-69        | D            |
| Below 60     | F            |

If you earn the minimum points for a letter grade, you are guaranteed that letter grade. There will be **no** downward curve in this class. I reserve the right to adjust grades upward at my sole discretion.

I endeavor to be extremely transparent about grading rubrics, and your grades should never come as a surprise to you. You may _not_ petition for a grade that you did not earn.

Your project teams will be mixed and include both graduate and undergraduate students. All team members will always receive the _same grade_ for each project, which will be graded holistically as a single product.

### Deadlines & Exceptions

#### Missing Class

If you choose to prioritize some other activity over your class time, that is perfectly fine. You are an adult and can make your own decisions. However, you forfeit your participation points for the day as well as any in-class assignments that we complete during your absence.

Due to the fact that there are several extra credit points built in to the grading system, there will be no excused absences from class or makeup work, barring truly [exceptional circumstances](#exceptional-circumstances). Please note that any days missed at the start of the term due to late registrations will not be excused.

When you are absent for any reason, _you (not me!) are responsible for:_

- Catching up on any missed course material.
- Proposing a plan for any missed assignments, if applicable.

#### Exceptional Circumstances

**_Please contact me_** in the case of any exceptional or unpredictable event that significantly impacts your ability to complete work or attend class — such as an illness, a sick child that cannot attend school or daycare, a family emergency, iced over roads, etc. I reserve the right to request documentation before granting any extensions, but they are possible given extenuating circumstances beyond your control.

Please note that *traffic, TriMet delays, bridge lifts, regular work schedules, and personal travel are not considered exceptional or unpredictable events*. Please warn your family and friends in advance (i.e. today) that they need to consult you regarding any future travel plans made on your behalf (e.g., weddings, bachelorette parties, cruises, etc.).

#### Assignment Deadlines

Since many assignments build on each-other (and often we will use homework as material for in-class discussions or activities), as a general rule, **_late work will not be accepted without prior approval_**.

You will always receive partial credit for work that is partially complete.

Please turn in whatever you have finished at the deadline.

### Group Work

In the professional world, you will almost never work alone; you will always be part of teams. In order to help prepare you for this, portions of this class will involve group work. Major group projects will include a peer evaluation component in which you will fill out a team-assessment and a self-assessment. These assessments will be used to determine students’ grades for the group assignment. It is important that you act professionally and make meaningful contributions to these projects. You should communicate early and often with your group regarding expectations.

#### Some strategies for successful group work

- Communicate early and often with your team during class meetings and via
  email.
- Follow the course code of conduct. Treat your peers with the respect with
  which you would like to be treated.
- Meet regularly outside of class to work in a co-located space, to share
  updates, and to coordinate any individual contributions.
- Plan ahead and create a **_reasonable_** timeline for team deliverables.
  Establish a clear set of expectations for your own and others’
  contributions. Consider putting these in writing (email is good for
  keeping a record of everyone's agreement!).
- Familiarize yourself with the skills and work of each individual student
  in your team. Be honest about your own strengths and weaknesses as well.
  Articulate these to your teammates. Ask for help when you need it. Show
  a mature and professional attitude in sharing responsibility.
- Pay attention to details without losing the big picture in your group
  work. Practice professional communication within your planning,
  documentation, and deliverables.
- If major conflicts arise, and students are not able to solve these
  conflicts, the whole group must meet with me to devise a working
  strategy. You should alert me to any major issues in a timely manner
  (i.e. no later than when the assignment in question is turned in).

#### Things to avoid

- Meeting with your group only at the last minute before class (or the
  same day) to patch things up and quickly integrate material and
  deliverables. This typically results in low quality deliverables, poor
  work integration within the team, clear evidence of disorganization and
  lack of coordination, unprofessional work, and lower grade.
- Alerting me to major team issues at the very end of the term or after
  grades are released.

### Communication

#### Meeting with me

My office hours will be held on Monday and Tuesday afternoons from 4:00-5:00, in my office, FAB 120-15.

You do not need an appointment to visit my office hours, you can simply drop in. Please do visit my office hours! I enjoy working with students!

If you need to meet with me, but cannot make my office hours, you can make an appointment with me via the PSU Google Calendar system ([directions](/~harmon8/faq/#meeting-with-me)).

#### Canvas

As you may have noticed there is no D2L shell for this course. We will be piloting Canvas, a new Learning Management System (LMS) that PSU is considering for future use campus-wide. I am hopeful that it will be a much easier place to turn in assignments, ask questions about course content, etc.

As this is a bit of an experiment, I would appreciate any feedback you have about the Canvas environment throughout the term. Staff from the Office of Academic Innovation (OAI) may also ask you questions about your experience throughout the term, and will be available to you if you have any difficulty with this new environment.

Please try to ask course-related questions on the Canvas Forums before emailing me. This way everyone can benefit from the answer -- and your peers may be faster at answering than myself :)

**Important class announcements** will be posted to the Canvas announcements page (and you should receive email notifications about them unless you have elected to turn notifications off). Please make sure to check the Canvas site and/or your PSU email at least once a day.


#### Email

If you need to email me directly, *you MUST include the course number as the first word in the subject*, for example 'CS305: Missing class due to illness, suggested plan for makeup work.'

#### Responsiveness

I will aim to answer all inquiries within **1 business day**. If you do not receive a reply from me within 1 day, please re-send your message.

I do not (cannot) respond to inquiries 24/7. *Do not count on responses from me after 6pm, before 9am, or on the weekends.*

Likewise, I will never expect a reply from you sooner than 1 business day.

### Code of Conduct

The computer science community is well-known for being an unwelcoming and toxic environment to many newcomers [^1]. Research shows that members of underrepresented groups (e.g., women, people of color, first generation college students) leave computer science programs and the tech industry at higher rates, and that this attrition is a result of environmental conditions[^4].

Many open source projects, professional societies, and businesses have
recognized that the lack of diversity amongst contributors is a problem since they miss out on ideas, perspectives, and contributions from underrepresented groups[^2]. Moreover, the history and prevalence of exclusionary practices and cultures is an ethical problem that limits the intellectual, personal, and financial opportunities of members of underrepresented groups[^3].

To address this, many organizations and events have established community guidelines and codes of conduct to support communities that are more welcoming to new and diverse contributors. For example:

- **Contributor Covenant:** a code of conduct shared by many open source
projects, including Atom, Eclipse, Mono, Rails, Swift, and many more.
[contributor-covenant.org](http://contributor-covenant.org)
- **Mozilla Community Participation Guidelines:** [mozilla.org/en-US/about/governance/policies/participation](http://mozilla.org/en-US/about/governance/policies/participation/)
- **PyCon Code of Conduct:** [us.pycon.org/2018/about/code-of-conduct](https://us.pycon.org/2018/about/code-of-conduct/)
- **Ubuntu Code of Conduct:** [ubuntu.com/about/about-ubuntu/conduct](http://ubuntu.com/about/about-ubuntu/conduct)

In this course, we will also have a code of conduct.

<p class="warning">
As a starting point, we will use this <a href="https://docs.google.com/document/d/1ZP-mYO0ss-zSnY0h24V7X4FzbZFDYd2_TbidJqE6qKI/edit?usp=sharing">code of conduct</a> based on the Mozilla participation guidelines and on feedback from students in prior classes. Please add any comments or suggestions to this document during weeks 1 & 2. We will finalize it at the end of week 2.
</p>

#### What to Do About Harassment

If you are a victim of harassment of any kind in this class, there are several resources available to you:

- You may [schedule a private meeting](/~harmon8/faq/#meetings-with-me) to talk to me. _Be aware: I have [Title IX reporting obligations](#title-ix-reporting-obligations)._

- Fill out the PSU [Bias Incident Report Form](https://goo.gl/forms/PMrV0tUbhDWqcBAy1)

- File a [formal complaint](https://www.pdx.edu/diversity/file-a-complaint-of-discriminationharassment)
  
- Contact the Office of Global Diversity and Inclusion: Market Center Building, 1600 SW 4th Avenue, Suite 830

- Contact the Dean of Student Life: <askdos@pdx.edu>


### Academic Integrity

_Note: Much of this section of the syllabus is copied (with permission) from the policy of [Nick Seaver](http://nickseaver.net); notable changes include the PSU student code of conduct, and commentary specifically relevant to computer science, such as computer programming details._

Our expressions are not our own. Humans communicate with words and concepts — and within cultures and arguments — that are not of our own making. Writing, like other forms of communication, is a matter of combining existing materials in communicative ways. Different groups of people have different norms that govern these combinations: modernist poets and collagists, mashup artists and programmers, blues musicians and attorneys, documentarians and physicists all abide by different sets of rules about what counts as “originality,” what kinds of copying are acceptable, and how one should relate to the materials from which one draws.

In this course, you will continue to learn the norms of citation and attribution shared by the community of scholars at Portland State University and at other higher education institutes in the United States. Failure to abide by these norms is considered plagiarism, as laid out in the Student Code of Conduct[^6] with which you should familiarize yourself:

> \(9) Academic Misconduct. Academic Misconduct is defined as, actual or attempted, fraud, deceit, or unauthorized use of materials prohibited or inappropriate in the context of the academic assignment. Unless otherwise specified by the faculty member, all submissions, whether in draft or final form, must either be the Student’s own work, or must clearly acknowledge the source(s). Academic Misconduct includes, but is not limited to: (a) cheating, (b) fraud, (c) plagiarism, such as word for word copying, using borrowed words or phrases from original text into new patterns without attribution, or paraphrasing another writer’s ideas; (d) the buying or selling of all or any portion of course assignments and research papers; (e) performing academic assignments (including tests and examinations) in another person’s stead; (f) unauthorized disclosure or receipt of academic information; (g) falsification of research data (h) unauthorized collaboration; (i) using the same paper or data for several assignments or courses without proper documentation; (j) unauthorized alteration of student records; and (k) academic sabotage, including destroying or obstructing another student’s work.

_Any academic misconduct, including plagiarism, will result in a grade of zero for the assignment concerned. All incidents of academic misconduct will be reported to the PSU Conduct Office._

If you are uncertain about what constitutes plagiarism, I recommend visiting me during my office hours and/or reviewing these online resources:

- [Indiana University First Principles website](https://www.indiana.edu/~academy/firstPrinciples/)

- [Purdue Online Writing Laboratory (OWL)](https://owl.english.purdue.edu/owl/section/2/)

#### Attribution vs. Originality

The degree to which collaborative and derivative work is allowed in this
class may vary by assignment.

- **_In all cases, attribution is paramount._** Because this is an educational setting, all copied *and derivative* computer code or written text **_must be attributed_**.
- Even if you are using public domain writings or open source code, you must note in your own work all places from which you draw inspiration, ideas, or implementation details. Even if you start with someone else’s code or text and then modify it significantly, you should still cite the author of the original work that you used to get started. **_It is always better to over-attribute than under-attribute._**

In all cases, the **work you submit _as your own_ must actually be your own**. It is not acceptable to hand in assignments in which substantial amounts of the work was completed by someone else.

That said, many university plagiarism policies tend to focus on the less productive side of the issue, urging students to be “original” and telling them what not to do (buying papers, copying text from the internet and passing it off as one’s own, etc.). It can be helpful to take more expansive view of what academic integrity means. _Academic integrity is not so much a matter of producing purely original thought, but of recognizing and acknowledging the resources on which you draw_. For example, you will notice in the acknowledgements throughout, that this syllabus draws heavily from a wide range of other university professors. References to others' work both lend your work additional authority and credibility, and also help the reader understand what unique contributions you might have made in bringing diverse resources together in a writing assignment or project. Originality is not just about a singular novel idea, but may also be about a novel combination of others' ideas.

In light of this, I do not use “plagiarism detection” services like Turnitin. Rather than expending your energy worrying about originality, I suggest that you think instead about what kind of citational network you are locating yourself in. What thinkers are you thinking with? What programmers are you coding with? Where do they come from? How might their positions in the world inform their thoughts – and, in turn, your thoughts? What is your position relative to these thinkers, writers, and makers? How might you re-shape your citational network to better reflect your priorities or ideals? How are you learning from or extending the code or text in question? How do you think the original author would feel about your use of their work? Are you generous in giving them credit for the parts that are the result of their own hard work or are you claiming it as your own?

If you are interested in these issues, I recommend these pieces:

- Ahmed, Sara. 2013. “Making Feminist Points.” *feministkilljoys*. \<[feministkilljoys.com/2013/09/11/making-feminist-points/](http://feministkilljoys.com/2013/09/11/making-feminist-points/)\>

- Frank, Will. 2015. “IP-rimer: A Basic Explanation of Intellectual Property.” Personal Blog Post (Medium). \<[medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711](https://medium.com/@scifantasy/ip-rimer-a-basic-explanation-of-intellectual-property-9be6f0ce6711)\>

- This comment thread regarding proposed changes to the Stack Exchange code licensing defaults: \<[meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required](https://meta.stackexchange.com/questions/272956/a-new-code-license-the-mit-this-time-with-attribution-required?cb=1)\>

You may write a 500-word response to these pieces for extra credit. Visit me in office hours for details.


##### Writing Assignments

*In the case of any writing assignments* such as reading responses and project reports, I expect you to quote and reference course readings, course discussions, and external sources. Indeed, your ability to locate and use such sources is part of the skill that you should be developing and demonstrating as a writer. At this point in your life, you should be an accomplished communicator and thinker. However, for full points on any writing assignment, I will also expect you to build on these works and move beyond simply repeating others’ ideas: to bring your own unique experiences, critiques, and perspectives into conversation with these authors.

##### Coding Assignments

This course does not have any coding assignments.



### Take Care of Yourself!

There are many resources available to support you at PSU. I encourage you to take advantage of them so that you will be successful at both your academic and non-academic pursuits.

- **We in Computer Science (WiCS):** student organization promoting diversity in computer science [wics.cs.pdx.edu](http://wics.cs.pdx.edu)
- **Writing Center:** The writing center can help you improve your writing! Visit them! [pdx.edu/writing-center](http://pdx.edu/writing-center)
- **Office of Information Technology (OIT):** Campus help desk for all things technology-related [pdx.edu/oit](http://www.pdx.edu/oit)
- **C.A.R.E. Team:** Central point of contact if you *or someone you know* is having a difficult time – mentally, financially, physically, anything! [pdx.edu/dos/care-team](http://pdx.edu/dos/care-team)
- **PSU Food Pantry**: Located in SMSU 325. <foodhelp@pdx.edu>
- **Center for Student Health and Counseling (SHAC):** free, drop-in mental and physical health care [pdx.edu/shac](http://pdx.edu/shac) For mental health, their capacity is limited to a few appointments. However, they will help you find a referral for ongoing therapy or other care.
- **Financial Wellness Center**: For many college students, money is an extremely important and often stressful topic. The Financial Wellness Center offers coaching sessions and meetings with peer mentors: [pdx.edu/student-financial/financial-wellness-center](https://www.pdx.edu/student-financial/financial-wellness-center)
- **PSU Cultural Resource Centers (CRCs)** create a student-centered inclusive environment that enriches the university experience. We provide student leadership, employment, and volunteer opportunities; student resources such as computer labs, event, lounge and study spaces; and extensive programming. All are welcome! [pdx.edu/cultural-resource-centers](https://www.pdx.edu/cultural-resource-centers), <cultures@pdx.edu>, 503-725-5351, [facebook](https://www.facebook.com/psuculturalcenters/)

See the dean of student life website for further student resources:
[pdx.edu/dos/student-resources](http://pdx.edu/dos/student-resources)

#### Additional Mental Health & Counseling Resources

College can be a stressful time for a variety of reasons, and taking care of your mental health is important! If SHAC isn't working out for you, these other resources may be useful[^neera-thanks]:

- [Community Counseling Clinic](https://www.pdx.edu/coun/clinic) - This is a low-cost resource for ongoing counseling, on campus, located in the grad school of education. The upside is that it is $15 per session for as long as you need, the potential downside is that the therapists are interns.
- [M.E.T.A.](https://meta-trainings.com/) - This is an off-campus resource (Belmont x Cesar Chavez-ish). The cost is between $30-45, depending on the experience level of your intern and on your budget. Like the Community Counseling Clinic, it is with interns.
- [Multnomah County Crisis Services](https://multco.us/mhas/mental-health-crisis-intervention) - if, at any point, you feel like you are in a mental health crisis, please call **(503) 988-488**. If this seems like a plausible scenario to you, I would put this number in your phone now. In addition to crisis services, they can also give referrals and information.

[^neera-thanks]: Resource list courtesy [Dr. Neera Malhotra](https://www.pdx.edu/profile/neera-malhotra), PSU, University Studies

### Access and Inclusion for Students with Disabilities

This section is lightly edited from: [pdx.edu/drc/syllabus-statement](https://www.pdx.edu/drc/syllabus-statement)

PSU values diversity and inclusion; we are committed to fostering mutual respect and full participation for all students. My goal is to create a learning environment that is equitable, useable, inclusive, and welcoming. If any aspects of instruction or course design result in barriers to your inclusion or learning, please notify me. The Disability Resource Center (DRC) provides reasonable accommodations for students who encounter barriers in the learning environment.

If you have, or think you may have, a disability that may affect your work in this class and feel you need accommodations, contact the Disability Resource Center to schedule an appointment and initiate a conversation about reasonable accommodations. The DRC is located in 116 Smith Memorial Student Union, 503-725-4150, <drc@pdx.edu>, <https://www.pdx.edu/drc>.

- If you already have accommodations, please contact me to make sure that I have received a faculty notification letter and discuss your accommodations.
- ~~Students who need accommodations for tests and quizzes are expected to schedule their tests to overlap with the time the class is taking the test.~~ (There are no tests or quizzes in this course.)
- For information about emergency preparedness, please go to the Fire and Life Safety webpage <https://www.pdx.edu/environmental-health-safety/fire-and-life-safety> for information.



### Title IX Reporting Obligations

*This section is lightly edited from [this suggested syllabus statement](https://www.pdx.edu/diversity/sites/www.pdx.edu.diversity/files/Syllabus%20Statement%2062619%20for%20Title%20IX%20Reporting%20Obligations_REVISED.pdf).*

Title IX is a federal law that requires the university to appropriately respond to any concerns of sex/gender discrimination, sexual harassment or sexual violence. To assure students receive support, faculty members are required to report any instances of sexual harassment, sexual violence and/or other forms of prohibited discrimination to PSU’s Title IX Coordinator, [Julie Caron](https://www.pdx.edu/directory/name/jucaron).

If you would rather share information about these experiences with an employee who does not have these reporting responsibilities and can keep the information confidential, please contact one of the following campus resources (or visit this link <https://www.pdx.edu/sexual-assault/get-help>):

- Women’s Resource Center (503-725-5672) or schedule on line at <https://psuwrc.youcanbook.me>
- Center for Student Health and Counseling (SHAC): 1880 SW 6th Ave, (503) 725-2800
- Student Legal Services: 1825 SW Broadway, (SMSU) M343, (503) 725-4556
  
PSU’s Title IX Coordinator and Deputy Title IX Coordinators can meet with you to discuss how to address concerns that you may have regarding a Title IX matter or any other form of discrimination or discriminatory harassment. Please note that they cannot keep the information you provide to them confidential but will keep it private and only share it with limited people that have a need to know. You may contact the Title IX Coordinators as follows:

- PSU’s Title IX Coordinator: Julie Caron by calling 503-725-4410, via email at <titleixcoordinator@pdx.edu> or in person at Richard and Maureen Neuberger Center (RMNC), 1600 SW 4th Ave, Suite 830
- Deputy Title IX Coordinator: Yesenia Gutierrez by calling 503-725-4413, via email at <yesenia.gutierrez.gdi@pdx.edu> or in person at RMNC, 1600 SW 4th Ave, Suite 830
- Deputy Title IX Coordinator: Dana Walton-Macaulay by calling 503-725-5651, via email at <dana26@pdx.edu> or in person at Smith Memorial Union, Suite, 1825 SW Broadway, Suite 433
  
For more information about the applicable regulations please complete the required student module Creating a Safe Campus in your D2L [pdx.edu/sexual-assault/safe-campus-module](http://pdx.edu/sexual-assault/safe-campus-module)



### Notes

[^1]: See, for example, last year's news about Richard Stallman stepping down from the Free Software Foundation and MIT, or, the previous year's news about Linus Torvalds stepping away from maintenance of Linux as he seeks help for improving his interpersonal skills. Lawler, Richard. 2019. "GNU founder Richard Stallman resigns from MIT, Free Software Foundation: Calls to fire Stallman grew after his comments about victims of Jeffrey Epstein." _Engadget_, September 19, 2019. <https://www.engadget.com/2019/09/17/rms-fsf-mit-epstein/>; Cohen, Noam. 2018. "After Years of Abusive E-mails, the Creator of Linux Steps Aside," _The New Yorker_, September 29, 2018. <https://www.newyorker.com/science/elements/after-years-of-abusive-e-mails-the-creator-of-linux-steps-aside>

[^2]: See, e.g., Vivian Hunt et al. 2018. “Delivering through Diversity” Report by *McKenzie and Co.* January 2018. <https://www.mckinsey.com/business-functions/organization/our-insights/delivering-through-diversity>.

[^3]: See, e.g., Jane Margollis et al. 2008. *Stuck in the Shallow End: Education, Race and Computing.* MIT Press.; Steve Henn. 2014. "When Women Stopped Coding" *Planet Money*, NPR. <https://www.npr.org/sections/money/2014/10/21/357629765/when-women-stopped-coding>; Michelle Kim. 2018. "Why focusing on the “business case” for diversity is a red flag" *Quartz at WORK,* 29 March 2018. <https://work.qz.com/1240213/focusing-on-the-business-case-for-diversity-is-a-red-flag/>.

[^4]: See, e.g., J. McGrath Cohoon. 2001. Toward improving female retention in the computer science major. *Commun. ACM* 44, 5 (May 2001), 108-114.<http://dx.doi.org/10.1145/374308.374367>; Tracey Lien. 2015. “Why are women leaving the tech industry in droves?” *LA Times*. <http://www.latimes.com/business/la-fi-women-tech-20150222-story.html>.

[^6]: PSU Student Code of Conduct: <https://www.pdx.edu/dos/psu-student-code-conduct>
