---
title: Reading Assignments
page-class: schedule
layout: "subpage"
icon: "fa-book"
weight: 30


menu:
    main:
        parent: "Teaching"
        identifier: "hci-202001-readings"

---

You are expected to come to each class meeting prepared for engaged discussion. There will be a reading assignment for every class period. Unless there is a note specifying otherwise, you should read _all_ readings listed on a certain day.

On 11 selected days (noted in the class schedule and on Canvas), you will have the opportunity to complete a reading reflection. You are required to complete at least 8 of these reflections.

### Logistics

- **Highest 8 grades kept**: If you complete more than 8 reflections, then the highest 8 grades will be kept, and the lowest grades dropped.
- **Extra Credit:** There is an extra credit opportunity for completing more than 8 reflections (details below).
- **Submission:** You will submit your reading response by filling out a survey on Qualtrics. For ease of access, survey links can be found in three places: (a) on this page (b) on the schedule and (c) on Canvas. I recommend writing answers in a document first so that you can have a saved copy. You can then quickly copy and paste each answer into the survey fields to turn in.
- **Deadline:** In order to receive any credit at all for the reading response, you must complete it **before the start of class** on the day that we will discuss the reading. This means **before 2:00pm** (not at 2:01, nor 2:05, nor 2:15).


#### Reading Response Grading

Each reading response is worth up to three (3) points. Together they account for 24% of your total grade in the class.

They will be graded on a very simple scale:

| Points | Criteria |
|---:|:---|
| **3** | Moves beyond simply repeating others’ ideas to bring your own unique experiences, critiques, and perspectives into conversation with these authors. Could be used as an example of excellent work in a future class. |
| **2.8** | Correctly and completely answers each question in the reading response, demonstrating comprehension of the reading material. |
| **2.6** | All questions are completely answered, and most are correct. There may be minor misunderstandings in 1 or 2 of your answers, but it is clear that you mostly understood the reading. |
| **2.2** | Answers are complete, but more than 1 or 2 answers are clearly wrong, showing a very incomplete understanding of the material. |
| **0** | Incomplete answers, not turned in, turned in _late_. |

#### Extra Credit Opportunity

For each reading response completed above the minimum (8) and on which you also score at least a 2.6, you will receive one point of extra credit on your final grade.

- For example, if you complete 6 reading responses with a grade of 3, 4 reading responses with a grade of 2.6 and one reading response with a grade of 2.2, you would score:
    - 6 x 3 + 2 x 2.6 = 23.2 points for your reading response grade (the highest 8 scores)
    - 2 x 1 = 2 points of extra credit added at the end (one point for each other reading response with a score of 2.6 or more)

You can earn a maximum of 3 extra points on your final score if you complete all reading responses, and earn at least a 2.6 on each one.

### RR Questions

#### RR1: Reading HCI Papers

**Due**: Week 1, Meeting 2

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_b2cTIT7tV7k7i73>

**Reading**:

- Dombrowski, Lynn, Adriana Alvarado Garcia, and Jessica Despard. 2017. "Low-Wage Precarious Workers' Sociotechnical Practices Working Towards Addressing Wage Theft." In *Proceedings of the 2017 CHI Conference on Human Factors in Computing Systems*, 4585--4598. CHI '17. New York, NY, USA: ACM. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/3025453.3025633>.

**Notes**: This reading reflection is designed to be completed as you read the paper. The questions follow the order and structure of the paper, with the goal of helping you learn to parse an academic article. I recommend answering them in a separate document, as you go, and then pasting into the survey at the end.

1. Reading only the abstract, identify (1) what the researchers did (what was their general area of research and what methods did they use) and (2) what did the researchers found.
2. Reading only the introduction, describe why this research project is important to do and relevant to the CHI community.
3. Read through the related work section. Identify one contribution to the literature that this paper will make. To answer this you might think about things like: What work do the authors say they will build on? Where do the authors show that there are gaps or weaknesses in the current literature that their work will fill in?
4. Read through the methods section. How did the researchers decide who to interview for their study?
5. What kinds of topics did researchers ask about in their interviews?
6. Choose one of the three sociotechnical practices that researchers found workers using to address wage theft. Give an example of how technology is implicated in that situation.
7. Describe one limitation or challenge the researhcers identified with respec to intervening in low-wage work through technology design.
8. Describe one way that the authors suggest technology could be useful in addressing wage theft.
9. This paper was recently published in ACM CHI, the major academic conference for the field of HCI. Drawing on what you know from this paper and from our class meetings so far, how would you describe the field of HCI to a friend? Has your idea of HCI changed since you signed up for this class? If so, how?


#### RR2: HCI + Research Ethics

**Due**: Week 2, Meeting 2

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_3QQPdycjRbFMa1f>

**Readings**:

- Suchman, Lucy. 1995. "Making Work Visible." *Communications of the ACM* 38 (9): 56--64. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/223248.223263>.
- Mortensen, Ditte. 2018. "Conducting Ethical User Research." *The Interaction Design Foundation* (blog). July 24, 2018. <https://www.interaction-design.org/literature/article/conducting-ethical-user-research>.

1. Identify 2 criteria from Mortensen’s article that you think are most important. Explain why each is important to you.

2. Identify 2 criteria from Mortensen’s article that you think would be most difficult to achieve. Explain why each is difficult or challenging.

3. Reflect on the project that you will be working on in light of Mortensen’s criteria. Evaluate your own work in terms of Mortensen’s criteria. Explain how you are or are not meeting each criteria. Did this article change your idea of what you might need to do in this class?

4. Suchman focuses her paper around a concept of ‘work’ and ‘work practice.’ What does ‘work practice’ mean for Suchman?

5. What does it mean to make ‘work practice’ visible?

6. What are some of the effects of making something visible? Why might we want to do it? And/or why might we _not_ want to do it?

7. What is the power of maps according to Wood (see p. 61) and how does that relate to the representations of work that Suchman is talking about?

8. How do Suchman’s concerns relate to Mortensen’s seven criteria? Do they raise any new questions or challenges?

#### RR3: Values

**Due**: Week 4, Meeting 1

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_af4qLWgp4Pvdlg9>

**Readings**:

- Sengers, Phoebe. 2011. "What I Learned on Change Islands." *Interactions* 18 (2). <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1925820.1925830>
- Cowan, Ruth Schwartz. Selections from “The Postwar Years.” In _More Work for Mother: The Ironies of Household Technology from the Open Hearth to the Microwave_, pp. 192-193, 195-201, 208–216. New York: Basic Books, 1983. [PDF (Google Drive)](https://drive.google.com/file/d/1IPnCrsJHktalSoLtpq9cV6jBMUW4RCkl/view?usp=sharing)

1. In the first page of her article, Sengers writes that design is part of an “ongoing process of modernization.” Name and briefly describe all three of the values that Sengers identifies as related to this larger process.

2. Sengers ends her article with the question, “Can IT design as an influence compete with a pervasive cultural atmosphere of overwork and overload?” How would you answer Sengers’ question? What do you think would be the best tactics or strategies for changing the cultural atmosphere she writes about?

3. In the selections from Ruth Schwartz Cowan’s book, you read about the “diffusion of amenities and appliances” between roughly 1940 to 1980. What are some of the new appliances that made their way into a majority of American households? How did these appliances change the labor of different people in middle-class households?

4. Cowan ends her the excerpted chapter with the call to “...neutralize both the sexual connotation of washing machines and vacuum cleaners and the senseless tyranny of spotless shirts and immaculate floors.” This statement references an argument explored in other chapters of the book about the relationship between technologies and social norms. In part, household technologies were designed for and marketed to women, influencing & reinforcing social norms about who does housework. In addition, as technologies like the washing machine made it easier and faster to (e.g.,) wash clothes, social standards for cleanliness also increased. The net result was an increase in time spent cleaning, especially for married heterosexual women. Cowan is thus calling for a resistance to both of these norms — the one that feminizes household technologies & labor and the one that demands high levels of cleanliness/technology use.

    **Identify another technology that has made something faster and/or easier. Explain whether and how it relates to changing social norms/expectations/obligations, and reflect on strategies for altering the social norms that prescribe either who should use it, or how often it should be used.**

5. Thinking about the two readings for today in conversation with each other, and your own life experiences, take a stand on the causal relationship between technology (design) and social life/cultural logics. How can design/technology create or influence cultural practices, beliefs, or norms? How is the power of design limited? Use examples from a reading to relate your own stance to the stance of _at least one_ of the authors we read for today.

6. Ultimately, both readings today are about the relationship between values/culture and technology. Brainstorm a list of at least 4 values that may be important to your design project. For one of these values, reflect on the meaning of the value in the context of your project. Your reflection should address all of the following questions:
    - What does the value mean in the context of your project?
    - Who is the value is important to?
    - Does the value have different meanings or are there multiple ways to enact it?
    - How is technology related?

    Here is an example of a good answer to the reflection: _Our team’s project is about family communication. In family life, family members often value ‘togetherness.’ This value is about spending time or being with another family member, and feeling connected. People sometimes enact this value by turning off their phones around the dinner table in order to feel more present and in the moment with other family members. Other times people enact this value by using their phones to share a photo with someone who cannot be present. The same technology (a phone) can be both supportive of this value (when people use it to share pictures), and can threaten this value (when people feel it is distracting)._

#### RR4: Problems & Solutions

**Due**: Week 4, Meeting 2

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_2fRj80levpPZGT3>

**Readings** (read both):

- Baumer, Eric P S, and M Six Silberman. 2011. "When the Implication Is Not to Design (Technology)." In *Proc. CHI 2011*, 2271--2274. CHI '11. Vancouver, BC, Canada: ACM. <https://dl-acm-org.proxy.lib.pdx.edu/citation.cfm?doid=1978942.1979275>.
- Sullivan, Paul. 2014. “Stop Jumping to Solutions and Think About the Problem.” ThoughtWorks (blog). October 27, 2014. <https://www.thoughtworks.com/insights/blog/you-need-understand-problem>.


1. Summarize Baumer & Silberman’s argument for why we should not always frame design in terms of problems and solutions.
2. In “When the Implication is Not to Design (Technology),” Eric Baumer and Six Silberman present a set of three specific questions. State and then answer each of these questions with regards to your design project.
3. In the closing section, “Arguments for Practice,” Baumer and Silberman conclude with 5 implications for HCI practitioners. Choose one of these, summarize it, and explain why or how it might be applied to some real world technology that you are familiar with.
4. In the second article you read, Paul Sullivan emphasizes the importance of identifying a problem. Briefly explain why problem identification is important from Sullivan’s perspective?
5. What do you think Baumer & Silberman would say in response to Sullivan’s article? Do they think problem identification as Sullivan describes it is necessarily bad? How would you describe the fundamental conflict between the two articles?
6. Thinking across both readings for today, identify one new question you have or one takeaway for you from these readings. Why is this question or takeaway important to you? Does it relate to previous course readings or discussions at all?



#### RR5: Critical Design

**Due**: Week 5, Meeting 1

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_5AycNMG5553KsYd>

**Readings** (choose 1):

- Gaver, Bill, and Heather Martin. “Alternatives: Exploring Information Appliances Through Conceptual Design Proposals.” In _Proceedings of the SIGCHI Conference on Human Factors in Computing Systems_, 209–216. CHI ’00. New York, NY, USA: ACM, 2000. <https://doi.org/10.1145/332040.332433>
- Steup, Rosemary, Lynn Dombrowski, and Norman Makoto Su. 2019. “Feeding the World with Data: Visions of Data-Driven Farming.” In _Proceedings of the 2019 on Designing Interactive Systems Conference_, 1503–15. DIS ’19. New York, NY, USA: ACM. <https://doi.org/10.1145/3322276.3322382>.

- **If you chose Gaver & Martin:**
    1. What motivates Bill Gaver and Heather Martin to develop the Alternatives workbook they share in this paper? Why did they create it? What work does it do?
    2. According to Fiona Raby, what makes a prototype "work" in a _design_ context?
    3. What does this tell you about the prototypes you will need to make in this class?
    4. What is a _value fiction_? Give a brief definition and then give an example from the paper. Explain why this example is a value fiction.
    5. Gaver & Martin outline six values that their designs are "meant to encourage." Identify three of these values and give a brief definition and/or example of each.
        5.1. Value 1
        5.2. Value 2
        5.3. Value 3
    6. Thinking across the reading for today as well as the readings from last week which are all about values and design in various ways, identify one new question or takeaway you have from across the readings. Explain why this question/takeaway is important and make sure to connect it to at least _two_ of the readings.
- **If you chose Steup et al.:**
    1. Steup et al.'s article is unique in that it does not report on traditional user research nor propose a design project; instead the authors "analyze the discourse" of websites for data-driven farming startups. Why do Steup et al. take this approach? What motivates their research?
    2. Briefly describe their approach to analyzing the discourse. What data do they collect? What is their sampling strategy? How did they analyze their data?
    3. Steup et al. identify four future visions for data-driven farming. Identify each of the four visions by name, and give a brief definition or key exemplar for each vision.
        3.1. Vision 1
        3.2. Vision 2
        3.3. Vision 3
        3.4. Vision 4
    4. At the start of the discussion section, Steup et al. write: "Rather than question the plausibility of these visions, we focus on the consequences for farming should these futurescome to pass." Why do you think they made this decision? What would be gained by focusing on consequences instead of plausibility?
    5. Thinking across the reading for today as well as the readings from last week which are all about challenging the status quo in some way, identify one new question or takeaway you have from across the readings. Explain why this question/takeaway is important and make sure to connect it to at least _one other_ reading.

#### RR6: Inclusion

**Due**: Week 6, Meeting 2

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_2sk9V7PZx1tuqNv>

**Readings**:

- Choice 1:
    - Schulman, Sarah. 2016. "Redefining the Politics of Inclusion with George and Dustin." *The Journal of Design Strategies* 8 (1): 50--55. <https://issuu.com/journalofdesignstrategies/docs/the_journal_of_design_strategies_vo>. [Alternate PDF (GDrive)](https://drive.google.com/open?id=1I_wIhqt-6o36zVHpIaeD8PF8MbsbHmEC)
    - Martin, Courtney. 2016. "The Reductive Seduction of Other People's Problems." *BRIGHT Magazine (Medium)*, January. <https://brightthemag.com/the-reductive-seduction-of-other-people-s-problems-3c07b307732d>.
    - Abreu, Amelia. 2018. "Why I Won't 'Try on' Disability to Build Empathy in the Design Process (and You Should Think Twice)" *Prototypr (Medium)*, May 1, 2018. <https://blog.prototypr.io/why-i-wont-try-on-disability-to-build-empathy-in-the-design-process-and-you-should-think-twice-7086ed6202aa>.
- Choice 2:
    - Hamraie, Aimi. 2018. “Mapping Access: Digital Humanities, Disability Justice, and Sociospatial Practice.” _American Quarterly_ 70 (3): 455–82. <https://doi.org/10.1353/aq.2018.0031>.

- **If you chose the first set of 3 readings**:
    1. Schulman writes about trying to change some of the vocabulary of design. For each of these three shifts that Schulman says we should make, describe what the shift is really about. That is, beyond just a word change, what change in design practice does each vocabulary shift entail?
        1. Imagining to Immersing
        2. Designing For to Making With
        3. Public to Private
    2. Summarize the main argument that Courtney Martin makes in "The Reductive Seduction of Other People’s Problems."
    3. What are two reasons that Martin argues "reductive seduction" is dangerous?
    4. According to Abreu, what are some examples of ways that designers try to build empathy with disabled people?
    5. What does Abreu argue about 'trying on' disability as part of a design process? Include at least 2 of her reasons for why she takes her stance.
    6. How does Abreau think we should include the concerns of disabled persons in our design process?
- **If you chose the Hamraie article**:
    1. The article opens with a description of a new Google Maps feature. Briefly describe the feature and how it relates to accessibility.
    2. In reflecting on this feature, Hamraie writes: "Critical access theories and digital projects, by contrast, approach access as an "interpretive relation between bodies" rather than an objective quality." Explain what you think they mean by this.
    3. Briefly describe the "Mapping Access" project Hamraie teaches in their class.
    4. Give an example of the kinds of questions raised by "critical digital mapping," and explain/show how they are different than literal questions about accessibility compliance.
    5. This is a dense article written for a humanities audience! Choose **two** quotes that stood out to you -- because they were impactful, insightful, confusing, etc. Copy and paste the quote in for reference, and then reflect briefly on why you chose it.
        1. Quote 1
        2. Quote 2
    6. What is one thing you will take with you after reading this article?
    7. Is it beneficial _for you_ to read articles from disciplines other than computer science? Why or why not?

#### RR7: Human Factors

**Survey Link:** <https://portlandstate.qualtrics.com/jfe/form/SV_4T15KoS2DeEcwCh>

**Readings**: See class schedule.

As always the goal of this reading response is to help prepare you for class. However, perhaps even more than usual, it will be helpful for you to come to class WITH YOUR ANSWERS on hand -- either printed or on your computer in digital format.

Although each reading is on a slightly different topic, the questions are the same for each one. Anywhere you see `<your topic>` in a question below, insert the topic area you read about. It should be one of the words in this list: Fitt's Law, Affordance, Mental Model, Distributed Cognition, Direct Manipulation, Intermediation, Accessibility.

1. Briefly define `<your topic>`.
2. Give two examples of a technology or other situation where the principle behind `<your topic>` is leveraged to positive effect.
3. Give one  example of a technology or other situation where the principle behind `<your topic>` is ignored or violated.
4. Why is/are `<your topic>` relevant to HCI researchers or practitioners?
5. How could `<your topic>` be applied to your class project?
6. One additional takeaway or question related to the reading.

#### RR8: HCI History

**Due**: Week 9, Meeting 1

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_3VN1oGvufRGgNZr>

**Reading**:

- Moggridge, Bill. 2007. "Interviews with Stu Card and Tim Mott." In *Designing Interactions*, 1st ed. MIT Press. [PDF (GDrive)](https://drive.google.com/open?id=1xmvq9cxq3kwwTA8Iv2v-udZ3_CLmGTK1)

1. Useful background: What is PARC and why would we care about the stories of people who worked there, i.e., why is it significant? _We discussed this briefly at the start of the term; you may need to look it up elsewhere if you don't remember, as this is not covered explicitly in the reading._
2. Why was Stu Card frustrated with precursors to HCI/Interaction Design like ‘Human Factors’? What did he want to do differently?
3. What was "forward-looking" about the slogan "Design is where all of the action is!"?
4. ~~How did the 'supporting science' that Stu Card feed into the design phase of product development?~~ Thinking about the example of the development of the mouse, how did Stu Card's "supporting science" contribute to the design?
    - _Sorry, the original question didn't really make sense. Updated 2/27._
5. What was Tim Mott’s initial reaction to the POLOS system when he first flew out to visit PARC?
6. How did Mott go about deciding how to change the POLOS system? Name and briefly describe the technique Mott used.
7. Give an exmaple of something not yet mentioned in your response that captures one way that the early HCI practitioners you read about continue to influence the way we use computers today.

#### RR9: HCI Presents

**Due**: Week 9, Meeting 2

**Link**: <https://portlandstate.qualtrics.com/jfe/form/SV_8uc9LI5jqta5xzf>

**Reading**:

- Taylor, Alex. “After Interaction.” _Interactions_ 22, no. 5 (August 2015): 48–53. <http://doi.acm.org.proxy.lib.pdx.edu/10.1145/2809888>.
- Khovanskaya, Vera, Lynn Dombrowski, Ellie Harmon, Matthias Korn, Ann Light, Michael Stewart, and Amy Voida. “Designing Against the Status Quo.” _Interactions_ 25, no. 2 (February 2018): 64–67. <https://doi.org/10.1145/3178560>.
- Dourish, Paul. “User Experience As Legitimacy Trap.” _Interactions_ 26, no. 6 (October 2019): 46–49. <https://doi.org/10.1145/3358908>.


1. What does Taylor mean by _world making_ and how does it relate to design? Or, phrased differently, beyond _the interface_, what work does design do?
2. Khovanskaya et al. identify three _comittments_ as a foundation for work to design against the status quo. Choose one of these committments:
    - 2.1. Name it.
    - 2.2. Give a brief definition.
    - 2.3. Describe how it could apply to your own project this quarter.
3. Dourish uses the concept of a _legitimacy trap_ to critique the current state of the field of HCI/UX. Arising out of a study of human resources (HR) departments, the concept is meant to capture the way that HR's successful work to establish its value by appealing to a need for corporate compliance later trapped the field in fulfilling this bureacratic role and prevented it from asserting its relevance to management. Explain how Dourish uses this same concept to critique HCI/UX.
    - 3.1. How did HCI/UX originally establish its value?
    - 3.2. How and why is HCI/UX now trapped?
4. What is one key takeaway or new question you have after reading these three articles? Explain why this takeaway or question is important to you.
5. In this assignment, you read three relatively recent reflections on the field of HCI/UX, jumping ahead several decades from the pioneering work of Card and Mott discussed in the Moggridge book. Over these years, the concerns of HCI researchers have changed and shifted. We will talk more in class, but from what you know now:
    - 5.1. What do you think the core methods, goals, and questions of HCI  when the field was just starting?
    - 5.2. What do you think are the core methods, goals, and questions today?


#### RR10 + 11: Choose your own adventure

For this reading response, you will (a) select a reading yourself and (b) prepare to share it with the rest of the class. You will only do this _once_, and the entire multi-part activity will count for _two_ reading response grades (i.e., 6 points total).

Your reading choice must be a research paper of at least 10 pages in length. It may be in the field of HCI (e.g., a CHI or CSCW or UIST paper) or it may be something relevant from an adjacent field (e.g., sociology, psychology, anthropology, etc.). Please contact me if you have questions about what counts or what would be appropriate.

See the [schedule in week 10](../schedule/#week10) for some ideas if you don't know what you want to read.

You may plan ahead to work with up to 3 others (group size max: 4) by all choosing to read the same article. You will sign up for an article to read in Week 9, meeting 1.

_Note 3/9: I rebalanced the grades on these to be more logical for the split between regular reading response prep work and in class work. This makes it easier to give people credit for a full reading response if they only complete the writing part, but do not attend class. Likewise, if you attend both classes, and don't do the prep part, you can also earn a full reading response worth of points._

##### Before Week 10, Meeting 1 (3 points)

For this reading response, each person should _individually_ prepare a document that you upload to Canvas (as RR10) no later than the START of Meeting 1, in week 10. It should have 2 parts:

1. A 300-500 word abstract and reflection:
    - Give a full bibliographic citation for the article in some standard format (e.g., Chicago, ACM, IEEE, APA, etc.).
    - What is the key research _question or problem_?
    - What did the author(s) _do_ to answer the question/respond to the problem?
    - What did the author(s) _find_, _argue_, and/or _propose_ in response to the question/problem?
    - What did you take away from this reading? What about it is important or interesting for HCI?
2. Identify at least 2 discussion questions that engage with the contents of your reading, but that your classmates could reasonably respond to even if all they know about your reading is the information you have written in your abstract & reflection.

##### In Class, Week 10, Meeting 1 (1.5 points):

(Bring your reading response for reference.)

I will put you in small groups with others who read either (a) the same thing as you or (b) something similar to you.

In your small group, you will work together to develop a shared understanding of your article or topic area and create a research poster that (1) captures the main points of your reading(s) and (2) poses 2-4 discussion questions for your classmates to respond to.

_We will devote any remaining class time to group project work._

##### In Class, Week 10, Meeting 2 (1.5 points):

I will distribute the posters from meeting 1 around the room and each individual student will respond to at least 3 different discussion questions via sticky-notes.

Each poster creation team will then have the opportunity to read the responses left by classmates, and synthesize these ideas along with their own ideas about how to answer the discussion questions on their own poster.

Each group will be given 5 minutes to present their entire poster, along with discussion questions and responses to the rest of the class.

_We will devote any remaining class time to group project work._
