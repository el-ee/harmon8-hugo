---
title: "Home"
date: 2018-08-15
---

I am a Senior Instructor in the [Department of Computer Science](http://cs.pdx.edu) at Portland State University.

I [teach](./classes/) courses in introductory computer science, human-computer interaction, and computing & society.

I conduct [research](./research/) about the promises, threats, and impacts of contemporary computing. I've studied: labor and the future of work, philanthropy and social change, microbial science, suburban family life, and possibilities for disconnection on the Pacific Crest Trail.

I work to make computer science more broadly accessible, relevant, and welcoming. I am developing [a new intro CS class](./classes/cs199/) at PSU; I am co-director of [CyberPDX](http://cyberpdx.org), a summer camp for broadening participation in cybersecurity; and I am the faculty advisor for the PSU [We in Computer Science (WiCS)](http://wics.cs.pdx.edu) student organization.
