---
title: "FAQ"
date: 2019-01-01

menu:
    main:
        title: "FAQ"
---

1. [Meetings with Me](#meetings-with-me)
2. [How to Find My Office](#how-to-find-my-office)
3. [Managing Multiple Google Accounts](#managing-multiple-google-accounts)

## Meetings with me

**If you are a current student, you can always drop in to my office hours without an appointment. These hours are currently running on Zoom during the originally scheduled course meeting times. Details are in the class syllabus.**

If you are not a current student, or need to meet with me outside of those times, please request a meeting through the PSU Google Calendar system.

1. Go to [cal.pdx.edu](http://cal.pdx.edu)
2. Click on the **+ Create** button to create a new event
3. Click on **More Options** in the popup
4. Click on **Find a Time**
5. Add me as a **guest** by my email: **<harmon8@pdx.edu>**
6. Choose a time when I am available, and schedule the event

In order to add a Zoom meeting, you can either:

- Check the box in the guest permissions section that says "Modify event" and I will add a Zoom meeting
- ... or ...
- [Install the "Zoom for GSuite" add-on](https://support.zoom.us/hc/en-us/articles/360020187492-Google-Calendar-add-on), and then add a Zoom Meeting on the "Event Details" tab.

Notes:

- **Do not assume we are meeting until I have accepted the invitation.** Even though my calendar may show that I am available at the time you request, I must reserve some time each week for course preparation and grading, as well as basic life sustenance activities such as eating lunch.
- All requests must be made with **at least 1 full business day advance notice** (i.e. requests for a 9am Monday meeting may be created no later than 9am Friday).
- **You will not be able to see my availability from your personal Google account.** You must [use your pdx.edu account](#managing-multiple-google-accounts).
- The event will have the same title in both of our calendars. "CS199 - Luke + Ellie" is a good title; "Meeting with Ellie" is not.


_Update 3/16: I assume that for the foreseeable future, all meetings will take place on Zoom._

~~All scheduled meetings outside of my regular drop in hours will be located _**in my office**_, FAB 120-15. See next question for how to find it.~~


## How to Find My Office

_Update 3/16: I assume that for the foreseeable future, all meetings will take place on Zoom._

. . . . . . . .

My office is #120-15 in the Fourth Avenue Building (FAB), located within the computer science suite (#120).

FAB is on the corner of Harrison and 4th Ave.

The CS suite is a large glassed in area on the "Plaza" level:

- If you enter on Harrison (by the Harrison & 3rd streetcar stop) you will need to follow the hallway straight ahead to the stairs and elevator, and go up one floor (to the "Plaza" level).
- If you enter on 4th Ave (across the street from the food trucks, in the City of Portland side of the building), you are already on the right floor, proceed down the hallway to the left. You will see signs for Computer Science and the School of Education.

Once you find the CS suite, you will go through two sets of double glass doors to reach the front desk.

Go past the front desk, left down the hallway, left again at the corner, and then continue until you see #120-15 on your left.

_Room numbers in the CS suite are **not sequential**. My office is farther down the hall than #120-16, across the hall from #120-23._

[FAB Floor Plans](https://www.pdx.edu/floorplans/sites/www.pdx.edu.floorplans/files/floorplans/floorplans/FAB-All%20Plan_0.pdf)


## Managing Multiple Google Accounts

To access some course materials and to create meeting requests, you must be logged in to your pdx.edu Google account. You cannot use a personal gmail account. Multiple accounts can be _very difficult to manage with the default web-based account switcher_.

I highly recommend using the account separation features of your browser to open different tabs/windows in different accounts.

- In Firefox, this feature is called [Multi-account containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)

- In Chrome, this is called [Profiles](https://support.google.com/chrome/answer/2364824?hl=en&co=GENIE.Platform=Desktop)

- Safari does not have this feature to my knowledge.

On a short term basis, you can also use the private browsing feature of your browser to get a clean browser profile to work in, log in to your PSU account there, and then access things from this clean window. You will have to re-log-in every time you create a new private browser session, though.

You are welcome to visit my office hours with any questions about how to use these features.
