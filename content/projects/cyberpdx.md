---
title: "CyberPDX"
date: 2018-01-01
collaborators: "Bob Liebman, Wu-chang Feng, Lois Delcambre, Michael Lupro, Rebecca Sexton, Veronica Hotton"
projectURL: http://cyberpdx.org
draft: true
---

The NSF identifies cybersecurity as a "defining issue of our time," an area of crucial importance for research, education, and workforce development (NSF 2018). Despite an acute workforce shortage – by 2022 there will be an estimated 265,000 unfilled cybersecurity jobs in North America – cybersecurity careers remain unattractive to large swaths of the population ((ISC)2 2017). As in other areas of computing, women and people of color face barriers to cybersecurity participation including non-identity with the field and a lack of confidence in early skills development (Jethwani et al. 2017; Margolis et al. 2010; Schulte and Knobelsdorf 2007).

Along with Bob Liebman (Sociology, PSU), I am co-director of the CyberPDX summer camp: a one-week residential program at Portland State University that aims to broaden participation in cybersecurity and computing. Currently funded by the NSA and NSF GenCyber program, we use a STEAM curriculum to link cybersecurity to the arts, humanities, and social sciences. Our curriculum includes creative programming projects, a cryptographic race linked to the young adult novel Divergent, smartphone filmmaking projects, and policy debates about phishing, algorithmic bias, and fake news. The team- and problem-based learning activities focus on building students’ technical competence and confidence. The interdisciplinary curriculum ensures that students recognize the breadth of cybersecurity career opportunities, and highlights cybersecurity’s relevance in their own lives. 