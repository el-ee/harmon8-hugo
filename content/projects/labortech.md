---
title: "The Future of Work"
date: 2018-01-01
collaborators: "Six Silberman, Lilly Irani, Kate Darling"
projectURL: http://faircrowdwork.org
draft: true
---

Recent research shows that so called 'alternative work arrangements' account for the majority of recent U.S. job growth (Katz and Krueger 2016). Major co-working space operator WeWork announced in early 2018 that it would be the largest employer in New York City -- if each of its paying desk-share members were employees. The future of work appears increasingly likely to be remote, freelance, and computer-mediated. What will this future look like for workers? 

In collaboration with Kate Darling and Lilly Irani, I'm currently starting a new project to understand the long-term effects of online platform work. What can we learn from people who have been working via online labor platforms such as Amazon Mechanical Turk (public launch, 2005) or Upwork (formerly eLance, founded 1999) for a decade or more? 

- How do these non-standard workers perceive and experience community and connection -- or atomization and isolation associated with individualized work?
- What are these workers’ emergent strategies for finding and creating communities outside the traditional workplace? What are their organizing strategies?
- What are their practices of care? What mutual assistance, civic engagement or political mobilization do digital workers’ engage in?
- What needs remain unmet for non-standard workers?

From 2016 to 2017, I was the technical lead for the Fair Crowd Work project led by IG Metall (German Metalworker's Union). Working with Six Silberman, I conducted surveys of over 200 online platform workers and created the [faircrowdwork.org](http://faircrowdwork.org) website.
