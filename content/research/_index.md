---
title: "Research"
date: 2018-08-15

menu: 
    main:
        title: "Research"
---

All ACM articles are linked through the ACM Authorizer service, and you should be able to download the official version for free. If you run into any trouble with this service (or with acquiring any other publications), please send me an email to request my author's copy.
