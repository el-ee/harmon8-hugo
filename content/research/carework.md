---
title: "Supporting everyday philanthropy: care work in situ and at scale"
date: 2017-02-25
authors: "Ellie Harmon, Matthias Korn, and Amy Voida"
pubUrl: "http://dl.acm.org/authorize?N22505"
pubInfo: "CSCW '17: Proceedings of the 2017 ACM Conference on Computer Supported Cooperative Work and Social Computing, pp. 1631-1645"
featured: true
---

Prior research has drawn attention to numerous problems with ICTs designed for philanthropic contexts. Yet, little is known about how to support philanthropic work in its own right, especially as it transcends formal engagements with nonprofit organizations and suffuses everyday life. In this research, we draw on data from a 33-day mobile diary study augmented by follow-up interviews to examine the work of everyday philanthropy. By taking individuals' philanthropic work-rather than a specific technology or its users-as our unit of analysis, we establish an empirical foundation for a new design space for philanthropic informatics. Our research shows that philanthropic work is not just about transactions of money, goods, or services. Instead, we argue that a lens of care work is a more useful framing of everyday philanthropy. We also draw renewed attention to the myriad roles of institutions beyond that of a resource intermediary.