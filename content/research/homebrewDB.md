---
title: "Homebrew databases: complexities of everyday information management in nonprofit organizations"
date: 2011-01-01 # publication date
authors: "Amy Voida, Ellie Harmon, and Ban Al-Ani"
pubUrl: "https://dl.acm.org/authorize?N18761" #official url
authorPDF: "" #link to author pdf
pubInfo: "CHI '11: Proceedings of the SIGCHI Conference on Human Factors in Computing Systems, pp. 915-924" #journal name, number, pages, etc.
featured: false
honMention: true
bestPaper: false
---

Many people manage a complex assortment of digital information in their lives. Volunteer coordinators at nonprofit organizations are no exception; they collectively manage information about millions of volunteers every year. Yet current information management systems are insufficient for their needs. In this paper, we present results of a qualitative study of the information management practices of volunteer coordinators. We identify the resource constraints and the diverse and fluid information needs, stakeholders, and work contexts that motivate their information management strategies. We characterize the assemblages of information systems that volunteer coordinators have created to satisfice their needs as 'homebrew databases.' Finally, we identify additional information management challenges that result from the use of these 'homebrew databases,' highlighting deficiencies in the appropriateness and usability of databases and information management systems, more generally.

