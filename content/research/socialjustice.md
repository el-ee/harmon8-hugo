---
title: "Social justice-oriented interaction design: outlining key design strategies and commitments"
date: 2016-06-04
authors: "Lynn Dombrowski, Ellie Harmon, and Sarah Fox"
pubUrl: "http://dl.acm.org/authorize?N18768"
pubInfo: "DIS '16: Proceedings of the 2016 ACM Conference on Designing Interactive Systems, pp 656-671 "
featured: true
bestPaper: true
---

In recent years, many HCI designers have begun pursuing research agendas that address large scale social issues. These systemic or "wicked" problems present challenges for design practice due to their scope, scale, complexity, and political nature. In this paper, we develop a _social justice orientation_ to designing for such challenges. We highlight a breadth of design strategies that target the goals of social justice along six dimensions -- _transformation_, _recognition_, _reciprocity_, _enablement_, _distribution_, and _accountability_ -- and elaborate three commitments necessary to developing a social justice oriented design practice -- _a commitment to conflict_, _a commitment to reflexivity_, and _a commitment to personal ethics and politics_. Although there are no easy solutions to systemic social issues, a social justice orientation provides one way to foster an engagement with the thorny political issues that are increasingly acknowledged as crucial to a field that is not just about technological possibility, but also about political responsibility.
