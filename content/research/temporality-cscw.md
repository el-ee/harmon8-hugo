---
title: "Circumscribed time and porous time: Logics as a way of studying temporality"
date: 2015-03-14
authors: "Melissa Mazmanian, Ingrid Erickson, and Ellie Harmon"
pubUrl: https://dl.acm.org/authorize?N08370
pubInfo: "CSCW '15: Proceedings of the 18th ACM Conference on Computer Supported Cooperative Work & Social Computing, pp. 1453-1464"
featured: false
---


In this paper, we introduce the notion of a temporal logic to characterize sets of organizing principles that perpetuate particular orientations to the lived experience of time. We identify a dominant temporal logic, circumscribed time, which has legitimated time as chunkable, single-purpose, linear, and ownable. We juxtapose this logic with the temporal experiences of participants in three ethnographic datasets to identify a set of alternative understandings of time -- that it is also spectral, mosaic, rhythmic, and obligated. We call this understanding porous time. We posit porous time as an expansion of circumscribed time in order to provoke reflection on how temporal logics underpin the ways that people orient to each other, research and design technologies, and normalize visions of success in contemporary life.

