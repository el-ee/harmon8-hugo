---
title: "Rating working conditions on digital labor platforms"
date: 2018-05-25
authors: "Ellie Harmon and Six Silberman"
pubUrl: "https://rdcu.be/PgEf"
authorPDF: "https://drive.google.com/file/d/1G8s9F3YQFPJslgVQYLcvxNsQZxNKXjQ1/view?usp=sharing"
doi: "https://doi.org/10.1007/s10606-018-9313-5"
pubInfo: "Journal of Computer Supported Cooperative Work, Online First, 25 May 2018"
featured: true
---

The relations between technology, work organization, worker power, workers’ rights, and workers’ experience of work have long been central concerns of CSCW. European CSCW research, especially, has a tradition of close collaboration with workers and trade unionists in which researchers aim to develop technologies and work processes that increase workplace democracy. This paper contributes a practitioner perspective on this theme in a new context: the (sometimes global) labor markets enabled by digital labor platforms. Specifically, the paper describes a method for rating working conditions on digital labor platforms (e.g., Amazon Mechanical Turk, Uber) developed within a trade union setting. Preliminary results have been made public on a website that is referred to by workers, platform operators, journalists, researchers, and policy makers. This paper describes this technical project in the context of broader cross-sectoral efforts to safeguard worker rights and build worker power in digital labor platforms. Not a traditional research paper, this article instead takes the form of a case study documenting the process of incorporating a human-centered computing perspective into contemporary trade union activities and communicating a practitioner’s perspective on how CSCW research and computational artifacts can come to matter outside of the academy. The paper shows how practical applications can beneﬁt from the work of CSCW researchers, while illustrating some practical constraints of the trade union context. The paper also offers some practical contributions for researchers studying digital platform workers’ experiences and rights: the artifacts and processes developed in the course of the work.