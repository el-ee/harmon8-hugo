---
title: "CyberPDX: An Interdisciplinary Professional Development Program for Middle and High School Teachers"
date: 2020-02-01
authors: "Ellie Harmon, Veronica Hotton, Robert Liebman, Michael Lupro, Wu-chang Feng, Lois Delcambre, and David Pouliot."
pubUrl: "https://doi.org/10.1145/3328778.3372652"

altURLs:
    -
        name: "Poster [PDF]"
        url: "https://drive.google.com/file/d/1PlWB9BYOeFI8DeGTCH7aBqTQCoYzsIJB/view?usp=sharing"
    - 
        name: "CyberPDX Website"
        url: "https://cyberpdx.org"   

pubInfo: "SIGCSE '20: 51st ACM Technical Symposium on Computer Science Education"
featured: true
---

CyberPDX is an annual professional development program hosted at Portland State University. Our long-term goal is to broaden participation in cybersecurity. Since 2016, over 70 middle and high school teachers from the Pacific Northwest have participated in the STREAM program, which offers interdisciplinary instruction in programming, cryptography, personal security, policy, literature, and arts. In this poster, we share our interdisciplinary curriculum, present data on short-term impacts, and describe our in-progress work to evaluate the program's longer term impacts.
