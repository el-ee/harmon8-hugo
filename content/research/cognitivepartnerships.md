---
title: "Cognitive partnerships on the bench top: designing to support scientific researchers"
date: 2008-02-25
authors: "Ellie Harmon and Nancy Nersessian"
pubUrl: "http://dl.acm.org/authorize?N18762"
pubInfo: "DIS '08: Proceedings of the 7th ACM conference on Designing interactive systems, pp. 119-128 "
featured: true
---

There has been a growing interest to develop technologies for laboratory environments. However, existing systems are underdeployed in real research labs. In order to create more successful technologies for the creative laboratory setting, we need a deeper understanding of the values of the researchers we are designing for and the unique roles of technology in the research laboratory. Our three-year ethnographic study of a biomedical engineering (BME) lab contributes to building this foundation for future design. Drawing form a distributed and situated cognition framework, our analysis highlights the ways in which technology is integrated into the researchers' daily practices. This study is one of the first deep ethnographies of a laboratory culture with a central focus on technology and provides several insights for the design community.