---
title: "Designing against the status quo"
date: 2018-03-01
authors: "Vera Khovanskaya, Lynn Dombrowski, Ellie Harmon, Matthias Korn, Ann Light, Michael Stewart, and Amy Voida"
pubUrl: "https://dl.acm.org/authorize?N42097"
pubInfo: "Interactions, Volume 25, Issue 2, March-April 2018"
featured: true
---

User-centered design methods typically design technology to fit better into the world as it is, thereby affirming the status quo.

It is possible, though difficult, to leverage HCI’s proximity to design production to explore and agitate for alternatives to the status quo.
