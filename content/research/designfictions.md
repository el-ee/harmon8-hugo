---
title: "The Design Fictions of Philanthropic IT: Stuck Between an Imperfect Present and an Impossible Future"
date: 2017-05-02
authors: "Ellie Haron, Chris Bopp, Amy Voida"
pubUrl: "https://dl.acm.org/authorize?N37801"
pubInfo: "CHI '17: Proceedings of the 2017 CHI Conference on Human Factors in Computing Systems, pp. 7015-7028"
featured: false
---



In this paper, we examine the stories about philanthropic IT that circulate via product websites, marketing materials, and third-party news articles. Through a series of product-centered case studies, we surface these texts' implicit and explicit visions about the (near) future of philanthropy. We detail their prescriptions about how, why, and in service of what ends nonprofit organizations could, should, and ought to leverage IT. We also examine their underlying assumptions about philanthropy: how social good is accomplished, how philanthropic organizations are - and might be more - effective, to whom organizations and beneficiaries should be accountable, and the terms of that accountability. Analyzing these visions as design fictions, we argue that they help cultivate unrealistic anticipatory relationships to the present and entail concomitantly unrealistic imperatives for the philanthropic sector. We conclude by arguing for the crucial role of HCI scholars in disrupting these impossible futures, and by highlighting areas needing further, re-imagined, research.

