---
title: "Digital labour platforms and the future of work: Towards decent work 
in the online world"
date: 2018-09-21
authors: "Janine Berg, Marianne Furrer, Ellie Harmon, Uma Rani and M. Six Silberman"
pubUrl: "https://www.ilo.org/global/publications/books/WCMS_645337/lang--en/index.htm"
pubInfo: "International Labour Office, 2018"
featured: true
---

This report provides one of the first comparative studies of working conditions on five major micro-task platforms that operate globally.

The emergence of online digital labour platforms has been one of the major transformations in the world of work over the past decade.

This report provides one of the first comparative studies of working conditions on five major micro-task platforms that operate globally. It is based on an ILO survey covering 3,500 workers in 75 countries around the world and other qualitative surveys. The report analyses the working conditions on these micro-task platforms, including pay rates, work availability and intensity, social protection coverage and work–life balance.

The report recommends 18 principles for ensuring decent work on digital labour platforms.