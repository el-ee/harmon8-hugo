---
title: "Bridging between organizations and the public: volunteer coordinators' uneasy relationship with social computing"
date: 2012-05-05 # publication date
authors: "Amy Voida, Ellie Harmon, and Ban Al-Ani"
pubUrl: "https://dl.acm.org/authorize?N18760" #official url
authorPDF: "" #link to author pdf
pubInfo: "CHI '12: Proceedings of the SIGCHI Conference on Human Factors in Computing Systems, pp. 1967-1976" #journal name, number, pages, etc.
featured: false
honMention: true
bestPaper: false
---

We present the results of a qualitative study of the use of social computing technologies by volunteer coordinators at nonprofit organizations. The work of volunteer coordinators is bridge-building work - bringing together numerous public constituencies as well as constituencies within their organizations. One might expect this class of work to be well supported by social software, some of which has been found to enable bridging social capital. However, we find that, in many ways, this class of technology fails to adequately support volunteer coordinators' bridge-building work. We discuss a number of strategies for bridge-building via social computing technologies, numerous challenges faced by volunteer coordinators in their use of these technologies, and opportunities for designing social software to better support bridge-building between organizations and the public.