---
title: "Competing Currencies: Designing for Politics in Units of Measurement"
date: 2017-02-25
authors: "Amy Voida, Ellie Harmon, Willa Weller, Aubrey Thornsbury, Ariana Casale, Samuel Vance, Forrest Adams, Zach Hoffman, Alex Schmidt, Kevin Grimley, Luke Cox , Aubrey Neeley, Christopher Goodyear"
pubUrl: "https://dl.acm.org/authorize?N22594"
pubInfo: "CSCW '17: Proceedings of the 2017 ACM Conference on Computer Supported Cooperative Work and Social Computing, pp. 847-860"
featured: false

---


We present results of a qualitative study of the information systems used by college and university food banks and find that their inventory systems are characterized by the patchwork use of multiple units of measurement-currencies-collected at different points in their workflow for different stakeholders. Considerations of whether to track information by item count, points, monetary value, or weight are immensely political and privilege some stakeholders over others. We contribute to an emergent body of research in computer-supported cooperative work about the ways in which the politics of measurement influences the design of organizational information systems through an explanation of the ways that these different currencies embody politics and stymie design at the most mundane level of the information system-the unit of measurement.

