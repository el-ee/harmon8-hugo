---
title: "XYZ 123" #Title for URL Bar, Menus
date: 2018-08-15 #Must be present for page to render, optional
quarter: 201804 #Quarter of class, used for sorting
courseNumber: "XYZ 123" # Course Number, e.g. CS 100
courseTitle: "Introduction to Consectetur Adipisicing Elit" #Class Title
sidebarItems:
    item1:
        name: "Eiusmod"
        icon: "fa-info-circle" # any font awesome icon, optional
        #url: 
meetings: "TBD"
location: "TBD"
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.